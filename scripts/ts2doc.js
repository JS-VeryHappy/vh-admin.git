const path = require('path');
const fs = require('fs');

const arguments = process.argv.splice(2);
const argPath = arguments[0];

// 根目录
function rootPath(...args) {
  return path.join(__dirname, '..', ...args);
}

// const res = require(rootPath(argPath));
// const res = fs.readFileSync(rootPath(argPath));
const str = `
/**
 * 审批人显示字段值
 */
flowNodeName?: string;
/**
 * 其它
 */
[key: string]: any;
`;

const arr = str.split(';');
let html = '';
arr.forEach((i) => {
  i = i.replace(/[\r\n]/g, '');
  if (!i) {
    return;
  }

  const a = i.split(':');
  let a1str = a[1];

  if (a.length > 2) {
    const newA = [...a];
    newA.splice(0, 1);
    a1str = newA.join(':');
  }
  const b = a[0].split('*/');

  const c = b[0].split('@default');
  let defstr = '-';
  if (c[1]) {
    defstr = c[1].replace(/\ +/g, '');
  }

  const type = a1str.replace(/\|/g, '\\|').replace(/\ +/g, '');
  const title = c[0].replace(/\ +/g, '').replace('/***', '').replace(/\*/g, '<br>');

  const field = b[1].replace(/\ +/g, '');
  const required = field.indexOf('?') === -1 ? true : false;
  const name = field.replace('?', '').replace(/\ +/g, '');
  // console.log(title, type, field, required);

  html += `| ${name}${required ? '(必须)' : ''} | ${title} | \`${type}\` | ${defstr} | - |\n`;
});

console.log('===================');
console.log(html);
console.log('===================');
