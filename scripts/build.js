#! /usr/bin/env node
const program = require('commander');
const build = require('./commands/build.js');
const watch = require('./commands/watch.js');

program
  .command('build')
  // .option('--type <type>', '需要编译的类型 es | lib 不设置两个都开启')
  .description('编译文件')
  .parse(process.argv)
  .action(build);

program
  .command('watch')
  // .option('--type <type>', '需要编译的类型 es | lib 不设置两个都开启')
  .description('watch编译文件')
  .parse(process.argv)
  .action(watch);

program.parse(process.argv);
