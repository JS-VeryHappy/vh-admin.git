const { join } = require('path');

function getPackagesPath(p) {
  return join(process.cwd(), 'packages', p);
}

const headPkgs = [
  'pro-utils',
  'pro-validator',
  'pro-services',
  'pro-hooks',
  'pro-components',
  'pro-flow-designer',
  'pro-form-designer',
];
// const headPkgs = ['pro-form-designer'];

module.exports = {
  getPackagesPath,
  headPkgs,
};
