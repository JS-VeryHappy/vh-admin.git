const { consola } = require('consola');
const execa = require('execa');
const { getPackagesPath, headPkgs } = require('./index');

async function watch() {
  try {
    for (let index = 0; index < headPkgs.length; index++) {
      const p = headPkgs[index];
      const path = getPackagesPath(p);
      try {
        consola.start(` ${p} watch start...`);
        execa('yarn', ['watch'], {
          stdio: 'inherit',
          cwd: path,
        });
      } catch (error) {
        console.log(error);
      }
    }
  } catch (err) {
    consola.error(err);
    process.exit(1);
  }
}

module.exports = watch;
