---
hero:
  title: 前端公共资源
  description: 高效、稳定、便捷、让业务开发变得更简单
  actions:
    - text: 快速上手
      link: /pro/table
features:
  - image: 🚀
    title: '@vh-admin/pro-components'
    description: 增强组件仓库，业务沉淀通用组件
  - image: 💎
    title: '@vh-admin/pro-hooks'
    description: 通用hooks，无业务逻辑
  - image: 🛸
    title: '@vh-admin/pro-services'
    description: 通用的各种请求形式
  - image: ⛱
    title: '@vh-admin/pro-utils'
    description: 通用的公用方法
  - image: 🕍
    title: '@vh-admin/pro-validator'
    description: 通用的验证规则
  - image: 🚀
    title: '@vh-admin/pro-flow-designer'
    description: 流程设计器
  - image: 💎
    title: '@vh-admin/pro-form-designer'
    description: 表单设计器
footer: Open-source MIT Licensed | Copyright © 2022<br />Powered by vh
---

## 🖥 浏览器兼容性

- 现代浏览器(with [polyfills](https://stackoverflow.com/questions/57020976/polyfills-in-2019-for-ie11))
- [Electron](https://www.electronjs.org/)

| [![edge](https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png)](http://godban.github.io/browsers-support-badges/) | [![Edge](https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png)](http://godban.github.io/browsers-support-badges/) | [![chrome](https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png)](http://godban.github.io/browsers-support-badges/) | [![safari](https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png)](http://godban.github.io/browsers-support-badges/) | [![electron_48x48](https://raw.githubusercontent.com/alrra/browser-logos/master/src/electron/electron_48x48.png)](http://godban.github.io/browsers-support-badges/) |
| --- | --- | --- | --- | --- |
| Edge | last 2 versions | last 2 versions | last 2 versions | last 2 versions |
