---
title: 开发流程规范
toc: content
order: 0
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 规范
  path: /standard
  order: 2
---

# 项目开发说明

一、本地分支开发

> 1. 接到任务
> 2. 拉取最新 dev(可能是修复 bug，可能是主分支和其它) 代码
> 3. coding coding...
> 4. 单元测试(自测)
> 5. 本地分支的所有 commit push 到 dev 分支

# vh-admin 组件开发说明

一、本地分支开发

> 1. 接到任务
> 2. 拉取最新 review(可能是修复 bug，可能是主分支和其它) 代码
> 3. coding coding...
> 4. 编写测试用例
> 5. 单元测试(自测)
> 6. 本地分支的所有 commit push 到 review 分支
> 7. 自动创建 Pull Request
> 8. 代码评审(自动化评审、自动测试、自动合并)
> 9. 代码评审通过或者不通过
