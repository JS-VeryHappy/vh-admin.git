---
title: 开启选择项目组件
toc: content
order: 8
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 项目使用说明
  path: /project
  order: 3
---

# 路由配置样例

```js
import { IBestAFSRoute } from '@umijs/plugin-layout';

export const routes: IBestAFSRoute[] = [

  {
    path: '/proform',
    name: 'ProForm',
    icon: 'AppleFilled',
    routes: [
    {
      path: '/proform/ordinary',
      name: '内嵌普通表单',
      component: '@/pages/proForm/ordinary/index',
      selectProject: true,//开启全局 选择项目组件
    },
  }
];
```

# 页面配置样例

```js
import { eventsBusFn } from '@vh-admin/pro-utils';
const eventsBus = eventsBusFn();

function MarketingConfigProjectConfig() {
  const [projectId, setProjectId] = useState < any > null;

  const onChangeProject = (pvalue: any) => {
    setProjectId(pvalue);
  };

  useEffect(() => {
    eventsBus.$emit('setPageContainerConfig', {
      selectProject: {
        value: setProjectId, // 可配置默认值
        onChange: onChangeProject, //选择项目切监听
      },
    });
  }, []);

  return <>项目配置</>;
}

export default MarketingConfigProjectConfig;
```
