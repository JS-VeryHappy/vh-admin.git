---
title: 文件命名规范
toc: content
order: 4
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 规范
  path: /standard
  order: 2
---

# 文件夹

1. **除 react 组件外的名称都使用小驼峰、react 组件使用大驼峰**

```js
//普通
'/pages/login';
//组件
'/components/FormCustom';
```

2. **pages 文件夹命名方式：以业务来分文件夹层级（根据业务菜单层级来对应命名）**

# services 请求命名

1. **services 文件夹命名方式：和 pages 一样,特殊公用的放 public**

# 文件

1. **使用小驼峰**

```js
'btnConfig.tsx';
```

# React 函数

1. **除 react 组件外的函数名称都使用小驼峰、react 组件函数使用大驼峰**

```js
    //普通 /utils/index.ts
    export const waitTime = (time: number = 100) => {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(true);
        }, time);
      });
    };

    //组件
    '/components/FormCustom'
    function FormCustom(Props: FormCustomProps) {
      ...
    }
    '/pages/login'
    function UserMobileLogin() {
      ...
    }
```

# CSS

1. **css 单独分离一个.less 文件管理**
2. **使用 cssModules 方式引入，名称使用小驼峰**
