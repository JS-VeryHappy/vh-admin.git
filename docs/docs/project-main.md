---
title: 初始化项目
order: 1
toc: content
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 项目使用说明
  path: /project
  order: 3
---

## 说明

> 1. `git clone XXX` 拉取对应项目 git 到本地
> 2. `pnpm i` 安装所有依赖
> 3. `pnpm init` 安装 vscode 本地带代码规范和自动补全配置，重要重启生效、重启生效、重启生效
> 4. `pnpm prepare` 安装git提交所需要的钩子
> 5. `pnpm start` 运行开发
