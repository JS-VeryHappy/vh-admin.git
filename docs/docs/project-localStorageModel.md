---
title: 全局localStorage管理
toc: content
order: 6
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 项目使用说明
  path: /project
  order: 3
---

# 说明

> 1. 所有关于 localStorage 的操作都放在这里面
> 2. 更多查看对应项目文件

```js
// /src/utils/localStorageModel

export const LocalStorageName = 'USER_INFO';
export const LocalStorageTokenName = 'USER_INFO_TOKEN';

export const getUserLocalStorage = () => {
  const userInfo: any = localStorage.getItem(LocalStorageName);
  return JSON.parse(userInfo);
};

export const setUserLocalStorage = (data: any) => {
  localStorage.setItem(LocalStorageName, JSON.stringify(data));
};

export const getUserTokenLocalStorage = () => {
  return localStorage.getItem(LocalStorageTokenName);
};

export const removeLocalStorageTokenName = () => {
  localStorage.removeItem(LocalStorageTokenName);
};
export const removeUserLocalStorage = () => {
  localStorage.removeItem(LocalStorageName);
};
```
