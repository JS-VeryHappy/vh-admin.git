---
title: 接口规范
toc: content
order: 5
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 规范
  path: /standard
  order: 2
---

## Request 使用

1. **强制要求：所有的请求都必须写在 services 文件夹内 1.统一求情标准、方便管理 2.方便查看、3.方便复用**
2. **文档**
   - **[plugin-request](https://umijs.org/zh-CN/plugins/plugin-request)**
   - **[umi-request](https://github.com/umijs/umi-request)**
   - **[hooks](https://ahooks.js.org/hooks/async)**
3. **配置接口返回数据的判断配置**

```js
// /services/config.ts
export const responseConfig = {
  data: 'xxx', // 存放数据字段
  success: 'xxx', // 判断成功值
  code: 'xxx', // 错误码字段
  message: 'xxx', // 返回信息字段
};
```

4. **内部封装**

   - **以下不限于的请求形式：**

     - **postBody：post 请求参数放 request-body 里面**
     - **postQuery：post 请求参数放 url 后面拼接**
     - **getBody：get 请求参数放 request-body 里面**
     - **getQuery：get 请求参数放 url 后面拼接**
     - **pathBody：post 请求，动态的 path 地址**
     - **pathQuery：post 请求，动态的 path 地址**

     ```js
         // /services/handler.ts
         export function postBody(url: string, data?: any, options?: any) {
           return requestQuery(url, { method: 'post', data, ...options });
         }
         ...

     ```

5. **请求拦截器**

```js
// services/config.ts
// 具体请查看 plugin-request 文档
const requestConfig: any = {
  timeout: 30000,
  requestInterceptors: [
    (config: any) => {
      let httpHeadersHook = undefined;

      // @ts-ignore
      if (window?.vhAdmin?.httpHeadersHook) {
        // @ts-ignore
        httpHeadersHook = window.vhAdmin?.httpHeadersHook;
      }

      const { url, ...options } = config;
      const time = parseInt(new Date().getTime() / 1000 + '');
      const sign = CryptoJs.MD5(
        // @ts-ignore
        process.env.version + time + process.env.sign_key,
      ).toString();

      options.headers.common = {
        ...options.headers.common,
        version: process.env.version,
        time: time,
        sign: sign,
      };

      //配置默认开启错误提示
      if (options.errorMessageShow === undefined) {
        options.errorMessageShow = true;
      }

      if (typeof httpHeadersHook === 'function') {
        options.headers = httpHeadersHook(options.headers);
      }

      return {
        url: `${process.env.api_url}${url}`,
        ...options,
      };
    },
  ],
  responseInterceptors: [
    [
      (response: any) => {
        let hookConfig: any;

        // @ts-ignore
        if (window?.vhAdmin?.responseConfig) {
          // @ts-ignore
          responseConfig = window.vhAdmin?.responseConfig;
        }

        // @ts-ignore
        if (window?.vhAdmin?.httpError) {
          httpError = {
            ...httpError,
            // @ts-ignore
            ...window.vhAdmin?.httpError,
          };
        }
        let httpErrorHook: any = undefined;
        // @ts-ignore
        if (window?.vhAdmin?.httpErrorHook) {
          // @ts-ignore
          httpErrorHook = window?.vhAdmin?.httpErrorHook;
        }

        const { request, config } = response;

        if (response.status !== 200) {
          const errMessage = httpError[response.status] || '未知错误';
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(response.status, null, response, response.data);
          }
          showErrorMessage(config, errMessage, hookConfig);
          return Promise.reject();
        }
        // 如果是二进制数据
        if (request.responseType === 'blob') {
          if (!response.headers['content-disposition']) {
            response.data.text().then((r: any) => {
              const rdata = JSON.parse(r);
              if (typeof httpErrorHook === 'function') {
                hookConfig = httpErrorHook(
                  response.status,
                  rdata[responseConfig.code],
                  response,
                  response.data,
                );
              }
              showErrorMessage(config, rdata[responseConfig.message], hookConfig);
            });
            return Promise.reject();
          }

          return response;
        }
        const resData: any = response.data;

        // 如果出现 没有code的情况
        if (resData[responseConfig.code] === undefined) {
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(
              response.status,
              resData[responseConfig.code],
              response,
              response.data,
            );
          }
          showErrorMessage(config, '未知错误', hookConfig);
          return Promise.reject('未知错误');
        }

        if (resData[responseConfig.code] !== responseConfig.success) {
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(
              response.status,
              resData[responseConfig.code],
              response,
              resData,
            );
          }
          showErrorMessage(config, resData[responseConfig.message] || '系统错误', hookConfig);
          return Promise.reject(resData[responseConfig.message] || '系统错误');
        }
        return response;
      },
      (error: any) => {
        let hookConfig: any;
        let httpErrorHook = undefined;
        // @ts-ignore
        if (window?.vhAdmin?.httpErrorHook) {
          // @ts-ignore
          httpErrorHook = window?.vhAdmin?.httpErrorHook;
        }

        const { code, config, response } = error;
        if (!response) {
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(undefined, code, response, error);
          }
          showErrorMessage(config, error.message, hookConfig);

          return Promise.reject();
        }
        if (response.status !== 200) {
          let errMessage: any = httpError[response.status] || '未知错误';

          if (code === 'ERR_NETWORK') {
            errMessage = '网络错误!';
          }

          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(response.status, code, response, response.data);
          }

          showErrorMessage(config, errMessage, hookConfig);

          return Promise.reject();
        }
        return Promise.reject();
      },
    ],
  ],
};
...

```

## 使用样例

## 普通请求使用

```js
import { getProTable } from '@/services';
```

## 防抖请求

```js
import { getProTable, proTableAddRow, proTableDetails } from '@/services';
const debounceProTableAddRow: any = requestDebounce(proTableAddRow, 500);
```
