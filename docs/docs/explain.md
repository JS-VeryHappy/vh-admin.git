---
title: 文档
toc: content
nav:
  title: 文档
  order: 1
  path: /docs
---

## 组件安装

```bash
$ pnpm add @vh-admin/pro-validator
$ pnpm add @vh-admin/pro-utils
$ pnpm add @vh-admin/pro-services
$ pnpm add @vh-admin/pro-hooks
$ pnpm add @vh-admin/pro-components
$ pnpm add @vh-admin/pro-flow-designer
$ pnpm add @vh-admin/pro-form-designer
```

## 组件说明（必须阅读完整整个文档）

组件文档、使用说明: [vh-admin](http://vh-admin.poetrysl.com/) 路径查看文档说明

> 1. Umijs 及其生态 进行二次封装。
> 2. 项目中的业务资源和业务相关配置需要根据业务调整和自行补齐。

## 组件特点

> 1. 框架思想：让开发变得简单，从编写式开发提升为开发配置综合式开发，大大提升开发效率和开发统一性，一个页面一个 json 配置完事。
> 1. 组件思想：业务组件是以项目业务的 UI 呈现形式为主的组件、具有特定业务性、贴近业务本身。
> 1. 通用性：具备相同业务通用性（例如：一家公司有多个管理后台、UI 呈现都基本一致、可以使用相同的组件）。
> 1. 完整文档说明：使用了 dumi 文档扩展，让项目说明、业务说明、组件说明、资源说明都具备快速学习、快读理解。
> 1. 脚手架封装二次封装表单和表格等组件、让编写业务可以配置化。

## 组件架构

Umi(React + AntD + Less + TypeScript + dumi )

Umi 中文网： [https://umijs.org/zh-CN/docs/](https://umijs.org/zh-CN/docs)

React 官网： [https://react.docschina.org/](https://react.docschina.org/)

AntDesign 中文网： [https://ant.design/index-cn](https://ant.design/index-cn)

AntDesign-Pro： [https://beta-pro.ant.design/index-cn/](https://beta-pro.ant.design/index-cn/)

AntDesign-Procomponents： [https://procomponents.ant.design/](https://procomponents.ant.design/)

Less 官网： [http://lesscss.cn/](http://lesscss.cn/)

TypeScript 官网： [https://www.tslang.cn/](https://www.tslang.cn/)

Dumi 中文网： [https://d.umijs.org/zh-CN/](https://d.umijs.org/zh-CN/)

## vscode 插件安装

[ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) 代码格式风格验证

[Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) 代码格式风格验证自动补全

[stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint) 样式风格验证

[git-commit-plugin](https://marketplace.visualstudio.com/items?itemName=redjue.git-commit-plugin) 提交 git 文案工具

[vh-tips-tool](https://marketplace.visualstudio.com/items?itemName=zhouyingchao.vh-tips-tool) 智能提示umijs的useModel、vh-admin、vh-mobile跳转相关组件

## vscode 本地 settings.json

```
{
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {},
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "search.exclude": {
    "**/node_modules": true,
    "**/bower_components": true,
    "**/dist": true
  },
  "stylelint.validate": ["css", "less", "postcss", "scss", "sass"],
  "GitCommitPlugin.CustomCommitType": [
    {
      "label": "revert",
      "detail": "⏪ Reverts | 回退"
    },
    {
      "label": "style",
      "detail": "💄 Styles | 风格"
    },
    {
      "label": "chore",
      "detail": "🎫 Chores | 其他更新"
    },
    {
      "label": "refactor",
      "detail": " ♻ Code Refactoring | 代码重构"
    },
    {
      "label": "test",
      "detail": " ✅ Tests | 测试"
    },
    {
      "label": "build",
      "detail": " 👷‍ Build System | 构建"
    },
    {
      "label": "ci",
      "detail": " 🔧 Continuous Integration | CI 配置"
    },
    {
      "label": "perf",
      "detail": "⚡ Performance Improvements | 性能优化"
    },
    {
      "label": "docs",
      "detail": "📝 Documentation | 文档"
    },
    {
      "label": "fix",
      "detail": "🐛 Bug Fixes | Bug 修复"
    },
    {
      "label": "feat",
      "detail": "✨ Features | 新功能"
    }
  ]
}


```


## git 设置结尾格式： 提交拉取均不转换
`git config --global core.autocrlf falses`
