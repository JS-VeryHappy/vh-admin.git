---
title: 可配置参数样例
toc: content
order: 9
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 项目使用说明
  path: /project
  order: 4
---

## 可配置参数

**可以在使用项目的 global.tsx 中配置**

```js
import { defineConfig } from '@vh-admin/pro-components';

defineConfig({
  // 本地存放 token信息的key
  LocalStorageTokenName:'USER_INFO_TOKEN',
  // 本地存放 用户信息的key
  LocalStorageName:'USER_INFO',
  // umi请求方法
  request:()=>{}
  // 定义http错误返回码
  httpError:{
      500: '服务器错误',
      401: '未授权',
      404: '数据不存在',
      405: '参数绑定错误',
      406: '参数验证错误',
  },
  /**
   * http请求前设置headers
   * headers: 请求要发给送的headers
   **/
  httpHeadersHook:(headers) =>  { return {...,...headers}},
  /**
   * 表格搜索请求开始字符串去空格
   * false 关闭
   * @default 'undefined'
   */
  requestParamsTrim:true;
  /**
   * http请求错误回调
   * status: http状态
   * code: 业务状态
   * ctx: 整个请求对象
   * resData: 业务返回对象
   * 可以返回和请求相同的请求配置 例如: return { errorMessageShow: false }; 隐藏错误提示
   **/
  httpErrorHook:(status:number,code: number,ctx: any, resData: any) => {return config},
  //上传公用方法
  uploadFile: () => {},
  //表格请求返回字段定义
  responseTableConfig: {
    data: 'data', // 表格数据列表字段名称
    total: 'total', // 表格数据列表总数字段名称
  },
  //请求公用返回定义
  responseConfig: {
    data: 'data', // 存放数据字段
    success: 0, // 判断成功值
    code: 'code', // 错误码字段
    message: 'msg', // 返回信息字段
  },
  //表格发送请求前钩子
  tableRequestParamsHook: (requestParams: any, sort: any, filter: any) => { return ...},
  //表格单元格操作栏最多显示按钮个数 默认2
  tableOperationMax:2,
  //自定义tabel内置导入返回错误信息的方法
  tableImportShowError: (res:any) => {},
  // 表格搜索默认是否收起
  tableSearchDefaultCollapsed:false,
  // 表格单元格格式
  tableBusinessStyleArr:{},
  //表格自动计算的高度值
  tableExtraHeight:undefined,
  // 表格上内置按钮补充
  btnConfig: {
    headerTitleConfigArr:{
      edit: {
        text: '项目1特有',
        type: 'link',
      },
    },
    tableAlertOptionRenderConfigArr:{
      edit: {
        text: '项目1特有',
        type: 'link',
      },
    },
    operationConfigRenderConfigArr:{
      edit: {
        text: '项目1特有',
        type: 'link',
      },
    },
  },
})
```
