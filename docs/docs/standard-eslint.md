---
title: ESLint规范
toc: content
order: 1
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 规范
  path: /standard
  order: 2
---

# 说明

> 1. 开发时必须启用 eslint,包含 prettier，eslint，stylelint 验证[@vh-cli/lint](https://gitee.com/JS-VeryHappy/vh-cli.git)

> 2. 开发业务组件必须有文档说明

> 3. 开发功能页面尽量有文档说明 比如复杂行比较高的业务、可以写业务说明

> 4. css 业务尽量使用 CSSModules 模式 [CSSModules](~docs/knowledge/css-modules)
