---
title: 使用接口定义的types
order: 4
toc: content
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 项目使用说明
  path: /project
  order: 3
---

## 更新接口定义的最新 types

```js
`pnpm openApi`;
// 生成的文件会在 /src/services/apiDocs/*
```

## 使用 types

```js
// 生成好的types直接可以全局调用例如:
// 请务必所有接口都加上定义定好的对应types
export async function roleQuery(data: API.jiaoseliebiaofenyechaxunshaixuan) {
  return postBody('/api/role/query', data);
}
```
