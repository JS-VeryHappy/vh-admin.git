---
title: 全局调用
toc: content
order: 5
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 项目使用说明
  path: /project
  order: 3
---

# 说明

> 1. 全局都可以调用的方法、在这里定义方法
> 2. 全局都需要用、调用比较方便都可以写在这里;

```js
// /src/utils/globalUtils

// 调用
global.log('我是渲染事前的钩子');
```

## global.log 开发欢迎下会打印、打包不显示

## eventsBus 单页面事件传递方法

> - src/layouts/ 页面已经监听 setPageContainerConfig 事件
> - 该事件用来修改 PageContainer 配置信息 所有参数 [PageContaine](https://procomponents.ant.design/components/page-container)

```js
import { eventsBusFn } from '@vh-admin/pro-utils';
const eventsBus = eventsBusFn();

// 监听和效果使用
useEffect(() => {
  eventsBus.$on('setPageContainerConfig', (data: PageContainerProps) => {
    setTimeout(() => {
      setConfig(data);
    }, 0);
  });
  return () => {
    eventsBus.$off('setPageContainerConfig');
  };
}, []);

// 触发事件使用
useEffect(() => {
  eventsBus.$emit('setPageContainerConfig', {
    content: '欢迎使用 ProLayout 组件',
    tabList: [
      {
        tab: '基本信息',
        key: 'base',
      },
      {
        tab: '详细信息',
        key: 'info',
      },
    ],
    onTabChange: (key: any) => {
      console.log('====================================');
      console.log(key);
      console.log('====================================');
    },
  });
}, []);
```
