---
title: PageContainer配置
toc: content
order: 7
nav:
  title: 文档
  order: 1
  path: /docs
group:
  title: 项目使用说明
  path: /project
  order: 3
---

# 文档地址

> 1. [PageContaine](https://procomponents.ant.design/components/page-container)

# 页面配置样例

```js
import { eventsBusFn } from '@vh-admin/pro-utils';
const eventsBus = eventsBusFn();

// 修改顶部菜单描述和配置tab
useEffect(() => {
  eventsBus.$emit('setPageContainerConfig', {
    content: '欢迎使用 ProLayout 组件',
    tabList: [
      {
        tab: '基本信息',
        key: 'base',
      },
      {
        tab: '详细信息',
        key: 'info',
      },
    ],
    onTabChange: (key: any) => {
      console.log('====================================');
      console.log(key);
      console.log('====================================');
    },
  });
}, []);

// 隐藏header
useEffect(() => {
  eventsBus.$emit('setPageContainerConfig', {
    header: {
      title: null,
      breadcrumb: {},
    },
  });
}, []);
```
