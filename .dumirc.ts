/// <reference path="./typings.d.ts" />

import { defineConfig } from 'dumi';

export default defineConfig({
  publicPath: process.env.BUILD_GH ? '/vh-admin/' : '/',
  base: process.env.BUILD_GH ? '/vh-admin/' : '/',
  // runtimePublicPath: {},
  mfsu: false,
  themeConfig: {
    name: 'vh-admin',
    logo: 'https://user-images.githubusercontent.com/9554297/83762004-a0761b00-a6a9-11ea-83b4-9c8ff721d4b8.png',
  },
  favicons: [
    'https://user-images.githubusercontent.com/9554297/83762004-a0761b00-a6a9-11ea-83b4-9c8ff721d4b8.png',
  ],
  // styles: ['https://unpkg.com/antd@5.0.1/dist/reset.css'],
  extraBabelPlugins: [
    // [
    //   'import',
    //   {
    //     libraryName: 'antd',
    //     libraryDirectory: 'es',
    //     style: true,
    //   },
    // ],
  ],
  outputPath: 'docs-dist',
  // apiParser: {},
  resolve: {
    // 配置入口文件路径，API 解析将从这里开始
    // entryFile: './packages/pro-components/src/md.tsx',
    atomDirs: [
      { type: 'pro-components', dir: 'packages/pro-components/src' },
      { type: 'pro-hooks', dir: 'packages/pro-hooks/src' },
      { type: 'pro-services', dir: 'packages/pro-services/src' },
      { type: 'pro-utils', dir: 'packages/pro-utils/src' },
      { type: 'pro-validator', dir: 'packages/pro-validator/src' },
      { type: 'pro-flow-designer', dir: 'packages/pro-flow-designer/src' },
      { type: 'pro-form-designer', dir: 'packages/pro-form-designer/src' },
    ],
  },
});
