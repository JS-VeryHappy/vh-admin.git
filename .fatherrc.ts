// const headPkgs: string[] = [
//   'pro-utils',
//   'pro-validator',
//   'pro-services',
//   'pro-hooks',
//   'pro-components',
//   'pro-flow-designer',
//   'pro-form-designer',
// ];
const headPkgs: string[] = ['pro-form-designer'];

export default {
  cjs: { type: 'babel', lazy: true },
  esm: {
    type: 'babel',
    importLibToEs: true,
  },
  // cssModules: true,
  // disableTypeCheck: true,
  pkgs: [...headPkgs],
  extraBabelPlugins: [
    [
      '@babel/plugin-transform-runtime',
      {
        corejs: 3,
      },
    ],
    // ['babel-plugin-import', { libraryName: 'antd', libraryDirectory: 'es', style: false }, 'antd'],
  ],
};
