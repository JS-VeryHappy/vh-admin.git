const lint = require('@vh-cli/lint');

module.exports = {
  overrides: [
    {
      files: '*.md',
      options: {
        proseWrap: 'preserve',
      },
    },
  ],
  ...lint.prettier,
};
