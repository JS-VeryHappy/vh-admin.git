import { defineConfig } from 'father';

export default defineConfig({
  cjs: {
    output: 'es',
    platform: 'browser',
    extraBabelPlugins: [
      [
        '@babel/plugin-transform-runtime',
        {
          corejs: 3,
        },
      ],
    ],
  },
  esm: {
    output: 'lib',
    platform: 'node',
    transformer: 'babel',
    extraBabelPlugins: [
      [
        '@babel/plugin-transform-modules-commonjs',
        {
          importInterop: 'node',
        },
      ],
    ],
  },
});
