# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [2.2.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.2.3...@vh-admin/pro-services@2.2.4) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([32b1222](https://gitee.com/JS-VeryHappy/vh-admin/commit/32b1222f18f796660ca3d8785d198636fb2f9bc5))



### [2.2.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.2.2...@vh-admin/pro-services@2.2.3) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([8a0694d](https://gitee.com/JS-VeryHappy/vh-admin/commit/8a0694d6d2ffc62ee2cd9c94ed808ededbef5425))
* **all:**  使用阿里云仓库 ([d7a734e](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7a734ea08b9abefc84c652a9091d091d67b5d02))



### [2.2.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.2.1...@vh-admin/pro-services@2.2.2) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([e525f08](https://gitee.com/JS-VeryHappy/vh-admin/commit/e525f08c7c71fca2c3452163836e85847e5872bf))



### [2.2.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.1.7...@vh-admin/pro-services@2.2.1) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  配置的字段支持deepGet ([887a988](https://gitee.com/JS-VeryHappy/vh-admin/commit/887a988959fd7058abf8d40e897655012e130fef))



### [2.1.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.1.6...@vh-admin/pro-services@2.1.7) (2024-06-17)


###   🐛 Bug Fixes | Bug 修复

* **表单:**  增加请求header参数 ([7fd0c10](https://gitee.com/JS-VeryHappy/vh-admin/commit/7fd0c10fd23a74df58cd4630aa214c8f36b3cbba))



### [2.1.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.1.5...@vh-admin/pro-services@2.1.6) (2024-06-14)


###   🐛 Bug Fixes | Bug 修复

* **流程配置:**  优化提示 ([8676145](https://gitee.com/JS-VeryHappy/vh-admin/commit/8676145f70ed67dfbea6dbb31b3c45422856f67d))



### [2.1.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.1.4...@vh-admin/pro-services@2.1.5) (2023-06-13)


###   ✨ Features | 新功能

* **优化:** 表单提交取出undefined ([a01e382](https://gitee.com/JS-VeryHappy/vh-admin/commit/a01e38231a3aa63265a5a3bf9cab2eb92c991abe))



### [2.1.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.1.3...@vh-admin/pro-services@2.1.4) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([af158f0](https://gitee.com/JS-VeryHappy/vh-admin/commit/af158f059da0dc13e939e41d5c96b573b8a9fd22))



### [2.1.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.1.2...@vh-admin/pro-services@2.1.3) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([852fc19](https://gitee.com/JS-VeryHappy/vh-admin/commit/852fc19fe05d188ef57db52524179542c1e892c9))



### [2.1.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.1.1...@vh-admin/pro-services@2.1.2) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([e005fa3](https://gitee.com/JS-VeryHappy/vh-admin/commit/e005fa347e2fbd0b0e064b125882c2e7a6355362))



### [2.1.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.10...@vh-admin/pro-services@2.1.1) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3f70312](https://gitee.com/JS-VeryHappy/vh-admin/commit/3f70312823ae2ea54d111f4ce517adbce5291c2c))



### [2.0.10](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.8...@vh-admin/pro-services@2.0.10) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3dfb77a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3dfb77af0ee96789a4aac9b4d6e36c5f83e93110))




### [2.0.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.7...@vh-admin/pro-services@2.0.8) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([14ed9a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/14ed9a6012042164a7d8255148979079b89c8f77))




### [2.0.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.6...@vh-admin/pro-services@2.0.7) (2023-04-18)

**Note:** Version bump only for package @vh-admin/pro-services






### [2.0.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.5...@vh-admin/pro-services@2.0.6) (2023-04-11)

**Note:** Version bump only for package @vh-admin/pro-services






### [2.0.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.4...@vh-admin/pro-services@2.0.5) (2023-03-10)

**Note:** Version bump only for package @vh-admin/pro-services

### [2.0.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.3...@vh-admin/pro-services@2.0.4) (2022-12-20)

### ✨ Features | 新功能

- **请求方式:** 增加 postBodyFormData ([6e12e92](https://gitee.com/JS-VeryHappy/vh-admin/commit/6e12e927edf2e247c565a4e4b9639427346304bd))

### [2.0.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.2...@vh-admin/pro-services@2.0.3) (2022-11-22)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([c108a26](https://gitee.com/JS-VeryHappy/vh-admin/commit/c108a2631ee548bbdb235ad8e7f854836662dd84))

### [2.0.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@2.0.1...@vh-admin/pro-services@2.0.2) (2022-11-22)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([dd05540](https://gitee.com/JS-VeryHappy/vh-admin/commit/dd0554083882eb693a85874ffbfaaed5db1769a8))

### [2.0.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@1.0.6...@vh-admin/pro-services@2.0.1) (2022-11-16)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([bc48ac2](https://gitee.com/JS-VeryHappy/vh-admin/commit/bc48ac294281472f97533a2ded6b564eec942b1e))
- **表单:** dumi2 升级 ([6a5503d](https://gitee.com/JS-VeryHappy/vh-admin/commit/6a5503d7be79eec098f21aabb0fc90137177d232))
- **表单:** dumi2 升级 ([8d94a4a](https://gitee.com/JS-VeryHappy/vh-admin/commit/8d94a4ac978d7458e4fc76c5197e309e9a518c9d))

### [1.0.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@1.0.5...@vh-admin/pro-services@1.0.6) (2022-10-26)

### ✨ Features | 新功能

- **表单:** 优化上传 ([5bdc68e](https://gitee.com/JS-VeryHappy/vh-admin/commit/5bdc68e565dadc8bd9611495d55fbc0cac46c465))

### [1.0.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@1.0.4...@vh-admin/pro-services@1.0.5) (2022-09-29)

**Note:** Version bump only for package @vh-admin/pro-services

### [1.0.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@1.0.3...@vh-admin/pro-services@1.0.4) (2022-08-31)

### ✨ Features | 新功能

- **表格:** 内置功能提交增加 tableRef 返回 ([48b1826](https://gitee.com/JS-VeryHappy/vh-admin/commit/48b18265c164239bf74b455914498645c005cd9f))
- **请求:** 下载错误 ([ea84256](https://gitee.com/JS-VeryHappy/vh-admin/commit/ea84256af32dd1a2a4544d4f574ae37e1288faab))

### [1.0.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@1.0.2...@vh-admin/pro-services@1.0.3) (2022-08-31)

### ✨ Features | 新功能

- **表格:** 增加汇总栏和和提示区域 ([8fea090](https://gitee.com/JS-VeryHappy/vh-admin/commit/8fea0906df0fa8f2ad53e5707a5cc88d0c03fccf))

### [1.0.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@1.0.1...@vh-admin/pro-services@1.0.2) (2022-08-24)

### ✨ Features | 新功能

- **弹窗:** 修改使用了 umi ([b7f2ac8](https://gitee.com/JS-VeryHappy/vh-admin/commit/b7f2ac8d0ddff243ce81030c09ef5d300848ffb8))
- **弹窗:** 修改使用了 umi ([0546dec](https://gitee.com/JS-VeryHappy/vh-admin/commit/0546dec9724205c0ac31b00ef6df2297e8d56fc7))
- **弹窗:** 修改使用了 umi ([d327b61](https://gitee.com/JS-VeryHappy/vh-admin/commit/d327b61d1a79becb5c3795f5a86cfa4bf726f9ab))

### [1.0.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.24...@vh-admin/pro-services@1.0.1) (2022-08-24)

### ✨ Features | 新功能

- **弹窗:** 修改使用了 umi ([95d2012](https://gitee.com/JS-VeryHappy/vh-admin/commit/95d2012f1fbf187a8548477daee3cc77d3bb78fe))

### [0.0.24](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.23...@vh-admin/pro-services@0.0.24) (2022-05-25)

### 🐛 Bug Fixes | Bug 修复

- **tree:** 修复无法选中值 ([41441e7](https://gitee.com/JS-VeryHappy/vh-admin/commit/41441e78e55331f684866d402faa07e1dfcf3546))

### [0.0.23](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.22...@vh-admin/pro-services@0.0.23) (2022-05-25)

**Note:** Version bump only for package @vh-admin/pro-services

### [0.0.22](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.21...@vh-admin/pro-services@0.0.22) (2022-05-19)

### ⚡ Performance Improvements | 性能优化

- **升级 react:** 1.8.0 ([a75bd60](https://gitee.com/JS-VeryHappy/vh-admin/commit/a75bd6039afcc06fcd993e6733a60bf2b7aa07ed))

### [0.0.21](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.20...@vh-admin/pro-services@0.0.21) (2022-05-09)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 修改 package 依赖方式 ([eeca8ac](https://gitee.com/JS-VeryHappy/vh-admin/commit/eeca8ac852c1ca415d85b7159effc17d4a04fe8b))

### [0.0.20](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.19...@vh-admin/pro-services@0.0.20) (2022-04-21)

**Note:** Version bump only for package @vh-admin/pro-services

### [0.0.19](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.18...@vh-admin/pro-services@0.0.19) (2022-04-21)

### ✨ Features | 新功能

- **组件:** 全局项目选择器 ([4c8277f](https://gitee.com/JS-VeryHappy/vh-admin/commit/4c8277f0719d17a511d02ae74d6afbc472584e49))

### 🎫 Chores | 其他更新

- **合并:** branch 'review' ([e4b26dc](https://gitee.com/JS-VeryHappy/vh-admin/commit/e4b26dc4f18d64cf6b04b4271b74b017e7842e32))
- **Merge:** branch 'review' ([a6764e1](https://gitee.com/JS-VeryHappy/vh-admin/commit/a6764e125d5b65c7f9274cf3a5b02cde88d8af64))

## [0.0.18](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-services@0.0.17...@vh-admin/pro-services@0.0.18) (2022-04-10)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.17](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.16...@vh-admin/pro-services@0.0.17) (2022-03-31)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.16](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.15...@vh-admin/pro-services@0.0.16) (2022-03-31)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.15](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.14...@vh-admin/pro-services@0.0.15) (2022-03-30)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.14](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.13...@vh-admin/pro-services@0.0.14) (2022-03-24)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.13](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.12...@vh-admin/pro-services@0.0.13) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.12](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.11...@vh-admin/pro-services@0.0.12) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.11](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.10...@vh-admin/pro-services@0.0.11) (2022-03-18)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.10](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.9...@vh-admin/pro-services@0.0.10) (2022-03-17)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.9](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.8...@vh-admin/pro-services@0.0.9) (2022-03-17)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.8](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.7...@vh-admin/pro-services@0.0.8) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.7](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.3...@vh-admin/pro-services@0.0.7) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-services

## [0.0.3](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-services@0.0.2...@vh-admin/pro-services@0.0.3) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-services
