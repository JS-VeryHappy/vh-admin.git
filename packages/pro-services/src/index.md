---
title: 请求类型
order: 1
toc: content
nav:
  order: 3
  title: 请求
  path: /services
group:
  title: 请求类型
  path: /index
  order: 1
---

## 说明

> 1. [umi-request](https://github.com/umijs/umi-request/blob/master/README_zh-CN.md)
> 2. [axios](https://www.axios-http.cn/)

## 发送请求

```js
// 调用
import { requestQuery } from '@vh-admin/pro-services';

//代码
const requestQuery = (url: string, options: any) => {
  return request(url, options);
};
```

## 发送 post 请求，Body 参数形式

```js
// 调用
import { postBody } from '@vh-admin/pro-services';

//代码
export function postBody(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'post', data, ...options });
}
```

## 发送 post 请求，Query 参数形式

```js
// 调用
import { postQuery } from '@vh-admin/pro-services';

//代码
export function postQuery(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'post', params: data, ...options });
}
```

## 发送 post 请求，Body 参数形式，提交 FormData

```js
// 调用
import { postBodyFormData } from '@vh-admin/pro-services';

//代码
export function postBodyFormData(url: string, data?: any, options?: any) {
  let formData: any = {};

  if (data instanceof FormData === false) {
    formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (data[key] instanceof Array) {
        data[key].forEach((item: any) => {
          formData.append(key, item);
        });
        return;
      }
      formData.append(key, data[key]);
    });
  } else {
    formData = data;
  }

  return requestQuery(url, { method: 'post', formData, ...options });
}
```

## 发送 get 请求, Body 参数形式

```js
// 调用
import { getBody } from '@vh-admin/pro-services';

//代码
export function getBody(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'get', data, ...options });
}
```

## 发送 get 请求, Query 参数形式

```js
// 调用
import { getQuery } from '@vh-admin/pro-services';

//代码
export function getQuery(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'get', params: data, ...options });
}
```

## 发送 path 请求, Body 参数形式

```js
// 调用
import { pathBody } from '@vh-admin/pro-services';

//代码
export function pathBody(url: string, data?: any, options?: any) {
  let nurl = url;
  Object.keys(data).forEach((key) => {
    nurl += `_${data[key]}`;
  });
  return requestQuery(nurl, { method: 'post', data, ...options });
}
```

## 发送 path 请求, Query 参数形式

```js
// 调用
import { pathQuery } from '@vh-admin/pro-services';

//代码
export function pathQuery(url: string, data?: any, options?: any) {
  let nurl = url;
  Object.keys(data).forEach((key) => {
    nurl += `_${data[key]}`;
  });
  return requestQuery(nurl, { method: 'post', params: data, ...options });
}
```

## 发送 post 请求, 下载二进制文件读取文件名称

```js
// 调用
import { postDownloadBlob } from '@vh-admin/pro-services';

//代码
export const postDownloadBlob = async (url: string, data?: any, options?: any) => {
  const res: any = await postBody(url, data, {
    ...options,
    responseType: 'blob',
    getResponse: true,
  });
  return {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  };
};
```

## post 以表单的方式 下载文件返回二进制数据

```js
// 调用
import { postFormDownloadBlob } from '@vh-admin/pro-services';

export const postFormDownloadBlob = async (url: string, data?: any, options?: any) => {
  let formData: any = {};

  if (data instanceof FormData === false) {
    formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (data[key] instanceof Array) {
        data[key].forEach((item: any) => {
          formData.append(key, item);
        });
        return;
      }
      formData.append(key, data[key]);
    });
  } else {
    formData = data;
  }

  const res: any = await postBody(url, formData, {
    ...options,
    responseType: 'blob',
    requestType: 'form',
    getResponse: true,
  });

  return {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  };
};
```

## 发送 post 请求, 下载二进制文件读取文件名称并且直接下载

```js
// 调用
import { postDownload } from '@vh-admin/pro-services';

//代码
export const postDownload = async (url: string, data?: any, options?: any) => {
  const res: any = await postBody(url, data, {
    ...options,
    responseType: 'blob',
    getResponse: true,
  });

  return downloadFile('blob', {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  });
};
```

## 发送 post 表单方式提交参数, 下载二进制文件读取文件名称并且直接下载

```js
// 调用
import { postFormDownload } from '@vh-admin/pro-services';

//代码
export const postFormDownload = async (url: string, data?: any, options?: any) => {
  const res: any = await postBody(url, data, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: '*/*',
    },
    ...options,
    responseType: 'blob',
    requestType: 'form',
    getResponse: true,
  });

  return downloadFile('blob', {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  });
};
```

## 发送 post 请求, 上传文件

```js
// 调用
import { postUploadFile } from '@vh-admin/pro-services';

//代码
export function postUploadFile(url: string, data?: any, options?: any) {
  let formData: any = {};

  if (data instanceof FormData === false) {
    formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (data[key] instanceof Array) {
        data[key].forEach((item: any) => {
          formData.append(key, item);
        });
        return;
      }
      formData.append(key, data[key]);
    });
  } else {
    formData = data;
  }

  return requestQuery(url, {
    method: 'post',
    data: formData,
    // headers: {
    //   'Content-Type': 'multipart/form-data',
    // },
    requestType: 'form',
    ...options,
  });
}
```

```js
// 可以关闭错误提示
import { postBody } from '@vh-admin/pro-services';

export async function userQuery(data: any) {
  return postBody('/api/user/query', data, { errorMessageShow: false });
}
```
