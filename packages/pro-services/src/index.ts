// @ts-ignore
import { downloadFile } from '@vh-admin/pro-utils';

export { default as requestConfig } from './config';

const requestQuery = (url: string, options: any) => {
  let request: any = () => {};
  // @ts-ignore
  if (window?.vhAdmin?.request) {
    // @ts-ignore
    request = window.vhAdmin?.request;
  }

  if (options.data instanceof FormData) {
    const del: any = [];
    options.data.keys().forEach((key: any) => {
      const v = options.data.get(key);
      if (v === undefined || v === 'undefined') {
        del.push(key);
      }
    });
    if (del.length > 0) {
      del.forEach((k: any) => {
        options.data.delete(k);
      });
    }
  }

  return request(url, options);
};
/**
 * post请求 body形式
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export function postBody(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'post', data, ...options });
}

/**
 * post请求 body形式 提交FormData
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export function postBodyFormData(url: string, data?: any, options?: any) {
  let formData: any = {};

  if (data instanceof FormData === false) {
    formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (data[key] instanceof Array) {
        data[key].forEach((item: any) => {
          formData.append(key, item);
        });
        return;
      }
      formData.append(key, data[key]);
    });
  } else {
    formData = data;
  }

  return requestQuery(url, { method: 'post', formData, ...options });
}

/**
 * post请求 query形式
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export function postQuery(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'post', params: data, ...options });
}

/**
 * get请求 body形式
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */

export function getBody(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'get', data, ...options });
}
/**
 * get请求 query形式
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */

export function getQuery(url: string, data?: any, options?: any) {
  return requestQuery(url, { method: 'get', params: data, ...options });
}

/**
 * get请求 路径形式
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */

export function pathBody(url: string, data?: any, options?: any) {
  let nurl = url;
  Object.keys(data).forEach((key) => {
    nurl += `_${data[key]}`;
  });
  return requestQuery(nurl, { method: 'post', data, ...options });
}
/**
 * get请求 路径形式
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export function pathQuery(url: string, data?: any, options?: any) {
  let nurl = url;
  Object.keys(data).forEach((key) => {
    nurl += `_${data[key]}`;
  });
  return requestQuery(nurl, { method: 'post', params: data, ...options });
}

/**
 * post下载 返回二进制数据
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export const postDownloadBlob = async (url: string, data?: any, options?: any) => {
  const res: any = await postBody(url, data, {
    ...options,
    responseType: 'blob',
    getResponse: true,
  });
  return {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  };
};

/**
 * post 表单方式提交参数下载文件返回二进制数据
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export const postFormDownloadBlob = async (url: string, data?: any, options?: any) => {
  let formData: any = {};

  if (data instanceof FormData === false) {
    formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (data[key] instanceof Array) {
        data[key].forEach((item: any) => {
          formData.append(key, item);
        });
        return;
      }
      formData.append(key, data[key]);
    });
  } else {
    formData = data;
  }

  const res: any = await postBody(url, formData, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: '*/*',
    },
    ...options,
    responseType: 'blob',
    requestType: 'form',
    getResponse: true,
  });

  return {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  };
};

/**
 * post 表单方式提交参数 直接下载
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export const postFormDownload = async (url: string, data?: any, options?: any) => {
  let formData: any = {};

  if (data instanceof FormData === false) {
    formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (data[key] instanceof Array) {
        data[key].forEach((item: any) => {
          formData.append(key, item);
        });
        return;
      }
      formData.append(key, data[key]);
    });
  } else {
    formData = data;
  }

  const res: any = await postBody(url, formData, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: '*/*',
    },
    ...options,
    responseType: 'blob',
    requestType: 'form',
    getResponse: true,
  });

  return downloadFile('blob', {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  });
};

/**
 * post 直接下载
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export const postDownload = async (url: string, data?: any, options?: any) => {
  const res: any = await postBody(url, data, {
    ...options,
    responseType: 'blob',
    getResponse: true,
  });

  return downloadFile('blob', {
    data: res.data,
    headers: {
      'content-disposition': res.headers['content-disposition'],
    },
  });
};

/**
 * post请求 body形式
 * @param url // url
 * @param data // 表单数据
 * @param options // 额外配置
 */
export function postUploadFile(url: string, data?: any, options?: any) {
  let formData: any = {};

  if (data instanceof FormData === false) {
    formData = new FormData();
    Object.keys(data).forEach((key) => {
      if (data[key] instanceof Array) {
        data[key].forEach((item: any) => {
          formData.append(key, item);
        });
        return;
      }
      formData.append(key, data[key]);
    });
  } else {
    formData = data;
  }

  return requestQuery(url, {
    method: 'post',
    data: formData,
    // headers: {
    //   'Content-Type': 'multipart/form-data',
    // },
    requestType: 'form',
    ...options,
  });
}
