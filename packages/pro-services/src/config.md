---
title: 请求拦截器
order: 1
toc: content
nav:
  order: 3
  title: 请求
  path: /services
group:
  title: 请求拦截器
  path: /config
  order: 1
---

## 说明

> 1. [@umijs/plugin-request](https://umijs.org/zh-CN/plugins/plugin-request)

## 拦截设置

```js
const requestConfig: RequestConfig = const requestConfig: RequestConfig = {
  timeout: 30000,
  requestInterceptors: [
    (config: any) => {
      let httpHeadersHook = undefined;

      // @ts-ignore
      if (window?.vhAdmin?.httpHeadersHook) {
        // @ts-ignore
        httpHeadersHook = window.vhAdmin?.httpHeadersHook;
      }

      const { url, ...options } = config;
      const time = parseInt(new Date().getTime() / 1000 + '');
      const sign = CryptoJs.MD5(
        // @ts-ignore
        process.env.version + time + process.env.sign_key,
      ).toString();

      options.headers.common = {
        ...options.headers.common,
        version: process.env.version,
        time: time,
        sign: sign,
      };

      //配置默认开启错误提示
      if (options.errorMessageShow === undefined) {
        options.errorMessageShow = true;
      }

      if (typeof httpHeadersHook === 'function') {
        options.headers = httpHeadersHook(options.headers);
      }

      return {
        url: `${process.env.api_url}${url}`,
        ...options,
      };
    },
  ],
  responseInterceptors: [
    [
      (response: any) => {
        let hookConfig: any;

        // @ts-ignore
        if (window?.vhAdmin?.responseConfig) {
          // @ts-ignore
          responseConfig = window.vhAdmin?.responseConfig;
        }

        // @ts-ignore
        if (window?.vhAdmin?.httpError) {
          httpError = {
            ...httpError,
            // @ts-ignore
            ...window.vhAdmin?.httpError,
          };
        }
        let httpErrorHook: any = undefined;
        // @ts-ignore
        if (window?.vhAdmin?.httpErrorHook) {
          // @ts-ignore
          httpErrorHook = window?.vhAdmin?.httpErrorHook;
        }

        const { request, config } = response;

        if (response.status !== 200) {
          const errMessage = httpError[response.status] || '未知错误';
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(response.status, null, response, response.data);
          }
          showErrorMessage(config, errMessage, hookConfig);
          return Promise.reject();
        }
        // 如果是二进制数据
        if (request.responseType === 'blob') {
          if (!response.headers['content-disposition']) {
            response.data.text().then((r: any) => {
              const rdata = JSON.parse(r);
              if (typeof httpErrorHook === 'function') {
                hookConfig = httpErrorHook(
                  response.status,
                  rdata[responseConfig.code],
                  response,
                  response.data,
                );
              }
              showErrorMessage(config, rdata[responseConfig.message], hookConfig);
            });
            return Promise.reject();
          }

          return response;
        }
        const resData: any = response.data;

        // 如果出现 没有code的情况
        if (resData[responseConfig.code] === undefined) {
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(
              response.status,
              resData[responseConfig.code],
              response,
              response.data,
            );
          }
          showErrorMessage(config, '未知错误', hookConfig);
          return Promise.reject('未知错误');
        }

        if (resData[responseConfig.code] !== responseConfig.success) {
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(
              response.status,
              resData[responseConfig.code],
              response,
              resData,
            );
          }
          showErrorMessage(config, resData[responseConfig.message] || '系统错误', hookConfig);
          return Promise.reject(resData[responseConfig.message] || '系统错误');
        }
        return response;
      },
      (error: any) => {
        let hookConfig: any;
        let httpErrorHook = undefined;
        // @ts-ignore
        if (window?.vhAdmin?.httpErrorHook) {
          // @ts-ignore
          httpErrorHook = window?.vhAdmin?.httpErrorHook;
        }

        const { code, config, response } = error;
        if (!response) {
          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(undefined, code, response, error);
          }
          showErrorMessage(config, error.message, hookConfig);

          return Promise.reject();
        }
        if (response.status !== 200) {
          let errMessage: any = httpError[response.status] || '未知错误';

          if (code === 'ERR_NETWORK') {
            errMessage = '网络错误!';
          }

          if (typeof httpErrorHook === 'function') {
            hookConfig = httpErrorHook(response.status, code, response, response.data);
          }

          showErrorMessage(config, errMessage, hookConfig);

          return Promise.reject();
        }
        return Promise.reject();
      },
    ],
  ],
};
```
