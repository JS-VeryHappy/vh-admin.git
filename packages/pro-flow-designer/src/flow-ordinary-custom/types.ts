import type { FormCustomColumnsType } from '@vh-admin/pro-components/lib/f-form-custom/types';

export declare type FlowOrdinaryCustomDataType = {
  /**
   * 自动生成
   */
  id: any;
  /**
   * 自动会生成
   */
  shape: string;
  /**
   * 数据
   */
  data?: {
    //node显示的名称值 如果存在会覆盖 attrs.name.text
    flowNodeName: string;
    [key: string]: any;
  };
  /**
   * 属性
   */
  attrs?: any;
};

export declare type SubmitOnDoneType = {
  /**
   * 状态
   */
  status?: 'success' | 'error';
  /**
   * 请求的请求数据
   */
  params?: Record<any, any>;
  /**
   * 请求结果
   */
  result?: Record<any, any> | any;
};

export declare type InitialValuesType = {
  /**
   * 审批人显示字段值
   */
  flowNodeName?: string;
  /**
   * 其它
   */
  [key: string]: any;
};

export declare type FlowOrdinaryCustomType = {
  /**
   * 自定义的 className
   */
  className?: string;

  /**
   * 如果不设定会根据外面父级宽高来设置
   * 画布高
   */
  height?: number;
  /**
   * 如果不设定会根据外面父级宽高来设置
   * 画布宽
   */
  width?: number;

  /**
   * 数据值
   */
  initialValues?: InitialValuesType[];
  /**
   * 排版方向,垂直或者居中
   * @default vertical
   */
  direction?: 'vertical ' | 'horizontal';
  /**
   * 优先取取 initialValues 里面的 flowTitleName
   * 审批节点标题显示字段
   */
  flowTitleName?: string;
  /**
   * 优先取取 initialValues 里面的 flowNodeName
   * 审批人显示字段
   */
  flowNodeName?: string;
  /**
   *
   * @param value 提交前数据处理
   * @returns
   */
  submitValuesBefor?: (data: FlowOrdinaryCustomDataType[]) => any;
  /**
   * 提交请求的Request
   * submitValue 提交数据
   * columns 当前表单配置
   * @default
   */
  submitRequest?: (data: FlowOrdinaryCustomDataType[]) => any;
  /**
   * 请求完成成功回调
   * @default
   */
  submitOnDone?: (params: SubmitOnDoneType) => void;
  /**
   * 发起网络请求的参数,与 request 配合使用
   * @default
   */
  params?: Record<any, any>;

  /**
   * 表单的配置
   */
  columns: FormCustomColumnsType<any>[];
  /**
   *
   * 每次数据切换前变动
   */
  onNodeFinishBefor?: (data: Record<any, any>) => Record<any, any>;
};
