//@ts-ignore
import { FlowOrdinaryCustom } from '@vh-admin/pro-flow-designer';
import { waitTime } from '@vh-admin/pro-utils';

function Demo1() {
  const options = [
    {
      label: '周大仙1',
      value: 1,
    },
    {
      label: '周大仙2',
      value: 2,
    },
    {
      label: '周大仙3',
      value: 3,
    },
  ];
  const request = async (data: any) => {
    await waitTime(2000);
    console.log('request', data);
    return { result: 'result' };
  };

  const submitValuesBefor = (data: any) => {
    console.log('submitValuesBefor', data);
    data.bbbb = 222;
    return data;
  };

  const submitOnDone = (params: any) => {
    console.log('submitOnDone', params);
  };

  const onNodeChangeBefor = (data: any) => {
    const has = options.find((i) => i.value === data.user_id);

    return { ...data, user_name: has?.label };
  };

  return (
    <>
      <FlowOrdinaryCustom
        columns={[
          {
            valueType: 'text',
            dataIndex: 'title',
            title: '标题',
            fieldProps: {
              placeholder: '请输入标题',
              maxLength: 20,
            },
            formItemProps: {
              rules: [{ required: true, message: '请输入标题' }],
            },
          },
          {
            title: '审批人',
            dataIndex: 'user_id',
            valueType: 'select',
            fieldProps: {
              options: options,
            },
          },
        ]}
        params={{ aa: 11 }}
        submitValuesBefor={submitValuesBefor}
        submitRequest={request}
        submitOnDone={submitOnDone}
        flowNodeName="user_name"
        flowTitleName="title"
        // direction="horizontal"
        onNodeFinishBefor={onNodeChangeBefor}
        initialValues={[
          {
            id: 111,
            user_id: 3,
            user_name: '周大仙3',
            title: '你好呀呀',
          },
        ]}
      />
    </>
  );
}

export default Demo1;
