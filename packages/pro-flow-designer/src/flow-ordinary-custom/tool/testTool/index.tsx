import React from 'react';
import ReactDom from 'react-dom/client';
import { Tooltip } from 'antd';
import { ToolsView } from '@antv/x6';
import type { EdgeView } from '@antv/x6';

class ToolEdge extends ToolsView.ToolItem<EdgeView, TooltipToolOptions> {
  private knob: HTMLDivElement | any;
  private rootContainer: HTMLDivElement | any;

  render() {
    if (!this.knob) {
      this.knob = ToolsView.createElement('div', false) as HTMLDivElement;
      this.knob.style.position = 'absolute';
      this.knob.style.display = 'block';
      console.log('===================');
      console.log(this.knob);
      console.log('===================');
      this.container.appendChild(this.knob);
      this.toggleTooltip(true);
    }
    return this;
  }

  private toggleTooltip(visible: boolean) {
    if (this.knob) {
      if (visible) {
        if (this.rootContainer) {
          this.rootContainer.unmount();
        }
        const root = ReactDom.createRoot(this.knob);
        this.rootContainer = root;
        this.rootContainer.render(
          <Tooltip
            title="这是连线上渲染的React内容"
            // defaultVisible={true}
          >
            <div>添加</div>
          </Tooltip>,
        );
      }
    }
  }

  // private onMosueEnter({ e }: { e: MouseEvent }) {
  //   this.updatePosition(e);
  //   this.toggleTooltip(true);
  // }

  // private onMouseLeave() {
  //   this.updatePosition();
  //   this.toggleTooltip(false);
  // }

  // private onMouseMove() {
  //   this.updatePosition();
  //   this.toggleTooltip(false);
  // }

  // delegateEvents() {
  //   this.cellView.on('cell:mouseenter', this.onMosueEnter, this);
  //   this.cellView.on('cell:mouseleave', this.onMouseLeave, this);
  //   this.cellView.on('cell:mousemove', this.onMouseMove, this);
  //   return super.delegateEvents();
  // }

  // private updatePosition(e?: MouseEvent) {
  //   const style = this.knob.style;
  //   if (e) {
  //     const p = this.graph.clientToGraph(e.clientX, e.clientY);
  //     style.display = 'block';
  //     style.left = `${p.x}px`;
  //     style.top = `${p.y}px`;
  //   } else {
  //     style.display = 'none';
  //     style.left = '-1000px';
  //     style.top = '-1000px';
  //   }
  // }

  // protected onRemove() {
  //   this.toggleTooltip(false);
  //   this.cellView.off('cell:mouseenter', this.onMosueEnter, this);
  //   this.cellView.off('cell:mouseleave', this.onMouseLeave, this);
  //   this.cellView.off('cell:mousemove', this.onMouseMove, this);
  // }
}

ToolEdge.config({
  tagName: 'div',
  isSVGElement: false,
});

export interface TooltipToolOptions extends ToolsView.ToolItem.Options {
  tooltip?: string;
}

export default ToolEdge;
