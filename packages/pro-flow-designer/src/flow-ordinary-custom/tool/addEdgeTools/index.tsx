import type { ToolsView } from '@antv/x6';
// @ts-ignore
import addPng from '../../images/add.png';

/**
 * 可以导出给界面使用，
 */
export const addEdgeToolsConfig: ToolsView.ToolItem.Options & {
  inherit?: string | undefined;
} & Record<string, any> = {
  inherit: 'button',
  markup: [
    {
      tagName: 'image',
      selector: 'img',
      attrs: {
        cursor: 'pointer',
        'xlink:href': addPng,
        width: 20,
        height: 20,
        x: -10,
        y: -10,
      },
    },
  ],
  distance: '50%',
};
