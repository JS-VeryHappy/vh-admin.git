import './index.less';
import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';
import { Graph } from '@antv/x6';
import type { Node } from '@antv/x6';
import { History } from '@antv/x6-plugin-history';
import { uuid } from '@vh-admin/pro-utils';
import { Button, Modal } from 'antd';
import { addEdgeConfig } from './edge/addEdge';
import { addEdgeToolsConfig } from './tool/addEdgeTools';

// @ts-ignore
// import setPng from './images/set.png';
// import savePng from './images/save.png';
// import chehuiPng from './images/chehui.png';
// import huifuPng from './images/huifu.png';
// @ts-ignore
import fangdaPng from './images/fangda.png';
// @ts-ignore
import suoxiaoPng from './images/suoxiao.png';
import { startNodeConfig } from './node/startNode';
import { endNodeConfig } from './node/endNode';
import { examineNodeConfig } from './node/examineNode';
import { message } from 'antd';

import { FormCustom } from '@vh-admin/pro-components';
import type { FlowOrdinaryCustomDataType, FlowOrdinaryCustomType } from './types';

const FlowOrdinaryCustom = (props: FlowOrdinaryCustomType) => {
  const {
    className,
    height,
    width,
    columns,
    submitValuesBefor,
    submitRequest,
    submitOnDone,
    params,
    initialValues,
    flowNodeName,
    flowTitleName,
    direction = 'vertical',
    onNodeFinishBefor,
  } = props;
  /** 画布配置 */
  const graphRef = useRef<Graph>();
  const clickCellRef = useRef<Node>();
  const dataSourceRef = useRef<FlowOrdinaryCustomDataType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [open, setOpen] = useState<boolean>(false);

  const [zoom, setZoom] = useState<any>(0);
  const [, setDataSource] = useState<FlowOrdinaryCustomDataType[]>([]);
  const [formData, setFormData] = useState<any>();

  const setEdgesData = (data: any) => {
    const nodes = data;
    const edges: any = [];
    const examine = nodes.filter((i: any) => {
      return i.shape === 'examine-node';
    });

    examine.forEach((i: any) => {
      let display = '';
      if (examine.length <= 1) {
        display = 'none';
      }
      i.attrs.delete = {
        display: display,
      };

      if (i.data) {
        if (i.data.flowNodeName) {
          i.attrs.name = {
            text: i.data.flowNodeName,
          };
        } else {
          if (flowNodeName) {
            i.attrs.name = {
              text: i.data[flowNodeName],
            };
          }
        }

        if (i.data.flowTitleName) {
          i.label = i.data.flowTitleName;
        } else {
          if (flowTitleName) {
            i.label = i.data[flowTitleName];
          }
        }
      }
    });

    if (direction === 'vertical') {
      //竖排
      nodes.forEach((i: any, index: any) => {
        if (i.shape === 'end-node' || i.shape === 'start-node') {
          i.x = 35;
        } else {
          i.x = 0;
        }

        i.y = index * 140;
      });
    } else {
      // 横排
      nodes.forEach((i: any, index: any) => {
        i.x = index * 250;
        i.y = 0;
      });
    }

    nodes.reduce((accumulator: any, currentValue: any, index: any) => {
      edges.push({
        shape: 'add-edge',
        source: accumulator.id,
        target: currentValue.id,
        data: {
          index: index,
        },
      });
      return currentValue;
    });

    return {
      nodes,
      edges,
    };
  };

  const loadData = (jsonData: any, centerContent: boolean = false) => {
    graphRef?.current?.fromJSON(jsonData);

    if (centerContent) {
      graphRef?.current?.centerContent();
    }
  };

  const refresh = () => {
    loadData(setEdgesData(dataSourceRef.current));
  };

  const setDataSourceFn = (data: any) => {
    dataSourceRef.current = data;
    setDataSource(data);
  };

  const setZoomNumber = () => {
    if (graphRef.current?.zoom()) {
      // @ts-ignore
      setZoom(parseInt(graphRef?.current?.zoom() * 100) + '%');
    }
  };

  const handleTools = (type: any) => {
    switch (type) {
      case 'set':
        break;
      case 'fangda':
        graphRef.current?.zoom(0.2);
        setZoomNumber();
        break;
      case 'suoxiao':
        graphRef.current?.zoom(-0.2);
        setZoomNumber();
        break;
      case 'chehui':
        // graphRef.current?.undo();
        break;
      case 'huifu':
        // graphRef.current?.redo();
        break;
    }
  };

  const handleBtns = async (type: any) => {
    if (type === 'set') {
    } else if (type === 'save') {
      const data: any[] = dataSourceRef.current
        .filter((i: FlowOrdinaryCustomDataType) => {
          return i.shape === 'examine-node' && i.data;
        })
        .map((i: FlowOrdinaryCustomDataType) => {
          return {
            ...i.data,
          };
        });
      let submitValue: any = { data: data };

      if (params) {
        submitValue = { ...params, ...submitValue };
      }

      if (submitValuesBefor) {
        submitValue = submitValuesBefor(submitValue);

        // 如果配置了自动请求
        if (submitRequest) {
          setLoading(true);
          try {
            const result = await submitRequest(submitValue);
            // 如果设置请求回调
            if (submitOnDone) {
              submitOnDone({
                status: 'success',
                result,
                params: submitValue,
              });
            } else {
              message.success('保存成功');
            }
            setLoading(false);
            return result;
          } catch (error) {
            console.log(error);
            if (submitOnDone) {
              submitOnDone({
                status: 'error',
                result: error,
                params: submitValue,
              });
            }
            setLoading(false);
            return false;
          }
        }
      }
    }
  };

  const addNode = ({ cell }: any) => {
    const data = cell.getData();
    const source: FlowOrdinaryCustomDataType[] = [...dataSourceRef.current];
    source.splice(data.index, 0, {
      id: uuid(),
      shape: 'examine-node',
      attrs: {
        name: {
          text: '请选择',
        },
      },
    });

    setDataSourceFn(source);

    refresh();
  };

  const onFinish = (value: any) => {
    let data: any = { ...formData, ...value };

    const source = [...dataSourceRef.current];
    const has: any = source.find((i) => i.id == clickCellRef?.current?.id);
    const hasIndex: any = source.findIndex((i) => i.id == clickCellRef?.current?.id);

    if (onNodeFinishBefor) {
      data = onNodeFinishBefor(data);
    }

    has.data = data;

    source.splice(hasIndex, 1, has);

    setDataSourceFn(source);

    refresh();

    setOpen(false);
  };

  const init = () => {
    Graph.registerEdge('add-edge', addEdgeConfig, true);

    // 注册自定义线添加工具
    Graph.registerEdgeTool(
      'add-edge-tools',
      {
        ...addEdgeToolsConfig,
        onClick: addNode,
      },
      true,
    );
    // 注册自定义发起节点
    Graph.registerNode('start-node', startNodeConfig, true);
    // 注册自定义结束节点
    Graph.registerNode('end-node', endNodeConfig, true);
    // 注册自定义审批节点
    Graph.registerNode('examine-node', examineNodeConfig, true);

    graphRef.current?.use(
      new History({
        enabled: true,
      }),
    );

    setZoomNumber();
  };

  const initEvent = () => {
    graphRef?.current?.on('node:start-node', () => {});

    graphRef?.current?.on('node:end-node', () => {});

    graphRef?.current?.on('node:examine-node', async (data: { node: Node<Node.Properties> }) => {
      const { node } = data;

      clickCellRef.current = node; // 用一个变量记录下当前被右键的节点

      const source = [...dataSourceRef.current];
      const has: any = source.find((i) => i.id == node.id);

      if (has && has.data) {
        setFormData(has.data);
      } else {
        setFormData({});
      }

      setOpen(true);
    });

    graphRef?.current?.on('node:examine-node-delete', (data: { node: Node<Node.Properties> }) => {
      const { node } = data;
      const { id } = node;
      Modal.confirm({
        title: '操作确认',
        content: '确认要删除该审批节点吗?删除后无法恢复。',
        okText: '确定',
        cancelText: '取消',
        onOk() {
          const source = [...dataSourceRef.current];

          const hasIndex = source.findIndex((i) => i.id == id);

          source.splice(hasIndex, 1);

          setDataSourceFn(source);

          refresh();
        },
        onCancel() {},
      });
    });

    graphRef?.current?.on('edge:mouseenter', ({ edge }) => {
      edge.setAttrs({ line: { stroke: '#333' } });
      edge.addTools([
        {
          name: 'add-edge-tools',
        },
      ]);
    });

    graphRef?.current?.on('edge:mouseleave', ({ edge }) => {
      edge.setAttrs({ line: { stroke: '#bbb' } });
      edge.removeTools();
    });
  };

  useEffect(() => {
    graphRef.current = new Graph({
      //@ts-ignore
      container: document.getElementById('container'),
      autoResize: true,
      width: width,
      height: height || 600,
      background: {
        color: '#fafafa',
      },
      panning: true,
      mousewheel: false,
      interacting: {
        nodeMovable: false,
        edgeMovable: false,
      },
      grid: {
        visible: true,
        type: 'fixedDot',
      },
    });

    init();

    initEvent();

    // register({
    //   ...startNodeConfig,
    //   component: StartNode,
    // });

    let source: FlowOrdinaryCustomDataType[] = [];

    if (initialValues && initialValues.length > 0) {
      source = initialValues.map((i: any) => {
        return {
          id: uuid(),
          shape: 'examine-node',
          data: {
            ...i,
          },
          attrs: {
            name: {
              text: '',
            },
          },
        };
      });
    } else {
      source = [
        {
          id: uuid(),
          shape: 'examine-node',
          attrs: {
            name: {
              text: '请选择',
            },
          },
        },
      ];
    }

    source.unshift({
      id: 'start',
      shape: 'start-node',
    });

    source.push({
      id: 'end',
      shape: 'end-node',
    });

    // const source: FlowOrdinaryCustomDataType[] = [
    //   {
    //     id: uuid(),
    //     shape: 'examine-node',
    //     attrs: {
    //       name: {
    //         text: '请选择',
    //       },
    //     },
    //   },
    // ];

    setDataSourceFn(source);

    loadData(setEdgesData(source), true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    //@ts-ignore
    <div
      className={classNames({
        'flow-ordinary-custom': true,
        className,
      })}
      id="flow-ordinary-custom"
    >
      <div className="header">
        <div className="title">流程设置</div>
        <div className="btns">
          <Button loading={loading} className="btn" onClick={() => handleBtns('save')}>
            保存
          </Button>
        </div>
      </div>

      <div className="content">
        <div id="container"></div>

        <div className="tools">
          <div className="item" title="放大" onClick={() => handleTools('fangda')}>
            <img src={fangdaPng} />
          </div>
          <div className="item">{zoom}</div>
          <div className="item" title="缩小" onClick={() => handleTools('suoxiao')}>
            <img src={suoxiaoPng} />
          </div>
          {/* <div className="item" title="撤回" onClick={() => handleTools('chehui')}>
            <img src={chehuiPng} />
          </div>
          <div className="item" title="恢复" onClick={() => handleTools('huifu')}>
            <img src={huifuPng} />
          </div> */}
        </div>
      </div>
      {open && (
        <FormCustom
          width={400}
          title="选择审批人"
          layoutType="DrawerForm"
          initialValues={formData}
          onFinish={onFinish}
          columns={columns}
          onOpenChange={(v: any) => {
            if (!v) {
              setOpen(false);
            }
          }}
          open={open}
        />
      )}
    </div>
  );
};
export default FlowOrdinaryCustom;
