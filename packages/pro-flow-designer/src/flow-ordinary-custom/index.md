---
title: '普通审批流程'
order: 1
nav:
  order: 8
  title: 流程设计器
  path: /flowDesigner
group:
  title: 流程设计器
  path: /flow
  order: 1
---


# 说明

> 1. [antv-xflow](https://xflow.antv.vision/docs/tutorial/intro/about)


<code src="./Example/demo1.tsx" >普通审批流程</code>


## API

### FlowOrdinaryCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| className | 自定义的className | `string` | - | - |
| height | 如果不设定会根据外面父级宽高来设置<br>画布高 | `number` | - | - |
| width | 如果不设定会根据外面父级宽高来设置<br>画布宽 | `number` | - | - |
| initialValues | 数据值 | `initialValuesType[]` | - | - |
| direction | 排版方向,垂直或者居中 | `'vertical ' \| 'horizontal'` | `vertical` | - |
| flowTitleName | 优先取取 initialValues 里面的 flowTitleName<br>审批节点标题显示字段 | `string` | - | - |
| flowNodeName | 优先取取initialValues里面的flowNodeName<br>审批人显示字段 | `string` | - | - |
| submitValuesBefor | <br>@paramvalue提交前数据处理<br>@returns | `(data:FlowOrdinaryCustomDataType[])=>any` | - | - |
| submitRequest | 提交请求的Request<br>submitValue提交数据<br>columns当前表单配置<br> | `(data:FlowOrdinaryCustomDataType[])=>any` |  | - |
| submitOnDone | 请求完成成功回调<br> | `(params:SubmitOnDoneType)=>void` |  | - |
| params | 发起网络请求的参数,与request配合使用<br> | `Record<any,any>` |  | - |
| columns(必须) | 表单的配置 | `FormCustomColumnsType<any>[]` | - | - |
| onNodeFinishBefor | <br>每次数据切换前变动 | `(data:Record<any,any>)=>Record<any,any>` | - | - |   


### SubmitOnDoneType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| status | 状态 | `'success'\|'error'` | - | - |
| params | 请求的请求数据 | `Record<any,any>` | - | - |
| result | 请求结果 | `Record<any,any>\|any` | - | - |

### initialValuesType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| flowNodeName | 审批人显示字段值 | `string` | - | - |
| ... | 其它 | `string]:any` | - | - |