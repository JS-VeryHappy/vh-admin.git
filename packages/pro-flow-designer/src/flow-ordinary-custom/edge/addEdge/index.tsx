import type { Edge } from '@antv/x6';

/**
 * 可以导出给界面使用，
 */
export const addEdgeConfig: Edge.Config = {
  inherit: 'edge',
  markup: [
    {
      tagName: 'path',
      selector: 'line',
      attrs: {
        fill: 'none',
        cursor: 'pointer',
      },
    },
  ],
  attrs: {
    line: {
      connection: true,
      stroke: '#bbb',
      strokeWidth: 3,
    },
  },
};
