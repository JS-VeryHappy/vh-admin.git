import type { Node } from '@antv/x6';
// @ts-ignore
import endPng from '../../images/end.png';

export const endNodeConfig: Node.Config = {
  inherit: 'rect',
  width: 80,
  height: 40,
  markup: [
    {
      tagName: 'rect', // 标签名称
      selector: 'body', // 选择器
      style: {
        cursor: 'pointer',
      },
    },
    {
      tagName: 'image',
      selector: 'img',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'text',
      selector: 'label',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
  ],
  attrs: {
    //节点样式数据
    body: {
      //节点主体背景
      rx: 20,
      ry: 20,
      fill: '#999',
      stroke: '#999',
      strokeWidth: 1,
      filter: {
        name: 'dropShadow',
        args: { color: 'rgba(0, 0, 0, 0.3)', dx: 0, dy: 2, blur: 10, opacity: 0.5 },
      },
      event: 'node:end-node',
    },
    text: {
      text: '结束',
      fill: '#fff',
      FontSize: 12,
      x: 8,
      y: 0,
    },
    img: {
      'xlink:href': endPng,
      width: 16,
      height: 16,
      x: 16,
      y: 12,
    },
  },
};
