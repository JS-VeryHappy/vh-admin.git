import type { Node } from '@antv/x6';
// @ts-ignore
import startPng from '../../images/start.png';
// @ts-ignore
import rightPng from '../../images/right.png';

export const startNodeConfig: Node.Config = {
  inherit: 'rect',
  width: 80,
  height: 40,
  markup: [
    {
      tagName: 'rect',
      selector: 'body',
      style: {
        cursor: 'pointer',
      },
    },
    {
      tagName: 'image',
      selector: 'img',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'text',
      selector: 'label',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'image',
      selector: 'arrow',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
  ],
  attrs: {
    //节点样式数据
    body: {
      //节点主体背景
      rx: 20,
      ry: 20,
      fill: '#333',
      stroke: '#333',
      strokeWidth: 1,
      filter: {
        name: 'dropShadow',
        args: { color: 'rgba(0, 0, 0, 0.3)', dx: 0, dy: 2, blur: 10, opacity: 0.5 },
      },
      event: 'node:start-node',
    },
    text: {
      text: '发起',
      fill: '#fff',
      FontSize: 12,
      x: 3,
      y: 0,
    },
    img: {
      'xlink:href': startPng,
      width: 16,
      height: 16,
      x: 12,
      y: 12,
    },
    arrow: {
      'xlink:href': rightPng,
      width: 14,
      height: 14,
      x: 54,
      y: 12.5,
    },
  },
};

// const StartNode = () => {
//   // const { text } = props.node.getData();

//   return (
//     <div className="start-container">
//       <div>发起</div>
//     </div>
//   );
// };
// export default StartNode;
