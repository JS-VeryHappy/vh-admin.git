import type { Node } from '@antv/x6';
// @ts-ignore
import right1Png from '../../images/right1.png';
// @ts-ignore
import examinePng from '../../images/examine.png';
// @ts-ignore
import deletePng from '../../images/delete.png';

export const examineNodeConfig: Node.Config = {
  inherit: 'rect',
  width: 200,
  height: 60,
  label: '审批节点',
  markup: [
    {
      tagName: 'rect',
      selector: 'body', // 选择器
      style: {
        cursor: 'pointer',
      },
    },
    {
      tagName: 'rect',
      selector: 'box', // 选择器
      style: {
        width: 180,
        height: 22,
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'image',
      selector: 'img',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'image',
      selector: 'right',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'text',
      selector: 'label',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'text',
      selector: 'name',
      style: {
        cursor: 'pointer',
        pointerEvents: 'none',
      },
    },
    {
      tagName: 'image',
      selector: 'delete',
      style: {
        cursor: 'pointer',
      },
    },
  ],
  attrs: {
    body: {
      rx: 5,
      ry: 5,
      fill: '#fff',
      strokeWidth: 1,
      stroke: '#fff',
      filter: {
        name: 'dropShadow',
        args: { color: 'rgba(0, 0, 0, 0.3)', dx: 0, dy: 2, blur: 10, opacity: 0.5 },
      },
      event: 'node:examine-node',
      x: -25,
      y: -10,
    },
    text: {
      refX: 0,
      refY: 18,
      fontSize: 12,
      strokeWidth: 0.6,
      stroke: '#333',
      x: 8,
      y: -10,
      textAnchor: 'start',
      textWrap: {
        width: 150,
        height: 20,
        ellipsis: true,
      },
    },
    img: {
      refX: 10,
      refY: 7,
      'xlink:href': examinePng,
      width: 20,
      height: 20,
      x: -25,
      y: -10,
    },
    box: {
      rx: 5,
      ry: 5,
      refX: 10,
      refY: 34,
      fill: '#eee',
      strokeWidth: 1,
      stroke: '#eee',
      x: -25,
      y: -10,
    },
    right: {
      refX: 172,
      refY: 38,
      'xlink:href': right1Png,
      width: 15,
      height: 15,
      x: -25,
      y: -10,
    },
    name: {
      refX: -10,
      refY: 46,
      fontSize: 12,
      strokeWidth: 0.2,
      stroke: '#999',
      textAnchor: 'start',
      textWrap: {
        width: 160,
        height: 20,
        ellipsis: true,
      },
      x: 0,
      y: -10,
    },
    delete: {
      refX: 180,
      refY: 10,
      'xlink:href': deletePng,
      width: 12,
      height: 12,
      event: 'node:examine-node-delete',
      x: -25,
      y: -10,
    },
  },
};
