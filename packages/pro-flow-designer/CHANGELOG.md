# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.1.10](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.9...@vh-admin/pro-flow-designer@0.1.10) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([32b1222](https://gitee.com/JS-VeryHappy/vh-admin/commit/32b1222f18f796660ca3d8785d198636fb2f9bc5))



### [0.1.9](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.8...@vh-admin/pro-flow-designer@0.1.9) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([4eaad85](https://gitee.com/JS-VeryHappy/vh-admin/commit/4eaad856bac78ee28f348c001a997e30ee3bfd37))
* **all:**  使用阿里云仓库 ([d7a734e](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7a734ea08b9abefc84c652a9091d091d67b5d02))



### [0.1.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.7...@vh-admin/pro-flow-designer@0.1.8) (2024-06-14)


###   🐛 Bug Fixes | Bug 修复

* **流程配置:**  优化流程配置 ([72af65e](https://gitee.com/JS-VeryHappy/vh-admin/commit/72af65e91632902d23c7e2ceb9ae76883d30269e))
* **流程配置:**  优化提示 ([8676145](https://gitee.com/JS-VeryHappy/vh-admin/commit/8676145f70ed67dfbea6dbb31b3c45422856f67d))



### [0.1.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.6...@vh-admin/pro-flow-designer@0.1.7) (2024-05-21)


###   🐛 Bug Fixes | Bug 修复

* **流程配置:**  基本审批流程配置 ([bd39555](https://gitee.com/JS-VeryHappy/vh-admin/commit/bd39555ed347b02f72f3299880c9166033d8e166))



### [0.1.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.5...@vh-admin/pro-flow-designer@0.1.6) (2024-05-06)


###   🐛 Bug Fixes | Bug 修复

* **流程配置:**  基本审批流程配置 ([ac03946](https://gitee.com/JS-VeryHappy/vh-admin/commit/ac0394678b1bd4ffedbb7944ee2444ec37350c94))
* **流程配置:**  基本审批流程配置 ([fa31190](https://gitee.com/JS-VeryHappy/vh-admin/commit/fa31190b41253e9a80158efe0afc577325eb2b5a))
* **流程配置:**  基本审批流程配置 ([ef5eab3](https://gitee.com/JS-VeryHappy/vh-admin/commit/ef5eab36cbce8b2b7682412ae1442de8e4870eb3))
* **流程配置:**  基本审批流程配置 ([a19fe78](https://gitee.com/JS-VeryHappy/vh-admin/commit/a19fe782c2619ad0f276e2296e79f505f38dd0cf))



### [0.1.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.4...@vh-admin/pro-flow-designer@0.1.5) (2023-10-20)


###   ✨ Features | 新功能

* **流程:** 更新流程组件 ([1fad0d6](https://gitee.com/JS-VeryHappy/vh-admin/commit/1fad0d67d7ce438405faeb63083199f6d010c2a3))
* **流程:** 更新流程组件 ([393b9b2](https://gitee.com/JS-VeryHappy/vh-admin/commit/393b9b226c685b6cd6171c620e4e97f2ae1d72ff))
* **d\表格:** 表格分页优化 ([633aac1](https://gitee.com/JS-VeryHappy/vh-admin/commit/633aac1e7b3f52dec47dd7947d1e61daa3fc58d5))



### [0.1.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.3...@vh-admin/pro-flow-designer@0.1.4) (2023-10-16)


###   ✨ Features | 新功能

* **流程:** 更新流程组件 ([c25fb53](https://gitee.com/JS-VeryHappy/vh-admin/commit/c25fb53c09b223b38c68e452b8451edb116bc7d6))
* **流程:** 更新流程组件 ([b1343ba](https://gitee.com/JS-VeryHappy/vh-admin/commit/b1343ba8ea7b5c7b196bbac1d7fc6efe85294793))



### [0.1.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.2...@vh-admin/pro-flow-designer@0.1.3) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([af158f0](https://gitee.com/JS-VeryHappy/vh-admin/commit/af158f059da0dc13e939e41d5c96b573b8a9fd22))



### [0.1.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.1.1...@vh-admin/pro-flow-designer@0.1.2) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([e005fa3](https://gitee.com/JS-VeryHappy/vh-admin/commit/e005fa347e2fbd0b0e064b125882c2e7a6355362))



### [0.1.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.0.9...@vh-admin/pro-flow-designer@0.1.1) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3f70312](https://gitee.com/JS-VeryHappy/vh-admin/commit/3f70312823ae2ea54d111f4ce517adbce5291c2c))



### [0.0.9](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.0.7...@vh-admin/pro-flow-designer@0.0.9) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3dfb77a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3dfb77af0ee96789a4aac9b4d6e36c5f83e93110))



### [0.0.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.0.6...@vh-admin/pro-flow-designer@0.0.7) (2023-05-25)


###   ✨ Features | 新功能

* **流程设计器:** 创建 ([6d0582a](https://gitee.com/JS-VeryHappy/vh-admin/commit/6d0582a149b7cfa70ea653806a56a640eaec1661))




### [0.0.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.0.5...@vh-admin/pro-flow-designer@0.0.6) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([14ed9a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/14ed9a6012042164a7d8255148979079b89c8f77))




### [0.0.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.0.4...@vh-admin/pro-flow-designer@0.0.5) (2023-04-19)


###   ✨ Features | 新功能

* **表单:**  优化 ([5d0158c](https://gitee.com/JS-VeryHappy/vh-admin/commit/5d0158cc04a37ad7056ee7bbe696d95aa56c8d5e))



### [0.0.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.0.3...@vh-admin/pro-flow-designer@0.0.4) (2023-04-19)

**Note:** Version bump only for package @vh-admin/pro-flow-designer





### [0.0.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-flow-designer@0.0.2...@vh-admin/pro-flow-designer@0.0.3) (2023-04-19)

**Note:** Version bump only for package @vh-admin/pro-flow-designer





### 0.0.2 (2023-04-18)


###   ✨ Features | 新功能

* **表单设计器:**  创建 ([47f74b9](https://gitee.com/JS-VeryHappy/vh-admin/commit/47f74b9812fa5d58da56e83f21d882d3c257dd77))
* **通用:**  增加配置导入方法 ([f1d643b](https://gitee.com/JS-VeryHappy/vh-admin/commit/f1d643b60217243437a0d1385e11b3d305ada419))
* **通用:**  增加配置导入方法 ([824c19c](https://gitee.com/JS-VeryHappy/vh-admin/commit/824c19ccf5a252f319ab4f67baf673f53933fe53))
