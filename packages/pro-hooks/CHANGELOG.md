# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.1.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.1.4...@vh-admin/pro-hooks@0.1.5) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([32b1222](https://gitee.com/JS-VeryHappy/vh-admin/commit/32b1222f18f796660ca3d8785d198636fb2f9bc5))



### [0.1.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.1.3...@vh-admin/pro-hooks@0.1.4) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([4eaad85](https://gitee.com/JS-VeryHappy/vh-admin/commit/4eaad856bac78ee28f348c001a997e30ee3bfd37))
* **all:**  使用阿里云仓库 ([d7a734e](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7a734ea08b9abefc84c652a9091d091d67b5d02))



### [0.1.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.1.2...@vh-admin/pro-hooks@0.1.3) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([af158f0](https://gitee.com/JS-VeryHappy/vh-admin/commit/af158f059da0dc13e939e41d5c96b573b8a9fd22))



### [0.1.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.1.1...@vh-admin/pro-hooks@0.1.2) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([e005fa3](https://gitee.com/JS-VeryHappy/vh-admin/commit/e005fa347e2fbd0b0e064b125882c2e7a6355362))



### [0.1.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.24...@vh-admin/pro-hooks@0.1.1) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3f70312](https://gitee.com/JS-VeryHappy/vh-admin/commit/3f70312823ae2ea54d111f4ce517adbce5291c2c))



### [0.0.24](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.22...@vh-admin/pro-hooks@0.0.24) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3dfb77a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3dfb77af0ee96789a4aac9b4d6e36c5f83e93110))




### [0.0.22](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.21...@vh-admin/pro-hooks@0.0.22) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([14ed9a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/14ed9a6012042164a7d8255148979079b89c8f77))




### [0.0.21](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.20...@vh-admin/pro-hooks@0.0.21) (2023-04-11)

**Note:** Version bump only for package @vh-admin/pro-hooks






### [0.0.20](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.19...@vh-admin/pro-hooks@0.0.20) (2022-11-22)

**Note:** Version bump only for package @vh-admin/pro-hooks

### [0.0.19](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.18...@vh-admin/pro-hooks@0.0.19) (2022-11-16)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([546b9da](https://gitee.com/JS-VeryHappy/vh-admin/commit/546b9daecdf31aeb4244db821445ae7a02ee57f9))
- **表单:** dumi2 升级 ([8d94a4a](https://gitee.com/JS-VeryHappy/vh-admin/commit/8d94a4ac978d7458e4fc76c5197e309e9a518c9d))

### [0.0.18](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.17...@vh-admin/pro-hooks@0.0.18) (2022-05-25)

### 🐛 Bug Fixes | Bug 修复

- **tree:** 修复无法选中值 ([41441e7](https://gitee.com/JS-VeryHappy/vh-admin/commit/41441e78e55331f684866d402faa07e1dfcf3546))

### [0.0.17](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.16...@vh-admin/pro-hooks@0.0.17) (2022-05-25)

**Note:** Version bump only for package @vh-admin/pro-hooks

### [0.0.16](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.15...@vh-admin/pro-hooks@0.0.16) (2022-05-19)

### ⚡ Performance Improvements | 性能优化

- **升级 react:** 1.8.0 ([a75bd60](https://gitee.com/JS-VeryHappy/vh-admin/commit/a75bd6039afcc06fcd993e6733a60bf2b7aa07ed))

### [0.0.15](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.14...@vh-admin/pro-hooks@0.0.15) (2022-05-09)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 修改 package 依赖方式 ([eeca8ac](https://gitee.com/JS-VeryHappy/vh-admin/commit/eeca8ac852c1ca415d85b7159effc17d4a04fe8b))

### [0.0.14](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.13...@vh-admin/pro-hooks@0.0.14) (2022-04-21)

### 🐛 Bug Fixes | Bug 修复

- **表格内置组件:** select 的值无法传递 ([2a4649c](https://gitee.com/JS-VeryHappy/vh-admin/commit/2a4649cb8cb46ecaca4a6eb00555530e7286bedf))
- **表格内置组件:** select 的值无法传递 ([9aca42a](https://gitee.com/JS-VeryHappy/vh-admin/commit/9aca42a4adb868ab9ffa5f6d4916472c141512f6))
- **组件:** select 选择自动关闭 ([77f99a7](https://gitee.com/JS-VeryHappy/vh-admin/commit/77f99a7d31164b7f02b06f0177a2060946d51a3a))

### 🎫 Chores | 其他更新

- **合并:** branch 'review' ([e4b26dc](https://gitee.com/JS-VeryHappy/vh-admin/commit/e4b26dc4f18d64cf6b04b4271b74b017e7842e32))
- **Merge:** branch 'review' ([a6764e1](https://gitee.com/JS-VeryHappy/vh-admin/commit/a6764e125d5b65c7f9274cf3a5b02cde88d8af64))

## [0.0.13](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-hooks@0.0.12...@vh-admin/pro-hooks@0.0.13) (2022-04-10)

**Note:** Version bump only for package @vh-admin/pro-hooks

## [0.0.12](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-hooks@0.0.11...@vh-admin/pro-hooks@0.0.12) (2022-04-02)

**Note:** Version bump only for package @vh-admin/pro-hooks

## [0.0.11](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-hooks@0.0.10...@vh-admin/pro-hooks@0.0.11) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-hooks

## [0.0.10](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-hooks@0.0.9...@vh-admin/pro-hooks@0.0.10) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-hooks

## [0.0.9](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-hooks@0.0.8...@vh-admin/pro-hooks@0.0.9) (2022-03-18)

**Note:** Version bump only for package @vh-admin/pro-hooks

## [0.0.8](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-hooks@0.0.7...@vh-admin/pro-hooks@0.0.8) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-hooks

## [0.0.7](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-hooks@0.0.3...@vh-admin/pro-hooks@0.0.7) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-hooks

## [0.0.3](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-hooks@0.0.2...@vh-admin/pro-hooks@0.0.3) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-hooks
