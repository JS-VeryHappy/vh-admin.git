# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.1.9](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.8...@vh-admin/pro-utils@0.1.9) (2024-10-12)


###   🐛 Bug Fixes | Bug 修复

* **pro-utils:**  deepGet函数错误 ([5f168f2](https://gitee.com/JS-VeryHappy/vh-admin/commit/5f168f2d0868853a1d1d55ad1b6d719b95952692))



### [0.1.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.7...@vh-admin/pro-utils@0.1.8) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([32b1222](https://gitee.com/JS-VeryHappy/vh-admin/commit/32b1222f18f796660ca3d8785d198636fb2f9bc5))



### [0.1.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.6...@vh-admin/pro-utils@0.1.7) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([4eaad85](https://gitee.com/JS-VeryHappy/vh-admin/commit/4eaad856bac78ee28f348c001a997e30ee3bfd37))
* **all:**  使用阿里云仓库 ([d7a734e](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7a734ea08b9abefc84c652a9091d091d67b5d02))



### [0.1.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.5...@vh-admin/pro-utils@0.1.6) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **pro-utils:**  增加deepGet方法 ([e290794](https://gitee.com/JS-VeryHappy/vh-admin/commit/e290794d771071843a9f6f2d64680ac8e4d02802))



### [0.1.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.4...@vh-admin/pro-utils@0.1.5) (2024-07-22)


###   🐛 Bug Fixes | Bug 修复

* **pro-utils:**  增加copy方法 ([ae64b1b](https://gitee.com/JS-VeryHappy/vh-admin/commit/ae64b1b2e6beb8252e4ad76660d5082cea6beba9))
* **pro-utils:**  增加copy方法 ([9e7d989](https://gitee.com/JS-VeryHappy/vh-admin/commit/9e7d989230efc0bd59f451ca62634a2d79305128))



### [0.1.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.3...@vh-admin/pro-utils@0.1.4) (2023-07-14)


###   ✨ Features | 新功能

* **优化:** 表格按钮tips的间隔 ([a33951d](https://gitee.com/JS-VeryHappy/vh-admin/commit/a33951d1f9b83a014b6f26d903db918a63fb8450))
* **优化:** 表格按钮tips的间隔 ([89ed423](https://gitee.com/JS-VeryHappy/vh-admin/commit/89ed42369f9d6819d0e8b3901cf04e0fa0ce0bb3))



### [0.1.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.2...@vh-admin/pro-utils@0.1.3) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([af158f0](https://gitee.com/JS-VeryHappy/vh-admin/commit/af158f059da0dc13e939e41d5c96b573b8a9fd22))



### [0.1.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.1.1...@vh-admin/pro-utils@0.1.2) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([e005fa3](https://gitee.com/JS-VeryHappy/vh-admin/commit/e005fa347e2fbd0b0e064b125882c2e7a6355362))



### [0.1.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.30...@vh-admin/pro-utils@0.1.1) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3f70312](https://gitee.com/JS-VeryHappy/vh-admin/commit/3f70312823ae2ea54d111f4ce517adbce5291c2c))



### [0.0.30](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.28...@vh-admin/pro-utils@0.0.30) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([51d9729](https://gitee.com/JS-VeryHappy/vh-admin/commit/51d97292ba87ccec6e289667b97ee0b241532e72))



### [0.0.28](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.26...@vh-admin/pro-utils@0.0.28) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3dfb77a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3dfb77af0ee96789a4aac9b4d6e36c5f83e93110))




### [0.0.26](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.25...@vh-admin/pro-utils@0.0.26) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([14ed9a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/14ed9a6012042164a7d8255148979079b89c8f77))




### [0.0.25](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.24...@vh-admin/pro-utils@0.0.25) (2023-04-18)


###   ✨ Features | 新功能

* **表单编辑:**  增加组件 ([74ca0e0](https://gitee.com/JS-VeryHappy/vh-admin/commit/74ca0e0e11d151e9c75e18bc9f614d66f101892c))
* 表单组件InputNumberSelect ([b9d8af7](https://gitee.com/JS-VeryHappy/vh-admin/commit/b9d8af75319e2bbb1b76b6132ea80cad3e80703a))




### [0.0.24](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.23...@vh-admin/pro-utils@0.0.24) (2023-04-11)


###   ✨ Features | 新功能

* **表单:**  treeCheckable 无法清空 ([915e3c3](https://gitee.com/JS-VeryHappy/vh-admin/commit/915e3c3cd42ce9049034893bcd457fd0b977d7aa))




### [0.0.23](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.22...@vh-admin/pro-utils@0.0.23) (2023-03-10)

### ✨ Features | 新功能

- **表单:** 升级 icon 依赖 ([11562b8](https://gitee.com/JS-VeryHappy/vh-admin/commit/11562b8197ebce1c604f0e40e302faedb69f4b5e))

### [0.0.22](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.21...@vh-admin/pro-utils@0.0.22) (2022-11-22)

**Note:** Version bump only for package @vh-admin/pro-utils

### [0.0.21](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.20...@vh-admin/pro-utils@0.0.21) (2022-11-16)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([6a5503d](https://gitee.com/JS-VeryHappy/vh-admin/commit/6a5503d7be79eec098f21aabb0fc90137177d232))
- **表单:** dumi2 升级 ([8d94a4a](https://gitee.com/JS-VeryHappy/vh-admin/commit/8d94a4ac978d7458e4fc76c5197e309e9a518c9d))

### [0.0.20](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.19...@vh-admin/pro-utils@0.0.20) (2022-09-29)

### ✨ Features | 新功能

- **表单:** 移动端打开支持 ([d7f265b](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7f265b859e66f2caffc7007123f77752621e224))

### [0.0.19](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.18...@vh-admin/pro-utils@0.0.19) (2022-08-31)

### ✨ Features | 新功能

- **请求:** 下载错误 ([ea84256](https://gitee.com/JS-VeryHappy/vh-admin/commit/ea84256af32dd1a2a4544d4f574ae37e1288faab))

### [0.0.18](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.17...@vh-admin/pro-utils@0.0.18) (2022-05-25)

### 🐛 Bug Fixes | Bug 修复

- **tree:** 修复无法选中值 ([41441e7](https://gitee.com/JS-VeryHappy/vh-admin/commit/41441e78e55331f684866d402faa07e1dfcf3546))

### [0.0.17](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.16...@vh-admin/pro-utils@0.0.17) (2022-05-25)

**Note:** Version bump only for package @vh-admin/pro-utils

### [0.0.16](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.15...@vh-admin/pro-utils@0.0.16) (2022-05-19)

### ⚡ Performance Improvements | 性能优化

- **升级 react:** 1.8.0 ([a75bd60](https://gitee.com/JS-VeryHappy/vh-admin/commit/a75bd6039afcc06fcd993e6733a60bf2b7aa07ed))

### [0.0.15](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.14...@vh-admin/pro-utils@0.0.15) (2022-05-09)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 修改 package 依赖方式 ([eeca8ac](https://gitee.com/JS-VeryHappy/vh-admin/commit/eeca8ac852c1ca415d85b7159effc17d4a04fe8b))

### [0.0.14](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.13...@vh-admin/pro-utils@0.0.14) (2022-04-21)

### ✨ Features | 新功能

- **组件:** 全局项目选择器 ([92124fa](https://gitee.com/JS-VeryHappy/vh-admin/commit/92124fa2602b56615fad15b8ff9394ffdba70917))

### [0.0.13](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.12...@vh-admin/pro-utils@0.0.13) (2022-04-21)

### ✨ Features | 新功能

- **组件:** 全局项目选择器 ([4c8277f](https://gitee.com/JS-VeryHappy/vh-admin/commit/4c8277f0719d17a511d02ae74d6afbc472584e49))

### 🎫 Chores | 其他更新

- **合并:** branch 'review' ([e4b26dc](https://gitee.com/JS-VeryHappy/vh-admin/commit/e4b26dc4f18d64cf6b04b4271b74b017e7842e32))
- **Merge:** branch 'review' ([a6764e1](https://gitee.com/JS-VeryHappy/vh-admin/commit/a6764e125d5b65c7f9274cf3a5b02cde88d8af64))

## [0.0.12](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-utils@0.0.11...@vh-admin/pro-utils@0.0.12) (2022-04-10)

**Note:** Version bump only for package @vh-admin/pro-utils

## [0.0.11](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-utils@0.0.10...@vh-admin/pro-utils@0.0.11) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-utils

## [0.0.10](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-utils@0.0.9...@vh-admin/pro-utils@0.0.10) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-utils

## [0.0.9](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-utils@0.0.8...@vh-admin/pro-utils@0.0.9) (2022-03-18)

**Note:** Version bump only for package @vh-admin/pro-utils

## [0.0.8](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-utils@0.0.7...@vh-admin/pro-utils@0.0.8) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-utils

## [0.0.7](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-utils@0.0.3...@vh-admin/pro-utils@0.0.7) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-utils

## [0.0.3](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-utils@0.0.2...@vh-admin/pro-utils@0.0.3) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-utils
