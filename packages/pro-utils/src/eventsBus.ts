/**
 * 单页面事件处理
 */
const eventsBusFn = () => {
  // @ts-ignore
  if (!window.vhAdmin) {
    // @ts-ignore
    window.vhAdmin = {};
  }

  // @ts-ignore
  if (!window.vhAdmin.eventsBus) {
    // @ts-ignore
    window.vhAdmin.eventsBus = {};
  }

  return {
    // @ts-ignore
    $bus: window.vhAdmin.eventsBus,
    $on(eventName: string, event: any) {
      this.$addEventListener(eventName, event);
    },
    $off(eventName: string) {
      this.$removeEventListener(eventName);
    },

    $emit(eventName: string, data: any) {
      const events = this.$bus[eventName];
      if (!events) {
        return;
      }
      events.forEach((event: any) => {
        event.event(data);
        if (event.eventType === 'once') {
          this.$removeEventListener(eventName);
        }
      });
    },
    $has(eventName: string) {
      return this.$bus[eventName];
    },
    $addEventListener(eventName: string, event: () => void) {
      // 绑定监听事件
      if (!this.$bus[eventName]) {
        this.$bus[eventName] = [];
      }
      this.$bus[eventName].push({
        eventName,
        event,
      });
    },
    $removeEventListener(eventName: string) {
      if (this.$bus[eventName]) {
        delete this.$bus[eventName];
      }
    },
    $postMessage(eventName: string, data: any) {
      if (!window.opener) {
        return;
      }
      window.opener.postMessage(
        {
          type: eventName,
          data,
        },
        window.location.origin,
      );
    },
    $onMessage(onMessage?: (data: any) => any, eventName?: string, event?: any) {
      const { data, origin } = event;
      if (data && data.type === eventName && window.location.origin === origin && onMessage) {
        onMessage(data.data);
      }
    },
    $onPostMessage(eventName: string, onMessage: (data: any) => any) {
      window.addEventListener('message', this.$onMessage.bind(this, onMessage, eventName));
    },
    $offPostMessage() {
      window.removeEventListener(
        'message',
        this.$onMessage.bind(this, () => {}, ''),
      );
    },
  };
};

export default eventsBusFn;
