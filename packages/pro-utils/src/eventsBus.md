---
title: eventsBus
order: 3
toc: content
nav:
  order: 4
  title: 方法
  path: /utils
group:
  title: eventsBus
  path: /eventsBus
  order: 2
---

## eventsBus 单页面事件传递方法

```jsx
/**
 * title: 基础使用
 */
import React, { useEffect } from 'react';
import { eventsBusFn } from './index.ts';
import { Button } from 'antd';
const eventsBus = eventsBusFn();

export default () => {
  useEffect(() => {
    eventsBus.$on('setPageContainerConfig', (data) => {
      console.log(data);
    });
    return () => {
      eventsBus.$off('setPageContainerConfig');
    };
  }, []);

  const showModal = () => {
    eventsBus.$emit('setPageContainerConfig', {
      content: '欢迎使用 ProLayout 组件',
      tabList: [
        {
          tab: '基本信息',
          key: 'base',
        },
        {
          tab: '详细信息',
          key: 'info',
        },
      ],
      onTabChange: (key) => {
        console.log('====================================');
        console.log(key);
        console.log('====================================');
      },
    });
  };
  return (
    <>
      <Button onClick={showModal}>发送事件</Button>
    </>
  );
};
```


```jsx
/**
 * title: 多窗口传递时间，需要在两个窗口中验证，注意目前只能在window.open的打开的窗口中使用
 */
import React, { useEffect } from 'react';
import { eventsBusFn } from './index.ts';
import { Button } from 'antd';
const eventsBus = eventsBusFn();

export default () => {
  useEffect(() => {
    eventsBus.$onPostMessage('message1', (data) => {
      console.log(data,'测试');
    });
    return () => {
      eventsBus.$offPostMessage('message1');
    };
  }, []);

  const showModal = () => {
    eventsBus.$postMessage('message1', {
      content: '欢迎使用 ProLayout 组件',
    });
  };

  const open = ()=>{
    window.open('/pro-utils/events-bus')
  }
  return (
    <>
      <Button onClick={open}>打开新窗口</Button>
      <Button onClick={showModal}>发送事件</Button>
    </>
  );
};
```
