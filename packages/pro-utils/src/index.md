---
title: 常用方法
order: 2
toc: content
nav:
  order: 4
  title: 方法
  path: /utils
group:
  title: 常用方法
  path: /index
  order: 2
---


## 复制文案到剪切板

```js
// 调用
import { copy } from '@vh-admin/pro-utils';
copy('www.baidu.com');
```

## 是否是 url

```js
// 调用
import { isUrl } from '@vh-admin/pro-utils';
isUrl('www.baidu.com');
```

## 延迟函数

```js
// 调用
import { waitTime } from '@vh-admin/pro-utils';
waitTime(200);
```

## 请求防抖

```js
// 调用
import { requestDebounce } from '@vh-admin/pro-utils';
import { proTableAddRow } from '@/services';

const debounceProTableAddRow: any = requestDebounce(proTableAddRow, 500);

debounceProTableAddRow();
```

## 防抖

```js
// 调用
import { debounce } from '@vh-admin/pro-utils';
import { proTableAddRow } from '@/services';

const debounceProTableAddRow: any = debounce(proTableAddRow, 500);

debounceProTableAddRow();
```

## 节流

```js
// 调用
import { throttle } from '@vh-admin/pro-utils';
import { proTableAddRow } from '@/services';

const throttleProTableAddRow: any = throttle(proTableAddRow, 500);

throttleProTableAddRow();
```

## 下载文件

```js
// 调用
import { downloadFile } from '@vh-admin/pro-utils';

downloadFile('a', {
  url: '/api/XXX',
  params: {},
});

// post 下载本地后下载
return downloadFile('blob', {
  data: res.data, // 二进制数据
  headers: {
    'content-disposition': res.headers['content-disposition'],
  },
});
```

## 对象拼接 url 参数

```js
// 调用返回完整url
import { convertObjToUrl } from '@vh-admin/pro-utils';
convertObjToUrl(url, params);
```

## [IconFont](https://ant.design/components/icon-cn/#components-icon-demo-iconfont) 公用使用导出

```js
import { IconFont } from '@vh-admin/pro-utils';

<IconFont type="icon-xiaoxi" />;
```

## 递归深度拷贝

```js
import { deepCopy } from '@vh-admin/pro-utils';
```

## 递归深度合并

```js
import { deepMerge } from '@vh-admin/pro-utils';
```

## 生成唯一随机值

```js
import { uuid } from '@vh-admin/pro-utils';
```

## 递归找到 tree 数据的指定值的节点 返回节点信息

```js
import { recursionTreeData } from '@vh-admin/pro-utils';
const trrData = [
  {
    label: '1',
    value: 1,
    parentId: 0,
    children: [
      {
        label: '11',
        value: 11,
        parentId: 1,
      },
    ],
  },
];
recursionTreeData(trrData, 1, {
  label: 'label',
  value: 'value',
  children: 'children',
  parentId: 'parentId',
});
//返回
{
  label: '1',
  value: 1,
  parentId: 0,
}
```

## 递归找到 tree 数据的指定值的节点 返回节点的顶层树

```js
import { recursionTreeTop } from '@vh-admin/pro-utils';
const trrData = [
  {
    label: '1',
    value: 1,
    parentId: 0,
    children: [
      {
        label: '11',
        value: 11,
        parentId: 1,
      },
    ],
  },
];
recursionTreeTop(trrData, 11, {
  label: 'label',
  value: 'value',
  children: 'children',
  parentId: 'parentId',
});
//返回
{
    label: '1',
    value: 1,
    parentId: 0,
    children: [
      {
        label: '11',
        value: 11,
        parentId: 0,
      },
    ],
  }
```

## 递归找到 tree 指定值，返回的找到有节点数据值

```js
import { loopTreeData } from '@vh-admin/pro-utils';
const trrData = [
  {
    label: '1',
    value: 1,
    parentId: 0,
    children: [
      {
        label: '11',
        value: 11,
        parentId: 1,
      },
    ],
  },
];
loopTreeData(trrData, 1, {
  label: 'label',
  value: 'value',
  children: 'children',
  parentId: 'parentId',
});

//返回
[11];
```

## 递归找到 tree 所有节点数据值

```js
import { loopTreeChildren } from '@vh-admin/pro-utils';
const trrData = [
  {
    label: '1',
    value: 1,
    parentId: 0,
    children: [
      {
        label: '11',
        value: 11,
        parentId: 1,
      },
    ],
  },
];
loopTreeChildren(trrData, {
  label: 'label',
  value: 'value',
  children: 'children',
  parentId: 'parentId',
});

//返回
[1, 11];
```

## 递归找到 tree 指定值所有节点数据值返回节点信息

```js
import { loopTreeDataAllNode } from '@vh-admin/pro-utils';
const trrData = [
  {
    label: '1',
    value: 1,
    parentId: 0,
    children: [
      {
        label: '11',
        value: 11,
        parentId: 1,
      },
    ],
  },
];
loopTreeDataAllNode(trrData,0,{
  value: 'parentId',
  children: 'children',
  parentId: 'parentId',
});

//返回
[{
    label: '1',
    value: 1,
    parentId: 0,
    children: [
     ...
    ],
  }, {
        label: '11',
        value: 11,
        parentId: 1,
      }];
```

## 是否是移动端

```js
import { isMobile } from '@vh-admin/pro-utils';
isMobile();
```



## 多层取值方法

```js
import { deepGet } from '@vh-admin/pro-utils';
const obj = {
  a: [
    {
      b: {
        c: 3,
      },
    },
  ],
  e: {
    f: 1,
  },
};

console.log(deepGet(obj, 'e.f')); // 1
console.log(deepGet(obj, ['e','f'])) // 1
console.log(deepGet(obj, 'a.x')); // undefined
console.log(deepGet(obj, 'a.x', '--')) // -- 
console.log(deepGet(obj, 'a[0].b.c')) // 3
console.log(deepGet(obj, ['a', 0, 'b', ,'c'])) // 3

```