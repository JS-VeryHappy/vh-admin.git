import { isUrl, convertObjToUrl, deepCopy, uuid, deepMerge } from '../index';

// 图片地址测试
describe('isUrl 测试', () => {
  it('should return false for invalid and corner case inputs', () => {
    //对于他无效或者极端的输入应该返回false
    expect(isUrl([] as any)).toBeFalsy();
    expect(isUrl({} as any)).toBeFalsy();
    expect(isUrl(false as any)).toBeFalsy();
    expect(isUrl(true as any)).toBeFalsy();
    expect(isUrl(NaN as any)).toBeFalsy();
    expect(isUrl(null as any)).toBeFalsy();
    expect(isUrl(undefined as any)).toBeFalsy();
    expect(isUrl('')).toBeFalsy();
  });

  it('should return false for invalid URLs', () => {
    //对于无效的url返回false
    expect(isUrl('foo')).toBeFalsy();
    expect(isUrl('bar')).toBeFalsy();
    expect(isUrl('bar/test')).toBeFalsy();
    expect(isUrl('http:/example.com/')).toBeFalsy();
    expect(isUrl('ttp://example.com/')).toBeFalsy();
  });

  it('should return true for valid URLs', () => {
    //对于有效的url返回true
    expect(isUrl('http://example.com/')).toBeTruthy();
    expect(isUrl('https://example.com/')).toBeTruthy();
    expect(isUrl('http://example.com/test/123')).toBeTruthy();
    expect(isUrl('https://example.com/test/123')).toBeTruthy();
    expect(isUrl('http://example.com/test/123?foo=bar')).toBeTruthy();
    expect(isUrl('https://example.com/test/123?foo=bar')).toBeTruthy();
    expect(isUrl('http://www.example.com/')).toBeTruthy();
    expect(isUrl('https://www.example.com/')).toBeTruthy();
    expect(isUrl('http://www.example.com/test/123')).toBeTruthy();
    expect(isUrl('https://www.example.com/test/123')).toBeTruthy();
    expect(isUrl('http://www.example.com/test/123?foo=bar')).toBeTruthy();
    expect(isUrl('https://www.example.com/test/123?foo=bar')).toBeTruthy();
  });
});

// 延迟函数
describe('convertObjToUrl 测试', () => {
  it('验证成功返回true', () => {
    expect(convertObjToUrl('http://example.com/', { name: 'xm', age: 18 })).toBeTruthy();
  });
});

//递归深度拷贝
describe('deepCopy 测试', () => {
  const arr = [11, 22, 33];
  it('开始测试deepCopy返回的数组里包不包含11', () => {
    expect(deepCopy(arr)).toContain(11);
  });
});

// 生成唯一随机值
describe('uuid 测试', () => {
  it('uuid得到的两个随机是是否相等测试', () => {
    const a = uuid();
    const b = uuid();
    expect(a !== b).toBeTruthy();
    expect(a === b).toBeFalsy();
  });
});

// 递归深度合并
describe('deepMerge 测试', () => {
  it('deepMerge得到的值是否包含传入的值', () => {
    expect(deepMerge([11, 22, 33], [33, 44])).toContain(44);
    expect(deepMerge({ name: 'xiaoming', age: 15 }, { 身高: '168cm' })).toMatchObject({ age: 15 });
  });
});
