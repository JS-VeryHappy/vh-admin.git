# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.1.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.1.4...@vh-admin/pro-form-designer@0.1.5) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([32b1222](https://gitee.com/JS-VeryHappy/vh-admin/commit/32b1222f18f796660ca3d8785d198636fb2f9bc5))



### [0.1.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.1.3...@vh-admin/pro-form-designer@0.1.4) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([4eaad85](https://gitee.com/JS-VeryHappy/vh-admin/commit/4eaad856bac78ee28f348c001a997e30ee3bfd37))
* **all:**  使用阿里云仓库 ([d7a734e](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7a734ea08b9abefc84c652a9091d091d67b5d02))



### [0.1.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.1.2...@vh-admin/pro-form-designer@0.1.3) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([af158f0](https://gitee.com/JS-VeryHappy/vh-admin/commit/af158f059da0dc13e939e41d5c96b573b8a9fd22))



### [0.1.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.1.1...@vh-admin/pro-form-designer@0.1.2) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([e005fa3](https://gitee.com/JS-VeryHappy/vh-admin/commit/e005fa347e2fbd0b0e064b125882c2e7a6355362))



### [0.1.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.12...@vh-admin/pro-form-designer@0.1.1) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3f70312](https://gitee.com/JS-VeryHappy/vh-admin/commit/3f70312823ae2ea54d111f4ce517adbce5291c2c))



### [0.0.12](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.10...@vh-admin/pro-form-designer@0.0.12) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3dfb77a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3dfb77af0ee96789a4aac9b4d6e36c5f83e93110))




### [0.0.10](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.9...@vh-admin/pro-form-designer@0.0.10) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([14ed9a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/14ed9a6012042164a7d8255148979079b89c8f77))



### [0.0.9](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.8...@vh-admin/pro-form-designer@0.0.9) (2023-05-10)


###   ✨ Features | 新功能

* **表单设计器:** 优化 ([72c5eda](https://gitee.com/JS-VeryHappy/vh-admin/commit/72c5eda760446d0ebc86d496a485d8b0c343eadd))



### [0.0.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.7...@vh-admin/pro-form-designer@0.0.8) (2023-04-27)


###   ✨ Features | 新功能

* **表单:**  编辑表单优化 ([f590c66](https://gitee.com/JS-VeryHappy/vh-admin/commit/f590c66c26ec4a91b47ebd709bcdfd1de544b8b9))
* **表单设计器:** 优化 ([7cf1953](https://gitee.com/JS-VeryHappy/vh-admin/commit/7cf1953ebfcc2fb3395b379a65930d039d0fd04f))
* **表单设计器:** 优化 ([b9ec3da](https://gitee.com/JS-VeryHappy/vh-admin/commit/b9ec3da0e1c529515af7c9899f9b55749a76008d))



### [0.0.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.6...@vh-admin/pro-form-designer@0.0.7) (2023-04-27)


###   ✨ Features | 新功能

* **表单:**  编辑表单优化 ([ad23228](https://gitee.com/JS-VeryHappy/vh-admin/commit/ad23228edeb9e2f6a1be167b94a694341db2dccb))



### [0.0.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.5...@vh-admin/pro-form-designer@0.0.6) (2023-04-26)


###   ✨ Features | 新功能

* **表单:**  编辑表单 ([2007df0](https://gitee.com/JS-VeryHappy/vh-admin/commit/2007df02017968006f38fe047191b05213e14577))




### [0.0.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.4...@vh-admin/pro-form-designer@0.0.5) (2023-04-19)


###   ✨ Features | 新功能

* **表单:**  优化 ([5d0158c](https://gitee.com/JS-VeryHappy/vh-admin/commit/5d0158cc04a37ad7056ee7bbe696d95aa56c8d5e))



### [0.0.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.3...@vh-admin/pro-form-designer@0.0.4) (2023-04-19)

**Note:** Version bump only for package @vh-admin/pro-form-designer





### [0.0.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-form-designer@0.0.2...@vh-admin/pro-form-designer@0.0.3) (2023-04-19)


###   ✨ Features | 新功能

* **表单编辑:**  增加组件 ([01877d2](https://gitee.com/JS-VeryHappy/vh-admin/commit/01877d26e9ceb326e3e5dd2eb776c2ed110d4359))



### 0.0.2 (2023-04-18)


###   ✨ Features | 新功能

* **表单设计器:**  创建 ([b8b2132](https://gitee.com/JS-VeryHappy/vh-admin/commit/b8b2132fd41aac56e70111efef7c4c4b7fbffe02))
* **表单设计器:**  创建 ([5239a0b](https://gitee.com/JS-VeryHappy/vh-admin/commit/5239a0b83434230adad6fe893f6a1f9af34e7481))
* **表单设计器:**  创建 ([599d58e](https://gitee.com/JS-VeryHappy/vh-admin/commit/599d58e5039bd2c4215d62501cc2b42efd2f51f2))
* **表单设计器:**  创建 ([afd4853](https://gitee.com/JS-VeryHappy/vh-admin/commit/afd4853ab0ca74fa7711363cfefd03cd41608881))
* **表单设计器:**  创建 ([1c5c366](https://gitee.com/JS-VeryHappy/vh-admin/commit/1c5c366063b26d59e047d387b38567bcc3183064))
* **表单设计器:**  创建 ([14b0478](https://gitee.com/JS-VeryHappy/vh-admin/commit/14b0478565fa6da69aaabf323d086ea9d04b55e4))
* **表单设计器:**  创建 ([0de550c](https://gitee.com/JS-VeryHappy/vh-admin/commit/0de550c92e7a8b00ae1fd8d43dab71ba7c96517b))
* **表单设计器:**  创建 ([8c011cf](https://gitee.com/JS-VeryHappy/vh-admin/commit/8c011cf45be80e7a5b184a6961f8292563a4fd37))
* **表单设计器:**  创建 ([384e457](https://gitee.com/JS-VeryHappy/vh-admin/commit/384e457be5327d3d1078331880beae92e98501ac))
* **表单设计器:**  创建 ([ad6b859](https://gitee.com/JS-VeryHappy/vh-admin/commit/ad6b859f99b0d31e74592c44bd7cc43ab7773ca6))
* **表单设计器:**  创建 ([b59bb08](https://gitee.com/JS-VeryHappy/vh-admin/commit/b59bb0897ee948cce61618e9179c03948065c37f))
* **表单设计器:**  创建 ([47f74b9](https://gitee.com/JS-VeryHappy/vh-admin/commit/47f74b9812fa5d58da56e83f21d882d3c257dd77))
* **通用:**  增加配置导入方法 ([f1d643b](https://gitee.com/JS-VeryHappy/vh-admin/commit/f1d643b60217243437a0d1385e11b3d305ada419))
* **通用:**  增加配置导入方法 ([824c19c](https://gitee.com/JS-VeryHappy/vh-admin/commit/824c19ccf5a252f319ab4f67baf673f53933fe53))
