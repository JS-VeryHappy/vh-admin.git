import { useImperativeHandle, useRef, useState } from 'react';
import type { ComponentsType, SettingsType } from '../types';
import { ProCard } from '@ant-design/pro-card';
import { FormCustom } from '@vh-admin/pro-components';
import type { FormCustomColumnsType } from '@vh-admin/pro-components/lib/f-form-custom/types';

const formColumns: FormCustomColumnsType<any>[] = [
  {
    title: '排列',
    dataIndex: 'layout',
    valueType: 'select',
    fieldProps: {
      allowClear: false,
      options: [
        // {
        //   label: '水平',
        //   value: 'horizontal',
        // },
        {
          label: '垂直',
          value: 'vertical',
        },
        // {
        //   label: '内联',
        //   value: 'inline',
        // },
      ],
    },
    initialValue: 'vertical',
  },
];
const setColumnsFn = (item: ComponentsType) => {
  const { type } = item;
  const columns: FormCustomColumnsType<any>[] = [
    {
      title: '字段D',
      dataIndex: 'dataIndex',
      valueType: 'text',
      fieldProps: {
        maxLength: 30,
      },
    },
    {
      title: '标题',
      dataIndex: 'title',
      valueType: 'text',
      fieldProps: {
        maxLength: 30,
      },
    },
    {
      title: 'placeholder',
      dataIndex: 'placeholder',
      valueType: 'text',
      fieldProps: {
        maxLength: 30,
      },
    },
    {
      title: '必须',
      dataIndex: 'required',
      valueType: 'radio',
      fieldProps: {
        options: [
          {
            value: true,
            label: '是',
          },
          {
            value: false,
            label: '否',
          },
        ],
      },
    },
  ];
  if (type?.includes('input')) {
    columns.push({
      title: '字符长度',
      dataIndex: 'maxLength',
      valueType: 'digit',
      fieldProps: {
        maxLength: 5,
      },
    });
  }
  if (type?.includes('select')) {
    columns.push({
      title: '选项',
      dataIndex: 'options',
      valueType: 'formList',
      columns: [
        {
          valueType: 'group',
          columns: [
            {
              title: '标题',
              dataIndex: 'label',
              width: '100px',
              formItemProps: {
                rules: [
                  {
                    required: true,
                  },
                ],
              },
            },
            {
              title: '值',
              dataIndex: 'value',
              formItemProps: {
                rules: [
                  {
                    required: true,
                  },
                ],
              },
              width: '100px',
            },
          ],
        },
      ],
    });
  }

  return columns;
};

const Settings = (props: SettingsType) => {
  const { settingsRef, onChangeSetting } = props;
  const [activeKey, setActiveKey] = useState<any>('component');
  const [select, setSelect] = useState<ComponentsType>();
  const [form, setForm] = useState<any>();
  const [componentColumns, setComponentColumns] = useState<FormCustomColumnsType<any>[]>([]);
  const formRef = useRef<any>();

  const setFormColumnsFn = (item: ComponentsType) => {
    if (item) {
      setSelect(item);
      setComponentColumns(setColumnsFn(item));
      setForm({
        dataIndex: item.dataIndex,
        placeholder: item.fieldProps?.placeholder || undefined,
        title: item.title,
        maxLength: item.fieldProps?.maxLength || undefined,
        options: item.fieldProps?.options || undefined,
        required: item.formItemProps?.rules && item.formItemProps.rules.length > 0 ? true : false,
      });
    } else {
      setSelect(undefined);
      setComponentColumns([]);
      setForm(undefined);
    }
  };

  useImperativeHandle(
    settingsRef,
    () => ({
      setSelect: (item: ComponentsType) => {
        setFormColumnsFn(item);
      },
    }),
    [],
  );

  const onFinish = (values: any) => {
    const { dataIndex, title, placeholder, maxLength, options, required } = values;
    const newItem: any = { ...select, dataIndex, title };

    newItem.fieldProps.placeholder = placeholder;

    if (maxLength) {
      newItem.fieldProps.maxLength = maxLength;
    }

    if (options) {
      newItem.fieldProps.options = options.filter((i: any) => {
        return i.label && i.value;
      });
    }

    if (required) {
      newItem.formItemProps.rules = [{ required: true }];
    } else {
      newItem.formItemProps.rules = [];
    }

    onChangeSetting(newItem);
  };

  const onValuesChange = () => {
    onFinish(formRef.current.getFieldsValue());
  };

  return (
    <div className="settings">
      <ProCard
        tabs={{
          type: 'line',
          activeKey: activeKey,
          onChange: (v: any) => {
            setActiveKey(v);
          },
        }}
      >
        <ProCard.TabPane key="component" tab="组件配置">
          {select && (
            <FormCustom
              onFinish={onFinish}
              formRef={formRef}
              key={select?.dataIndex}
              columns={componentColumns}
              onValuesChange={onValuesChange}
              initialValues={form}
              submitter={false}
            />
          )}
        </ProCard.TabPane>
        <ProCard.TabPane key="form" tab="表单配置">
          <FormCustom columns={formColumns} submitter={false} />
        </ProCard.TabPane>
      </ProCard>
    </div>
  );
};
export default Settings;
