import type { Ref } from 'react';

export declare type ComponentsType = {
  /**
   * 组件的唯一id
   */
  id?: any;
  /**
   * 显示的宽度
   */
  width?: any;
  /**
   * 配置使用字段，首次加入会自动生成
   */
  dataIndex?: string;
  /**
   * 分组
   */
  group?: string;
  /**
   * 对于组件类型
   */
  valueType: string;
  /**
   * 显示名称
   */
  title: string;
  /**
   * 显示的icon
   */
  icon?: React.ReactNode;

  /**
   * 组件对于的属性类型
   */
  type?: ('input' | 'select' | 'date' | 'uploadPicture')[];

  /**
   * 配置字段的属性
   */
  fieldProps?: {
    placeholder?: string;
    maxLength?: number;
    options?: any[];
    [key: string]: any;
  };
  /**
   * 配置
   */
  formItemProps?: {
    rules?: [];
    [key: string]: any;
  };
};

export declare type ComponentsGroupType = Record<any, ComponentsType[]>;

export declare type FormDesignerCustomType = {
  /**
   * 传递给antd的
   */
  configProvider?: any;

  /**
   * 获取组价的ref
   */
  fdRef?: FormDesignerCustomRefType;

  /**
   * 数据点击提交保存
   * @param data
   * @returns
   */
  onSubmit: (data: any) => void;
  /**
   * 传给组件的数据
   */
  value: ComponentsType[];
};

export declare type FormDesignerCustomRefType = Ref<any>;

export declare type ColumnTypeType = 'active' | 'move' | 'delete' | 'copy' | 'save';

export declare type ColumnType = {
  /**
   * 配置信息
   */
  column: ComponentsType;
  /**
   * 是否被选择
   */
  active: boolean;
  /**
   * 点击触发想挨揍呢
   * tpye 类型  item值
   */
  handle: (type: ColumnTypeType, item: ComponentsType | any) => void;
};

export declare type CanvasRefType = {
  /**
   * 插入一个新的item
   * @param item
   * @returns
   */
  addColumn: (item: ComponentsType) => void;
  /**
   * 保存修改一个item
   * @param item
   * @returns
   */
  saveItem: (item: ComponentsType) => void;

  /**
   * 得到当前的配置
   * @returns
   */
  getColumns: () => ComponentsType[];
};

export declare type CanvasType = {
  /**
   * canvas的ref
   */
  canvasRef: React.MutableRefObject<CanvasRefType | undefined>;
  /**
   *
   * 当前组件被选中时
   * @param data
   * @returns
   */
  onChangeSelect: (data: any) => void;
  /**
   * 数据点击提交保存
   * @param data
   * @returns
   */
  onSubmit: (data: any) => void;
  /**
   * 传给组件的数据
   */
  value: ComponentsType[];
};

export declare type CPRefRefType = {
  /**
   * 插入一个新的item
   */
  addColumn: (item: ComponentsType) => void;
};

export declare type ComponentsPanelType = {
  /**
   * 点击触发想挨揍呢
   * tpye 类型  item值
   */
  handle: (type: any, item: ComponentsType) => void;
};

export declare type SettingsRefType = {
  /**
   * 设置一个选中的item
   * @param item
   * @returns
   */
  setSelect: (item: ComponentsType) => void;
};

export declare type SettingsType = {
  /**
   * settings的ref
   */
  settingsRef: React.MutableRefObject<SettingsRefType | undefined>;
  /**
   * 属性保存的item
   * @param item
   * @returns
   */
  onChangeSetting: (item: ComponentsType) => void;
};
