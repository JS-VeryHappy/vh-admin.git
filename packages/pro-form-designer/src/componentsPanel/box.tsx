import { Button } from 'antd';
import type { ComponentsType } from '../types';
import { useDrag } from 'react-dnd';

const Box = (props: ComponentsType) => {
  const { valueType, icon, title } = props;
  const [{ isDragging }, dragRef] = useDrag(() => ({
    type: 'box',
    item: props,
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
      handlerId: monitor.getHandlerId(),
    }),
  }));
  const opacity = isDragging ? 0.4 : 1;
  return (
    <div ref={dragRef}>
      <Button key={valueType} style={{ opacity }} type="dashed" icon={icon}>
        {title}
      </Button>
    </div>
  );
};
export default Box;
