import {
  CalendarOutlined,
  UnlockOutlined,
  CreditCardOutlined,
  FieldTimeOutlined,
  ScheduleOutlined,
  TableOutlined,
  SaveOutlined,
  TagsOutlined,
  LayoutOutlined,
  ForkOutlined,
} from '@ant-design/icons';
import type { ComponentsType } from '../types';

export const components: ComponentsType[] = [
  {
    group: '普通组件',
    valueType: 'text',
    title: '文本框',
    icon: <CalendarOutlined />,
    type: ['input'],
  },
  {
    group: '普通组件',
    valueType: 'textarea',
    title: '文本域',
    icon: <CreditCardOutlined />,
    type: ['input'],
  },
  {
    group: '普通组件',
    valueType: 'password',
    title: '密码输入框',
    icon: <UnlockOutlined />,
    type: ['input'],
  },
  {
    group: '选择组件',
    valueType: 'select',
    title: '下拉框',
    icon: <ScheduleOutlined />,
    type: ['select'],
  },
  {
    group: '选择组件',
    valueType: 'checkbox',
    title: '多选框',
    icon: <SaveOutlined />,
    type: ['select'],
  },
  {
    group: '选择组件',
    valueType: 'radio',
    title: '单选框',
    icon: <TagsOutlined />,
    type: ['select'],
  },
  {
    group: '时间组件',
    valueType: 'date',
    title: '日期',
    icon: <FieldTimeOutlined />,
    type: ['date'],
  },
  {
    group: '业务组件',
    valueType: 'UploadCustom',
    title: '上传图片',
    icon: <ForkOutlined />,
    type: ['uploadPicture'],
  },
];
