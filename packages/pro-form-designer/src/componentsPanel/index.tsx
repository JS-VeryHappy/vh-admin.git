import { Input, Space, Divider } from 'antd';
import { useEffect, useState } from 'react';
import { components } from './componentsGroup';
import { ProCard } from '@ant-design/pro-card';
import type { ComponentsType, ComponentsGroupType, ComponentsPanelType } from '../types';
import _ from 'lodash';
import Box from './box';

const { Search } = Input;

const ComponentsPanel = (props: ComponentsPanelType) => {
  const { handle } = props;
  const [oldList, setOldList] = useState<ComponentsType[]>(components);
  const [groupList, setGroupList] = useState<ComponentsGroupType>({});

  const getGroupListFn = (list: ComponentsType[]) => {
    setGroupList(_.groupBy(list, 'group'));
  };

  const onSearch = (value: string) => {
    if (value) {
      const newArr = oldList.filter((i: ComponentsType) => {
        return i.title.indexOf(value) !== -1;
      });
      getGroupListFn(newArr);
    } else {
      getGroupListFn(oldList);
    }
  };

  useEffect(() => {
    setOldList(components);
    getGroupListFn(components);
  }, []);

  return (
    <div className="components-panel">
      <Search placeholder="搜索组件" onSearch={onSearch} />
      <Divider style={{ margin: '16px 0 0 0' }} />
      <div className="components">
        {Object.keys(groupList).map((key: string) => {
          const arr: ComponentsType[] = groupList[key];
          return (
            <ProCard title={key} key={key} ghost>
              <Space wrap>
                {arr.map((value: ComponentsType) => {
                  return (
                    <div onClick={() => handle('BoxClick', value)} key={value.valueType}>
                      <Box {...value} />
                    </div>
                  );
                })}
              </Space>
            </ProCard>
          );
        })}
      </div>
    </div>
  );
};
export default ComponentsPanel;
