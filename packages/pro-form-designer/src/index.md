---
title: 'test'
order: 1
nav:
  order: 9
  title: 表单设计器
  path: /formDesigner

---


<!-- <code src="./designer/index.tsx" >test</code> -->

<code src="./Example/demo1.tsx" >demo1</code>

## API

### FormDesignerCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| configProvider | 传递给antd的 | `any` | - | - |
| fdRef | 获取组价的ref | `FormDesignerCustomRefType` | - | - |
| onSubmit | 点击保存 | `(data)=>void` | - | - |
| value | 传递给设计器的数据 | `ComponentsType[]` | - | - |


### ComponentsGroupType
`Record<any, ComponentsType[]>;`

### ComponentsType
| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| id | 组件的唯一id | `any` | - | - |
| width | 显示的宽度 | `any` | - | - |
| dataIndex | 配置使用字段，首次加入会自动生成 | `string` | - | - |
| group | 分组 | `string` | - | - |
| valueType(必须) | 对于组件类型 | `string` | - | - |
| title(必须) | 显示名称 | `string` | - | - |
| icon | 显示的icon | `React.ReactNode` | - | - |
| type | 组件对于的属性类型 | `('input'\|'select'\|'date'\|'uploadPicture')[]` | - | - |
| fieldProps | 表单fieldProps配置 | `fieldProps` | - | - |
| formItemProps | 表单formItemProps配置 | `formItemProps` | - | - |


### FormDesignerCustomRefType
`Ref<any>;`


### ColumnTypeType
`'active' | 'move' | 'delete' | 'copy' | 'save'`

### ColumnType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| column(必须) | 配置信息 | `ComponentsType` | - | - |
| active(必须) | 是否被选择 | `boolean` | - | - |
| handle(必须) | 点击触发想挨揍呢<br>tpye类型item值 | `(type:ColumnTypeType,item:ComponentsType\|any)=>void` | - | - |


### CanvasRefType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| addColumn(必须) | 插入一个新的item<br>@paramitem<br>@returns | `(item:ComponentsType)=>void` | - | - |    
| saveItem(必须) | 保存修改一个item<br>@paramitem<br>@returns | `(item:ComponentsType)=>void` | - | - |     

### CanvasType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| canvasRef(必须) | canvas的ref | `React.MutableRefObject<CanvasRefType\|undefined>` | - | - |
| onChangeSelect(必须) | <br>当前组件被选中时<br>@paramdata<br>@returns | `(data:any)=>void` | - | - |      
  

### CPRefRefType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| addColumn(必须) | 插入一个新的item | `(item:ComponentsType)=>void` | - | - |    
  
### ComponentsPanelType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| handle(必须) | 点击触发想挨揍呢<br>tpye类型item值 | `(type:any,item:ComponentsType)=>void` | - | - |      


### SettingsRefType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| setSelect(必须) | 设置一个选中的item<br>@paramitem<br>@returns | `(item:ComponentsType)=>void` | - | - |  

### SettingsType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| settingsRef(必须) | settings的ref | `React.MutableRefObject<SettingsRefType\|undefined>` | - | - |        
| onChangeSetting(必须) | 属性保存的item<br>@paramitem<br>@returns | `(item:ComponentsType)=>void` | - | - |

