import { ProCard } from '@ant-design/pro-card';
import ComponentsPanel from '../componentsPanel';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import './index.less';
import { ConfigProvider } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import { useImperativeHandle, useRef } from 'react';
import type {
  ComponentsType,
  CanvasRefType,
  FormDesignerCustomType,
  SettingsRefType,
} from '../types';
import Canvas from '../canvas';
import Settings from '../settings';

const FormDesignerCustom = (props: FormDesignerCustomType) => {
  const canvasRef = useRef<CanvasRefType>();
  const settingsRef = useRef<SettingsRefType>();

  const { configProvider = {}, fdRef, onSubmit, value } = props;

  const handleComponentsPanel = (type: any, item: ComponentsType) => {
    // 如果是点击则自动生成一个组件
    if (type === 'BoxClick') {
      canvasRef.current?.addColumn(item);
    }
  };

  const onChangeSelect = (item: any) => {
    if (settingsRef) {
      settingsRef.current?.setSelect(item);
    }
  };

  const onChangeSetting = (item: any) => {
    if (canvasRef) {
      canvasRef.current?.saveItem(item);
    }
  };

  useImperativeHandle(fdRef, () => ({}));

  return (
    <DndProvider backend={HTML5Backend} context={window}>
      <ConfigProvider locale={zhCN} {...configProvider}>
        <ProCard split="vertical" bordered className="form-designer-custom">
          <ProCard
            className="components-panel-pro-card pro-card"
            title="组件库"
            colSpan="280px"
            headerBordered
          >
            <ComponentsPanel handle={handleComponentsPanel} />
          </ProCard>
          <ProCard className="canvas-pro-card pro-card">
            <Canvas
              canvasRef={canvasRef}
              value={value}
              onSubmit={onSubmit}
              onChangeSelect={onChangeSelect}
            />
          </ProCard>
          <ProCard
            className="settings-pro-card pro-card"
            title="属性面板"
            colSpan="330px"
            headerBordered
          >
            <Settings settingsRef={settingsRef} onChangeSetting={onChangeSetting} />
          </ProCard>
        </ProCard>
      </ConfigProvider>
    </DndProvider>
  );
};
export default FormDesignerCustom;
