//@ts-ignore
import { FormDesignerCustom } from '@vh-admin/pro-form-designer';

const value: any = [
  {
    group: '普通组件',
    valueType: 'textarea',
    title: '文本域',
    type: ['input'],
    fieldProps: {
      placeholder: '请输入',
      maxLength: 30,
    },
    formItemProps: {},
    id: 'textarea-PWuHWv',
    dataIndex: 'textarea-PWuHWv',
    width: '100%',
  },
];

const Demo1 = () => {
  const onSubmit = (v: any) => {
    console.log('===================');
    console.log(v);
    console.log('===================');
  };
  return (
    <>
      <FormDesignerCustom onSubmit={onSubmit} value={value} />
    </>
  );
};

export default Demo1;
