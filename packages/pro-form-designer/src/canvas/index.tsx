import { useEffect, useImperativeHandle, useRef, useState } from 'react';
import { useDrop } from 'react-dnd';
import type { ComponentsType, CanvasType, ColumnTypeType } from '../types';
import { nanoid } from 'nanoid';
import { ProForm } from '@ant-design/pro-form';
import Column from './column';
import { Button, Space } from 'antd';
import { FormCustom, ModalCustom } from '@vh-admin/pro-components';
import { ProDescriptions } from '@ant-design/pro-descriptions';

const setDefaultItemFn = (item: ComponentsType) => {
  const id: string = `${item.valueType}-${nanoid(6)}`;
  const newItem: ComponentsType = { ...item };
  if (!newItem.fieldProps) {
    newItem.fieldProps = {};
  }
  if (!newItem.fieldProps.placeholder) {
    let placeholder = '请输入';
    if (
      newItem.type?.includes('select') ||
      newItem.type?.includes('date') ||
      newItem.type?.includes('uploadPicture')
    ) {
      placeholder = '请选择';
    }
    newItem.fieldProps.placeholder = placeholder;
  }
  if (newItem.type?.includes('input') && !newItem.fieldProps.maxLength) {
    newItem.fieldProps.maxLength = 30;
  }
  if (newItem.type?.includes('select') && !newItem.fieldProps.options) {
    newItem.fieldProps.options = [
      {
        label: '显示',
        value: '1',
      },
    ];
  }
  if (newItem.type?.includes('uploadPicture')) {
    newItem.fieldProps = {
      format: ['jpg', 'jpeg', 'png'],
      listType: 'picture-card',
      maxCount: 5,
      size: 2000,
      ...newItem.fieldProps,
    };
  }

  if (!newItem.formItemProps) {
    newItem.formItemProps = {};
  }

  newItem.id = id;
  newItem.dataIndex = id;
  newItem.width = '100%';

  return newItem;
};

const Canvas = (props: CanvasType) => {
  const { canvasRef, onChangeSelect, onSubmit, value } = props;
  const [columns, setColumns] = useState<ComponentsType[]>([]);
  const columnsRef = useRef<ComponentsType[]>([]);
  const [select, setSelect] = useState<ComponentsType>();
  const [open, setOpen] = useState<boolean>(false);
  const [openJson, setOpenJson] = useState<boolean>(false);

  useEffect(() => {
    if (value) {
      setColumns(value);
      columnsRef.current = value;
    } else {
      setColumns([]);
      columnsRef.current = [];
    }
  }, [value]);

  const setItemFn = (item: ComponentsType) => {
    return setDefaultItemFn(item);
  };

  const setColumnsFn = (v: any, type = 'add') => {
    const old = [...columnsRef.current];
    if (type === 'add') {
      old.push(v);
    } else {
      old.splice(v, 1);
    }
    columnsRef.current = old;
    setColumns(old);
  };

  const setSelectFn = (item: any) => {
    setSelect(item);
    onChangeSelect(item);
  };

  const [{ canDrop, isOver }, dropRef] = useDrop(() => ({
    accept: 'box',
    drop: async (item: ComponentsType, monitor: any) => {
      const didDrop = monitor.didDrop();

      if (item.dataIndex && columnsRef.current.find((i: any) => i.dataIndex === item.dataIndex)) {
        return;
      }

      if (didDrop) {
        return;
      }
      const newItem = setItemFn(item);
      setColumnsFn(newItem);
      setSelectFn(newItem);
    },

    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }));

  const handle = (type: ColumnTypeType, item: ComponentsType | any) => {
    if (type === 'active') {
      setSelectFn(item);
    } else if (type === 'delete') {
      const index = columnsRef.current.findIndex((i: any) => i.dataIndex === item.dataIndex);
      setColumnsFn(index, '');
      setSelectFn(undefined);
    } else if (type === 'copy') {
      setColumnsFn(setItemFn(item));
    } else if (type === 'move') {
      const { item: old, column, position } = item;

      let oldItem = old;
      if (!oldItem.id) {
        oldItem = setItemFn(oldItem);
      }

      const oldData = [...columnsRef.current];
      const oldIndex = columnsRef.current.findIndex((i: any) => i.dataIndex === oldItem.dataIndex);
      const columnIndex = columnsRef.current.findIndex(
        (i: any) => i.dataIndex === column.dataIndex,
      );

      if (columnIndex !== -1 && position) {
        if (oldIndex !== -1) {
          oldData.splice(oldIndex, 1);
        }

        oldData.splice(columnIndex, 0, oldItem);
        columnsRef.current = oldData;
        setColumns(oldData);
      }
    } else if (type === 'save') {
      const oldData = [...columnsRef.current];
      const index = oldData.findIndex((i: any) => i.dataIndex === item.dataIndex);
      oldData.splice(index, 1, item);
      columnsRef.current = oldData;

      setColumns(oldData);
    }
  };

  const isActive = canDrop && isOver;

  useImperativeHandle(
    canvasRef,
    () => ({
      addColumn: (item: ComponentsType) => {
        setColumnsFn(setItemFn(item));
      },
      saveItem: (item: ComponentsType) => {
        handle('save', item);
      },
      getColumns: () => {
        return columnsRef.current;
      },
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <div className="canvas">
      <div className="tools">
        <ModalCustom
          open={openJson}
          footer={false}
          onCancel={() => {
            setOpenJson(false);
          }}
        >
          <ProDescriptions>
            <ProDescriptions.Item valueType="jsonCode" copyable>
              {JSON.stringify(columnsRef.current, null, 4)}
            </ProDescriptions.Item>
          </ProDescriptions>
        </ModalCustom>
        {open && (
          <FormCustom
            columns={columnsRef.current}
            open={open}
            onOpenChange={(v: boolean) => {
              setOpen(v);
            }}
            layoutType="ModalForm"
          />
        )}

        <Space wrap>
          <Button
            type="primary"
            onClick={() => {
              setOpen(true);
            }}
          >
            预览
          </Button>
          <Button
            type="primary"
            onClick={() => {
              setOpenJson(true);
            }}
          >
            获取配置
          </Button>
          <Button
            type="primary"
            onClick={() => {
              if (onSubmit) {
                onSubmit(columnsRef.current);
              }
            }}
          >
            保存
          </Button>
        </Space>
      </div>
      <div
        className={`canvas-drag ${isActive ? 'canvas-active' : ''} ${
          canDrop ? 'canvas-candrop' : ''
        }`}
        ref={dropRef}
      >
        <ProForm submitter={false}>
          {columnsRef.current.map((item: ComponentsType) => {
            const active = select?.dataIndex === item.dataIndex;
            return (
              <Column
                key={item.dataIndex}
                // dropRef={dropRef}
                active={active}
                handle={handle}
                column={item}
              />
            );
          })}
        </ProForm>
      </div>
    </div>
  );
};
export default Canvas;
