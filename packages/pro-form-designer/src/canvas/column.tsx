import { FormCustom } from '@vh-admin/pro-components';
import type { ComponentsType, ColumnType } from '../types';
import { DragOutlined, DeleteOutlined, CopyOutlined } from '@ant-design/icons';
import { useDrag, useDrop } from 'react-dnd';
import { useRef, useState } from 'react';
import CryptoJs from 'crypto-js';

const Column = (props: ColumnType) => {
  const { column, active, handle } = props;
  const boxRef = useRef<any>(null);
  const [position, setPosition] = useState<any>();
  const keystring = CryptoJs.MD5(JSON.stringify(column)).toString();

  const setPositionFn = (direction: any) => {
    setPosition(direction);
  };

  const [{ isDragging }, dragRef, dragPreview] = useDrag({
    type: 'box',
    item: column,
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const [{ canDrop, isOver }, dropRef] = useDrop(
    () => ({
      accept: 'box',
      drop(item: any) {
        handle('move', {
          item,
          column,
          position,
        });
      },
      hover(item: ComponentsType, monitor: any) {
        const didHover = monitor.isOver({ shallow: true });
        if (!didHover) {
          return;
        }
        if (item.dataIndex === column.dataIndex) {
          return;
        }
        const hoverBoundingRect = boxRef.current && boxRef.current.getBoundingClientRect();
        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
        // Determine mouse position
        // const clientOffset = monitor.getClientOffset();
        const dragOffset = monitor.getSourceClientOffset();
        // Get pixels to the top
        const hoverClientY = dragOffset.y - hoverBoundingRect.top;
        if (hoverClientY <= hoverMiddleY) {
          setPositionFn('up');
        } else if (hoverClientY > hoverMiddleY) {
          setPositionFn('down');
        }
      },
      collect: (monitor) => ({
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
      }),
    }),
    [position],
  );

  const isActive = canDrop && isOver;

  const handleClick = (type: any, e: any) => {
    e.stopPropagation();
    handle(type, column);
  };

  dragPreview(dropRef(boxRef));

  let overwriteStyle: any = {
    backgroundColor: '#fff',
    opacity: isDragging ? 0 : 1,
  };
  if (isActive) {
    if (position === 'up') {
      overwriteStyle = {
        ...overwriteStyle,
        boxShadow: '0 -3px 0 red',
      };
    } else if (position === 'down') {
      overwriteStyle = {
        ...overwriteStyle,
        boxShadow: '0 3px 0 red',
      };
    }
  }

  return (
    <div
      ref={boxRef}
      style={overwriteStyle}
      className={`column ${active ? 'column-active' : ''}`}
      onClick={handleClick.bind(null, 'active')}
    >
      <div className="dataIndex">{column.dataIndex}</div>
      {active && (
        <>
          <div className="pointer-move" ref={dragRef} title="拖动">
            <DragOutlined />
          </div>
          <div className="pointer-wrapper" onClick={handleClick.bind(null, 'delete')}>
            <div className="pointer" title="删除">
              <DeleteOutlined />
            </div>
            <div className="pointer" title="复制" onClick={handleClick.bind(null, 'copy')}>
              <CopyOutlined />
            </div>
          </div>
        </>
      )}

      <FormCustom key={keystring} layoutType="Embed" columns={[{ ...column }]} />
    </div>
  );
};
export default Column;
