# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [2.2.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.2.7...@vh-admin/pro-components@2.2.8) (2024-12-23)


###   ✨ Features | 新功能

* **pro-table:**  分页显示优化 ([7dd7738](https://gitee.com/JS-VeryHappy/vh-admin/commit/7dd77385d982ffd573ac13b2a90c620e89bdd901))



### [2.2.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.2.6...@vh-admin/pro-components@2.2.7) (2024-12-20)


###   🐛 Bug Fixes | Bug 修复

* **pro-table:**  分页显示优化 ([95e67b0](https://gitee.com/JS-VeryHappy/vh-admin/commit/95e67b0f9465ab01d5763215153210b097de0d1b))
* **pro-table:**  字段单词错误 ([5a1ae48](https://gitee.com/JS-VeryHappy/vh-admin/commit/5a1ae488adfe962c77b2ac19838f8bed93a48cd9))



### [2.2.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.2.5...@vh-admin/pro-components@2.2.6) (2024-12-09)


###   🐛 Bug Fixes | Bug 修复

* **pro-form:**  preview增加参数 ([6027239](https://gitee.com/JS-VeryHappy/vh-admin/commit/602723913e39aedcc797117240f64c9875b2324c))



### [2.2.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.2.4...@vh-admin/pro-components@2.2.5) (2024-12-09)


###   🐛 Bug Fixes | Bug 修复

* **pro-form:**  preview增加参数 ([aac2465](https://gitee.com/JS-VeryHappy/vh-admin/commit/aac24654a80056e60889596e8b6990facd85ce43))
* **pro-form:**  types ([257b711](https://gitee.com/JS-VeryHappy/vh-admin/commit/257b7119acb4082546008b00c72c736762872c84))



### [2.2.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.2.3...@vh-admin/pro-components@2.2.4) (2024-11-22)


###   🐛 Bug Fixes | Bug 修复

* **pro-form:**  表单删除evel的使用 ([c4c8862](https://gitee.com/JS-VeryHappy/vh-admin/commit/c4c8862e61b9f8959dd4502a2fc827c3ca9f724e))



### [2.2.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.2.2...@vh-admin/pro-components@2.2.3) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([32b1222](https://gitee.com/JS-VeryHappy/vh-admin/commit/32b1222f18f796660ca3d8785d198636fb2f9bc5))



### [2.2.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.2.1...@vh-admin/pro-components@2.2.2) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([4eaad85](https://gitee.com/JS-VeryHappy/vh-admin/commit/4eaad856bac78ee28f348c001a997e30ee3bfd37))
* **all:**  使用阿里云仓库 ([d7a734e](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7a734ea08b9abefc84c652a9091d091d67b5d02))



### [2.2.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.50...@vh-admin/pro-components@2.2.1) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  配置的字段支持deepGet ([887a988](https://gitee.com/JS-VeryHappy/vh-admin/commit/887a988959fd7058abf8d40e897655012e130fef))



### [2.1.50](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.49...@vh-admin/pro-components@2.1.50) (2024-07-31)


###   🐛 Bug Fixes | Bug 修复

* **pro-components:**  导入组件可以自定义默认表单 ([f2886bf](https://gitee.com/JS-VeryHappy/vh-admin/commit/f2886bf7f8d7b029a51a7cc9ea399ef8518f1604))



### [2.1.49](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.48...@vh-admin/pro-components@2.1.49) (2024-07-26)


###   🐛 Bug Fixes | Bug 修复

* **pro-components:**  优化ts ([12396ed](https://gitee.com/JS-VeryHappy/vh-admin/commit/12396ed696eead096102d9ee0b7ef3f80304bafd))



### [2.1.48](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.47...@vh-admin/pro-components@2.1.48) (2024-07-10)


###   🐛 Bug Fixes | Bug 修复

* **组件:**  优化多选方式 ([0b86a34](https://gitee.com/JS-VeryHappy/vh-admin/commit/0b86a34e73b6e5eded30b0f97256df0bbc5cf93c))



### [2.1.47](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.46...@vh-admin/pro-components@2.1.47) (2024-06-24)


###   🐛 Bug Fixes | Bug 修复

* **表格:**  优化按钮提示警告 ([1461d5f](https://gitee.com/JS-VeryHappy/vh-admin/commit/1461d5fd2ec09229dff979dcbc67183273fe5c90))



### [2.1.46](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.45...@vh-admin/pro-components@2.1.46) (2024-06-17)


###   🐛 Bug Fixes | Bug 修复

* **表单:**  优化提示 ([e5df638](https://gitee.com/JS-VeryHappy/vh-admin/commit/e5df6389c8447b6cb2ff5ba3ef864cfff21ca1b2))
* **表单:**  增加请求header参数 ([7fd0c10](https://gitee.com/JS-VeryHappy/vh-admin/commit/7fd0c10fd23a74df58cd4630aa214c8f36b3cbba))



### [2.1.45](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.44...@vh-admin/pro-components@2.1.45) (2024-06-14)


###   🐛 Bug Fixes | Bug 修复

* **表单:**  优化提示 ([4829a9e](https://gitee.com/JS-VeryHappy/vh-admin/commit/4829a9ed64a09c1733c85ba3a3101a05bf51d2b8))
* **流程配置:**  优化提示 ([3e2023a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3e2023a1f9ccee8bfdda8c2ea207cda6bcdefdb9))



### [2.1.44](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.43...@vh-admin/pro-components@2.1.44) (2024-05-27)


###   🐛 Bug Fixes | Bug 修复

* **流程配置:**  修复N个问题 ([ea60587](https://gitee.com/JS-VeryHappy/vh-admin/commit/ea605873233e7d9b1bfb307b4c0dbf8054e0b402))



### [2.1.43](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.42...@vh-admin/pro-components@2.1.43) (2024-05-27)


###   🐛 Bug Fixes | Bug 修复

* **流程配置:**  修复N个问题 ([b0f5d88](https://gitee.com/JS-VeryHappy/vh-admin/commit/b0f5d8874016022f66b4517db87305ec33ee020d))



### [2.1.42](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.41...@vh-admin/pro-components@2.1.42) (2024-05-21)


###   🐛 Bug Fixes | Bug 修复

* **流程配置:**  基本审批流程配置 ([bd39555](https://gitee.com/JS-VeryHappy/vh-admin/commit/bd39555ed347b02f72f3299880c9166033d8e166))
* **流程配置:**  修复N个问题 ([86d6b5b](https://gitee.com/JS-VeryHappy/vh-admin/commit/86d6b5b0e225e62f84098b57a6bc7347cb579e4b))



### [2.1.41](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.40...@vh-admin/pro-components@2.1.41) (2024-05-06)


###   🐛 Bug Fixes | Bug 修复

* **表单:**  上传赋值类型错误 ([a452d10](https://gitee.com/JS-VeryHappy/vh-admin/commit/a452d108906299c2863aa0d0b2942a8486d66a39))
* **表单:**  上传赋值类型错误 ([337fe69](https://gitee.com/JS-VeryHappy/vh-admin/commit/337fe69ffffe358958719f53fc35fce05357387f))
* **表单:**  上传赋值类型错误 ([a7f8b83](https://gitee.com/JS-VeryHappy/vh-admin/commit/a7f8b833b90f1998004037dafd2da8b633954322))
* **流程配置:**  基本审批流程配置 ([a19fe78](https://gitee.com/JS-VeryHappy/vh-admin/commit/a19fe782c2619ad0f276e2296e79f505f38dd0cf))



### [2.1.40](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.39...@vh-admin/pro-components@2.1.40) (2024-01-04)


###   ✨ Features | 新功能

* **表格:**  ts优化 ([d95c08c](https://gitee.com/JS-VeryHappy/vh-admin/commit/d95c08c467bfe0a0dc8023abe1099ab36913d72f))


###   🐛 Bug Fixes | Bug 修复

* **表单:**  上传赋值类型错误 ([5dc7c56](https://gitee.com/JS-VeryHappy/vh-admin/commit/5dc7c5612ff370c6f858293a41c9d021935b361e))



### [2.1.39](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.38...@vh-admin/pro-components@2.1.39) (2023-12-20)


###   ✨ Features | 新功能

* **表格:**  虚拟滚动 ([6730199](https://gitee.com/JS-VeryHappy/vh-admin/commit/6730199c9b0a90d1a9535d1d202dec116ffe093b))
* **表格:**  ts优化 ([7210c47](https://gitee.com/JS-VeryHappy/vh-admin/commit/7210c4789d3e673d7e8e40cecadec031d2e32ea0))



### [2.1.38](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.37...@vh-admin/pro-components@2.1.38) (2023-12-15)


###   ✨ Features | 新功能

* **表格:**  虚拟滚动 ([a446b27](https://gitee.com/JS-VeryHappy/vh-admin/commit/a446b274a88befd32a60076974f451fb2e3b7784))



### [2.1.37](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.36...@vh-admin/pro-components@2.1.37) (2023-12-14)


###   ✨ Features | 新功能

* **表格:**  虚拟滚动 ([90aa567](https://gitee.com/JS-VeryHappy/vh-admin/commit/90aa56747123fed2de0ea70270fde065c91d17a5))



### [2.1.36](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.35...@vh-admin/pro-components@2.1.36) (2023-12-13)


###   ✨ Features | 新功能

* **表格:**  虚拟滚动 ([e57b117](https://gitee.com/JS-VeryHappy/vh-admin/commit/e57b11741d3516018dbdd15db614ed421558978b))
* **表格:**  虚拟滚动 ([ce82693](https://gitee.com/JS-VeryHappy/vh-admin/commit/ce826937310c565df3f2594787a0fe34e60328f4))



### [2.1.35](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.34...@vh-admin/pro-components@2.1.35) (2023-12-13)


###   ✨ Features | 新功能

* **表格:**  虚拟滚动 ([9f69741](https://gitee.com/JS-VeryHappy/vh-admin/commit/9f69741734a11ee870822fd578c4958ccf3f54c6))



### [2.1.34](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.33...@vh-admin/pro-components@2.1.34) (2023-12-13)


###   ✨ Features | 新功能

* **表格:**  虚拟滚动 ([c114fe6](https://gitee.com/JS-VeryHappy/vh-admin/commit/c114fe603b87ff286b705c85ce58b1cbcc4bc707))



### [2.1.33](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.32...@vh-admin/pro-components@2.1.33) (2023-11-06)


###   ✨ Features | 新功能

* **表单:**  上传判断object ([a31795e](https://gitee.com/JS-VeryHappy/vh-admin/commit/a31795e9634aa0a27353ddcd88ed9d8b4ddbc521))



### [2.1.32](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.31...@vh-admin/pro-components@2.1.32) (2023-11-01)


###   ✨ Features | 新功能

* **Select:**  增加远程请求 ([a12f75f](https://gitee.com/JS-VeryHappy/vh-admin/commit/a12f75f480b0cce815aa0d5636676901c3eb4c3e))



### [2.1.31](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.30...@vh-admin/pro-components@2.1.31) (2023-11-01)


###   ✨ Features | 新功能

* **Select:**  增加远程请求 ([9377f95](https://gitee.com/JS-VeryHappy/vh-admin/commit/9377f953b397fcbafabe0413b34de18c8fd297bf))



### [2.1.30](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.29...@vh-admin/pro-components@2.1.30) (2023-11-01)


###   ✨ Features | 新功能

* **Select:**  增加远程请求 ([79d25e9](https://gitee.com/JS-VeryHappy/vh-admin/commit/79d25e96027b6cec6c48afdcf21ea4a567e32d0e))



### [2.1.29](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.28...@vh-admin/pro-components@2.1.29) (2023-11-01)


###   ✨ Features | 新功能

* **Select:**  增加远程请求 ([451e5c0](https://gitee.com/JS-VeryHappy/vh-admin/commit/451e5c02cd27f1e5582cefd83a84c2d1a9508ce6))



### [2.1.28](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.27...@vh-admin/pro-components@2.1.28) (2023-10-30)


###   ✨ Features | 新功能

* **优化:** 主题token ([de0eb28](https://gitee.com/JS-VeryHappy/vh-admin/commit/de0eb28b1923533b3c12cb584d9f4dc74b821823))



### [2.1.27](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.26...@vh-admin/pro-components@2.1.27) (2023-10-24)


###   ✨ Features | 新功能

* **上传弹窗:** 增加大小限制 ([78c25a0](https://gitee.com/JS-VeryHappy/vh-admin/commit/78c25a0df2f34091cdb0e9693c37f81aa7559fef))



### [2.1.26](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.25...@vh-admin/pro-components@2.1.26) (2023-10-24)


###   ✨ Features | 新功能

* **表格:** 表格快捷按钮增加父级类型 ([451e0cb](https://gitee.com/JS-VeryHappy/vh-admin/commit/451e0cb8c425365bd52786c365a0ed1975492661))



### [2.1.25](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.24...@vh-admin/pro-components@2.1.25) (2023-10-23)


###   ✨ Features | 新功能

* **优化:** ts ([aeeeef0](https://gitee.com/JS-VeryHappy/vh-admin/commit/aeeeef0b11628f91d9d387d018423927054fe800))



### [2.1.24](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.23...@vh-admin/pro-components@2.1.24) (2023-10-23)


###   ✨ Features | 新功能

* **表格:** 表格分页优化 ([ae568ea](https://gitee.com/JS-VeryHappy/vh-admin/commit/ae568ea4a690a2af2909e8df95d6fb78c1c99fb7))



### [2.1.23](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.22...@vh-admin/pro-components@2.1.23) (2023-10-20)


###   ✨ Features | 新功能

* **流程:** 更新流程组件 ([ce83d54](https://gitee.com/JS-VeryHappy/vh-admin/commit/ce83d54ba60cdc7718a617a331e134159b2e1517))



### [2.1.22](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.21...@vh-admin/pro-components@2.1.22) (2023-10-17)


###   ✨ Features | 新功能

* **d\表格:** 表格分页优化 ([7c4fa86](https://gitee.com/JS-VeryHappy/vh-admin/commit/7c4fa868f171757979db7e2c496a960ada973ef2))



### [2.1.21](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.20...@vh-admin/pro-components@2.1.21) (2023-10-16)


###   ✨ Features | 新功能

* **d\表格:** 表格分页优化 ([7c78619](https://gitee.com/JS-VeryHappy/vh-admin/commit/7c786197e071c7fb0a653d48429b4f7852b50599))



### [2.1.20](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.19...@vh-admin/pro-components@2.1.20) (2023-10-16)


###   ✨ Features | 新功能

* **d\表格:** 表格分页优化 ([2d97d48](https://gitee.com/JS-VeryHappy/vh-admin/commit/2d97d488aadfd08aec304f862a4ee3f5ccd87142))



### [2.1.19](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.18...@vh-admin/pro-components@2.1.19) (2023-10-08)


###   ✨ Features | 新功能

* **优化:** 取消table 默认勾选下拉 ([d37536c](https://gitee.com/JS-VeryHappy/vh-admin/commit/d37536c0b4af40dc99fd8a1a85535036a075c0ea))



### [2.1.18](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.17...@vh-admin/pro-components@2.1.18) (2023-10-08)


###   ✨ Features | 新功能

* **优化:** 取消table 默认勾选下拉 ([78ccca0](https://gitee.com/JS-VeryHappy/vh-admin/commit/78ccca0361768716259314bf7041ae1f880b0ce4))
* **优化:** 依赖更新 ([30183b5](https://gitee.com/JS-VeryHappy/vh-admin/commit/30183b534e7723cf7193e548c7d27e28daef51ea))
* **优化:** 依赖更新 ([534d605](https://gitee.com/JS-VeryHappy/vh-admin/commit/534d6054f6fe2acd4f9f54408bd4045ab180d1b8))



### [2.1.17](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.16...@vh-admin/pro-components@2.1.17) (2023-07-18)


###   ✨ Features | 新功能

* **优化:** 样式 ([0a69f3d](https://gitee.com/JS-VeryHappy/vh-admin/commit/0a69f3d5b7c314458d96f13a31bea4ed6381e215))



### [2.1.16](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.15...@vh-admin/pro-components@2.1.16) (2023-07-17)


###   ✨ Features | 新功能

* **优化:** sort返回错误 ([eb1b0bf](https://gitee.com/JS-VeryHappy/vh-admin/commit/eb1b0bf668b116fb03237bfdf8e10eb76d6de60c))



### [2.1.15](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.14...@vh-admin/pro-components@2.1.15) (2023-07-14)


###   ✨ Features | 新功能

* **优化:** 表格按钮tips的间隔 ([66c2a58](https://gitee.com/JS-VeryHappy/vh-admin/commit/66c2a580d6001413b7035d40acf1f8210771af6a))



### [2.1.14](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.13...@vh-admin/pro-components@2.1.14) (2023-07-14)


###   ✨ Features | 新功能

* **优化:** 表格按钮tips的间隔 ([2aa9b50](https://gitee.com/JS-VeryHappy/vh-admin/commit/2aa9b506d1ac9e75ed3a4fb9fb42793e78b9732c))



### [2.1.13](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.12...@vh-admin/pro-components@2.1.13) (2023-07-14)


###   ✨ Features | 新功能

* **优化:** 新增内置自定义弹窗 ([1a70b00](https://gitee.com/JS-VeryHappy/vh-admin/commit/1a70b00aad9da89b79679d5be1c68e41b294a5d3))



### [2.1.12](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.11...@vh-admin/pro-components@2.1.12) (2023-07-06)


###   ✨ Features | 新功能

* **优化:** getFieldFormatValueObject ([c020196](https://gitee.com/JS-VeryHappy/vh-admin/commit/c020196f3b44f2e3161e2950b691f59c39ae3ffb))



### [2.1.11](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.10...@vh-admin/pro-components@2.1.11) (2023-06-30)


###   ✨ Features | 新功能

* **优化:** SelectDatePickerCustom 优化 ([9272d82](https://gitee.com/JS-VeryHappy/vh-admin/commit/9272d8246b57f8585fdaf477520be4f6e8e6d58e))



### [2.1.10](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.8...@vh-admin/pro-components@2.1.10) (2023-06-30)


###   ✨ Features | 新功能

* **优化:** SelectDatePickerCustom 优化 ([7d03278](https://gitee.com/JS-VeryHappy/vh-admin/commit/7d03278475ddf31df9940f4918749413bb658ba0))


### 🎫 Chores | 其他更新

* **release:** publish ([2cee2b4](https://gitee.com/JS-VeryHappy/vh-admin/commit/2cee2b45a511224426a3e35c3f2bbb85ad41088e))



### [2.1.9](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.8...@vh-admin/pro-components@2.1.9) (2023-06-30)


###   ✨ Features | 新功能

* **优化:** SelectDatePickerCustom 优化 ([7d03278](https://gitee.com/JS-VeryHappy/vh-admin/commit/7d03278475ddf31df9940f4918749413bb658ba0))



### [2.1.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.7...@vh-admin/pro-components@2.1.8) (2023-06-20)


###   ✨ Features | 新功能

* **优化:** types 优化 ([b96260f](https://gitee.com/JS-VeryHappy/vh-admin/commit/b96260fdf16714c264e7ab4dfc3928e89c18404f))



### [2.1.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.6...@vh-admin/pro-components@2.1.7) (2023-06-20)


###   ✨ Features | 新功能

* **优化:** types 优化 ([c8ca97b](https://gitee.com/JS-VeryHappy/vh-admin/commit/c8ca97b29dc77a9c97ab1e9eaf05b127900ae717))
* **优化:** types 优化 ([7d7b7bd](https://gitee.com/JS-VeryHappy/vh-admin/commit/7d7b7bd7c565475d26e9a3367577e2e7b8615389))



### [2.1.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.5...@vh-admin/pro-components@2.1.6) (2023-06-13)


###   ✨ Features | 新功能

* **优化:** tableFormRef修复 ([4ac1a96](https://gitee.com/JS-VeryHappy/vh-admin/commit/4ac1a96fd9e5dc96c735e18ab08a13672cbe42fb))



### [2.1.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.4...@vh-admin/pro-components@2.1.5) (2023-06-13)


###   ✨ Features | 新功能

* **优化:** tableFormRef修复 ([f022f74](https://gitee.com/JS-VeryHappy/vh-admin/commit/f022f745b23be28f7ed3f2c319147dc630abdc01))



### [2.1.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.3...@vh-admin/pro-components@2.1.4) (2023-06-13)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([54ae405](https://gitee.com/JS-VeryHappy/vh-admin/commit/54ae405c8903abcebe579e3a74ea83802f6ea2e0))



### [2.1.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.2...@vh-admin/pro-components@2.1.3) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([af158f0](https://gitee.com/JS-VeryHappy/vh-admin/commit/af158f059da0dc13e939e41d5c96b573b8a9fd22))



### [2.1.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.1.1...@vh-admin/pro-components@2.1.2) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([e005fa3](https://gitee.com/JS-VeryHappy/vh-admin/commit/e005fa347e2fbd0b0e064b125882c2e7a6355362))



### [2.1.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.70...@vh-admin/pro-components@2.1.1) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3f70312](https://gitee.com/JS-VeryHappy/vh-admin/commit/3f70312823ae2ea54d111f4ce517adbce5291c2c))



### [2.0.70](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.68...@vh-admin/pro-components@2.0.70) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3dfb77a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3dfb77af0ee96789a4aac9b4d6e36c5f83e93110))



### [2.0.68](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.67...@vh-admin/pro-components@2.0.68) (2023-05-12)


###   ✨ Features | 新功能

* **表格:** 优化className ([cc5d5e4](https://gitee.com/JS-VeryHappy/vh-admin/commit/cc5d5e4fac5f5e99d59a36a723494f732920d01d))




### [2.0.67](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.66...@vh-admin/pro-components@2.0.67) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([3b02fc4](https://gitee.com/JS-VeryHappy/vh-admin/commit/3b02fc408cd0dde125dd85132e2ccbebb531914d))



### [2.0.66](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.65...@vh-admin/pro-components@2.0.66) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([14ed9a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/14ed9a6012042164a7d8255148979079b89c8f77))



### [2.0.65](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.64...@vh-admin/pro-components@2.0.65) (2023-05-09)


###   ✨ Features | 新功能

* **表单:** 内置表格增加多选 ([cf8aea9](https://gitee.com/JS-VeryHappy/vh-admin/commit/cf8aea936152317db89c02a3b041f73371aa1b6b))
* **表单:** 内置表格增加多选 ([55b8f62](https://gitee.com/JS-VeryHappy/vh-admin/commit/55b8f629ee71fec13eab2e6f1be7601f9d06b627))



### [2.0.64](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.63...@vh-admin/pro-components@2.0.64) (2023-05-08)


###   ✨ Features | 新功能

* **表单:** 内置表格增加多选 ([9c849a4](https://gitee.com/JS-VeryHappy/vh-admin/commit/9c849a42503bd1a3ebcff018e17832ad923f52a1))



### [2.0.63](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.62...@vh-admin/pro-components@2.0.63) (2023-05-06)


###   ✨ Features | 新功能

* **表单:** 远程请求优化 ([f84f1dd](https://gitee.com/JS-VeryHappy/vh-admin/commit/f84f1dd6bcbb30ab34a1270aa60cbc7da5dc37ff))
* **表单:** 远程请求优化 ([def739c](https://gitee.com/JS-VeryHappy/vh-admin/commit/def739cb68aaefee6f9a1e9dfc58c904397343c7))



### [2.0.62](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.61...@vh-admin/pro-components@2.0.62) (2023-05-05)


###   ✨ Features | 新功能

* **表单:** 优化 ([cbc96ac](https://gitee.com/JS-VeryHappy/vh-admin/commit/cbc96ac0ea78033a1abe51d52a5a6ef94b4f254a))



### [2.0.61](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.60...@vh-admin/pro-components@2.0.61) (2023-04-27)


###   ✨ Features | 新功能

* **表单:**  编辑表单优化 ([ad23228](https://gitee.com/JS-VeryHappy/vh-admin/commit/ad23228edeb9e2f6a1be167b94a694341db2dccb))



### [2.0.60](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.59...@vh-admin/pro-components@2.0.60) (2023-04-26)


###   ✨ Features | 新功能

* **表单:**  编辑表单优化 ([bd09b0b](https://gitee.com/JS-VeryHappy/vh-admin/commit/bd09b0bacb5dd01ce39c677684c21b343b6c79ce))
* **表单:**  编辑表单优化 ([0093c7d](https://gitee.com/JS-VeryHappy/vh-admin/commit/0093c7dc4565986951f1b9451f911c88b2cefd9f))
* **表单:**  编辑表单优化 ([afa1200](https://gitee.com/JS-VeryHappy/vh-admin/commit/afa12005dd6d7734af8b9e677b050462abe94005))



### [2.0.59](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.58...@vh-admin/pro-components@2.0.59) (2023-04-26)


###   ✨ Features | 新功能

* **表单:**  编辑表单优化 ([291b001](https://gitee.com/JS-VeryHappy/vh-admin/commit/291b001d332e06e12c6e80f07c1dbb046b538ce2))



### [2.0.58](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.56...@vh-admin/pro-components@2.0.58) (2023-04-26)


###   ✨ Features | 新功能

* **表单:**  编辑表单 ([a2bcd8f](https://gitee.com/JS-VeryHappy/vh-admin/commit/a2bcd8fe8b1573287d64663b1304a0ff2beb35ef))




### [2.0.56](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.54...@vh-admin/pro-components@2.0.56) (2023-04-25)


###   ✨ Features | 新功能

* **表单:**  编辑表单 ([feb1d32](https://gitee.com/JS-VeryHappy/vh-admin/commit/feb1d32909b58e14e105197db15632880466cc80))



### [2.0.54](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.53...@vh-admin/pro-components@2.0.54) (2023-04-25)


###   ✨ Features | 新功能

* **表单:**  编辑表单 ([5eee4bd](https://gitee.com/JS-VeryHappy/vh-admin/commit/5eee4bdaf75d2e1021d62f4f53fb5d4b9d16649e))
* **表单:**  编辑表单 ([1b342ba](https://gitee.com/JS-VeryHappy/vh-admin/commit/1b342baeacff2c1bd12a7cfcc96acba841ca2d9a))



### [2.0.53](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.52...@vh-admin/pro-components@2.0.53) (2023-04-24)


###   ✨ Features | 新功能

* **表单:**  编辑表单 ([5bd33f2](https://gitee.com/JS-VeryHappy/vh-admin/commit/5bd33f250a0e57d6ac1682df6aff621cef777368))
* **表单:**  编辑表单 ([35fe07e](https://gitee.com/JS-VeryHappy/vh-admin/commit/35fe07eb482ca1dbdd32a4e5665a36ae85ca6068))
* **表单:**  编辑表单 ([c06eefe](https://gitee.com/JS-VeryHappy/vh-admin/commit/c06eefeada80261b03201df460a9c22a565a7a7c))



### [2.0.52](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.51...@vh-admin/pro-components@2.0.52) (2023-04-23)


###   ✨ Features | 新功能

* **表单:**  表单格式配置 ([4540ab1](https://gitee.com/JS-VeryHappy/vh-admin/commit/4540ab1f4debbcd43b6db116df5e5458ac1d276c))



### [2.0.51](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.50...@vh-admin/pro-components@2.0.51) (2023-04-23)


###   ✨ Features | 新功能

* **表单:**  表单格式配置 ([f37d5b9](https://gitee.com/JS-VeryHappy/vh-admin/commit/f37d5b909f0bee27d0f6e52c559359f6775fcb6a))
* **表单:**  表单格式配置 ([3b34cd9](https://gitee.com/JS-VeryHappy/vh-admin/commit/3b34cd984b5d7b73bb23195444c5a40ef0e97de7))
* **表单:**  treeSelect 增加远程请求 ([75d1db6](https://gitee.com/JS-VeryHappy/vh-admin/commit/75d1db66ee144ca09f7526cdbfecc08e0d18ea05))



### [2.0.50](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.49...@vh-admin/pro-components@2.0.50) (2023-04-21)


###   ✨ Features | 新功能

* **表单:**  treeSelect 增加远程请求 ([13448ac](https://gitee.com/JS-VeryHappy/vh-admin/commit/13448acf276fef454d5ca811474d8ed65d8c0af8))



### [2.0.49](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.48...@vh-admin/pro-components@2.0.49) (2023-04-21)


###   ✨ Features | 新功能

* **表单:**  优化types子项提示 ([6046173](https://gitee.com/JS-VeryHappy/vh-admin/commit/60461733e0efd04175bf1cd56e3006417bc454c4))
* **表单:**  treeSelect 增加远程请求 ([4b9affa](https://gitee.com/JS-VeryHappy/vh-admin/commit/4b9affa93beed203a8219195e0569f8ce2a6a7be))



### [2.0.48](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.47...@vh-admin/pro-components@2.0.48) (2023-04-21)


###   ✨ Features | 新功能

* **表单:**  高级字段的应用 ([c838d43](https://gitee.com/JS-VeryHappy/vh-admin/commit/c838d43fa22bde78987fa5651b5864fd7b1496d0))
* **表单:**  高级字段的应用 ([1bb751d](https://gitee.com/JS-VeryHappy/vh-admin/commit/1bb751dcb8742dadd5f727c4feaa3ca8ba89318a))



### [2.0.47](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.46...@vh-admin/pro-components@2.0.47) (2023-04-21)


###   ✨ Features | 新功能

* **表单:**  高级字段的应用 ([ec6900c](https://gitee.com/JS-VeryHappy/vh-admin/commit/ec6900c8f0350957fe81698a604c5ffda56ec758))



### [2.0.46](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.45...@vh-admin/pro-components@2.0.46) (2023-04-21)


###   ✨ Features | 新功能

* **表单:**  高级字段的应用 ([7ba8a9e](https://gitee.com/JS-VeryHappy/vh-admin/commit/7ba8a9e693fe3e397efd35b1c79a7abd052f75ab))
* **表单:**  高级字段的应用 ([88514b9](https://gitee.com/JS-VeryHappy/vh-admin/commit/88514b986cee728c6bb50939111ca709c41b5b42))
* **表单:**  高级字段的应用 ([1a223a8](https://gitee.com/JS-VeryHappy/vh-admin/commit/1a223a827a9c33da04efd6aaffa86c40e90e3216))
* **表单:**  高级字段的应用 ([ac4a9c4](https://gitee.com/JS-VeryHappy/vh-admin/commit/ac4a9c40e17e22be9c0693cbfbb795e97d37411f))
* **表单:**  高级字段的应用 ([f6e88d3](https://gitee.com/JS-VeryHappy/vh-admin/commit/f6e88d34919727413c8637b4a3e635aafe996507))



### [2.0.45](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.44...@vh-admin/pro-components@2.0.45) (2023-04-20)


###   ✨ Features | 新功能

* **表单:**  高级字段的应用 ([d6a4f00](https://gitee.com/JS-VeryHappy/vh-admin/commit/d6a4f0050ffd84bdc3ceefc58feeba2503f435e9))



### [2.0.44](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.43...@vh-admin/pro-components@2.0.44) (2023-04-20)


###   ✨ Features | 新功能

* **表单:**  SelectInput ([d4eb140](https://gitee.com/JS-VeryHappy/vh-admin/commit/d4eb140c1822b015765ec420dabf648cf2bb2c0a))



### [2.0.43](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.42...@vh-admin/pro-components@2.0.43) (2023-04-20)


###   ✨ Features | 新功能

* **表单:**  优化 ([9c513a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/9c513a6612fb452b86fb607b87d0664007a136de))
* **表单:**  SelectInput ([f43ba28](https://gitee.com/JS-VeryHappy/vh-admin/commit/f43ba283c199c21158a74a509f4371c5af4fe7e9))



### [2.0.42](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.41...@vh-admin/pro-components@2.0.42) (2023-04-20)


###   ✨ Features | 新功能

* **表单:**  优化 ([3650051](https://gitee.com/JS-VeryHappy/vh-admin/commit/3650051745e2ee99ef05ea2899ee18295bdb16f2))



### [2.0.41](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.40...@vh-admin/pro-components@2.0.41) (2023-04-19)


###   ✨ Features | 新功能

* **表单:**  优化 ([5d0158c](https://gitee.com/JS-VeryHappy/vh-admin/commit/5d0158cc04a37ad7056ee7bbe696d95aa56c8d5e))



### [2.0.40](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.39...@vh-admin/pro-components@2.0.40) (2023-04-19)


###   ✨ Features | 新功能

* **表单:**  优化 ([46d8012](https://gitee.com/JS-VeryHappy/vh-admin/commit/46d80122e885a72633186c40867d371b44880400))
* **表单:**  优化 ([23f7a5e](https://gitee.com/JS-VeryHappy/vh-admin/commit/23f7a5edbe5140767c3f9f544f7e861255b5211d))
* **表单:**  优化 ([0c31541](https://gitee.com/JS-VeryHappy/vh-admin/commit/0c31541a22a39084e5a09aebca8c38643ace89e7))



### [2.0.39](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.38...@vh-admin/pro-components@2.0.39) (2023-04-19)


###   ✨ Features | 新功能

* 表单组件InputNumberSelect优化 ([74f974e](https://gitee.com/JS-VeryHappy/vh-admin/commit/74f974e58877ef9158552066bec82c711f08f27b))
* **表格:**  高度计算优化 ([2ec9526](https://gitee.com/JS-VeryHappy/vh-admin/commit/2ec9526d4dcb1533aa0c570281c5d9e60b55f269))



### [2.0.38](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.37...@vh-admin/pro-components@2.0.38) (2023-04-18)


###   ✨ Features | 新功能

* **表单编辑:**  增加组件 ([74ca0e0](https://gitee.com/JS-VeryHappy/vh-admin/commit/74ca0e0e11d151e9c75e18bc9f614d66f101892c))
* 表单组件InputNumberSelect ([b9d8af7](https://gitee.com/JS-VeryHappy/vh-admin/commit/b9d8af75319e2bbb1b76b6132ea80cad3e80703a))
* **表单编辑:**  增加组件 ([086ba7b](https://gitee.com/JS-VeryHappy/vh-admin/commit/086ba7b96f2af2c52f8c4792263eb67252dd6f29))
* **表单编辑:**  增加组件 ([cd95b0f](https://gitee.com/JS-VeryHappy/vh-admin/commit/cd95b0f1a94ae023122d52cae6249d994cdf13a4))
* **表单编辑:**  增加组件 ([59b32cd](https://gitee.com/JS-VeryHappy/vh-admin/commit/59b32cd4f48733afdafaa00e1ec80db87f559d40))
* **表单编辑:**  增加组件 ([17574af](https://gitee.com/JS-VeryHappy/vh-admin/commit/17574afbe2b290fc7cc4a6160c32d447179e9450))
* **表单编辑:**  增加组件 ([0772b8c](https://gitee.com/JS-VeryHappy/vh-admin/commit/0772b8c3fcd815d3e4f12e57ba3f563ce60f7c86))
* 表单组件InputNumberSelect ([6f0644f](https://gitee.com/JS-VeryHappy/vh-admin/commit/6f0644fb1fd2405e0a8ea60a00ec2070e2994afe))
* **表单编辑:**  优化 ([22215c3](https://gitee.com/JS-VeryHappy/vh-admin/commit/22215c303f25ab9cc012b2cc6f75fd0ec4edf1a7))
* **表单设计器:**  创建 ([b8b2132](https://gitee.com/JS-VeryHappy/vh-admin/commit/b8b2132fd41aac56e70111efef7c4c4b7fbffe02))




### [2.0.37](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.36...@vh-admin/pro-components@2.0.37) (2023-04-13)


###   ✨ Features | 新功能

* **通用:**  增加配置导入方法 ([2a61d0f](https://gitee.com/JS-VeryHappy/vh-admin/commit/2a61d0fa6428441a10480a30faec6fd008ab6d7b))



### [2.0.36](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.35...@vh-admin/pro-components@2.0.36) (2023-04-11)


###   ✨ Features | 新功能

* **表单:**  增加下拉选择表格组件 ([584ed7f](https://gitee.com/JS-VeryHappy/vh-admin/commit/584ed7f438726ccaca25b76a9d8b19c6732d334c))
* **表单:**  treeCheckable 无法清空 ([c159dbb](https://gitee.com/JS-VeryHappy/vh-admin/commit/c159dbbc9dcf14e9cbd67189d60d4f0b0ef58ead))
* **表单:**  treeCheckable 无法清空 ([7634885](https://gitee.com/JS-VeryHappy/vh-admin/commit/7634885435801b31de9ffcf47058bafeacadf095))
* **表单:**  treeCheckable 无法清空 ([915e3c3](https://gitee.com/JS-VeryHappy/vh-admin/commit/915e3c3cd42ce9049034893bcd457fd0b977d7aa))
* **表单:**  treeCheckable 无法清空 ([437f527](https://gitee.com/JS-VeryHappy/vh-admin/commit/437f527447c9af5fbde0d535f88d04322a3dc262))
* **通用:**  按钮倒计时 ([dabb180](https://gitee.com/JS-VeryHappy/vh-admin/commit/dabb180c251d3e18e09a07460014a03e76f3bb1f))
* **通用:**  按钮倒计时 ([acbce4e](https://gitee.com/JS-VeryHappy/vh-admin/commit/acbce4eb7586151174b206a5844f3b93d0ef2cb3))
* **通用:**  按钮倒计时 ([75ae3d0](https://gitee.com/JS-VeryHappy/vh-admin/commit/75ae3d03e4a25af4822da3700e6c40c3550ec519))
* **通用:**  打字机 ([40c4893](https://gitee.com/JS-VeryHappy/vh-admin/commit/40c4893e04e7fb1412f7585c8822bbbd64ad2c04))
* **通用:**  数字计时器 ([99994e6](https://gitee.com/JS-VeryHappy/vh-admin/commit/99994e6bba9bbe91f1a62bab645a16d488a4c6d5))
* **通用:**  文件预览器 ([6e1ef25](https://gitee.com/JS-VeryHappy/vh-admin/commit/6e1ef25437682138fdc4129b84a784b31a782bcb))
* **docs:**  文档更新 ([0dd7e63](https://gitee.com/JS-VeryHappy/vh-admin/commit/0dd7e63bfd190b75a76fb31cd2840317988288b7))
* **docs:**  文档更新 ([36db715](https://gitee.com/JS-VeryHappy/vh-admin/commit/36db715982b88666f6c23f6c4cd8ed23b79ac1bf))
* **docs:**  文档更新 ([55d9bec](https://gitee.com/JS-VeryHappy/vh-admin/commit/55d9bec1c9942c1b69e3bc15f6641e8ead5058cd))
* **docs:**  文档更新 ([16f1711](https://gitee.com/JS-VeryHappy/vh-admin/commit/16f17112542827e554138f02e0275cbe4491be45))
* **docs:**  文档更新 ([d24796d](https://gitee.com/JS-VeryHappy/vh-admin/commit/d24796d8299afd3788dc784531c9683f695702b8))
* **docs:**  文档更新 ([358ff28](https://gitee.com/JS-VeryHappy/vh-admin/commit/358ff28e65142aac1dff73642817af52c5462a13))
* **docs:**  文档更新 ([941f18c](https://gitee.com/JS-VeryHappy/vh-admin/commit/941f18c12b8f9155b74c9aec8ebbaccd6b72ea83))
* **docs:**  文档更新 ([4add54c](https://gitee.com/JS-VeryHappy/vh-admin/commit/4add54c7c892e66d121329f7666150a9f85503a5))
* **docs:**  文档更新 ([5d3659b](https://gitee.com/JS-VeryHappy/vh-admin/commit/5d3659b87877251d2367bfd7210123edd8daa94b))
* **docs:**  文档更新 ([68d0e60](https://gitee.com/JS-VeryHappy/vh-admin/commit/68d0e60796a249d67dbafe1536fb681d4eab2661))
* **docs:**  文档更新 ([744b174](https://gitee.com/JS-VeryHappy/vh-admin/commit/744b174318abbfd4ed5e4c7d59638a1c6bec38a1))
* **docs:**  文档更新 ([dc133e0](https://gitee.com/JS-VeryHappy/vh-admin/commit/dc133e0d4fd3bdb66a34e7a0001c8fb853183f48))
* **docs:**  文档更新 ([e0c9101](https://gitee.com/JS-VeryHappy/vh-admin/commit/e0c910123bdf3a784c2ef3333903246f0645e4e8))
* **docs:**  文档更新 ([5e53891](https://gitee.com/JS-VeryHappy/vh-admin/commit/5e53891be0cb2b6509c5ff069f344c40edecc064))
* **docs:**  文档更新 ([9e880f8](https://gitee.com/JS-VeryHappy/vh-admin/commit/9e880f8fe3658a3de0ec161b305e534e2297604f))
* **docs:**  文档更新 ([66734b0](https://gitee.com/JS-VeryHappy/vh-admin/commit/66734b05cceb3b0447d7dee025f4f64e895d72a0))




### [2.0.35](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.34...@vh-admin/pro-components@2.0.35) (2023-03-10)

### ✨ Features | 新功能

- **表单:** 升级 icon 依赖 ([11562b8](https://gitee.com/JS-VeryHappy/vh-admin/commit/11562b8197ebce1c604f0e40e302faedb69f4b5e))
- **表单:** treeCheckable 无法清空 ([2ff4bce](https://gitee.com/JS-VeryHappy/vh-admin/commit/2ff4bce9734be5e3bcdeeba8a759b2beb7cfc012))

### [2.0.34](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.33...@vh-admin/pro-components@2.0.34) (2023-02-24)

### ✨ Features | 新功能

- **表单:** 上传文件转义 ([2a7db7f](https://gitee.com/JS-VeryHappy/vh-admin/commit/2a7db7f7a0b1c1793b28d5bdffc2a3682643f6a9))

### [2.0.33](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.32...@vh-admin/pro-components@2.0.33) (2023-02-24)

### ✨ Features | 新功能

- **表单:** 上传文件转义 ([acdc2eb](https://gitee.com/JS-VeryHappy/vh-admin/commit/acdc2ebe368653f435920b275971ca607739e63e))

### [2.0.32](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.31...@vh-admin/pro-components@2.0.32) (2023-02-17)

### ✨ Features | 新功能

- **表单:** 上传文件转义 ([4632121](https://gitee.com/JS-VeryHappy/vh-admin/commit/4632121267f388ffdfa118fe700fc0178948283d))

### [2.0.31](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.30...@vh-admin/pro-components@2.0.31) (2023-02-10)

### ✨ Features | 新功能

- **表单:** 上传大小提示优化 ([03d7a67](https://gitee.com/JS-VeryHappy/vh-admin/commit/03d7a670e77d6b683645651bebd4fd69f6e579b4))
- **表单:** 上传大小提示优化 ([075779d](https://gitee.com/JS-VeryHappy/vh-admin/commit/075779d508434e55743a7c7bc24cd90fc4a0b31d))

### [2.0.30](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.29...@vh-admin/pro-components@2.0.30) (2023-02-02)

### ✨ Features | 新功能

- **表格:** 动态计算高度增加配置保底值 ([e2c4888](https://gitee.com/JS-VeryHappy/vh-admin/commit/e2c4888d5990c1c3fce7591f3f244ff23d579209))

### [2.0.29](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.28...@vh-admin/pro-components@2.0.29) (2023-02-02)

### ✨ Features | 新功能

- **表格:** 动态计算高度增加配置保底值 ([eb83a75](https://gitee.com/JS-VeryHappy/vh-admin/commit/eb83a75bd7fff248402894f5b8c962594cd49791))

### [2.0.28](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.27...@vh-admin/pro-components@2.0.28) (2023-01-11)

### ✨ Features | 新功能

- **表格:** 优化表格的 formRef ([e328fb9](https://gitee.com/JS-VeryHappy/vh-admin/commit/e328fb979e82a2d0750fdae036427fa46e248ea9))

### [2.0.27](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.26...@vh-admin/pro-components@2.0.27) (2023-01-10)

### ✨ Features | 新功能

- **表格:** 优化增加分组情况 ([15b7c89](https://gitee.com/JS-VeryHappy/vh-admin/commit/15b7c898cb77821e25ab4a4b0e1e9e7b507bd8e8))
- **表格:** 优化增加分组情况 ([c4350ef](https://gitee.com/JS-VeryHappy/vh-admin/commit/c4350ef15f7762245e6d33f0dee82caa60901d88))

### [2.0.26](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.25...@vh-admin/pro-components@2.0.26) (2023-01-10)

### ✨ Features | 新功能

- **表格:** 优化增加分组情况 ([9f878bb](https://gitee.com/JS-VeryHappy/vh-admin/commit/9f878bbdd224099e9a10597d4d5488f91c0dbba5))

### [2.0.25](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.24...@vh-admin/pro-components@2.0.25) (2023-01-10)

### ✨ Features | 新功能

- **表格:** 优化增加分组情况 ([92ad2d2](https://gitee.com/JS-VeryHappy/vh-admin/commit/92ad2d24c8209f89ad61aaea75ba6739b2fdef66))

### [2.0.24](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.23...@vh-admin/pro-components@2.0.24) (2023-01-09)

### ✨ Features | 新功能

- **表格:** 搜索去空格 ([0eda0ff](https://gitee.com/JS-VeryHappy/vh-admin/commit/0eda0ffb74a248a0182d3a599aa1445dcc46902b))

### [2.0.23](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.22...@vh-admin/pro-components@2.0.23) (2023-01-05)

### ✨ Features | 新功能

- **文档:** 增加说明 ([8f7d4e3](https://gitee.com/JS-VeryHappy/vh-admin/commit/8f7d4e303b08d2838c2f3e5b6961fc00b8ad02b4))
- **文档:** 增加说明 ([e7608f6](https://gitee.com/JS-VeryHappy/vh-admin/commit/e7608f62ac5e45d6658679705812912724dabb81))

### [2.0.22](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.21...@vh-admin/pro-components@2.0.22) (2023-01-05)

### ✨ Features | 新功能

- **表格:** 文档修改 ([55f7e04](https://gitee.com/JS-VeryHappy/vh-admin/commit/55f7e0478b6ed7ed80990c780663121726724bab))
- **表格单元格:** 增加单元格使用内置功能 ([874aae1](https://gitee.com/JS-VeryHappy/vh-admin/commit/874aae1f6a2a82491d2f8086d53c5ff118ab943c))

### [2.0.21](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.20...@vh-admin/pro-components@2.0.21) (2023-01-05)

### ✨ Features | 新功能

- **表格:** 单元格下载组件 ([845b87c](https://gitee.com/JS-VeryHappy/vh-admin/commit/845b87cbbf69b5c7c2633f91b0621246d5226dfc))

### [2.0.20](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.19...@vh-admin/pro-components@2.0.20) (2023-01-05)

### ✨ Features | 新功能

- **表格:** 单元格下载组件 ([8f7fea4](https://gitee.com/JS-VeryHappy/vh-admin/commit/8f7fea41d91d91e1cf42c740e19a338bcec89623))

### [2.0.19](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.18...@vh-admin/pro-components@2.0.19) (2022-12-30)

### ✨ Features | 新功能

- **选择项目:** 优化动画 ([8e50f45](https://gitee.com/JS-VeryHappy/vh-admin/commit/8e50f453abc6252f5baf511f98cdd9c9e0908af6))

### [2.0.18](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.17...@vh-admin/pro-components@2.0.18) (2022-12-29)

### ✨ Features | 新功能

- **表格:** 增 VhAdminTypes ([e90d166](https://gitee.com/JS-VeryHappy/vh-admin/commit/e90d166be80162afae255f0e7b03a16f5263bae1))

### [2.0.17](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.16...@vh-admin/pro-components@2.0.17) (2022-12-29)

### ✨ Features | 新功能

- **表格:** 增 VhAdminTypes ([feb1269](https://gitee.com/JS-VeryHappy/vh-admin/commit/feb1269686e41fcf93d35edbb649907e7c5a81f3))

### [2.0.16](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.15...@vh-admin/pro-components@2.0.16) (2022-12-29)

### ✨ Features | 新功能

- **表格:** 增 VhAdminTypes ([3036891](https://gitee.com/JS-VeryHappy/vh-admin/commit/3036891cf743b44571ddd0b3966275f535243e5b))

### [2.0.15](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.14...@vh-admin/pro-components@2.0.15) (2022-12-29)

### ✨ Features | 新功能

- **表格:** 单元格格式例子 ([b519de9](https://gitee.com/JS-VeryHappy/vh-admin/commit/b519de9cea7a36ed15400c2049e04913a3e76510))
- **表格:** 增 VhAdminTypes ([658ce40](https://gitee.com/JS-VeryHappy/vh-admin/commit/658ce40af7fe5bf1b9944c03039c499caf3a709e))

### [2.0.14](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.13...@vh-admin/pro-components@2.0.14) (2022-12-23)

### ✨ Features | 新功能

- **多选组件:** maxTagCount 修改默认为 1 ([8229db8](https://gitee.com/JS-VeryHappy/vh-admin/commit/8229db8a23cffa7e594b029093993cde0b4a16c8))

### [2.0.13](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.12...@vh-admin/pro-components@2.0.13) (2022-12-22)

### ✨ Features | 新功能

- **单元格样式:** 增加图片预览 ([6c4ab8c](https://gitee.com/JS-VeryHappy/vh-admin/commit/6c4ab8c6476da743283466bc3212a30dec41302b))

### [2.0.12](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.11...@vh-admin/pro-components@2.0.12) (2022-12-09)

### ✨ Features | 新功能

- **日期多选:** 增加默认值参数 ([d7ff551](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7ff551cda96faaf7966a7a656a90849cd511e93))

### [2.0.11](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.10...@vh-admin/pro-components@2.0.11) (2022-12-09)

### ✨ Features | 新功能

- **表格:** 增加记录返回值 ([218d3a8](https://gitee.com/JS-VeryHappy/vh-admin/commit/218d3a82f9b75fdfb80dd8e95f6e334bbb4d4965))

### [2.0.10](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.9...@vh-admin/pro-components@2.0.10) (2022-12-07)

### ✨ Features | 新功能

- **表格:** 默认值增强 ([be3a9ea](https://gitee.com/JS-VeryHappy/vh-admin/commit/be3a9eac597496338dcc6d7ce8209939958ed6aa))

### [2.0.9](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.8...@vh-admin/pro-components@2.0.9) (2022-12-06)

### ✨ Features | 新功能

- **表格:** 默认值增强 ([cb4d54f](https://gitee.com/JS-VeryHappy/vh-admin/commit/cb4d54fc5d71590a0fe5e45647f5bf8d2c35603e))

### [2.0.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.7...@vh-admin/pro-components@2.0.8) (2022-12-06)

### ✨ Features | 新功能

- **表格:** ts 增强 ([d658689](https://gitee.com/JS-VeryHappy/vh-admin/commit/d6586898a47adc36fe631c43a240337375f1cd00))
- **表格:** ts 增强 ([4d4b61c](https://gitee.com/JS-VeryHappy/vh-admin/commit/4d4b61ce9d6362adeb1b682ef66e66b5259c2838))

### [2.0.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.6...@vh-admin/pro-components@2.0.7) (2022-12-06)

### ✨ Features | 新功能

- **表单:** 增加样式 ([b24f795](https://gitee.com/JS-VeryHappy/vh-admin/commit/b24f795a727bed9c2b1f2840da4caf8a60e6de0d))
- **表格:** 汇总婆优化 ([3319d4d](https://gitee.com/JS-VeryHappy/vh-admin/commit/3319d4d6c59f226177342467520633dfce0dcccd))

### [2.0.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.5...@vh-admin/pro-components@2.0.6) (2022-11-30)

### ✨ Features | 新功能

- **表格:** 汇总婆优化 ([f6fd3b7](https://gitee.com/JS-VeryHappy/vh-admin/commit/f6fd3b75c036d6e09ea50ebdb26eb0672c36cc5c))
- **表格:** 汇总婆优化 ([bf47d0a](https://gitee.com/JS-VeryHappy/vh-admin/commit/bf47d0aa2c8c5e8d5ec4ae7f8db6ff005317179c))

### [2.0.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.4...@vh-admin/pro-components@2.0.5) (2022-11-24)

### ✨ Features | 新功能

- **升级:** 修改颜色 ([cefba63](https://gitee.com/JS-VeryHappy/vh-admin/commit/cefba6357ec23a4470065802343272f6b5d6ef7a))

### [2.0.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.3...@vh-admin/pro-components@2.0.4) (2022-11-23)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([7e48c40](https://gitee.com/JS-VeryHappy/vh-admin/commit/7e48c408bd9d020bd2bbcddb6bad26b53bc53de5))

### [2.0.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.2...@vh-admin/pro-components@2.0.3) (2022-11-22)

### 🎫 Chores | 其他更新

- **release:** publish ([5c99162](https://gitee.com/JS-VeryHappy/vh-admin/commit/5c99162f0e354f4cfc7388ff7c4597581dd1175d))

### ✨ Features | 新功能

- **表单:** 优化弹窗 ([a7fe54d](https://gitee.com/JS-VeryHappy/vh-admin/commit/a7fe54d2f808255b6d00e6fa51b0766544e590a5))
- **表单:** dumi2 升级 ([f196138](https://gitee.com/JS-VeryHappy/vh-admin/commit/f1961381e1b9cb5de912cad196e55e59dbae39f6))

### [1.0.15](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.14...@vh-admin/pro-components@1.0.15) (2022-11-17)

### ✨ Features | 新功能

- **表单:** 优化弹窗 ([a7fe54d](https://gitee.com/JS-VeryHappy/vh-admin/commit/a7fe54d2f808255b6d00e6fa51b0766544e590a5))
- **表单:** dumi2 升级 ([82f3629](https://gitee.com/JS-VeryHappy/vh-admin/commit/82f3629ea7efbee2ebf7f4f07b3309844b9aaedb))

### [2.0.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@2.0.1...@vh-admin/pro-components@2.0.2) (2022-11-22)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([dd05540](https://gitee.com/JS-VeryHappy/vh-admin/commit/dd0554083882eb693a85874ffbfaaed5db1769a8))
- **表单:** dumi2 升级 ([5b19683](https://gitee.com/JS-VeryHappy/vh-admin/commit/5b196839e5c543462939aeb3de3542f65a86de6a))
- **表单:** dumi2 升级 ([d77c5c3](https://gitee.com/JS-VeryHappy/vh-admin/commit/d77c5c310b0e58661be2d3c88a7f2c8f7dc7cb55))
- **表单:** dumi2 升级 ([a150f95](https://gitee.com/JS-VeryHappy/vh-admin/commit/a150f9516b23e1f76e572419e731ccaf9708630b))
- **表单:** dumi2 升级 ([635cbe3](https://gitee.com/JS-VeryHappy/vh-admin/commit/635cbe30abce6098b7111018cda14c97ca5bef6e))
- **表单:** dumi2 升级 ([cb23ea6](https://gitee.com/JS-VeryHappy/vh-admin/commit/cb23ea6aea0a2d517a6c0af3628e53fde591e78c))

### [2.0.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.14...@vh-admin/pro-components@2.0.1) (2022-11-16)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([546b9da](https://gitee.com/JS-VeryHappy/vh-admin/commit/546b9daecdf31aeb4244db821445ae7a02ee57f9))
- **表单:** dumi2 升级 ([bc48ac2](https://gitee.com/JS-VeryHappy/vh-admin/commit/bc48ac294281472f97533a2ded6b564eec942b1e))
- **表单:** dumi2 升级 ([ac8ebc1](https://gitee.com/JS-VeryHappy/vh-admin/commit/ac8ebc1a4503e5e630aed46be6dcfec3bafb9d89))
- **表单:** dumi2 升级 ([fbc1fcb](https://gitee.com/JS-VeryHappy/vh-admin/commit/fbc1fcbee769b484098cade04b01a3b0d71eadce))
- **表单:** dumi2 升级 ([b826ea8](https://gitee.com/JS-VeryHappy/vh-admin/commit/b826ea8821cfffa2b900e7578713551f23307160))
- **表单:** dumi2 升级 ([f64e5cc](https://gitee.com/JS-VeryHappy/vh-admin/commit/f64e5ccb8df9e587ae170029c95d59107c0b8d79))
- **表单:** dumi2 升级 ([233533a](https://gitee.com/JS-VeryHappy/vh-admin/commit/233533a6f0b380c1337eb4edb03e75d712769d4e))
- **表单:** dumi2 升级 ([6a5503d](https://gitee.com/JS-VeryHappy/vh-admin/commit/6a5503d7be79eec098f21aabb0fc90137177d232))
- **表单:** dumi2 升级 ([8072120](https://gitee.com/JS-VeryHappy/vh-admin/commit/8072120129180662f713952d92f139d416ace5e5))
- **表单:** dumi2 升级 ([82f3629](https://gitee.com/JS-VeryHappy/vh-admin/commit/82f3629ea7efbee2ebf7f4f07b3309844b9aaedb))
- **表单:** dumi2 升级 ([8d94a4a](https://gitee.com/JS-VeryHappy/vh-admin/commit/8d94a4ac978d7458e4fc76c5197e309e9a518c9d))

### [1.0.14](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.13...@vh-admin/pro-components@1.0.14) (2022-11-03)

### ✨ Features | 新功能

- **表单:** 导入提示 ([897ce2a](https://gitee.com/JS-VeryHappy/vh-admin/commit/897ce2a32632398258ef9867629c86c39da02480))

### [1.0.13](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.12...@vh-admin/pro-components@1.0.13) (2022-11-01)

### ✨ Features | 新功能

- **表单:** 分页保留选择 ([aff0b9c](https://gitee.com/JS-VeryHappy/vh-admin/commit/aff0b9c9456c90960558392a282d071c5ab2b47f))

### [1.0.12](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.11...@vh-admin/pro-components@1.0.12) (2022-11-01)

### ✨ Features | 新功能

- **表单:** 上传文件错误提示 ([dd25c59](https://gitee.com/JS-VeryHappy/vh-admin/commit/dd25c59848323b4cb9342435c3fa06aa9fcc5c02))
- **表单:** 上传文件错误提示 ([54a1a59](https://gitee.com/JS-VeryHappy/vh-admin/commit/54a1a590b2fbe94bf8012d9c9932b92400013643))

### [1.0.11](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.10...@vh-admin/pro-components@1.0.11) (2022-10-26)

### ✨ Features | 新功能

- **表单:** 优化上传 ([5bdc68e](https://gitee.com/JS-VeryHappy/vh-admin/commit/5bdc68e565dadc8bd9611495d55fbc0cac46c465))

### [1.0.10](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.9...@vh-admin/pro-components@1.0.10) (2022-10-19)

### ✨ Features | 新功能

- **表单:** 修复表格自动高度问题 ([957f521](https://gitee.com/JS-VeryHappy/vh-admin/commit/957f521f3107f78264c9db75a03eb1a63ea0681e))
- **表单:** 修复表格自动高度问题 ([d3e31b7](https://gitee.com/JS-VeryHappy/vh-admin/commit/d3e31b7dd0fb1920df303a95a9137e734917e067))

### [1.0.9](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.8...@vh-admin/pro-components@1.0.9) (2022-09-29)

### ✨ Features | 新功能

- **表单:** 表单支持定义单元格格式 ([356fb38](https://gitee.com/JS-VeryHappy/vh-admin/commit/356fb383dcb3122b6c5619cc3e57bcdc26017497))
- **表单:** 移动端打开支持 ([d7f265b](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7f265b859e66f2caffc7007123f77752621e224))

### [1.0.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.7...@vh-admin/pro-components@1.0.8) (2022-09-28)

### ✨ Features | 新功能

- **表单:** 表单支持定义单元格格式 ([05e8184](https://gitee.com/JS-VeryHappy/vh-admin/commit/05e8184a7db2fc59308d0b085d1340954cb88509))

### [1.0.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.6...@vh-admin/pro-components@1.0.7) (2022-09-28)

### ✨ Features | 新功能

- **表单:** 表单支持 dependency 联动 ([c63926a](https://gitee.com/JS-VeryHappy/vh-admin/commit/c63926a18db99bc57dd4924bec0b60d3c54e9e17))
- **表单:** 表单支持 dependency 联动 ([d82dea5](https://gitee.com/JS-VeryHappy/vh-admin/commit/d82dea501ed5d319f942a42ddf8d07414a446ef6))

### [1.0.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.5...@vh-admin/pro-components@1.0.6) (2022-09-28)

### ✨ Features | 新功能

- **表单:** 表单支持 dependency 联动 ([f7a29bb](https://gitee.com/JS-VeryHappy/vh-admin/commit/f7a29bb1bcca0234d32d3aee1366114693af8bed))

### [1.0.5](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.4...@vh-admin/pro-components@1.0.5) (2022-09-22)

### ✨ Features | 新功能

- **表单:** 升级 UI 组件 ([f049799](https://gitee.com/JS-VeryHappy/vh-admin/commit/f0497998b62a038697517c63de1aeb3897db88fc))

### [1.0.4](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.3...@vh-admin/pro-components@1.0.4) (2022-09-21)

### ✨ Features | 新功能

- **表单:** 升级 UI 组件 ([b36bfc1](https://gitee.com/JS-VeryHappy/vh-admin/commit/b36bfc113487df248861e6c4b0b778f657f4d80d))

### [1.0.3](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.2...@vh-admin/pro-components@1.0.3) (2022-09-20)

### ✨ Features | 新功能

- **表单:** 升级 UI 组件 ([f13716f](https://gitee.com/JS-VeryHappy/vh-admin/commit/f13716f0cd81a57cdc19f18bf656f850f9f98808))

### [1.0.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@1.0.1...@vh-admin/pro-components@1.0.2) (2022-09-19)

### ✨ Features | 新功能

- **表单:** 升级 UI 组件 ([da8dd7e](https://gitee.com/JS-VeryHappy/vh-admin/commit/da8dd7e3927ed6a8380d447a9c33320a835caf92))

### [1.0.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.90...@vh-admin/pro-components@1.0.1) (2022-09-19)

### ✨ Features | 新功能

- **表单:** 表单支持 dependency 联动 ([fe1d6b9](https://gitee.com/JS-VeryHappy/vh-admin/commit/fe1d6b95d06c001844b81490c4b2547eb983ccb2))
- **表单:** 升级 UI 组件 ([f80da93](https://gitee.com/JS-VeryHappy/vh-admin/commit/f80da93bb5b59d6507e630a918d07c5f6b263e4b))

### [0.0.90](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.89...@vh-admin/pro-components@0.0.90) (2022-09-16)

### ✨ Features | 新功能

- **表单:** 表单支持 dependency 联动 ([b6d5383](https://gitee.com/JS-VeryHappy/vh-admin/commit/b6d53839c0ef28bb946d9300a711369b18164368))

### [0.0.89](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.88...@vh-admin/pro-components@0.0.89) (2022-09-16)

### ✨ Features | 新功能

- **表单:** 日期多选类型重名 ([72aa61a](https://gitee.com/JS-VeryHappy/vh-admin/commit/72aa61a4e171755087e6c9cb1da9a8627cec7eb1))

### [0.0.88](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.87...@vh-admin/pro-components@0.0.88) (2022-09-09)

### ✨ Features | 新功能

- **表格:** 统计 0 的情况 ([8530a04](https://gitee.com/JS-VeryHappy/vh-admin/commit/8530a0414e25d8b3e4525feae35e5838e26095f6))

### [0.0.87](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.86...@vh-admin/pro-components@0.0.87) (2022-09-07)

### ✨ Features | 新功能

- **表格:** 增加时间区间结尾判断 ([9d6f27a](https://gitee.com/JS-VeryHappy/vh-admin/commit/9d6f27ac361f3aef0913dfbdabc98b9ea39bd530))

### [0.0.86](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.85...@vh-admin/pro-components@0.0.86) (2022-09-01)

### ✨ Features | 新功能

- **表格:** 增加内置功能表格 ([0053b75](https://gitee.com/JS-VeryHappy/vh-admin/commit/0053b753b76ad8e18de9071cf118dce520028b48))

### [0.0.85](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.84...@vh-admin/pro-components@0.0.85) (2022-09-01)

### ✨ Features | 新功能

- **表格:** 内置功能提交增加 tableRef 返回 ([537a226](https://gitee.com/JS-VeryHappy/vh-admin/commit/537a226102bb2daedbbfaf2c500d4936738855f2))

### [0.0.84](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.83...@vh-admin/pro-components@0.0.84) (2022-08-31)

### ✨ Features | 新功能

- **表格:** 内置功能提交增加 tableRef 返回 ([bef03a1](https://gitee.com/JS-VeryHappy/vh-admin/commit/bef03a12c7ccd1d91df4326b892a849047d2ade7))
- **请求:** 下载错误 ([ea84256](https://gitee.com/JS-VeryHappy/vh-admin/commit/ea84256af32dd1a2a4544d4f574ae37e1288faab))

### [0.0.83](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.82...@vh-admin/pro-components@0.0.83) (2022-08-31)

### ✨ Features | 新功能

- **表格:** 增加汇总栏和和提示区域 ([6c2afe0](https://gitee.com/JS-VeryHappy/vh-admin/commit/6c2afe08c15a67391147c24778bed07ad9ba4f70))
- **表格:** 增加汇总栏和和提示区域 ([fae6218](https://gitee.com/JS-VeryHappy/vh-admin/commit/fae62180e15117756f8d0861711c532f870a0098))

### [0.0.82](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.81...@vh-admin/pro-components@0.0.82) (2022-08-31)

### ✨ Features | 新功能

- **表格:** 增加汇总栏和和提示区域 ([8fea090](https://gitee.com/JS-VeryHappy/vh-admin/commit/8fea0906df0fa8f2ad53e5707a5cc88d0c03fccf))

### [0.0.81](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.80...@vh-admin/pro-components@0.0.81) (2022-08-24)

### ✨ Features | 新功能

- **弹窗:** 修改使用了 umi ([95d2012](https://gitee.com/JS-VeryHappy/vh-admin/commit/95d2012f1fbf187a8548477daee3cc77d3bb78fe))

### [0.0.80](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.79...@vh-admin/pro-components@0.0.80) (2022-08-17)

### ✨ Features | 新功能

- **弹窗:** html 宽度 ([c17be39](https://gitee.com/JS-VeryHappy/vh-admin/commit/c17be39a2080759ee3f42078d927b6e69ae8681d))

### [0.0.79](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.78...@vh-admin/pro-components@0.0.79) (2022-07-13)

### 🐛 Bug Fixes | Bug 修复

- **项目多选:** 判断错误 ([3513e47](https://gitee.com/JS-VeryHappy/vh-admin/commit/3513e4762e21e85cb01cb860e9c05e53d8fc191b))

### [0.0.78](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.77...@vh-admin/pro-components@0.0.78) (2022-07-05)

### 🐛 Bug Fixes | Bug 修复

- **表格:** 多选配置 ([32515de](https://gitee.com/JS-VeryHappy/vh-admin/commit/32515dec5b8a149041ccf0c392b79f172ec5b449))

### [0.0.77](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.76...@vh-admin/pro-components@0.0.77) (2022-07-05)

### 🐛 Bug Fixes | Bug 修复

- **表格:** 多选配置 ([f7d0ca9](https://gitee.com/JS-VeryHappy/vh-admin/commit/f7d0ca955c6265af5dbf4f96aa2cf9928deb681a))

### [0.0.76](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.75...@vh-admin/pro-components@0.0.76) (2022-07-04)

### 🐛 Bug Fixes | Bug 修复

- **表格:** 多选配置 ([4ae628e](https://gitee.com/JS-VeryHappy/vh-admin/commit/4ae628e2c9ba0d139ade1976b306cc2a14bf140b))

### [0.0.75](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.74...@vh-admin/pro-components@0.0.75) (2022-07-04)

### 🐛 Bug Fixes | Bug 修复

- **表格:** 多选配置 ([ec506dc](https://gitee.com/JS-VeryHappy/vh-admin/commit/ec506dcb97dd38f8d9365d7cf930a4908c748943))

### [0.0.74](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.73...@vh-admin/pro-components@0.0.74) (2022-07-01)

### 🐛 Bug Fixes | Bug 修复

- **表格:** 多选配置 ([27610d5](https://gitee.com/JS-VeryHappy/vh-admin/commit/27610d50fd279e9f6e12e2e32181c6c90ee0fab6))

### [0.0.73](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.72...@vh-admin/pro-components@0.0.73) (2022-07-01)

### 🐛 Bug Fixes | Bug 修复

- **表格:** 多选配置 ([bdb6825](https://gitee.com/JS-VeryHappy/vh-admin/commit/bdb6825545d62a3fc9be0cc34dbb0ef9d2d6538e))

### [0.0.72](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.71...@vh-admin/pro-components@0.0.72) (2022-06-24)

### 🐛 Bug Fixes | Bug 修复

- **日期类型选择:** 清空 bug ([8f661a9](https://gitee.com/JS-VeryHappy/vh-admin/commit/8f661a960674c1b1979fb5609c32646fae8276b2))

### [0.0.71](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.70...@vh-admin/pro-components@0.0.71) (2022-06-20)

### ✨ Features | 新功能

- **项目选择:** 增加上级类别显示 ([9b13014](https://gitee.com/JS-VeryHappy/vh-admin/commit/9b13014fcf406358948fd96d39544e0ea8154bd0))

### [0.0.70](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.69...@vh-admin/pro-components@0.0.70) (2022-06-20)

### ✨ Features | 新功能

- **项目选择:** 增加上级类别显示 ([ee5633b](https://gitee.com/JS-VeryHappy/vh-admin/commit/ee5633b53883a037650de10df920c285ab519cb2))

### [0.0.69](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.68...@vh-admin/pro-components@0.0.69) (2022-06-17)

### ✨ Features | 新功能

- **项目选择:** 增加上级类别显示 ([724795e](https://gitee.com/JS-VeryHappy/vh-admin/commit/724795e955808496b62f8b8ecdc687e2c2f70bfb))

### [0.0.68](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.67...@vh-admin/pro-components@0.0.68) (2022-06-17)

### ✨ Features | 新功能

- **项目选择:** 增加上级类别显示 ([c6128ae](https://gitee.com/JS-VeryHappy/vh-admin/commit/c6128aeefe134b69d265ddce36b107b3fe472f75))

### [0.0.67](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.66...@vh-admin/pro-components@0.0.67) (2022-06-17)

### ✨ Features | 新功能

- **项目选择:** 增加上级类别显示 ([1544ffb](https://gitee.com/JS-VeryHappy/vh-admin/commit/1544ffb19ff3157ed4195bb706f37a1e243a8dd5))
- **增加组件:** 日期类型选择 ([a43b103](https://gitee.com/JS-VeryHappy/vh-admin/commit/a43b103e91956dda802123236c539e09a4e0f297))

### [0.0.66](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.65...@vh-admin/pro-components@0.0.66) (2022-06-01)

### 🐛 Bug Fixes | Bug 修复

- **select:** 选中相同 ([619d80a](https://gitee.com/JS-VeryHappy/vh-admin/commit/619d80a97da963f7dcf9ee46366df1796a7ed9bb))

### [0.0.65](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.64...@vh-admin/pro-components@0.0.65) (2022-05-31)

### 🐛 Bug Fixes | Bug 修复

- **select:** 选中相同 ([6c6c809](https://gitee.com/JS-VeryHappy/vh-admin/commit/6c6c809ae6d5d15dd6ba99295ffcf3bce2074dff))

### [0.0.64](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.63...@vh-admin/pro-components@0.0.64) (2022-05-30)

### 🐛 Bug Fixes | Bug 修复

- **select:** 选中相同 ([4fe61f6](https://gitee.com/JS-VeryHappy/vh-admin/commit/4fe61f6f0145fcdded9016cbc0ed10ec21971693))

### [0.0.63](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.61...@vh-admin/pro-components@0.0.63) (2022-05-25)

### 🐛 Bug Fixes | Bug 修复

- **tree:** 修复无法选中值 ([b57d433](https://gitee.com/JS-VeryHappy/vh-admin/commit/b57d433a266db08d422d9aa5ff22b38b9086d973))

### [0.0.61](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.60...@vh-admin/pro-components@0.0.61) (2022-05-25)

### 🐛 Bug Fixes | Bug 修复

- **tree:** 修复无法选中值 ([609619e](https://gitee.com/JS-VeryHappy/vh-admin/commit/609619e8e5c2c6666840ddfa166809a425a2ec4d))
- **tree:** 修复无法选中值 ([c868f68](https://gitee.com/JS-VeryHappy/vh-admin/commit/c868f68d3e9e6e69232b3d707daf22fcbde40771))
- **tree:** 修复无法选中值 ([d0680f5](https://gitee.com/JS-VeryHappy/vh-admin/commit/d0680f588c019baa6f56aa41f9a9bff1efa4145a))
- **tree:** 修复无法选中值 ([5d74f97](https://gitee.com/JS-VeryHappy/vh-admin/commit/5d74f9728d590f6daa7e202757c56b3cf8954d2f))

### [0.0.60](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.59...@vh-admin/pro-components@0.0.60) (2022-05-25)

### 🐛 Bug Fixes | Bug 修复

- **table:** 修复窗口变化刷新数据 ([7a50aae](https://gitee.com/JS-VeryHappy/vh-admin/commit/7a50aae0dd246f304d576074689c7736fade8edf))

### [0.0.59](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.58...@vh-admin/pro-components@0.0.59) (2022-05-21)

### 🐛 Bug Fixes | Bug 修复

- **上传组件:** 修复升级遗留的 bug ([5d85d9a](https://gitee.com/JS-VeryHappy/vh-admin/commit/5d85d9aff81c37a2fa02cc48d411dd895a1df04c))

### [0.0.58](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.57...@vh-admin/pro-components@0.0.58) (2022-05-21)

### 🐛 Bug Fixes | Bug 修复

- **上传组件:** 修复升级遗留的 bug ([b20b604](https://gitee.com/JS-VeryHappy/vh-admin/commit/b20b604ac2f15f01e4f304bbfc55faa9303e5a1f))
- **上传组件:** 修复升级遗留的 bug ([1288ea8](https://gitee.com/JS-VeryHappy/vh-admin/commit/1288ea89e4cc3557c5dbf4d4d700a007cb7cd2f4))

### [0.0.57](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.56...@vh-admin/pro-components@0.0.57) (2022-05-21)

### 🐛 Bug Fixes | Bug 修复

- **上传组件:** 修复升级遗留的 bug ([9e53398](https://gitee.com/JS-VeryHappy/vh-admin/commit/9e533980138f1a92a4f841b61a5713f9222afadf))

### [0.0.56](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.55...@vh-admin/pro-components@0.0.56) (2022-05-20)

### 🐛 Bug Fixes | Bug 修复

- **上传组件:** 修复升级遗留的 bug ([201906a](https://gitee.com/JS-VeryHappy/vh-admin/commit/201906a2f4b168db4cc036f94ba2d210b12a6aab))

### [0.0.55](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.54...@vh-admin/pro-components@0.0.55) (2022-05-20)

### 🐛 Bug Fixes | Bug 修复

- **表格按钮:** 修改合并 ([e8553aa](https://gitee.com/JS-VeryHappy/vh-admin/commit/e8553aadb250cc726f3a49c3dec2ce5a5cc54465))

### [0.0.54](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.53...@vh-admin/pro-components@0.0.54) (2022-05-19)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 修改 package 依赖方式 ([e4e5064](https://gitee.com/JS-VeryHappy/vh-admin/commit/e4e5064336f6ed0b80642eccafc93cc0ae808af3))
- **高级配置:** 修改 package 依赖方式 ([c0661db](https://gitee.com/JS-VeryHappy/vh-admin/commit/c0661dbf5006ffe101f96bdcd8edc8bf06ec12c1))

### ⚡ Performance Improvements | 性能优化

- **升级 react:** 1.8.0 ([a75bd60](https://gitee.com/JS-VeryHappy/vh-admin/commit/a75bd6039afcc06fcd993e6733a60bf2b7aa07ed))

### [0.0.53](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.52...@vh-admin/pro-components@0.0.53) (2022-05-09)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 修改 package 依赖方式 ([eeca8ac](https://gitee.com/JS-VeryHappy/vh-admin/commit/eeca8ac852c1ca415d85b7159effc17d4a04fe8b))

### [0.0.52](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.51...@vh-admin/pro-components@0.0.52) (2022-05-09)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 样式错误 ([9a3ebcd](https://gitee.com/JS-VeryHappy/vh-admin/commit/9a3ebcd1868d9596a7ff0ebc01e48d66fe47d640))
- **高级配置:** 增加标签数据 ([710c4da](https://gitee.com/JS-VeryHappy/vh-admin/commit/710c4dae94236cf132bd787674b5913204a29fea))
- **高级配置:** 增加标签数据 ([4bd787d](https://gitee.com/JS-VeryHappy/vh-admin/commit/4bd787dba8b4394c5af8d7e8d7c9d7bcf14f9c01))
- **高级配置:** 增加标签数据 ([3aa996d](https://gitee.com/JS-VeryHappy/vh-admin/commit/3aa996d5ac4a778192e6f7d370fefece0f97b9b8))

### [0.0.51](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.50...@vh-admin/pro-components@0.0.51) (2022-05-05)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 样式错误 ([96cd710](https://gitee.com/JS-VeryHappy/vh-admin/commit/96cd710b5765368bfc8054a3dc5ba2677b4e788d))

### [0.0.50](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.49...@vh-admin/pro-components@0.0.50) (2022-05-05)

### 🐛 Bug Fixes | Bug 修复

- **高级配置:** 样式错误 ([53e5978](https://gitee.com/JS-VeryHappy/vh-admin/commit/53e5978090649a49729c068cde247d9fad975bb9))

### [0.0.49](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.48...@vh-admin/pro-components@0.0.49) (2022-04-28)

### ✨ Features | 新功能

- **高级配置组件:** 增加操作 ref ([1073c2b](https://gitee.com/JS-VeryHappy/vh-admin/commit/1073c2b487be6cdb3918bea3f1befa59c920aa12))
- **高级配置组件:** 增加 formref 的返回 ([ed00412](https://gitee.com/JS-VeryHappy/vh-admin/commit/ed0041230b9b83fb1d0645a8a680e50c5b0e749c))
- **高级配置组件:** npm 发布无效 ([5faa270](https://gitee.com/JS-VeryHappy/vh-admin/commit/5faa270c17ba2f10c8e441d0e832bdf6ab04be8a))

### [0.0.48](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.47...@vh-admin/pro-components@0.0.48) (2022-04-27)

### ✨ Features | 新功能

- **高级配置组件:** 增加操作 ref ([0493696](https://gitee.com/JS-VeryHappy/vh-admin/commit/0493696ebfd737fcbbea336b2da27bd81cc3e1bc))

### [0.0.47](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.46...@vh-admin/pro-components@0.0.47) (2022-04-27)

### ✨ Features | 新功能

- **高级配置组件:** 增加操作 ref ([23109f8](https://gitee.com/JS-VeryHappy/vh-admin/commit/23109f83da8acee2c266489b1ca8691a0fb30a60))

### [0.0.46](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.45...@vh-admin/pro-components@0.0.46) (2022-04-27)

### ✨ Features | 新功能

- **高级配置组件:** 增加操作 ref ([8646855](https://gitee.com/JS-VeryHappy/vh-admin/commit/86468559c5022873548ea3f2260d60eec2596621))

### [0.0.45](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.44...@vh-admin/pro-components@0.0.45) (2022-04-26)

### 🐛 Bug Fixes | Bug 修复

- **高级配置组件:** 选项删除逻辑错误 ([0616f4e](https://gitee.com/JS-VeryHappy/vh-admin/commit/0616f4e6da9ff0256e4e20fc4a229247f261b8a3))

### [0.0.44](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.43...@vh-admin/pro-components@0.0.44) (2022-04-26)

### 🐛 Bug Fixes | Bug 修复

- **高级配置组件:** 选项删除逻辑错误 ([1b81196](https://gitee.com/JS-VeryHappy/vh-admin/commit/1b81196f05369bd4bd993e7a4be8083de5d64783))
- **高级配置组件:** 选项删除逻辑错误 ([2f9abfc](https://gitee.com/JS-VeryHappy/vh-admin/commit/2f9abfce86e881ea460f8a69698c5923306b9096))

### [0.0.43](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.42...@vh-admin/pro-components@0.0.43) (2022-04-26)

### 🐛 Bug Fixes | Bug 修复

- **高级配置组件:** 提交返回当前选择的行 ([84c5376](https://gitee.com/JS-VeryHappy/vh-admin/commit/84c537629cd2830405fcd8e7031d6cbea654f714))
- **高级配置组件:** 提交返回当前选择的行 ([5d3dcd3](https://gitee.com/JS-VeryHappy/vh-admin/commit/5d3dcd34ff51ac790363102dda62b17c6de48355))
- **高级配置组件:** 提交返回当前选择的行 ([37aa4d1](https://gitee.com/JS-VeryHappy/vh-admin/commit/37aa4d18375ca17b377965b08f1a99e269aef917))
- **高级配置组件:** 选项删除逻辑错误 ([990591a](https://gitee.com/JS-VeryHappy/vh-admin/commit/990591a39e29ead42fcff2fd4fca07562d5735a4))
- **高级配置组件:** 选项删除逻辑错误 ([fd1c2d8](https://gitee.com/JS-VeryHappy/vh-admin/commit/fd1c2d8b1b21554733fda0a114ded807a32ecc4e))

### [0.0.42](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.41...@vh-admin/pro-components@0.0.42) (2022-04-25)

### 🐛 Bug Fixes | Bug 修复

- **高级配置组件:** ts 定义错误 ([0beb44c](https://gitee.com/JS-VeryHappy/vh-admin/commit/0beb44c721d159c180c5c49193dca629cf65b99c))

### [0.0.41](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.40...@vh-admin/pro-components@0.0.41) (2022-04-25)

### 🐛 Bug Fixes | Bug 修复

- **高级配置组件:** 增加额外回调属性 ([435acdd](https://gitee.com/JS-VeryHappy/vh-admin/commit/435acddb688c7c6d421886e96ac96ce08f1ac135))

### [0.0.40](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.39...@vh-admin/pro-components@0.0.40) (2022-04-25)

### 🐛 Bug Fixes | Bug 修复

- **高级配置组件:** 请求提示错误 ([2be73cf](https://gitee.com/JS-VeryHappy/vh-admin/commit/2be73cfa5446c7dad862a634d56410c32fb6a950))

### [0.0.39](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.38...@vh-admin/pro-components@0.0.39) (2022-04-25)

### 🐛 Bug Fixes | Bug 修复

- **表格组件:** 内置功能按钮 key 不唯一 ([e46cd63](https://gitee.com/JS-VeryHappy/vh-admin/commit/e46cd63b93d6628dd09b3be525fdc3185aad7dd8))

### [0.0.38](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.37...@vh-admin/pro-components@0.0.38) (2022-04-25)

### ✨ Features | 新功能

- **组件:** 项目选择增加本地历史 ([dd81229](https://gitee.com/JS-VeryHappy/vh-admin/commit/dd812295f2bed0db4e44dd5f21669764fa180da1))
- **组件:** 项目选择增加本地历史 ([540f486](https://gitee.com/JS-VeryHappy/vh-admin/commit/540f486f0156a08d4bcc156f7492febf18bf44bb))

### [0.0.37](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.36...@vh-admin/pro-components@0.0.37) (2022-04-24)

### ✨ Features | 新功能

- **组件:** 高级配置列表 ([7f00224](https://gitee.com/JS-VeryHappy/vh-admin/commit/7f00224a23c9c1d26f9f59fad1978999e71df6ac))

### [0.0.36](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.35...@vh-admin/pro-components@0.0.36) (2022-04-24)

### ✨ Features | 新功能

- **组件:** 高级配置列表 ([3caee87](https://gitee.com/JS-VeryHappy/vh-admin/commit/3caee87cf6508399898817bfaea6959abc2c36e6))
- **组件:** 高级配置列表 ([568c4cf](https://gitee.com/JS-VeryHappy/vh-admin/commit/568c4cf9c30961205333f54d2d54c84400abf5ed))
- **组件:** 高级配置列表 ([5b4ef56](https://gitee.com/JS-VeryHappy/vh-admin/commit/5b4ef56a15c4314dab8ef0a6d44852a111fe7e56))
- **组件:** 高级配置列表 ([0375d9b](https://gitee.com/JS-VeryHappy/vh-admin/commit/0375d9b2d562dd46c0f63cd0ecf97c6b03b5492b))
- **组件:** 高级配置列表 ([cf10f22](https://gitee.com/JS-VeryHappy/vh-admin/commit/cf10f22888156bf0472fb655140c7cc2da23e96e))

### [0.0.35](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.34...@vh-admin/pro-components@0.0.35) (2022-04-21)

### ✨ Features | 新功能

- **组件:** 全局项目选择器 ([92124fa](https://gitee.com/JS-VeryHappy/vh-admin/commit/92124fa2602b56615fad15b8ff9394ffdba70917))

### [0.0.34](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.33...@vh-admin/pro-components@0.0.34) (2022-04-21)

### 🐛 Bug Fixes | Bug 修复

- **表格内置组件:** select 的值无法传递 ([2a4649c](https://gitee.com/JS-VeryHappy/vh-admin/commit/2a4649cb8cb46ecaca4a6eb00555530e7286bedf))
- **表格内置组件:** select 的值无法传递 ([9aca42a](https://gitee.com/JS-VeryHappy/vh-admin/commit/9aca42a4adb868ab9ffa5f6d4916472c141512f6))
- **组件:** select 选择自动关闭 ([77f99a7](https://gitee.com/JS-VeryHappy/vh-admin/commit/77f99a7d31164b7f02b06f0177a2060946d51a3a))

### ✨ Features | 新功能

- **组件:** 导入弹窗增加表单定位 ([a9c1e94](https://gitee.com/JS-VeryHappy/vh-admin/commit/a9c1e94b9f04c6cbfeecee142e92afd7c519c3a9))
- **组件:** 全局项目选择器 ([4c8277f](https://gitee.com/JS-VeryHappy/vh-admin/commit/4c8277f0719d17a511d02ae74d6afbc472584e49))

### 🎫 Chores | 其他更新

- **合并:** branch 'review' ([e4b26dc](https://gitee.com/JS-VeryHappy/vh-admin/commit/e4b26dc4f18d64cf6b04b4271b74b017e7842e32))
- **Merge:** branch 'review' ([a6764e1](https://gitee.com/JS-VeryHappy/vh-admin/commit/a6764e125d5b65c7f9274cf3a5b02cde88d8af64))

## [0.0.33](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-components@0.0.32...@vh-admin/pro-components@0.0.33) (2022-04-10)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.32](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.31...@vh-admin/pro-components@0.0.32) (2022-04-03)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.31](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.30...@vh-admin/pro-components@0.0.31) (2022-04-02)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.30](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.29...@vh-admin/pro-components@0.0.30) (2022-04-02)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.29](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.28...@vh-admin/pro-components@0.0.29) (2022-04-01)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.28](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.27...@vh-admin/pro-components@0.0.28) (2022-03-30)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.27](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.26...@vh-admin/pro-components@0.0.27) (2022-03-30)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.26](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.25...@vh-admin/pro-components@0.0.26) (2022-03-30)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.25](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.24...@vh-admin/pro-components@0.0.25) (2022-03-30)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.24](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.23...@vh-admin/pro-components@0.0.24) (2022-03-29)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.23](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.22...@vh-admin/pro-components@0.0.23) (2022-03-29)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.22](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.21...@vh-admin/pro-components@0.0.22) (2022-03-29)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.21](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.20...@vh-admin/pro-components@0.0.21) (2022-03-29)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.20](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.19...@vh-admin/pro-components@0.0.20) (2022-03-25)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.19](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.17...@vh-admin/pro-components@0.0.19) (2022-03-24)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.17](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.16...@vh-admin/pro-components@0.0.17) (2022-03-24)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.16](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.14...@vh-admin/pro-components@0.0.16) (2022-03-24)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.14](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.12...@vh-admin/pro-components@0.0.14) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.12](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.11...@vh-admin/pro-components@0.0.12) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.11](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.10...@vh-admin/pro-components@0.0.11) (2022-03-18)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.10](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.9...@vh-admin/pro-components@0.0.10) (2022-03-17)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.9](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.8...@vh-admin/pro-components@0.0.9) (2022-03-17)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.8](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.7...@vh-admin/pro-components@0.0.8) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.7](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.3...@vh-admin/pro-components@0.0.7) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-components

## [0.0.3](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-components@0.0.2...@vh-admin/pro-components@0.0.3) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-components
