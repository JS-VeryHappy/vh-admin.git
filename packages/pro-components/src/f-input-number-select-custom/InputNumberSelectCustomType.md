### InputNumberSelectCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| value | 默认值<br>自定义必须要实现的 | `[string\|number,any]` | - | - |
| options | select选择数据<br> | `DefaultOptionType[]\|undefined` | [] | - |
| selectProps | 传递给antdselect组件属性或者内容使用的组件参考ant组件属性<br>[select](https://ant.design/components/select-cn)  | `SelectProps` | - | - |
| inputNumberProps | 传递给antdInputNumber组件属性或者内容使用的组件参考ant组件属性<br>[inputNumber](https://ant.design/components/input-number-cn) | `InputNumberProps` | - | - |
| disabled | 是否禁用<br> | `boolean` | false | - |
| readonly | 是否只读模式自定义必须要实现的 | `boolean` | - | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
