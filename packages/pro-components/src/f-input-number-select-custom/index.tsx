import type { InputNumberProps, SelectProps } from 'antd';
import { Space } from 'antd';
import { InputNumber } from 'antd';
import { Select } from 'antd';
import type { DefaultOptionType } from 'antd/lib/select';
import { useEffect, useState } from 'react';
import type { CustomType } from '../f-form-custom/types';

// import './index.less';

export declare type InputNumberSelectCustomType = {
  /**
   * 默认值
   * 自定义必须要实现的
   */
  value?: [string | number, any];
  /**
   * select选择数据
   * @default []
   */
  options?: DefaultOptionType[] | undefined;
  /**
   * 传递给antd select组件属性或者内容使用的组件 参考ant组件属性
   */
  selectProps?: SelectProps;
  /**
   * 传递给antd InputNumber组件属性或者内容使用的组件 参考ant组件属性
   */
  inputNumberProps?: InputNumberProps;
  /**
   * 是否禁用
   * @default false
   */
  disabled?: boolean;
  /**
   * 是否只读模式 自定义必须要实现的
   */
  readonly?: boolean;
} & CustomType;

const InputNumberSelectCustom = (props: InputNumberSelectCustomType) => {
  const {
    id,
    style,
    className,
    onChange,
    value,
    options = [
      { value: 1, label: '%单套' },
      { value: 2, label: '元单套' },
      { value: 3, label: '元整体', disabled: true },
    ],
    selectProps,
    disabled = false,
    inputNumberProps,
    readonly,
  } = props;
  const [inputValue, setInputValue] = useState<any>(null);
  const [selectValue, setSelectValue] = useState<any>(null);
  useEffect(() => {
    if (value) {
      setInputValue(value[0]);
    }
    if (value && value[1]) {
      setSelectValue(value[1]);
    } else if (options) {
      setSelectValue(options[0].value);
    } else {
      setSelectValue('%');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  const onSelectChange = (data: any) => {
    setSelectValue(data);
    if (onChange && typeof onChange === 'function') {
      onChange([inputValue, data]);
    }
  };

  const onInputChange = (data: any) => {
    setInputValue(data);
    if (onChange && typeof onChange === 'function') {
      onChange([data, selectValue]);
    }
  };

  return (
    <>
      <div
        className={`input-number-select-custom ${className ? className : ''}`}
        id={id}
        style={style}
      >
        {readonly ? (
          `${value && value[0] ? value[0] : ''} ${
            value && value[1] ? options?.find((i: any) => i.value === value[1])?.label : ''
          }`
        ) : (
          <div className="main">
            <Space.Compact>
              <InputNumber
                className="left-inp"
                disabled={disabled}
                value={inputValue}
                onChange={onInputChange}
                {...inputNumberProps}
              />
              <Select
                className="select-inp"
                placeholder="请输入"
                disabled={disabled}
                onChange={onSelectChange}
                value={selectValue}
                options={options}
                {...selectProps}
              />
            </Space.Compact>
          </div>
        )}
      </div>
    </>
  );
};

export default InputNumberSelectCustom;
