---
title: InputNumberSelect
toc: content
order: 10
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. InputNumberSelect

```jsx
/**
 * title: InputNumberSelect
 */
import React from 'react';
import { InputNumberSelectCustom } from '@vh-admin/pro-components';

function InputNumberSelectCustomDemo() {
  const options = [
    { value: 1, label: '%单套' },
    { value: 3, label: '元整体' }
  ];


  return <InputNumberSelectCustom options={options}  />;
}

export default InputNumberSelectCustomDemo;
```

## API

<embed src="./InputNumberSelectCustomType.md"></embed>


