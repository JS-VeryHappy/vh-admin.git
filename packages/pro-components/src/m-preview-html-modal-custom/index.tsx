import ModalCustom from '../m-modal-custom';
import './index.less';
import type { ModalProps } from 'antd';
import { Typography, Space } from 'antd';
const { Title, Text } = Typography;
import React from 'react';

export declare type PreviewHtmlModalCustomType = {
  /**
   * 定义className
   */
  className?: string;
  /**
   * 需要显示的标题 空为不显示
   * @default ""
   */
  title?: string | ((initialValues: any) => React.ReactNode);
  /**
   * 需要显示的简介 空为不显示
   * @default ""
   */
  desc?: string | ((initialValues: any) => React.ReactNode);
  /**
   * 需要显示的时间 空为不显示
   * @default ""
   */
  date?: string | ((initialValues: any) => React.ReactNode);
  /**
   * 需要显示的内容 空为不显示
   * @default ""
   */
  content?: string | ((initialValues: any) => React.ReactNode) | (() => HTMLElement);
  /**
   * 预览外部链接 如果配置此项将会直接引用外部链接显示
   */
  link?: string;
  /**
   * 设置link前的钩子
   */
  linkBefor?: (link: any, initialValues: any) => any;
  /**
   * 点击内容区域的事件
   */
  onBody?: (e: React.MouseEvent<HTMLElement>) => void;
  /**
   * 如果是table内置使用点击row会传这个值过来
   */
  initialValues?: any;
  /**
   * 传递给iframe属性
   */
  iframeProps?: any;
} & ModalProps;

/**
 * 业务弹窗预览html
 * @param Props
 * @returns
 */
function PreviewHtmlModalCustom(Props: PreviewHtmlModalCustomType) {
  const {
    className,
    title = '',
    desc = '',
    date = '',
    content = '',
    onCancel,
    onBody,
    link,
    linkBefor,
    initialValues,
    iframeProps,
    ...rest
  } = Props;

  const handleCancel = (e: any) => {
    if (onCancel && typeof onCancel === 'function') {
      onCancel(e);
    }
  };

  let titleRender: any;
  let descRender: any;
  let dateRender: any;
  let contentRender: any;

  if (!link) {
    if (typeof title === 'function') {
      titleRender = title(initialValues);
    } else {
      titleRender = <Title level={4}>{title}</Title>;
    }

    if (typeof desc === 'function') {
      descRender = desc(initialValues);
    } else {
      descRender = <Text>{desc}</Text>;
    }

    if (typeof date === 'function') {
      dateRender = date(initialValues);
    } else {
      dateRender = <Text type="secondary">{date}</Text>;
    }

    if (typeof content === 'function') {
      contentRender = content(initialValues);
    } else {
      contentRender = <div dangerouslySetInnerHTML={{ __html: content }} />;
    }
  }

  return (
    <>
      <ModalCustom
        title={link ? title : ''}
        destroyOnClose={true}
        className={className}
        wrapClassName="preview-html-modal-custom"
        width="double"
        onCancel={handleCancel}
        footer={null}
        zIndex={9999}
        {...rest}
      >
        {link ? (
          <iframe
            className="content-iframe"
            frameBorder={0}
            width="100%"
            height="500"
            src={linkBefor ? linkBefor(link, initialValues) : link}
            onClick={onBody}
            {...iframeProps}
          />
        ) : (
          <Space className={`content-body space`} direction="vertical" onClick={onBody}>
            <div className="title">{titleRender}</div>
            <div className="desc">{descRender}</div>
            <div className="date">{dateRender}</div>
            <div className="content">{contentRender}</div>
          </Space>
        )}
      </ModalCustom>
    </>
  );
}

export default PreviewHtmlModalCustom;
