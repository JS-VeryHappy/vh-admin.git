### PreviewHtmlModalCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| className | 定义className | `string` | - | - |
| title | 需要显示的标题空为不显示<br> | `string\|((initialValues:any)=>React.ReactNode)` | "" | - |        
| desc | 需要显示的简介空为不显示<br> | `string\|((initialValues:any)=>React.ReactNode)` | "" | - |
| date | 需要显示的时间空为不显示<br> | `string\|((initialValues:any)=>React.ReactNode)` | "" | - |
| content | 需要显示的内容空为不显示<br> | `string\|((initialValues:any)=>React.ReactNode)\|(()=>HTMLElement)` | "" | - |
| link | 预览外部链接如果配置此项将会直接引用外部链接显示 | `string` | - | - |
| linkBefor | 设置link前的钩子 | `(link:any,initialValues:any)=>any` | - | - |
| onBody | 点击内容区域的事件 | `(e:React.MouseEvent<HTMLElement>)=>void` | - | - |
| initialValues | 如果是table内置使用点击row会传这个值过来 | `any` | - | - |
| iframeProps | 传递给iframe属性 | `any` | - | - |
| `...ModalProps` | antd [ModalProps](https://ant.design/components/modal-cn) | `ModalProps` | - | - |
