---
title: 快捷按钮
order: 2
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表格组件
  path: /table-custom
  order: 2
---

## 自定义快捷菜单按钮配置

- 一.菜单快捷配置是根据一个配置就能快捷配出出固定业务的菜单配置项、例如：颜色、大小、图标。具体配置查看按钮 API

  - **1.菜单快捷配置三种位置类型：selection：多选位置 、 header：table 顶部位置 、 operation:row 记录行位置;**

  ```js
       // btnConfig.tsx
       /**
        * 配置顶部header快捷菜单按钮
       */
       export const headerTitleConfigArr: BtnConfigTypes = {
          create: {
             text: '新增',
             icon: PlusOutlined,
             type: 'primary',
             style: {
                background: '#1890ff',
                borderColor: '#1890ff',
             },
          }
          default: {
             text: '按钮', // 按钮显示名称
             icon: PlusOutlined, // 按钮图标
             type: 'primary', // 按钮类型
             auth: () => true, // 显示权限
             style: {
                // 按钮显示样式
                background: '#1890ff',
                borderColor: '#1890ff',
             },
          },
       };
       /**
       * 配置顶部select选中快捷菜单配置
       */
       export const tableAlertOptionRenderConfigArr: BtnConfigTypes = {
          delete: {
             text: '批量删除',
             type: 'link',
             danger: true,
          },
          default: {
             text: '按钮', // 按钮显示名称
             type: 'link',
             auth: () => true, // 显示权限
          },
       };

       /**
       * 配置row记录快捷菜单配置
       */
       export const operationConfigRenderConfigArr: BtnConfigTypes = {
          edit: {
             text: '编辑',
             type: 'link',
          },
          default: {
             text: '按钮', // 按钮显示名称
             type: 'link',
             auth: () => true, // 显示权限
          },
       };

  ```

  - **2.每个位置按钮配置对象 default 属性必须存在，它也是所有按钮配置的公用配置、如果一个按钮没有配置对应属性则会默认取 default**
  - ````js

           // table 中配置使用按钮
           headerTitleConfig={{
              export: () => {},
           }}
           // 按钮配置文件
           // 如果有配置export按钮属性，则按钮完成配置为{...export,...default}
           // 如果没有{...default}
           {
              export: {
                 text: '导出',
                 icon: ExportOutlined,
                 type: 'primary',
                 style: {
                    background: '#269884',
                    borderColor: '#269884',
                 },
              },
              default: {
                 text: '按钮', // 按钮显示名称
                 icon: PlusOutlined, // 按钮图标
                 type: 'primary', // 按钮类型
                 auth: () => true, // 显示权限
                 style: {
                    // 按钮显示样式
                    background: '#1890ff',
                    borderColor: '#1890ff',
                 },
              },
           }
        ```

    ````

## 使用例子

<code src="./t-table-custom/Example/demo8.jsx" description="可以直接使用按钮样式">按钮样式</code>


## API 

<embed src="./t-table-custom/Example/TableCustomTypes/BtnConfigType.md"></embed>

<embed src="./t-table-custom/Example/TableCustomTypes/ModalPropsType.md"></embed>
