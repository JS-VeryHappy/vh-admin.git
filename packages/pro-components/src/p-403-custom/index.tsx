import { Result, Button } from 'antd';
import React from 'react';

const P403Custom = () => {
  return (
    <Result
      status="403"
      title="403"
      subTitle="你没有此页面的访问权限"
      extra={
        <Button type="primary">
          <a href="/" style={{ color: '#fff' }}>
            去首页
          </a>
        </Button>
      }
    />
  );
};
export default P403Custom;
