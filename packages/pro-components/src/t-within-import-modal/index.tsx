import ImportModalCustom from '../m-import-modal-custom';
import { useState } from 'react';
import type { ModalRenderPropsType } from '../t-table-custom/types';
import { Modal, message } from 'antd';
import React from 'react';

/**
 * 内置功能表单
 */
function Import(props: ModalRenderPropsType) {
  const { modelchildName, closeModal, btnConfig, clickConfig, tableRef, tableFormRef } = props;
  // 内部显示状态
  const [visible, setVisible] = useState<boolean>(true);
  // 解构按钮配置的弹窗配置
  const { config, edit = false } = btnConfig.modalConfig || {};
  // 表单配置参数
  const {
    submitValuesBefor,
    submitRequest,
    submitOnDone,
    showError = true,
    ...configRest
  } = config;

  let initialValues: any = {};

  // 如果显示 并且 开启编辑模式
  if (visible && edit) {
    initialValues = { ...clickConfig.irecord };

    // 如果动态标题
    if (typeof configRest.title === 'function') {
      configRest.title = configRest.title(initialValues);
    }
  }

  const defaultConfig = {
    visible,
    onOk: async (values: any) => {
      // 遍历处理默认数据
      let submitValue: any = values;

      // 数据提交前的钩子函数
      if (submitValuesBefor) {
        submitValue = submitValuesBefor(
          submitValue,
          null,
          tableRef,
          tableFormRef,
          clickConfig.irecord || {},
        );
      }
      // 如果配置了自动请求
      if (submitRequest) {
        try {
          const result = await submitRequest(
            submitValue,
            null,
            tableRef,
            tableFormRef,
            clickConfig.irecord || {},
          );

          if (showError) {
            // @ts-ignore
            if (window?.vhAdmin?.tableImportShowError) {
              // @ts-ignore
              window.vhAdmin?.tableImportShowError(result);
            } else {
              const error = result.data.error;
              const summaryText = result.data.summaryText;
              if ((error && error.length > 0) || summaryText) {
                let errorDom: any = null;
                if (error && error.length > 0) {
                  errorDom = error.map((i: string, index: any) => {
                    return (
                      <div key={`error${index * 1}`} style={{ color: 'red' }}>
                        {i}
                      </div>
                    );
                  });
                }

                Modal.warning({
                  title: '数据导入结果:',
                  content: (
                    <>
                      {summaryText && (
                        <div
                          style={{ color: '#ffad0e', fontSize: '15px', marginBottom: '10px' }}
                          dangerouslySetInnerHTML={{ __html: summaryText }}
                        />
                      )}
                      {errorDom && (
                        <div style={{ maxHeight: 400, overflow: 'auto' }}>{errorDom}</div>
                      )}
                    </>
                  ),
                });
              }
            }
          }

          // 如果设置请求回调
          if (submitOnDone) {
            submitOnDone({
              status: 'success',
              result,
              params: submitValue,
            });
          } else {
            message.success(config.title + '成功');
          }
          setVisible(false);

          if (tableRef && tableRef.current && tableRef.current.reload) {
            tableRef.current.reload();
          }

          return true;
        } catch (error) {
          if (submitOnDone) {
            submitOnDone({
              status: 'error',
              result: {},
              params: submitValue,
            });
          }
          return false;
        }
      }

      try {
        await btnConfig.onClick(submitValue, tableRef);
        setVisible(false);
        return true;
      } catch (error) {
        return false;
      }
    },
    onCancel: () => {
      setVisible(false);
    },
    afterClose: () => {
      if (closeModal) {
        closeModal();
      }
    },
  };

  const newConfig = { ...defaultConfig, ...configRest };

  return <ImportModalCustom id={modelchildName} key={modelchildName} {...newConfig} />;
}

export default Import;
