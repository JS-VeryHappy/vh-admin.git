import FormCustom from '../f-form-custom';
import type { ModalProps } from 'antd';
import { ModalForm } from '@ant-design/pro-form';
import type { FormCustomColumnsType } from '../f-form-custom/types';
import { Typography, Space } from 'antd';
const { Text, Link } = Typography;
import React from 'react';

export declare type ImportModalCustomType = {
  /**
   * 绑定的一个id
   */
  id?: string | undefined;
  /**
   * 显示标题
   * @description
   * @default 导入数据
   */
  title?: string;
  /**
   * 显示描述
   * @description
   * @default 请仔细核对数据后上传,上传后操作无法撤回
   */
  desc?: string;
  /**
   * tooltip提示
   */
  tooltip?: string;
  /**
   * 设置模板下载地址 如果设置显示下载模板
   */
  templateUrl?: string;
  /**
   * 设置模板下载显示文字
   * @default 模板
   */
  templateName?: string;
  /**
   * 自定义表单配置 和表单格式一样
   * @default
   */
  columns?: FormCustomColumnsType[];
  /**
   * 默认表单配置 和表单格式一样
   * @default
   */
  defaultColumns?: FormCustomColumnsType[];
  /**
   * 上传的路径前缀
   * @default common
   */
  path?: string;
  /**
   * 请求上传地址api方法
   */
  request?: any;

  /**
   * 是否返回blob数据
   * @default false
   */
  blob?: boolean;
  /**
   * 上传字段的 字段名称
   * @default file
   */
  name?: string;
  /**
   * 导入位于表单的什么位置
   * @default top
   */
  position?: 'top' | 'bottom';
  /**
   * 上传组件的限制KB
   * @default 10000
   */
  size?: number;
} & ModalProps;

function ImportModalCustom(Props: ImportModalCustomType) {
  const {
    id,
    title = '导入数据',
    desc = '请仔细核对数据后上传,上传后操作无法撤回',
    tooltip = null,
    templateUrl,
    templateName = '模板',
    onOk,
    onCancel,
    visible,
    afterClose,
    columns,
    path = 'common',
    blob = false,
    request,
    name = 'file',
    position = 'top',
    size = 10000,
    defaultColumns = [
      {
        title: '选择文件',
        dataIndex: name,
        valueType: 'UploadCustom',
        tooltip: tooltip,
        fieldProps: {
          path: path,
          maxCount: 1,
          format: ['xlsx', 'xls'],
          size: size,
          blob: blob,
          request: request,
          name: name,
          buttonProps: {
            type: 'primary',
            ghost: true,
          },
        },
        formItemProps: {
          rules: [{ required: true, message: '请上传文件' }],
        },
      },
    ],
  } = Props;

  const onFinish = async (values: any) => {
    if (onOk && typeof onOk === 'function') {
      return await onOk(values);
    }
  };

  const setModalVisit = (value: any) => {
    if (!value && onCancel && typeof onCancel === 'function') {
      onCancel(value);
    }
    if (!value && afterClose && typeof afterClose === 'function') {
      afterClose();
    }
  };

  const customDom = (
    <>
      {templateUrl && (
        <Link href={templateUrl} target="_blank">
          下载模板:{`${templateName}`}
        </Link>
      )}
      <FormCustom layoutType="Embed" columns={defaultColumns} />
    </>
  );

  return (
    <>
      <ModalForm
        id={id}
        title={title}
        modalProps={{
          destroyOnClose: true,
          // maskClosable: false,
        }}
        onFinish={onFinish}
        onVisibleChange={setModalVisit}
        width={400}
        visible={visible}
      >
        <Space direction="vertical">
          {desc && <Text>{desc}</Text>}
          {columns ? (
            position === 'top' ? (
              <>
                {customDom}
                <FormCustom layoutType="Embed" columns={columns} />
              </>
            ) : (
              <>
                <FormCustom layoutType="Embed" columns={columns} />
                {customDom}
              </>
            )
          ) : (
            customDom
          )}
        </Space>
      </ModalForm>
    </>
  );
}

export default ImportModalCustom;
