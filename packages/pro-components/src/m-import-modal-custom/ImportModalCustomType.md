### ImportModalCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| id | 绑定的一个id | ` string \| undefined` | - | - |
| title | 显示标题<br>@description<br> | `string` | 导入数据 | - |
| desc | 显示描述<br>@description<br> | `string` | 请仔细核对数据后上传,上传后操作无法撤回 | - |
| tooltip | tooltip提示 | `string` | - | - |
| templateUrl | 设置模板下载地址如果设置显示下载模板 | `string` | - | - |
| templateName | 设置模板下载显示文字<br> | `string` | 模板 | - |
| columns | 自定义表单配置和表单格式一样<br> | `FormCustomColumnsType[]` |  | - |
| defaultColumns | 默认表单配置和表单格式一样<br> | `FormCustomColumnsType[]` |  | - |
| path | 上传的路径前缀<br> | `string` | common | - |
| request | 请求上传地址api方法 | `any` | - | - |
| blob | 是否返回blob数据<br> | `boolean` | false | - |
| name | 上传字段的字段名称<br> | `string` | file | - |
| position | 导入位于表单的什么位置<br> | `'top'\|'bottom'` | top | - |
| size | 上传大小kb | `number'` | 10000 | - |
| `...ModalProps` | antd [ModalProps](https://ant.design/components/modal-cn) | `ModalProps` | - | - |
