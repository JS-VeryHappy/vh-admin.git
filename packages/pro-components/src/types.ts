import type { BtnConfigTypes } from './t-table-custom/types';
/**
 * 表格单元格特殊样式配置
 */
export declare type VhAdminTypes = {
  /**
   * 当前设置的主题token
   */
  theme?: any;
  /**
   * 本地存放 token信息的key
   * @default 'USER_INFO_TOKEN'
   */
  LocalStorageTokenName?: string;
  /**
   * 本地存放 用户信息的key
   * @default 'USER_INFO'
   */
  LocalStorageName?: string;
  /**
   * umi请求方法
   */
  request?: (() => any) | ((params: any) => Promise<any>) | any;
  /**
   * 表格搜索请求开始字符串去空格
   * false 关闭
   * @default 'undefined'
   */
  requestParamsTrim?: boolean;
  /**
     * 定义http错误返回码
     * @default {
        500: '服务器错误',
        401: '未授权',
        404: '数据不存在',
        405: '参数绑定错误',
        406: '参数验证错误',
      }
     */
  httpError?: Record<any, string> | ((httpError: any) => Record<any, string>);
  /**
   * http请求前设置headers
   * headers: 请求要发给送的headers
   * @returns  headers
   **/
  httpHeadersHook?: (headers: any, config: any) => any;
  /**
   * http请求错误回调
   * status: http状态
   * code: 业务状态
   * ctx: 整个请求对象
   * resData: 业务返回对象
   * 可以返回和请求相同的请求配置 例如: return { errorMessageShow: false }; 隐藏错误提示
   * @returns  config - 请求接口配置的option相同
   **/
  httpErrorHook?: (status: number | undefined, code: number, ctx: any, resData: any) => any;
  /**
   * 上传公用方法
   * @returns
   */
  uploadFile?: (() => any) | ((params: any) => Promise<any>) | any;
  /**
   * 表格请求返回字段定义
   */
  responseTableConfig?: {
    /**
     * 表格数据列表字段名称
     * @default 'data'
     */
    data: string;
    /**
     * 表格数据列表总数字段名称
     * @default 'total'
     */
    total: string;
  };
  /**
   * 请求公用返回定义
   */
  responseConfig?: {
    /**
     * 存放数据字段
     * @default 'data'
     */
    data: string;
    /**
     * 判断成功值
     * @default 0
     */
    success: string | number;
    /**
     * 错误码字段
     * @default 'code'
     */
    code: string | number;
    /**
     * 返回信息字段
     * @default 'msg'
     */
    message: string;
  };
  /**
   * 表格发送请求前钩子
   * @returns params 请求的参数
   */
  tableRequestParamsHook?: (requestParams: any, sort: any, filter: any) => any;
  /**
   * 表格单元格操作栏最多显示按钮个数 默认2
   * @default 2
   */
  tableOperationMax?: number;
  /**
   * 自定义tabel内置导入返回错误信息的方法
   */
  tableImportShowError?: (res: any) => any;
  /**
   * 表格搜索默认是否收起
   * @default false
   */
  tableSearchDefaultCollapsed?: boolean;
  /**
   * 自定义表格单元格格式
   * 参考表格 单元格 businessStyleConfig 样式开发
   */
  tableBusinessStyleArr?: Record<any, any>;
  /**
   * table动态计算高度时，自定义的保底高度
   * @default 96 + 44
   */
  tableExtraHeight?: number | any;
  /**
   * 表格上内置按钮补充
   * 参考 表格 btnConfig 按钮样式开发
   */
  btnConfig?: {
    /**
         * 顶部按钮样式
         * 例如：{
          edit: {
            text: '项目1特有',
            type: 'link',
          },
        }
         */
    headerTitleConfigArr?: BtnConfigTypes;
    /**
         * 表格选择后按钮配置
         * 例如：{
          edit: {
            text: '项目1特有';
            type: 'link';
          };
        }
         */
    tableAlertOptionRenderConfigArr?: BtnConfigTypes;
    /**
         * 表格row按钮配置
         * 例如：{
          edit: {
            text: '项目1特有';
            type: 'link';
          };
        }
         */
    operationConfigRenderConfigArr?: BtnConfigTypes;
  };
};
