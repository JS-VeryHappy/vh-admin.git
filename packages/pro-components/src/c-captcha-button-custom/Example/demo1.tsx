import { message, Space } from 'antd';
//@ts-ignore
import { CCaptchaButtonCustom } from '@vh-admin/pro-components';

const demo1 = () => {
  return (
    <div>
      <Space>
        <CCaptchaButtonCustom
          onClick={() => {
            message.info('倒计时开始');
          }}
          onEnd={() => {
            message.info('倒计时完成');
          }}
          cacheKey="__CaptchaButton__Phone__1"
        />

        <CCaptchaButtonCustom
          type="primary"
          disabledText="重新获取"
          cacheKey="__CaptchaButton__Phone__2"
        >
          获取手机验证码
        </CCaptchaButtonCustom>
      </Space>
    </div>
  );
};

export default demo1;
