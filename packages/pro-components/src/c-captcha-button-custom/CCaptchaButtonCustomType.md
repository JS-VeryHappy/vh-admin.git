### CCaptchaButtonCustomType


继承 [ant Button](https://ant.design/components/button-cn/) 属性`(disabled 除外)`

|     参数     |                说明                |     类型     |       默认值        |
| :----------: | :--------------------------------: | :----------: | :-----------------: |
|    start     |            是否开始发送            |  `boolean`   |       `true`        |
|    second    |             倒计时秒数             |   `number`   |        `60 `        |
| disabledText |   倒计时的文字, 会带上 second 秒   |   `string`   |       `重发 `       |
|   cacheKey   | 缓存的 key、页面刷新后倒计时继续。 |   `string`   | `__CaptchaButton__` |
|    onEnd     |          倒计时完成后触发          | `() => void` |        `- `         |
