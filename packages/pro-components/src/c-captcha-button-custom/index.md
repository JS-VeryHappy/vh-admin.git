---
title: 按钮倒计时
toc: content
order: 11
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 通用
  path: /Exception
  order: 4
---


处理获取验证码之类的组件, 刷新页面也会继续计时

<mark>当存在多个验证码组件时, 请设置不同的 cacheKey 值</mark>

## 代码演示

### 基础用法

<code src='./Example/demo1.tsx'></code>



## API

<embed src="./CCaptchaButtonCustomType.md"></embed>

