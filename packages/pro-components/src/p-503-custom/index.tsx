import { Result, Button } from 'antd';
import React from 'react';

const P503Custom = () => {
  return (
    <Result
      status="500"
      title="服务维护中..."
      subTitle="程序员已使出吃奶的力气了,请您耐心等待下哦"
      extra={
        <Button type="primary">
          <a href="/" style={{ color: '#fff' }}>
            去首页
          </a>
        </Button>
      }
    />
  );
};
export default P503Custom;
