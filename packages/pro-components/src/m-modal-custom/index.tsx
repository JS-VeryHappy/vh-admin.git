import { useState } from 'react';
import { Modal } from 'antd';
import type { ModalProps } from 'antd';
import React from 'react';

export declare type ModalPropsType = ModalProps & {
  /**
   * 显示的内容
   */
  children?: React.ReactNode;
  /**
   * 弹窗的宽度
   * @default single
   */
  width?: 'single' | 'double' | number | any;

  /**
   * 控制显示状态 兼容以前
   */
  visible?: boolean;
};
/**
 * 弹窗和antd一样
 * 因为业务ui可能会有改变封装一层作为基础弹窗
 * @param Props
 * @returns
 */
function ModalCustom(Props: ModalPropsType) {
  const [confirmLoading, setConfirmLoading] = useState<boolean>(false);
  const { className, children, width = 'single', onOk, onCancel, visible, ...rest } = Props;

  const handleOk = async (e: any) => {
    if (onOk && typeof onOk === 'function') {
      setConfirmLoading(true);
      try {
        await onOk(e);
        setConfirmLoading(false);
      } catch (error) {
        setConfirmLoading(false);
      }
    }
  };

  const handleCancel = (e: any) => {
    if (onCancel && typeof onCancel === 'function') {
      onCancel(e);
    }
  };

  let customWidth = width;
  if (width == 'single') {
    customWidth = '378px';
  } else if (width == 'double') {
    customWidth = '738px';
  }

  return (
    <>
      <Modal
        okText="确定"
        cancelText="取消"
        open={visible}
        {...rest}
        width={customWidth}
        confirmLoading={confirmLoading}
        onOk={handleOk}
        onCancel={handleCancel}
        className={`modal-custom ${className ? className : ''}`}
      >
        {children}
      </Modal>
    </>
  );
}

export default ModalCustom;
