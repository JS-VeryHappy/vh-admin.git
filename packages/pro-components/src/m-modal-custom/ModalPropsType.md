### ModalPropsType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| children | 显示的内容 | `React.ReactNode` | - | - |
| width | 弹窗的宽度<br> | `'single'\|'double'\|number\|any` | single | - |
| visible | 控制显示状态兼容以前 | `boolean` | - | - |
| `...ModalProps` | antd [ModalProps](https://ant.design/components/modal-cn) | `ModalProps` | - | - |
