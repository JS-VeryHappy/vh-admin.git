---
title: Select-Tabel 选择器
toc: content
order: 14
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1.Select-Tabel 多选,单选

<code src="./Example/demo1.jsx" description="例子" >例子</code>

## API

<embed src="./FSelectTableCustomType.md"></embed>
