import React from 'react';
import { useState } from 'react';
//@ts-ignore
import { FSelectTableCustom } from '@vh-admin/pro-components';
import { getProTable } from '../../example';

function FSelectTableCustomDemo() {
  const [selectValue, setSelectValue] = useState([
    // [1, 2],
    // ['哈哈哈', '2222'],
  ]);

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      valueType: 'indexBorder',
      hideInForm: true,
      width: 48,
    },
    {
      title: '标题',
      dataIndex: 'title',
      formGroup: 1,
      formOrder: 1,
    },
    {
      title: '描述',
      dataIndex: 'description',
      copyable: true,
      formGroup: 1,
      formOrder: 1,
    },
    {
      title: '状态',
      dataIndex: 'status',
      valueType: 'select',
      formGroup: 2,
      fieldProps: {
        options: [
          {
            label: '全部',
            value: null,
          },
          {
            label: '启用',
            value: 1,
          },
          {
            label: '禁用',
            value: 2,
          },
          {
            label: '等待',
            value: 3,
          },
        ],
      },
    },
  ];

  const onChange = (values) => {
    setSelectValue(values);
  };

  return (
    <>
      <FSelectTableCustom
        selectProps={{
          style: {
            minWidth: '200px',
          },
        }}
        // readonly
        // disabled
        multiple
        tableCustomProps={{
          request: getProTable,
          search: false,
          columns,
        }}
        value={selectValue}
        onChange={onChange}
        fieldName="title"
      />
    </>
  );
}
export default FSelectTableCustomDemo;
