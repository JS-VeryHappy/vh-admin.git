### FSelectTableCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| dropdownWidth | 弹窗宽度<br> | `number` | 500 | - |
| fieldName | 配置字段显示字段的显示取值 | `string` | - | - |
| className | className | `string` | - | - |
| tableCustomProps | 传递给[TableCustom](/pro-components/t-table-custom)的Props | `TableCustomTypes<any>` | - | - |
| selectProps | 传递给[Select](https://ant.design/components/select-cn)的Props | `SelectProps` | - | - |
| disabled | 是否禁用<br> | `boolean` | false | - |
| maxTagCount | 最多显示多少个，响应式模式会对性能产生损耗<br> | `number\|'responsive'` | 1 | - |
| dropdownMatchSelectWidth | 下拉菜单和选择器同宽。默认将设置min-width，当值小于选择框宽度时会被忽略。false时会关闭虚拟滚动 | `boolean\|number` | - | - |
| multiple | 是否多选<br> | `boolean` | false | - |
| value | ` 值，格式为 [[1,2],['显示1','显示2']] ,数组[0]值数组，[1] 显示数组` | `any` | false | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
