import type { SelectProps } from 'antd';
import { Select } from 'antd';
import type { CustomType } from '../f-form-custom/types';
import TableCustom from '../t-table-custom';
import type { TableCustomTypes } from '../t-table-custom/types';
import { useEffect, useState } from 'react';

export type FSelectTableCustomType = {
  /**
   * 弹窗宽度
   * @default 500
   */
  dropdownWidth?: number;
  /**
   * 配置字段 显示字段的 显示取值
   */
  fieldName?: string;
  /**
   * className
   */
  className?: string;

  /**
   * 传递给 tableCustom的Props
   */
  tableCustomProps?: TableCustomTypes<any>;
  /**
   * 传递给select的Props
   */
  selectProps?: SelectProps;
  /**
   * 是否禁用
   * @default false
   */
  disabled?: boolean;
  /**
   * 最多显示多少个，响应式模式会对性能产生损耗
   * @default 1
   */
  maxTagCount?: number | 'responsive';

  /**
   * 下拉菜单和选择器同宽。默认将设置 min-width，当值小于选择框宽度时会被忽略。false 时会关闭虚拟滚动
   */
  dropdownMatchSelectWidth?: boolean | number;
  /**
   * 是否多选
   * @default false
   */
  multiple?: boolean;

  /**
   * 默认值
   *  格式为 [[1,2],['显示1','显示2']] ,数组[0]值数组，[1] 显示数组
   */
  value?: any;
} & CustomType;

const FSelectTableCustom = (props: FSelectTableCustomType) => {
  const {
    id,
    style,
    className,
    readonly,
    onChange,
    value,
    maxTagCount = 1,
    dropdownMatchSelectWidth = 500,
    fieldName = 'label',
    disabled = false,
    placeholder = '请选择',
    multiple = false,
    tableCustomProps,
    selectProps,
  } = props;
  const [selectValue, setSelectValue] = useState<any>([]);
  const [selectName, setSelectName] = useState<any>([]);

  useEffect(() => {
    if (value) {
      setSelectValue(value[0]);
      setSelectName(value[1]);
    } else {
      setSelectValue([]);
      setSelectName([]);
    }
  }, [value]);

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
      console.log(`selectedRowKeys:`, selectedRowKeys, 'selectedRows: ', selectedRows);
      if (typeof onChange === 'function') {
        const selectedName = selectedRows.map((item) => {
          return item[fieldName];
        });

        onChange([selectedRowKeys, selectedName]);
      }
    },
  };
  const selectOnChange = (values: any) => {
    if (typeof onChange === 'function') {
      onChange(values);
    }
  };

  return (
    <div
      className={`f-select-table-custom ${className ? className : ''}`}
      id={id}
      style={{ ...style }}
    >
      {readonly ? (
        selectName ? (
          selectName.join('、')
        ) : (
          ''
        )
      ) : (
        <Select
          mode="multiple"
          placeholder={placeholder}
          allowClear
          value={selectName}
          maxTagCount={maxTagCount}
          onChange={selectOnChange}
          dropdownMatchSelectWidth={dropdownMatchSelectWidth}
          disabled={disabled}
          {...selectProps}
          dropdownRender={() => (
            <>
              <TableCustom
                size="small"
                options={false}
                pagination={{
                  pageSize: 5,
                }}
                rowSelection={{
                  selectedRowKeys: selectValue,
                  type: multiple ? 'checkbox' : 'radio',
                  ...rowSelection,
                }}
                {...tableCustomProps}
              />
            </>
          )}
        />
      )}
    </div>
  );
};

export default FSelectTableCustom;
