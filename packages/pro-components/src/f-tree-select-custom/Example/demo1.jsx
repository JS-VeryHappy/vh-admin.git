import { Button, message } from 'antd';
// @ts-ignore
import { FormCustom } from '@vh-admin/pro-components';
import { PlusOutlined } from '@ant-design/icons';
import React from 'react';
// @ts-ignore

const organizationQueryByCompanyId = async (params) => {
  let list = [
    {
      title: 'Node1',
      value: '0-0',
      children: [
        {
          title: 'Child Node1',
          value: '0-0-0',
        },
      ],
    },
    {
      title: 'Node2',
      value: '0-1',
      children: [
        {
          title: 'Child Node3',
          value: '0-1-0',
        },
        {
          title: 'Child Node4',
          value: '0-1-1',
        },
        {
          title: 'Child Node5',
          value: '0-1-2',
        },
      ],
    },
  ];

  if (params.type && params.type.length > 0) {
    list = [
      {
        title: 'Node2',
        value: '0-1',
        children: [
          {
            title: 'Child Node3',
            value: '0-1-0',
          },
          {
            title: 'Child Node4',
            value: '0-1-1',
          },
        ],
      },
    ];
  }
  console.log('===================');
  console.log(list, params);
  console.log('===================');
  return list;
};

const projectColumnsFn = () => {
  return [
    {
      title: '',
      dataIndex: 'label',
      renderFormItem: () => {
        return (
          <>
            <div>可将该规则关联到选择的项目集合。</div>
          </>
        );
      },
    },
    {
      title: '项目类型',
      dataIndex: 'type',
      valueType: 'SelectMultipleCustom',
      fieldProps: {
        options: [
          {
            label: '未解决',
            value: 1,
          },
          {
            label: '已解决',
            value: 2,
          },
          {
            label: '解决中',
            value: 3,
          },
        ],
        maxTagCount: 3,
      },
    },
    // {
    //   valueType: 'dependency',
    //   name: ['type'],
    //   columns: ({ type }) => {
    //     return [
    //       {
    //         title: '项目',
    //         dataIndex: 'projectIds',
    //         valueType: 'TreeSelectCustom',
    //         fieldProps: {
    //           params: { aaa: 11, type: type },
    //           request: organizationQueryByCompanyId,
    //           dropdownStyle: {
    //             maxHeight: 400,
    //             overflow: 'auto',
    //           },
    //           maxTagCount: 2,
    //           treeData: [
    //             {
    //               title: 'Node1',
    //               value: '0-0',
    //               children: [
    //                 {
    //                   title: 'Child Node1',
    //                   value: '0-0-0',
    //                 },
    //               ],
    //             },
    //           ],
    //           treeCheckable: true,
    //           multiple: true,
    //           placeholder: '请选择',
    //           treeDefaultExpandAll: true,
    //           treeNodeFilterProp: 'title',
    //           showSearch: true,
    //           allowClear: true,
    //           fieldNames: {
    //             label: 'title',
    //             value: 'value',
    //             children: 'children',
    //           },
    //         },
    //         formItemProps: {
    //           rules: [{ required: true }],
    //         },
    //       },
    //     ];
    //   },
    // },
    {
      title: '项目',
      dataIndex: 'projectIds',
      valueType: 'TreeSelectCustom',
      // fieldProps: {
      //   params: { aaa: 11 },
      //   request: organizationQueryByCompanyId,
      //   dropdownStyle: {
      //     maxHeight: 400,
      //     overflow: 'auto',
      //   },
      //   maxTagCount: 2,
      //   treeData: [
      //     {
      //       title: 'Node1',
      //       value: '0-0',
      //       children: [
      //         {
      //           title: 'Child Node1',
      //           value: '0-0-0',
      //         },
      //       ],
      //     },
      //   ],
      //   treeCheckable: true,
      //   multiple: true,
      //   placeholder: '请选择',
      //   treeDefaultExpandAll: true,
      //   treeNodeFilterProp: 'title',
      //   showSearch: true,
      //   allowClear: true,
      //   fieldNames: {
      //     label: 'title',
      //     value: 'value',
      //     children: 'children',
      //   },
      // },
      fieldProps: (from) => {
        const type = from.getFieldValue('type');

        return {
          params: { aaa: 11, type: type },
          request: organizationQueryByCompanyId,
          dropdownStyle: {
            maxHeight: 400,
            overflow: 'auto',
          },
          maxTagCount: 2,
          treeData: [
            {
              title: 'Node1',
              value: '0-0',
              children: [
                {
                  title: 'Child Node1',
                  value: '0-0-0',
                },
              ],
            },
          ],
          treeCheckable: true,
          multiple: true,
          placeholder: '请选择',
          treeDefaultExpandAll: true,
          treeNodeFilterProp: 'title',
          showSearch: true,
          allowClear: true,
          fieldNames: {
            label: 'title',
            value: 'value',
            children: 'children',
          },
        };
      },
      formItemProps: {
        rules: [{ required: true }],
      },
    },
  ];
};

function Demo1() {
  const onFinish = async (values) => {
    try {
      message.success('成功');
      console.log(values);
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  };

  return (
    <>
      <FormCustom
        columns={projectColumnsFn()}
        layoutType="ModalForm"
        title="ModalForm"
        trigger={
          <Button type="primary">
            <PlusOutlined />
            ModalForm
          </Button>
        }
        onFinish={onFinish}
        // request={async (params) => {
        //   console.log(params);
        //   return {
        //     datetime: '2006-02-03 21:30:57',
        //     description: '确天确年',
        //     id: 262,
        //     status: 2,
        //     title: '确天确年',
        //     type: 2,
        //   };
        // }}
      />
    </>
  );
}

export default Demo1;
