import { TreeSelect } from 'antd';
import type { CustomType } from '../f-form-custom/types';
import React, { useState, useEffect, useRef } from 'react';
import { recursionTreeData, loopTreeData, loopTreeChildren } from '@vh-admin/pro-utils';
import { useDeepCompareEffect } from 'ahooks';

declare type FieldNamesType = {
  /**
   * 显示字段
   */
  label?: string;
  /**
   * 值
   */
  value?: string;
  /**
   * 子集
   */
  children?: string;
  /**
   * 父级ID
   */
  parentId?: string;
};

export declare type TreeSelectCustomType = CustomType & {
  /**
   * 完全受控的情况下  点击子节点会选中父节点 父级选中会影响子节点
   * 是否选择父节点
   */
  checkedParent?: boolean;
  /**
   * checkable 状态下节点选择完全受控（父子节点选中状态不再关联），会使得 labelInValue 强制为 true
   * @default false
   */
  treeCheckStrictly?: boolean;
  /**
   * treeNodes 数据，如果设置则不需要手动构造 TreeNode 节点（value 在整个树范围内唯一）
   */
  treeData?: any;
  /**
   * 显示 Checkbox
   * @default false
   */
  treeCheckable?: boolean;
  /**
   * 自定义节点 label、value、children,parentId的字段
   * @default { label：'label', value：'value', children：'children', parentId：'parentId' }
   */
  fieldNames?: FieldNamesType;
  /**
   * 从网络请求枚举数据
   */
  request?: (params: any, Props: any) => Promise<any>;
  /**
   * 发起网络请求的参数,与 request 配合使用
   */
  params?: Record<any, any>;
};

function TreeSelectCustom(Props: TreeSelectCustomType) {
  const {
    id,
    style,
    className,
    customMode,
    readonly,
    checkedParent = false,
    treeCheckStrictly = false,
    treeCheckable = false,
    treeData = [],
    fieldProps,
    value,
    onChange,
    request,
    params,
    ...rest
  } = Props;
  const [selectValue, setSelectValue] = useState<any>(treeCheckable ? [] : null);
  const [formTreeData, setFormTreeData] = useState<any>();
  const formTreeDataRef = useRef<any>();

  const treeSelectProps: any = { ...fieldProps, ...rest };
  const { fieldNames = null } = rest;
  let field: any = { label: 'label', value: 'value', children: 'children', parentId: 'parentId' };
  if (fieldNames) {
    field = { ...field, ...fieldNames };
  }

  useDeepCompareEffect(() => {
    if (request) {
      const getOptions = async () => {
        const data = await request(params, Props);
        setFormTreeData(data);
        formTreeDataRef.current = data;
      };
      getOptions();
    } else {
      setFormTreeData(treeData);
      formTreeDataRef.current = treeData;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  useEffect(() => {
    if (!formTreeData) {
      return;
    }

    if (value) {
      if (treeCheckable) {
        const newValues = value.map((i: any) => {
          const childrenHas = recursionTreeData(formTreeDataRef.current, i, field);
          return { value: i, label: childrenHas[field.label] };
        });
        setSelectValue(newValues);
      } else {
        setSelectValue(value);
      }
    } else {
      setSelectValue(treeCheckable ? [] : null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value, formTreeData]);

  const selectOnChange = (values: any) => {
    if (typeof onChange === 'function') {
      if (treeCheckStrictly === true) {
        const newValues = values.map((i: any) => {
          return i[field.value] ? i[field.value] : i.value;
        });

        if (checkedParent === true) {
          let newSelectValue: any;
          if (treeCheckable) {
            newSelectValue = selectValue.map((i: any) => i.value);
          } else {
            newSelectValue = selectValue;
          }

          const reduceArr = newSelectValue.filter((x: any) => newValues.indexOf(x) === -1);
          const addArr = newValues.filter((x: any) => newSelectValue.indexOf(x) === -1);

          if (addArr.length > 0) {
            const has = loopTreeData(formTreeDataRef.current, addArr[0], field);
            const childrenHas = recursionTreeData(formTreeDataRef.current, addArr[0], field);
            let childrens = [];
            if (childrenHas[field.children]) {
              childrens = loopTreeChildren(childrenHas[field.children], field);
            }
            onChange(Array.from(new Set([...newValues, ...has, ...childrens])));
          }
          if (reduceArr.length > 0) {
            const childrenHas = recursionTreeData(formTreeDataRef.current, reduceArr[0], field);
            let childrens = [];
            if (childrenHas[field.children]) {
              childrens = loopTreeChildren(childrenHas[field.children], field);
            }
            const newChildrens = Array.from(new Set([...childrens]));

            onChange(newValues.filter((i: any) => newChildrens.indexOf(i) === -1));
          }
        } else {
          onChange(newValues);
        }
      } else {
        onChange(values);
      }
    }
  };

  if (!formTreeData) {
    return null;
  }

  return (
    <div
      className={`${className ? className : ''} ${customMode ? customMode : ''}`}
      id={id}
      style={style}
    >
      {readonly ? (
        value
      ) : (
        <TreeSelect
          value={selectValue}
          treeCheckStrictly={treeCheckStrictly}
          treeData={formTreeData}
          treeCheckable={treeCheckable}
          onChange={selectOnChange}
          fieldNames={field}
          {...treeSelectProps}
        />
      )}
    </div>
  );
}

export default TreeSelectCustom;
