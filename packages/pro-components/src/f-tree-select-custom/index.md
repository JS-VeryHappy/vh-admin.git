---
title: TreeSelect树选择
toc: content
order: 14
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. TreeSelect 树选择

```jsx
import React, { useState } from 'react';
import TreeSelectCustom from './index.tsx';
import { MailTwoTone } from '@ant-design/icons';
import { message } from 'antd';

const treeData = [
  {
    label: 'Node1',
    value: '0-0',
    children: [
      {
        label: 'Child Node1',
        value: '0-0-1',
      },
      {
        label: 'Child Node2',
        value: '0-0-2',
      },
    ],
  },
  {
    label: 'Node2',
    value: '0-1',
  },
];
const fieldProps = {
  style: {
    width: '100%',
  },
  fieldProps:{
    style: {
      width: '200px',
    },
  },
  placeholder: 'Please select',
  dropdownStyle: {
    maxHeight: 400,
    overflow: 'auto',
  },
  treeData,
  treeDefaultExpandAll: true,
};

function TreeSelectCustomDemo() {
  const [value, setValue] = useState();

  const onChange = (value) => {
    setValue(value);
  };

  return <TreeSelectCustom value={value} onChange={onChange} {...fieldProps} />;
}
export default TreeSelectCustomDemo;
```

> 1. TreeSelect 完全受控时，子节和父节点会关联

```jsx
import React, { useState } from 'react';
import { TreeSelectCustom } from '@vh-admin/pro-components';
import { MailTwoTone } from '@ant-design/icons';
import { message } from 'antd';

const treeData = [
  {
    title: '选项1',
    value1: 1,
    childrenA: [
      {
        title: '选项子集1',
        value1: 11,
      },
      {
        title: '选项子集2',
        value1: 12,
      },
    ],
  },
  {
    title: '选项2',
    value1: 2,
  },
];
const fieldProps = {
  style: {
    width: '100%',
  },
  fieldProps:{
    style: {
      width: '200px',
    },
  },
  dropdownStyle: {
    maxHeight: 400,
    overflow: 'auto',
  },
  fieldNames: {
    label: 'title',
    value: 'value1',
    children: 'childrenA',
    parentId: 'parentId',
  },
  checkedParent: true,
  maxTagCount: 2,
  treeData: treeData,
  showCheckedStrategy: true,
  treeCheckable: true,
  treeCheckStrictly: true,
  multiple: true,
  placeholder: '请选择',
  treeDefaultExpandAll: true,
  treeNodeFilterProp: 'title',
  showSearch: true,
  allowClear: true,
};

function TreeSelectCustomDemo1() {
  const [value, setValue] = useState([]);

  const onChange = (value) => {
    console.log(value);

    setValue(value);
  };

  return <TreeSelectCustom value={value} onChange={onChange} {...fieldProps} />;
}
export default TreeSelectCustomDemo1;
```

<code src="./Example/demo1.jsx" 
      title="动态获取数据" 
      description="动态获取数据">动态获取数据</code>


## API

<embed src="./TreeSelectCustomType.md"></embed>

