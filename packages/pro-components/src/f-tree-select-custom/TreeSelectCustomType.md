### TreeSelectCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| checkedParent | 完全受控的情况下点击子节点会选中父节点父级选中会影响子节点<br>是否选择父节点 | `boolean` | - | - |
| treeCheckStrictly | checkable状态下节点选择完全受控（父子节点选中状态不再关联），会使得labelInValue强制为true<br> | `boolean` | false | - |
| treeData | treeNodes数据，如果设置则不需要手动构造TreeNode节点（value在整个树范围内唯一） | `any` | - | - |
| treeCheckable | 显示Checkbox<br> | `boolean` | false | - |
| fieldNames | 自定义节点label、value、children,parentId的字段<br>[FieldNamesType](/pro-components/f-tree-select-custom#fieldnamestype) | `FieldNamesType` | {label：'label',value：'value',children：'children',parentId：'parentId'} | - |
| request | 从网络请求枚举数据 | `(params: any, Props: any) => Promise<any>;` | - | - |
| params | 发起网络请求的参数,与 request 配合使用 | `Record<any, any>` | - | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |

### FieldNamesType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| label | 显示字段 | `string` | - | - |
| value | 值 | `string` | - | - |
| children | 子集 | `string` | - | - |
| parentId | 父级ID | `string` | - | - |
