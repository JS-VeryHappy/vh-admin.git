import type { VhAdminTypes } from './types';
export { default as Page403Custom } from './p-403-custom';
export { default as Page404Custom } from './p-404-custom';
export { default as Page500Custom } from './p-500-custom';
export { default as Page503Custom } from './p-503-custom';
export { default as LoadingCustom } from './c-loading-custom';
export { default as AmapCustom } from './c-amap-custom';
export { default as FormCustom } from './f-form-custom';
export * from './f-form-custom/components';
export { default as ModalCustom } from './m-modal-custom';
export { default as AmapModalCustom } from './m-amap-modal-custom';
export { default as CaptchaModalCustom } from './m-captcha-modal-custom';
export { default as ConfirmModalCustom } from './m-confirm-modal-custom';
export { default as DeleteModalCustom } from './m-delete-modal-custom';
export { default as ImportModalCustom } from './m-import-modal-custom';
export { default as PreviewHtmlModalCustom } from './m-preview-html-modal-custom';
export { default as TableCustom } from './t-table-custom';
export { default as SelectProjectCustom } from './c-select-project-custom';
export { default as ProConfigureListCustom } from './c-configure-list-custom';
export { default as CTypeitCustom } from './c-typeit-custom';
export { default as CFilePreviewCustom } from './c-file-preview-custom';
export { default as CNumberRollCustom } from './c-number-roll-custom';
export { default as CCaptchaButtonCustom } from './c-captcha-button-custom';
export { default as ModalTypeRenderConfig } from './t-table-custom/modalTypeRenderConfig';

declare global {
  interface Window {
    // @ts-ignore
    vhAdmin: VhAdminTypes;
  }
}

export const defineConfig = (config: VhAdminTypes) => {
  //@ts-ignore
  window.vhAdmin = config;
  return config;
};
