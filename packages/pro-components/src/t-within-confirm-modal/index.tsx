import ConfirmModalCustom from '../m-confirm-modal-custom';
import { useState } from 'react';
import type { ModalRenderPropsType } from '../t-table-custom/types';
import React from 'react';
import { message } from 'antd';

/**
 * 内置功能表单
 */
function Confirm(props: ModalRenderPropsType) {
  const { modelchildName, closeModal, btnConfig, clickConfig, tableRef, tableFormRef } = props;
  // 内部显示状态
  const [visible, setVisible] = useState<boolean>(true);
  // 解构按钮配置的弹窗配置
  const { config, edit = false } = btnConfig.modalConfig || {};
  // 表单配置参数
  const { submitValuesBefor, submitRequest, submitOnDone, ...configRest } = config;

  let initialValues: any = {};

  // 如果显示 并且 开启编辑模式
  if (visible && edit) {
    initialValues = { ...clickConfig.irecord };

    // 如果动态标题
    if (typeof configRest.title === 'function') {
      configRest.title = configRest.title(initialValues);
    }
  }

  const defaultConfig = {
    title: '确认信息',
    description: '确定要此操作？',
    visible,
    onOk: async () => {
      // 遍历处理默认数据
      let submitValue: any = {};
      if (edit) {
        // 如果是编辑默认带上id
        submitValue = { ...clickConfig.irecord };
      }
      // 数据提交前的钩子函数
      if (submitValuesBefor) {
        submitValue = submitValuesBefor(
          submitValue,
          null,
          tableRef,
          tableFormRef,
          clickConfig.irecord || {},
        );
      }
      // 如果配置了自动请求
      if (submitRequest) {
        try {
          const result = await submitRequest(
            submitValue,
            null,
            tableRef,
            tableFormRef,
            clickConfig.irecord || {},
          );
          // 如果设置请求回调
          if (submitOnDone) {
            submitOnDone({
              status: 'success',
              result,
              params: submitValue,
              tableRef,
            });
          } else {
            message.success(config.title + '成功');
          }
          setVisible(false);

          if (tableRef && tableRef.current && tableRef.current.reload) {
            tableRef.current.reload();
          }
          // //如果有开启select多选 清空
          // if (tableProps.selectionConfig && tableRef.current.clearSelected) {
          //   tableRef.current.clearSelected();
          // }
        } catch (error) {
          if (submitOnDone) {
            submitOnDone({
              status: 'error',
              result: error,
              params: submitValue,
              tableRef,
            });
          }
        }
      }

      try {
        await btnConfig.onClick(submitValue, tableRef);
        setVisible(false);
      } catch (error) {}
    },
    onCancel: () => {
      setVisible(false);
    },
    afterClose: () => {
      if (closeModal) {
        closeModal();
      }
    },
  };

  const newConfig = { ...defaultConfig, ...configRest };

  return <ConfirmModalCustom id={modelchildName} key={modelchildName} {...newConfig} />;
}

export default Confirm;
