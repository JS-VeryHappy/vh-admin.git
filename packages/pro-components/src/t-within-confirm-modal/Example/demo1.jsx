import { TableCustom } from '@vh-admin/pro-components';
import { getProTable, proTableDelete } from '../../example';
import { message } from 'antd';
import React from 'react';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    valueType: 'indexBorder',
    hideInForm: true,
    width: 48,
  },
  {
    title: '标题',
    dataIndex: 'title',
  },
  {
    title: '描述',
    dataIndex: 'description',
    copyable: true,
  },
  {
    title: '状态',
    dataIndex: 'status',
    valueType: 'select',
    fieldProps: {
      options: [
        {
          label: '全部',
          value: null,
        },
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: '时间',
    dataIndex: 'datetime',
    valueType: 'dateTime',
    sorter: (a, b) => a.datetime - b.datetime,
  },
];

function ConfirmModalDemo1() {
  return (
    <>
      <TableCustom
        request={getProTable}
        columns={columns}
        search={false}
        pagination={{
          pageSize: 5,
        }}
        operationConfig={{
          delete: {
            modalConfig: {
              modalType: 'Confirm',
              config: {
                title: '删除',
                initialValuesBefor: (data) => {
                  return { ...data, title: 111 };
                },
                submitValuesBefor: (data) => {
                  return { ...data, name: '小周周' };
                },
                // debounceProTableAddRow
                submitRequest: proTableDelete,
              },
              edit: true,
            },
          },
        }}
      />
    </>
  );
}

export default ConfirmModalDemo1;
