import React, { useState, useEffect } from 'react';
import { DatePicker, Form, Input, Select, Space } from 'antd';
import { message } from 'antd';
import type { CustomType, OptionType } from '../f-form-custom/types';
import dayjs from 'dayjs';
const { RangePicker } = DatePicker;

export declare type SelectDatePickerCustomType = {
  /**
   * 显示的选择类型范围
   * @default ['day', 'month', 'year', 'week', 'quarter']
   */
  showSelectOptions?: string[];
  /**
   * 开启区间选择
   * @default false
   */
  rangePicker?: boolean;

  /**
   * 区间格式为
   * 单选 ['day','2022-03-03 xx'] 区间 ['day', ['2022-03-03 xx', '2022-03-03 xx']]
   */
  value?: any;
  /**
   * 返回值是否携带 类型
   * @default true
   */
  showType?: any;
  /**
   * select切换时 返回值的钩子
   */
  selectChangeBefer?: (paramValue: any) => any;
  /**
   * 默认选中的值
   * @default 'day''
   */
  defaultSelect?: 'day' | 'month' | 'year' | 'week' | 'quarter';
  /**
   * antd select的Props
   */
  selectProps?: any;
  /**
   * antd picker的Props
   */
  pickerProps?: any;
} & CustomType;

const options: OptionType[] = [
  {
    label: '日',
    value: 'day',
  },
  {
    label: '月',
    value: 'month',
  },
  {
    label: '年',
    value: 'year',
  },
  {
    label: '周',
    value: 'week',
  },
  {
    label: '季',
    value: 'quarter',
  },
];

function SelectDatePickerCustom(Props: SelectDatePickerCustomType) {
  const [datePickerValue, setDatePickerValue] = useState<any>('');
  const [selectOptions, setSelectOptions] = useState<any>([]);
  const {
    id,
    style,
    className,
    onChange,
    showSelectOptions = ['day', 'month', 'year', 'week', 'quarter'],
    defaultSelect = 'day',
    value,
    fieldProps,
    selectProps,
    pickerProps,
    rangePicker,
    showType = true,
    selectChangeBefer,
  } = Props;

  const [selectValue, setSelectValue] = useState<any>(defaultSelect);

  useEffect(() => {
    if (value) {
      // 如果返回值需要携带类型
      if (showType) {
        /**
         * 如果父级传有默认值则赋值默认值 或者默认值变换
         */
        if (value[1]) {
          if (rangePicker) {
            setDatePickerValue(
              value[1].map((d: any) => {
                return dayjs(d);
              }),
            );
          } else {
            setDatePickerValue(dayjs(value[1]));
          }
        }
        if (value[0]) {
          setSelectValue(value[0]);
        }
      } else {
        if (rangePicker) {
          setDatePickerValue(
            value.map((d: any) => {
              return dayjs(d);
            }),
          );
        } else {
          setDatePickerValue(dayjs(value));
        }
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  useEffect(() => {
    const newOptions: OptionType[] = options.filter((i: OptionType) => {
      return showSelectOptions.includes(i.value);
    });
    setSelectOptions(newOptions);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const changeFormat: any = (dayjsDate: any, index: any = 0) => {
    let newDateString: string = '';
    if (selectValue === 'month') {
      if (index && index > 0) {
        newDateString = dayjsDate.endOf('month').endOf('day').format('YYYY-MM-DD');
      } else {
        newDateString = dayjsDate.startOf('month').startOf('day').format('YYYY-MM-DD');
      }
    } else if (selectValue === 'year') {
      if (index && index > 0) {
        newDateString = dayjsDate.endOf('year').endOf('month').endOf('day').format('YYYY-MM-DD');
      } else {
        newDateString = dayjsDate
          .startOf('year')
          .startOf('month')
          .startOf('day')
          .format('YYYY-MM-DD');
      }
    } else if (selectValue === 'week') {
      if (index && index > 0) {
        newDateString = dayjsDate.endOf('week').format('YYYY-MM-DD');
      } else {
        newDateString = dayjsDate.startOf('week').format('YYYY-MM-DD');
      }
    } else if (selectValue === 'quarter') {
      if (index && index > 0) {
        newDateString = dayjsDate.endOf('quarter').endOf('day').format('YYYY-MM-DD');
      } else {
        newDateString = dayjsDate.startOf('quarter').startOf('day').format('YYYY-MM-DD');
      }
    } else {
      if (fieldProps && fieldProps.showTime) {
        newDateString = dayjsDate.format('YYYY-MM-DD HH:mm:ss');
      } else {
        newDateString = dayjsDate.format('YYYY-MM-DD');
      }
    }
    return newDateString;
  };

  /**
   * 切换值变换。如果父级传入监听方法调用
   */
  const onDatePickerChange = (date: any) => {
    // console.log(date, date.format('YYYY-MM-DD HH:mm:ss'));
    setDatePickerValue(date);
    let dateString: any;
    if (rangePicker) {
      if (!date) {
        dateString = [];
      } else {
        dateString = date.map((d: any, index: any) => {
          return changeFormat(d, index);
        });
      }
    } else {
      if (!date) {
        dateString = null;
      } else {
        dateString = changeFormat(date);
      }
    }

    if (onChange && typeof onChange === 'function') {
      if (showType) {
        onChange([selectValue, dateString]);
      } else {
        onChange(dateString);
      }
    } else {
      message.info(`DatePicker切换值${dateString}`);
    }
  };
  /**
   * 切换值变换。如果父级传入监听方法调用
   * @param paramValue
   */
  const onSelectChange = (paramValue: any) => {
    setSelectValue(paramValue);
    setDatePickerValue('');
    let dv: any = null;
    if (selectChangeBefer) {
      dv = selectChangeBefer(paramValue);
    }
    if (onChange && typeof onChange === 'function') {
      if (showType) {
        onChange([paramValue, dv]);
      } else {
        onChange(dv);
      }
    } else {
      message.info(`Select切换值${paramValue}`);
    }
  };

  return (
    <div
      className={`f-select-date-picker-custom ${className ? className : ''}`}
      id={id}
      style={{ ...style }}
    >
      <Space.Compact>
        <Form.Item noStyle>
          <Select
            style={{ width: '60px' }}
            onChange={onSelectChange}
            options={selectOptions}
            value={selectValue}
            {...selectProps}
          />
        </Form.Item>
        <Form.Item noStyle>
          {rangePicker ? (
            <RangePicker
              style={{ width: 'calc(100% - 60px)' }}
              picker={selectValue}
              value={datePickerValue}
              {...fieldProps}
              {...pickerProps}
              onChange={onDatePickerChange}
            />
          ) : (
            <DatePicker
              style={{ width: 'calc(100% - 60px)' }}
              picker={selectValue}
              value={datePickerValue}
              {...fieldProps}
              {...pickerProps}
              onChange={onDatePickerChange}
            />
          )}
        </Form.Item>
      </Space.Compact>
    </div>
  );
}

export default SelectDatePickerCustom;
