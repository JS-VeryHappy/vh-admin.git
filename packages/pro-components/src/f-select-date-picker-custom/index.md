---
title: 多类型时间选择器
toc: content
order: 12
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

```jsx
/**
 * title: 多类型时间选择器 年，月，日，季，周
 */
import React, { useState } from 'react';
import { SelectDatePickerCustom } from '@vh-admin/pro-components';

function SelectDatePickerCustomDemo() {
  const [value, setValue] = useState(['day', '2022-03-03 00:00:00']);

  const fieldProps = {
    style: {
      width: '200px',
    },
    // showTime: true,
  };

  const onChange = (v) => {
    console.log(v);
    setValue(v);
  };
  return (
    <SelectDatePickerCustom
      showSelectOptions={['day', 'month', 'year', 'week', 'quarter']}
      fieldProps={fieldProps}
      onChange={onChange}
      value={value}
    />
  );
}

export default SelectDatePickerCustomDemo;
```

```jsx
/**
 * title: 多类型时间选择器 年区间，月区间，日区间，季区间，周区间
 * description: 我默认值
 */
import React, { useState } from 'react';
import { SelectDatePickerCustom } from '@vh-admin/pro-components';

function SelectDatePickerCustomDemo1() {
  const [value, setValue] = useState(['day', ['2022-03-03 00:00:00', '2022-03-03 00:00:00']]);

  const fieldProps = {
    style: {
      width: '400px',
    },
    showTime: true,
  };

  const onChange = (v) => {
    console.log(v);
    setValue(v);
  };
  return (
    <SelectDatePickerCustom
      showSelectOptions={['day', 'month', 'year', 'week', 'quarter']}
      fieldProps={fieldProps}
      onChange={onChange}
      value={value}
      rangePicker={true}
    />
  );
}

export default SelectDatePickerCustomDemo1;
```

```jsx
/**
 * title: 返回类型不需要返回 类型字段，默认值和返回值格式
 */
import React, { useState } from 'react';
import { SelectDatePickerCustom } from '@vh-admin/pro-components';

function SelectDatePickerCustomDemo2() {
  const [value, setValue] = useState('2022-03-03 00:00:00');

  const fieldProps = {
    style: {
      width: '200px',
    },
    // showTime: true,
  };

  const onChange = (v) => {
    console.log(v);
    setValue(v);
  };
  return (
    <SelectDatePickerCustom
      showSelectOptions={['day', 'month', 'year', 'week', 'quarter']}
      fieldProps={fieldProps}
      onChange={onChange}
      value={value}
      showType={false}
    />
  );
}

export default SelectDatePickerCustomDemo2;
```

```jsx
/**
 *
 * title: 返回类型不需要返回例子，多类型时间选择器 年区间，月区间，日区间，季区间，周区间
 * description: 我默认值
 */
import React, { useState } from 'react';
import { SelectDatePickerCustom } from '@vh-admin/pro-components';

function SelectDatePickerCustomDemo3() {
  const [value, setValue] = useState(['2022-03-03 00:00:00', '2022-03-03 00:00:00']);

  const fieldProps = {
    style: {
      width: '400px',
    },
    showTime: true,
  };

  const onChange = (v) => {
    console.log(v);
    setValue(v);
  };
  return (
    <SelectDatePickerCustom
      showSelectOptions={['day', 'month', 'year', 'week', 'quarter']}
      fieldProps={fieldProps}
      onChange={onChange}
      value={value}
      rangePicker={true}
      showType={false}
    />
  );
}

export default SelectDatePickerCustomDemo3;
```

## API

<embed src="./SelectDatePickerCustomType.md"></embed>


