### SelectDatePickerCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| showSelectOptions | 显示的选择类型范围<br> | `string[]` | ['day','month','year','week','quarter'] | - |
| rangePicker | 开启区间选择<br> | `boolean` | false | - |
| value | 区间格式为<br>单选['day','2022-03-03xx']区间['day',['2022-03-03xx','2022-03-03xx']] | `any` | - | - |
| showType | 返回值是否携带类型<br> | `any` | true | - |
| selectChangeBefer | select切换时返回值的钩子 | `(paramValue:any)=>any` | - | - |
| defaultSelect | 默认选中的值<br> | `'day'\|'month'\|'year'\|'week'\|'quarter'` | 'day'' | - |
| selectProps | antd select的Props | `any` | - | - |
| pickerProps | antd picker的Props | `any` | - | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
