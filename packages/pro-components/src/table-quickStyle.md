---
title: 单元格样式
order: 2
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表格组件
  path: /table-custom
  order: 2
---

## 自定义单元格快捷样式

- 一.businessStyleConfig.tsx 文件下面存放所有表格单元格特殊显示形式
- 二.自定义方法

  - **1.businessStyleConfig.tsx 直接定义函数方法**
  - ````js
          // businessStyleConfig.tsx
           const tagView = ()=>{}

           /**
            * 标签方法对应表
           */
           const BusinessStyleArr: any = {
               tag: tagView,
           };
           // 定义类型 types.d.ts
           export declare type BusinessStyleType = {
              type?: 'tag';
              ...
           };
           export declare type modalType = 'Form';
        ```

    ````

## 使用例子

<code src="./t-table-custom/Example/demo7.jsx" 
      description="可以直接使用内置样式" >单元格</code>


## 自定义单元格路由跳转、图片查看、下载文件-使用例子

<code src="./t-table-custom/Example/demo9.jsx" 
      description="路由跳转、图片查看、下载文件"
     >路由跳转、图片查看、下载文件</code>



## API 

<embed src="./t-table-custom/Example/TableCustomTypes/BusinessStyleType.md"></embed>

