import React, { useState, useEffect } from 'react';
import type { CustomType, OptionType } from '../f-form-custom/types';
import { CheckCard } from '@ant-design/pro-card';

export declare type CheckCardCustomType = {
  /**
   * 选择数据
   * @default []
   */
  options: OptionType[] | undefined;
  /**
   * 禁用
   * @default false
   */
  disabled?: boolean;
  /**
   * 是否多选
   * @default false
   */
  multiple?: boolean;
} & CustomType;

function CheckCardCustom(Props: CheckCardCustomType) {
  const [selectValue, setSelectValue] = useState<any>([]);

  const {
    style,
    className,
    readonly,
    onChange,
    value,
    options = [],
    fieldProps,
    multiple = false,
    disabled = false,
  } = Props;

  useEffect(() => {
    if (value) {
      setSelectValue(value);
    }
  }, [value]);

  const selectOnChange = (values: any) => {
    if (typeof onChange === 'function') {
      onChange(values);
    }
  };
  if (!options) {
    return null;
  }

  options.forEach((i: any) => {
    i.description = i.label;
  });

  return (
    <div className={className} style={{ ...style }}>
      {readonly ? (
        `${value && value[0] ? options?.find((i: any) => i.value === value[0])?.label : ''}`
      ) : (
        <CheckCard.Group
          onChange={selectOnChange}
          options={options}
          disabled={disabled}
          value={selectValue}
          multiple={multiple}
          {...fieldProps}
        />
      )}
    </div>
  );
}

export default CheckCardCustom;
