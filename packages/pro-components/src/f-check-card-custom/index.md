---
title: 卡片选择器
toc: content
order: 4
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1.卡片选择

```jsx
/**
 * title: 标题，描述，单选，禁用,默认值
 *
 */
import React from 'react';
import { Button } from 'antd';
import { CheckCardCustom } from '@vh-admin/pro-components';
import { useState } from 'react';

function CheckCardCustomDemo() {
  const [selectValue, setSelectValue] = useState(3);

  const onChange = (values) => {
    setSelectValue(values);
    console.log(values);
  };

  const options = [
    {
      title: 'Card A',
      label: '选择一个由流程编排提供的典型用户案例，可以从中学习到流程编排很多设计理念',
      value: 1,
      disabled: true,
    },
    {
      title: 'Card b',
      label: '选择一个由流程编排提供的典型用户案例，可以从中学习到流程编排很多设计理念',
      value: 2,
    },
    {
      title: 'Card c',
      label: '选择一个由流程编排提供的典型用户案例，可以从中学习到流程编排很多设计理念',
      value: 3,
    },
  ];

  return (
    <>
      <CheckCardCustom options={options} value={selectValue} onChange={onChange} />
    </>
  );
}
export default CheckCardCustomDemo;
```

```jsx
/**
 * title: 多选
 *
 */
import React from 'react';
import { Button } from 'antd';
import { CheckCardCustom } from '@vh-admin/pro-components';
import { useState } from 'react';

function CheckCardCustomDemo1() {
  const [selectValue, setSelectValue] = useState([3]);

  const onChange = (values) => {
    setSelectValue(values);
    console.log(values);
  };

  const options = [
    {
      title: 'Card A',
      label: '选择一个由流程编排提供的典型用户案例，可以从中学习到流程编排很多设计理念',
      value: 1,
      disabled: true,
    },
    {
      title: 'Card b',
      label: '选择一个由流程编排提供的典型用户案例，可以从中学习到流程编排很多设计理念',
      value: 2,
    },
    {
      title: 'Card c',
      label: '选择一个由流程编排提供的典型用户案例，可以从中学习到流程编排很多设计理念',
      value: 3,
    },
  ];

  return (
    <>
      <CheckCardCustom options={options} multiple={true} value={selectValue} onChange={onChange} />
    </>
  );
}
export default CheckCardCustomDemo1;
```

## API

<embed src="./CheckCardCustomType.md"></embed>


