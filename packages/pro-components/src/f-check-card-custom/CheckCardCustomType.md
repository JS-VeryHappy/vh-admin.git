### CheckCardCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| options(必须) | 选择数据<br> | `OptionType[]\|undefined` | [] | - |
| disabled | 禁用<br> | `boolean` | false | - |
| multiple | 是否多选<br> | `boolean` | false | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
