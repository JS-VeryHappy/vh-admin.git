---
title: 内置-预览HTML或富文本
order: 3
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表格组件
  path: /table-custom
  order: 2
---

## 被调用的组件具体参数说明参考：[PreviewHtml](/pro-components/m-preview-html-modal-custom)

- 一.配合快捷菜单按钮使用内置功能
  - 1.  方便快捷的配置出增、改业务功能逻辑，值需要简单的配置就可以完成、一般不能单独使用，必须配合表格快捷菜单。
  - 2.  点击显示和请求接口都有做防抖处理、安全放心。如果提交事件自定义处理，请使用防抖函数配合使用.
    ```js
    import { proTableAddRow } from '@/services';
    import { requestDebounce } from '@/utils';
    const debounceProTableAddRow: any = requestDebounce(proTableAddRow, 500);
    ```
- 二. 弹窗类型:
  - **_modalType="PreviewHtml"_**

## 完整使用例子

<code src="./Example/demo1.jsx" 
      title="使用例子"
      description="配置初始化请求例子显示富文本信息">使用例子</code>

## API 

<embed src="../t-table-custom/Example/TableCustomTypes/ModalPropsType.md"></embed>

<embed src="../t-table-custom/Example/TableCustomTypes/ModalPropsConfigType.md"></embed>

<embed src="../t-table-custom/Example/TableCustomTypes/SubmitOnDoneType.md"></embed>
