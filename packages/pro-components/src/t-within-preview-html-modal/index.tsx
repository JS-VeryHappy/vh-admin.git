import PreviewHtmlModalCustom from '../m-preview-html-modal-custom';
import { useState, useEffect } from 'react';
import type { ModalRenderPropsType } from '../t-table-custom/types';
import { message } from 'antd';
import React from 'react';
import { deepGet } from '@vh-admin/pro-utils';

/**
 * 内置功能表单
 */
function PreviewHtml(props: ModalRenderPropsType) {
  const { modelchildName, closeModal, btnConfig, clickConfig } = props;
  // 内部显示状态
  const [visible, setVisible] = useState<boolean>(false);
  const [initialValues, setInitialValues] = useState<any>({});
  // 解构按钮配置的弹窗配置
  const { config } = btnConfig.modalConfig || {};

  // 表单配置参数
  const {
    params,
    request,
    initialValuesBefor,
    submitValuesBefor,
    submitRequest,
    submitOnDone,
    ...configRest
  } = config;

  useEffect(() => {
    let values: any = { ...clickConfig.irecord };

    // 如果配置了网络请求数据
    if (request) {
      const hide = message.loading('数据请求中...', 0);
      const requestParams = { ...params };
      requestParams.id = values.id;

      let responseConfig: any = {
        data: 'data', // 存放数据字段
        success: 0, // 判断成功值
        code: 'code', // 错误码字段
        message: 'msg', // 返回信息字段
      };
      // @ts-ignore
      if (window?.vhAdmin?.responseConfig) {
        // @ts-ignore
        responseConfig = window.vhAdmin?.responseConfig;
      }

      request(requestParams)
        .then((data: any) => {
          if (
            deepGet(data, responseConfig.code) !== undefined &&
            deepGet(data, responseConfig.message) &&
            deepGet(data, responseConfig.data)
          ) {
            values = deepGet(data, responseConfig.data);
          } else {
            values = data;
          }
          // 数据初始化复制前的钩子执行
          if (initialValuesBefor) {
            values = initialValuesBefor(values);
          }
          setInitialValues(values);
          setVisible(true);
        })
        .catch(() => {})
        .finally(() => {
          hide();
        });
    } else {
      // 数据初始化复制前的钩子执行
      if (initialValuesBefor) {
        values = initialValuesBefor(values);
      }
      setInitialValues(values);
      setVisible(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const defaultConfig = {
    visible,
    title: initialValues.title || '',
    description: initialValues.desc || '',
    date: initialValues.date || '',
    content: initialValues.content || '',
    initialValues: initialValues,
    onCancel: () => {
      setVisible(false);
    },
    afterClose: () => {
      if (closeModal) {
        closeModal();
      }
    },
  };

  const newConfig = { ...defaultConfig, ...configRest };

  return <PreviewHtmlModalCustom id={modelchildName} key={modelchildName} {...newConfig} />;
}

export default PreviewHtml;
