const isIntervalOptions = [
  {
    value: false,
    label: '普通',
  },
  {
    value: true,
    label: '区间',
  },
];

export const defaultFormColumns: any = [
  {
    title: '字段名',
    dataIndex: 'name',
    valueType: 'text',
    fieldProps: {
      maxLength: 20,
      placeholder: '请输入，不超过20字符',
    },
    formItemProps: {
      rules: [{ required: true }],
    },
  },
  {
    title: '字段类型',
    dataIndex: 'type',
    valueType: 'select',
    fieldProps: {
      options: [
        {
          value: 1,
          label: '列表',
        },
        {
          value: 2,
          label: '日期',
        },
        {
          value: 3,
          label: '文本',
        },
        {
          value: 4,
          label: '数字',
        },
      ],
    },
    formItemProps: {
      rules: [{ required: true }],
    },
  },
  {
    title: '列表类型',
    dataIndex: 'isMultiple',
    valueType: 'radio',
    initialValue: false,
    hideInForm: true,
    fieldProps: {
      options: [
        {
          value: false,
          label: '单选',
        },
        {
          value: true,
          label: '多选',
        },
      ],
    },
    formItemProps: {
      rules: [{ required: true }],
    },
  },
  {
    title: '是否为区间选择',
    dataIndex: 'isInterval',
    valueType: 'radio',
    initialValue: false,
    hideInForm: true,
    fieldProps: {
      options: isIntervalOptions,
    },
    formItemProps: {
      rules: [{ required: true }],
    },
  },
  {
    title: '区间单位',
    dataIndex: 'intervalUnit',
    valueType: 'text',
    hideInForm: true,
    fieldProps: {
      maxLength: 5,
      placeholder: '请输入，不超过5字符',
    },
  },
];

export const TagFormColumnsFn: any = (
  field: any,
  newFieldValuesField: any,
  record: any,
  type: string,
  itemTag: any,
) => {
  const isInterval = record[field.isInterval];

  return [
    {
      title: '选项类型',
      dataIndex: field.isInterval,
      valueType: 'select',
      fieldProps: {
        options: isIntervalOptions,
        disabled: true,
      },
    },
    {
      title: '选项名称',
      dataIndex: newFieldValuesField.name,
      valueType: 'text',
      hideInForm: isInterval === true,
      fieldProps: {
        maxLength: 20,
        placeholder: '请输入，不超过20字符',
      },
      formItemProps: {
        rules: [{ required: true }],
      },
    },
    {
      title: '',
      valueType: 'group',
      hideInForm: isInterval !== true,
      columns: [
        {
          title: '左区间',
          tooltip: '左区间<x<=右区间',
          dataIndex: newFieldValuesField.minValue,
          valueType: 'digit',
          fieldProps: {
            placeholder: '请输入数字',
            disabled: itemTag[newFieldValuesField.isUnder] ? true : false,
          },
          formItemProps: {
            rules: itemTag[newFieldValuesField.isUnder] ? [] : [{ required: true }],
          },
          width: '178px',
        },
        {
          title: '  ',
          dataIndex: newFieldValuesField.isUnder,
          valueType: 'checkbox',
          fieldProps: {
            options: [
              {
                value: true,
                label: '起始选项',
              },
            ],
          },

          width: '100px',
        },
      ],
    },
    {
      title: '',
      valueType: 'group',
      hideInForm: isInterval !== true,
      columns: [
        {
          title: '右区间',
          tooltip: '左区间<x<=右区间',
          dataIndex: newFieldValuesField.maxValue,
          valueType: 'digit',
          fieldProps: {
            placeholder: '请输入数字',
            disabled: itemTag[newFieldValuesField.isAbove] ? true : false,
          },
          formItemProps: {
            rules: itemTag[newFieldValuesField.isAbove] ? [] : [{ required: true }],
          },
          width: '178px',
        },
        {
          title: '  ',
          dataIndex: newFieldValuesField.isAbove,
          valueType: 'checkbox',
          fieldProps: {
            options: [
              {
                value: true,
                label: '最末选项',
              },
            ],
          },

          width: '100px',
        },
      ],
    },
    {
      title: '选项隐藏',
      dataIndex: newFieldValuesField.disabledProjectIds,
      valueType: 'SelectMultipleCustom',
      hideInForm: newFieldValuesField.showDisabledProjectIds !== true,
      fieldProps: {
        options: newFieldValuesField.projectOptions || [],
      },
      formItemProps: {
        rules: [{ required: true }],
      },
    },
    {
      title: '选项隐藏',
      dataIndex: newFieldValuesField.isDisabled,
      valueType: 'radio',
      initialValue: type === 'valueCreate' ? false : undefined,
      hideInForm: newFieldValuesField.showDisabledProjectIds === true,
      fieldProps: {
        options: [
          {
            value: false,
            label: '显示',
          },
          {
            value: true,
            label: '隐藏',
          },
        ],
      },
      formItemProps: {
        rules: [{ required: true }],
      },
    },
  ];
};
