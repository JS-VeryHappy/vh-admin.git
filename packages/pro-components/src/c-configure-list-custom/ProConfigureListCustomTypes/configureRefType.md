### configureRefType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| setTagFormColumns(必须) | 设置Tag的FormColumns | `(columns:any)=>void` | - | - |
| setNewFieldColumns(必须) | 设置显示的FormColumns | `(columns:any)=>void` | - | - |
| setNewRequiredFormColumns(必须) | 设置录入验证FormColumns | `(columns:any)=>void` | - | - |
| setTagRecord(必须) | 设置tagRecord | `(record:any)=>void` | - | - |
| setRowRecord(必须) | 设置RowRecord | `(record:any)=>void` | - | - |
| formRef(必须) | 设置RowRecord | `React.MutableRefObject<any>` | - | - |
