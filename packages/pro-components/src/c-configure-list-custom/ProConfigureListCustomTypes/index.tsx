/* eslint-disable @typescript-eslint/no-unused-vars */
import type {
  fieldType as fieldTypeApi,
  fieldValuesFieldType as fieldValuesFieldTypeApi,
} from '../index';

export function fieldType(props: fieldTypeApi) {}

export function fieldValuesFieldType(props: fieldValuesFieldTypeApi) {}

export function submitRequestType(props: {
  /**
   * 下面这些等,具体查看types具体定义
   */
  submitRequestType?:
    | 'create'
    | 'edit'
    | 'required'
    | 'delete'
    | 'valueCreate'
    | 'valueEdit'
    | 'valueDelete';
}) {}
