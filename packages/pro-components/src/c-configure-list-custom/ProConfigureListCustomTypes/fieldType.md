### fieldType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| name | 字段名称取值字段<br> | `string` | name | - |
| type | 字段类型取值字段<br> | `string` | type | - |
| isMultiple | 字段是否多选取值字段<br> | `string` | isMultiple | - |
| isInterval | 字段是否是区间取值字段<br> | `string` | isInterval | - |
| intervalUnit | 字段区间单位取值字段<br> | `string` | intervalUnit | - |
| levelType | 字段级别显示取值字段<br> | `string` | level | - |
| levelTypeOptions | 字字段级别显示取值对应限制options<br>如果配置会取这个label作为显示<br> | `{value: string \| number;label: string \| number;}[]` | [{value: 1,label: '系统层级',},{value: 2,label: '公司层级',},{value: 3,label: '项目层级',}] | - |
| tooltip | 字段名称tip显示取值字段<br> | `string` | note | - |
| fieldValues | 字段列表类型为列表的值的取值字段<br> | `string` | fieldValues | - |
