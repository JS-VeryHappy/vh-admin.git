### ProConfigureListCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| proConfigureRef | 操作组件的一些方法 | `React.MutableRefObject<configureRefType\|undefined>` | - | - |    
| loading | 加载状态 | `boolean\|((item:any)=>boolean)` | - | - |
| dataSource(必须) | 列表的数据如果request不传则dataSource必须传如 | `any[]` | - | - |
| pagination | 是否启用分页和tableCustom一样<br> | `boolean` | false | - |
| search | 是否开启搜索和tableCustom一样<br> | `boolean` | false | - |
| field | 配置限制字段取值字段 | `fieldType` | - | - |
| fieldValuesField | 字段列表类型为列表的列表选项属性值的取值字段 | `fieldValuesFieldType` | - | - |        
| tabelColumns | 扩表格配置 | `any[]` | - | - |
| fieldColumns | 扩展表格row显示的字段配置<br>默认和合并类型+录入验证的判断 | `any[]` | - | - |
| requiredFormColumns | 验证表单配置<br>默认为普通默认 | `any[]` | - | - |
| titleBefore | 显示标题设置各种弹窗标题 | `(text:any,type:submitRequestType,rowRecord:any,tagRecord:any)=>string` | - | - |
| submitRequest(必须) | 新增编辑验证新建值等回调表单的字段配置<br>type当前点击的类型<br>record记录数据ornull<br>rowRecord当前选中的row数据仅在选项增删改数据操作时使用<br>tagRecord当前选中的tag数据仅在选项增删改数据操作时使用 | `(type:submitRequestType,record:any,rowRecord:any,tagRecord:any)=>void` | - | - |
| submitOnDone | 请求成功回调 | `(type:submitRequestType,record:any,rowRecord:any,tagRecord:any)=>void` | - | - |
| auth(必须) | 新增编辑验证新建值显示前权限回调<br>type当前点击的类型<br>record记录数据ornull<br>true有false没有权限 | `(type:submitRequestType,record:any,rowRecord:any,tagRecord:any)=>boolean` | - | - |
| columnBefor | 字段表单数据前回调钩子 | `(type:submitRequestType,columns:any,initialValues:any)=>any` | - | - |
| tagFormColumnsBefor | 选项表单数据复制前回调钩子<br> | `(type:submitRequestType,columns:any,record:any,itemTag:any)=>any` | - | - |
| operationBtnShowMax | 操作显示最大个数和tableCustom一样<br> | `number` | 5 | - |
