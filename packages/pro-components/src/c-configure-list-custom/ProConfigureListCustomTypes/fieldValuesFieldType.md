### fieldValuesFieldType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| disabledProjectIds | 禁用项目ID<br> | `string` | disabledProjectIds | - |
| showDisabledProjectIds | 是否显示禁用的多选选项<br>false显示的是单选值<br> | `boolean` | true | - |       
| projectOptions | 选择项目的项目数据<br>false显示的是单选值<br> | `OptionType[]` | [] | - |
| fieldId | 列表字段id<br> | `string` | fieldId | - |
| id | 选项id<br> | `string` | id | - |
| isAbove | 是否以上<br> | `string` | isAbove | - |
| isDisabled | 是否禁用<br> | `string` | isDisabled | - |
| isUnder | 是否以下<br> | `string` | isUnder | - |
| maxValue | 选项值最大值<br> | `string` | maxValue | - |
| minValue | 选项值最小值<br> | `string` | minValue | - |
| name | 选项值名称<br> | `string` | name | - |
