---
title: 高级配置列表
# toc: content
order: 6
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 通用
  path: /Exception
  order: 4
---

# 说明

> 1.ProList 高级列表

```jsx
/**
 * title: 高级列表,
 */
import React from 'react';
import { Button } from 'antd';
import { useState } from 'react';
import { ProConfigureListCustom } from '@vh-admin/pro-components';

function ProConfigureListCustomDemo() {
  let dataSource = [
    {
      id: '1',
      name: '关注物业类型',
      broadType: 2,
      type: 1,
      level: 1,
      businessId: '-1',
      required: 2,
      isMultiple: false,
      isInterval: false,
      fieldValues: [
        {
          id: '1',
          name: '公寓',
          isDisabled: false,
        },
        {
          id: '2',
          name: '住宅',
          isDisabled: false,
        },
        {
          id: '3',
          name: '联排',
          isDisabled: false,
        },
        {
          id: '4',
          name: '叠墅',
          isDisabled: false,
        },
        {
          id: '5',
          name: '商铺',
          isDisabled: false,
        },
        {
          id: '6',
          name: '车位',
          isDisabled: false,
        },
      ],
    },
    {
      id: '3',
      name: '客户白描',
      broadType: 2,
      type: 3,
      level: 1,
      businessId: '-1',
      required: 2,
      isMultiple: false,
      isInterval: false,
      maxValue: 500,
      fieldValues: [
        {
          isDisabled: false,
        },
      ],
    },
    {
      id: '6',
      name: '对比点',
      broadType: 2,
      type: 4,
      level: 1,
      businessId: '-1',
      required: 2,
      isMultiple: false,
      isInterval: false,
      maxValue: 500,
      fieldValues: [
        {
          isDisabled: false,
        },
      ],
    },
    {
      id: '7',
      name: '预计下次到访时间',
      broadType: 2,
      type: 2,
      level: 1,
      businessId: '-1',

      required: 1,
      isMultiple: false,
      isInterval: false,
      fieldValues: [
        {
          isDisabled: false,
        },
      ],
    },
    {
      id: '21',
      name: '客户年龄',
      broadType: 2,
      type: 1,
      level: 2,
      businessId: '1718527',
      required: 3,
      isMultiple: false,
      isInterval: true,
      intervalUnit: '岁',
      fieldValues: [],
    },
  ];

  const submitRequest = (type, values, rowRecord, tagRecord) => {
    console.log(type, values, rowRecord, tagRecord, '----');
  };
  const auth = (type, record, rowRecord, tagRecord) => {
    // console.log(type, record);
    return true;
  };

  dataSource = dataSource.map((item) => {
    return item;
  });
  return (
    <>
      <ProConfigureListCustom
        dataSource={dataSource}
        submitRequest={submitRequest}
        auth={auth}
        fieldValuesField={{
          projectOptions: [
            {
              label: '1',
              value: 1,
            },
          ],
        }}
        // fieldColumns={}
        // field={{
        //   tooltip: 'note',
        // }}
      />
    </>
  );
}
export default ProConfigureListCustomDemo;
```

```jsx
/**
 * title: 高级列表,自定义数据，录入验证等,提交错误处理等
 */
import React from 'react';
import { Button } from 'antd';
import { useState, useRef } from 'react';
import { ProConfigureListCustom } from '@vh-admin/pro-components';

function ProConfigureListCustomDemo1() {
  const proConfigureRef = useRef();

  let dataSource = [
    {
      id: '1',
      name: '关注物业类型',
      broadType: 2,
      type: 1,
      level: 1,
      businessId: '-1',
      required: 2,
      isMultiple: false,
      isInterval: false,
      fieldValues: [
        {
          id: '1',
          name: '公寓',
          isDisabled: false,
        },
        {
          id: '2',
          name: '住宅',
          isDisabled: false,
        },
        {
          id: '3',
          name: '联排',
          isDisabled: false,
        },
        {
          id: '4',
          name: '叠墅',
          isDisabled: false,
        },
        {
          id: '5',
          name: '商铺',
          isDisabled: false,
        },
        {
          id: '6',
          name: '车位',
          isDisabled: false,
        },
      ],
    },
    {
      id: '3',
      name: '客户白描',
      broadType: 2,
      type: 3,
      level: 1,
      businessId: '-1',
      required: 2,
      isMultiple: false,
      isInterval: false,
      maxValue: 500,
    },
    {
      id: '6',
      name: '对比点',
      broadType: 2,
      type: 4,
      level: 1,
      businessId: '-1',
      required: 2,
      isMultiple: false,
      isInterval: false,
      maxValue: 500,
    },
    {
      id: '7',
      name: '预计下次到访时间',
      broadType: 2,
      type: 2,
      level: 1,
      businessId: '-1',

      required: 1,
      isMultiple: false,
      isInterval: false,
    },
    {
      id: '21',
      name: '客户年龄',
      broadType: 2,
      type: 1,
      level: 2,
      businessId: '1718527',
      required: 3,
      isMultiple: false,
      isInterval: true,
      intervalUnit: '岁',
      fieldValues: [
        {
          id: '1',
          name: '公寓',
          isDisabled: false,
        },
      ],
    },
  ];
  const submitRequest = async (type, values, rowRecord, tagRecord) => {
    console.log(proConfigureRef);
    console.log(type, values, rowRecord, tagRecord);
    try {
      await getFieldSaveField({
        ...values,
        ...params,
      });
      return true;
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  };
  const auth = (type, record, rowRecord, tagRecord) => {
    // console.log(type, record);
    return true;
  };

  dataSource = dataSource.map((item) => {
    return item;
  });
  return (
    <>
      <ProConfigureListCustom
        proConfigureRef={proConfigureRef}
        dataSource={dataSource}
        submitRequest={submitRequest}
        auth={auth}
        requiredFormColumns={[
          {
            title: '来电业务',
            valueType: 'radio',
            dataIndex: 'telRequired',
            fieldProps: {
              options: [
                {
                  value: 1,
                  label: '选填',
                },
                {
                  value: 2,
                  label: '必填',
                },
                {
                  value: 3,
                  label: '必填不可改',
                },
              ],
            },
            formItemProps: {
              rules: [{ required: true }],
            },
          },
          {
            title: '来访业务',
            valueType: 'radio',
            dataIndex: 'visitRequired',
            fieldProps: {
              options: [
                {
                  value: 1,
                  label: '选填',
                },
                {
                  value: 2,
                  label: '必填',
                },
                {
                  value: 3,
                  label: '必填不可改',
                },
              ],
            },
            formItemProps: {
              rules: [{ required: true }],
            },
          },
        ]}
        field={{
          name: 'name',
          type: 'type',
          isMultiple: 'isMultiple',
          isInterval: 'isInterval',
          intervalUnit: 'intervalUnit',
          levelType: 'level',
          levelTypeOptions: [
            {
              value: 1,
              label: '系统层级',
            },
            {
              value: 2,
              label: '公司层级',
            },
            {
              value: 3,
              label: '项目层级',
            },
          ],
          tooltip: 'note',
        }}
        fieldValuesField={{
          disabledProjectIds: 'disabledProjectIds',
          showDisabledProjectIds: false,
          projectOptions: [
            {
              label: '1',
              value: 1,
            },
          ],
          fieldId: 'fieldId',
          id: 'id',
          isAbove: 'isAbove',
          isDisabled: 'isDisabled',
          isUnder: 'isUnder',
          maxValue: 'maxValue',
          minValue: 'minValue',
          name: 'name',
        }}
      />
    </>
  );
}
export default ProConfigureListCustomDemo1;
```

## API

<embed src="./ProConfigureListCustomTypes/ProConfigureListCustomType.md"></embed>

<embed src="./ProConfigureListCustomTypes/configureRefType.md"></embed>

<embed src="./ProConfigureListCustomTypes/fieldValuesFieldType.md"></embed>

<embed src="./ProConfigureListCustomTypes/fieldType.md"></embed>

<embed src="./ProConfigureListCustomTypes/submitRequestType.md"></embed>


