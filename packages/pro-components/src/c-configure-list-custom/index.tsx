import { CloseOutlined, PlusOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { Space, Tag, Typography, Tooltip, message } from 'antd';
import ProDescriptions from '@ant-design/pro-descriptions';
import React, { useState, useEffect, useRef } from 'react';
import TableCustom from '../t-table-custom';
import './index.less';
import FormCustom from '../f-form-custom';
import { defaultFormColumns, TagFormColumnsFn } from './defind';
import { deepCopy } from '@vh-admin/pro-utils';
import DeleteModalCustom from '../m-delete-modal-custom';
import { useMount } from 'ahooks';
import type { OptionType } from '../f-form-custom/types';

const { Title, Text } = Typography;

export declare type submitRequestType =
  | 'create'
  | 'edit'
  | 'required'
  | 'delete'
  | 'valueCreate'
  | 'valueEdit'
  | 'valueDelete';

export declare type fieldType = {
  /**
   * 字段名称取值字段
   * @default name
   */
  name?: string;
  /**
   * 字段类型 取值字段
   * @default type
   */
  type?: string;
  /**
   * 字段是否多选 取值字段
   * @default isMultiple
   */
  isMultiple?: string;
  /**
   * 字段是否是区间 取值字段
   * @default isInterval
   */
  isInterval?: string;
  /**
   * 字段区间单位 取值字段
   * @default intervalUnit
   */
  intervalUnit?: string;
  /**
   * 字段级别显示取值字段
   * @default level
   */
  levelType?: string;
  /**
   * 字字段级别显示取值 对应限制options
   * 如果配置会取这个label作为显示
   * @default [{value: 1,label: '系统层级',},{value: 2,label: '公司层级',},{value: 3,label: '项目层级',}]
   */
  levelTypeOptions?: {
    value: string | number;
    label: string | number;
  }[];
  /**
   * 字段名称tip显示取值字段
   * @default note
   */
  tooltip?: string;
  /**
   * 字段列表类型为列表的  值的取值字段
   * @default fieldValues
   */
  fieldValues?: string;
};

export declare type fieldValuesFieldType = {
  /**
   * 禁用项目ID
   * @default disabledProjectIds
   */
  disabledProjectIds?: string;
  /**
   * 是否显示禁用的多选选项
   * false显示的是 单选值
   * @default true
   */
  showDisabledProjectIds?: boolean;
  /**
   * 选择项目的项目数据
   * false显示的是 单选值
   * @default []
   */
  projectOptions?: OptionType[];
  /**
   * 列表字段id
   * @default fieldId
   */
  fieldId?: string;
  /**
   * 选项id
   * @default id
   */
  id?: string;
  /**
   * 是否以上
   * @default isAbove
   */
  isAbove?: string;
  /**
   * 是否禁用
   * @default isDisabled
   */
  isDisabled?: string;
  /**
   * 是否以下
   * @default isUnder
   */
  isUnder?: string;
  /**
   * 选项值最大值
   * @default maxValue
   */
  maxValue?: string;
  /**
   * 选项值最小值
   * @default minValue
   */
  minValue?: string;
  /**
   * 选项值名称
   * @default name
   */
  name?: string;
};

export declare type ProConfigureListCustomType = {
  /**
   * 操作组件的一些方法
   */
  proConfigureRef?: React.MutableRefObject<configureRefType | undefined>;
  /**
   * 加载状态
   */
  loading?: boolean | ((item: any) => boolean);
  /**
   * 列表的数据 如果request不传则dataSource必须传如
   */
  dataSource: any[];
  /**
   * 是否启用分页 和tableCustom 一样
   * @default false
   */
  pagination?: boolean;
  /**
   * 是否开启搜索 和tableCustom 一样
   * @default false
   */
  search?: boolean;
  /**
   * 配置限制字段取值字段
   */
  field?: fieldType;
  /**
   * 字段列表类型为列表的 列表选项属性值的取值字段
   */
  fieldValuesField?: fieldValuesFieldType;
  /**
   * 扩表格配置
   */
  tabelColumns?: any[];
  /**
   * 扩展表格row显示的字段配置
   * 默认和合并 类型+录入验证的判断
   */
  fieldColumns?: any[];
  /**
   * 验证表单配置
   * 默认为普通默认
   */
  requiredFormColumns?: any[];
  /**
   * 显示标题设置各种弹窗标题
   */
  titleBefore?: (text: any, type: submitRequestType, rowRecord: any, tagRecord: any) => string;
  /**
   * 新增 编辑 验证 新建值 等回调表单的字段配置
   * type 当前点击的类型
   * record 记录数据 or null
   * rowRecord 当前选中的row数据 仅在选项增删改数据操作时使用
   * tagRecord 当前选中的tag数据 仅在选项增删改数据操作时使用
   */
  submitRequest: (type: submitRequestType, record: any, rowRecord: any, tagRecord: any) => void;
  /**
   * 请求成功回调
   */
  submitOnDone?: (type: submitRequestType, record: any, rowRecord: any, tagRecord: any) => void;
  /**
   * 新增 编辑 验证 新建值 显示前权限回调
   * type 当前点击的类型
   * record 记录数据 or null
   * true 有 false 没有权限
   */
  auth: (type: submitRequestType, record: any, rowRecord: any, tagRecord: any) => boolean;
  /**
   * 字段 表单数据前回调钩子
   */
  columnBefor?: (type: submitRequestType, columns: any, initialValues: any) => any;
  /**
   * 选项表单数据复制前回调钩子
   *
   */
  tagFormColumnsBefor?: (type: submitRequestType, columns: any, record: any, itemTag: any) => any;
  /**
   * 操作显示最大个数 和tableCustom 一样
   * @default 5
   */
  operationBtnShowMax?: number;
};

export declare type configureRefType = {
  /**
   * 设置Tag的FormColumns
   */
  setTagFormColumns: (columns: any) => void;
  /**
   * 设置显示的FormColumns
   */
  setNewFieldColumns: (columns: any) => void;
  /**
   * 设置录入验证FormColumns
   */
  setNewRequiredFormColumns: (columns: any) => void;
  /**
   * 设置tagRecord
   */
  setTagRecord: (record: any) => void;
  /**
   * 设置RowRecord
   */
  setRowRecord: (record: any) => void;
  /**
   * 设置RowRecord
   */
  formRef: React.MutableRefObject<any>;
};

function ProConfigureListCustom(Props: ProConfigureListCustomType) {
  //ProConfigureListCustom 的ref
  // 数据存放
  const [newDataSource, setNewDataSource] = useState<any>([]);
  // 配置关键字段取值
  const [newField, setNewField] = useState<fieldType>({});
  // 配置关键字段取值
  const [newFieldValuesField, setNewFieldValuesField] = useState<fieldValuesFieldType>({});
  // 表格的显示配置
  const [newFieldColumns, setNewFieldColumns] = useState<any>([]);
  // 录入验证的表单配置
  const [newRequiredFormColumns, setNewRequiredFormColumns] = useState<any>([]);
  //tag form 显示
  const [tagFormVisible, setTagFormVisible] = useState<boolean>(false);
  //tag 点击选择的表格的row数据
  const [rowRecord, setRowRecord] = useState<any>(null);
  //tag 点击的数据
  const [tagRecord, setTagRecord] = useState<any>(null);
  //tag 表单数据
  const [tagFormColumns, setTagFormColumns] = useState<any>(null);
  //tag 表单ref
  const formRef = useRef<any>(null);
  // 存放点击业务类型
  const [clickType, setClickType] = useState<submitRequestType>();
  //tag delete 显示
  const [tagDeleteVisible, setTagDeleteVisible] = useState<boolean>(false);
  const {
    proConfigureRef,
    loading,
    submitRequest,
    auth,
    tagFormColumnsBefor,
    columnBefor,
    dataSource,
    pagination = false,
    search = false,
    tabelColumns,
    fieldColumns,
    requiredFormColumns,
    field,
    fieldValuesField,
    operationBtnShowMax = 5,
    titleBefore,
  } = Props;

  useMount(() => {
    if (proConfigureRef) {
      proConfigureRef.current = {
        setTagFormColumns: (columns: any) => {
          setTagFormColumns(columns);
        },
        setNewFieldColumns: (columns: any) => {
          setNewFieldColumns(columns);
        },
        setNewRequiredFormColumns: (columns: any) => {
          setNewRequiredFormColumns(columns);
        },
        setTagRecord: (record: any) => {
          setTagRecord(record);
        },
        setRowRecord: (record: any) => {
          setRowRecord(record);
        },
        formRef,
      };
    }
  });
  useEffect(() => {
    // 字段取值的字段默认配置
    let defaultField: fieldType = {
      name: 'name',
      type: 'type',
      isMultiple: 'isMultiple',
      isInterval: 'isInterval',
      intervalUnit: 'intervalUnit',
      levelType: 'level',
      levelTypeOptions: [
        {
          value: 1,
          label: '系统层级',
        },
        {
          value: 2,
          label: '公司层级',
        },
        {
          value: 3,
          label: '项目层级',
        },
      ],
      tooltip: 'note',
      fieldValues: 'fieldValues',
    };

    //合并传进来的自定义
    if (field) {
      defaultField = { ...defaultField, ...field };
    }

    let defaultFieldValuesField: fieldValuesFieldType = {
      disabledProjectIds: 'disabledProjectIds',
      showDisabledProjectIds: true,
      projectOptions: [],
      fieldId: 'fieldId',
      id: 'id',
      isAbove: 'isAbove',
      isDisabled: 'isDisabled',
      isUnder: 'isUnder',
      maxValue: 'maxValue',
      minValue: 'minValue',
      name: 'name',
    };
    //合并传进来的自定义
    if (fieldValuesField) {
      defaultFieldValuesField = { ...defaultFieldValuesField, ...fieldValuesField };
    }

    // 处理公用必须的限制值 类型和等级
    const source: any = dataSource.map((item: any) => {
      // 处理系统等级显示值
      if (defaultField.levelType) {
        if (defaultField.levelTypeOptions) {
          item.level_type_name = defaultField.levelTypeOptions.find(
            //@ts-ignore
            (i: any) => i.value === item[defaultField.levelType],
          )?.label;
        } else {
          item.level_type_name = item[defaultField.levelType];
        }
      }
      // 处理字段类型显示值
      item.type_name = '';
      if (defaultField.type && defaultField.isMultiple && defaultField.isInterval) {
        if (item[defaultField.type] === 1) {
          let str = '列表(';
          if (item[defaultField.isMultiple]) {
            str = str + '多选、';
          } else {
            str = str + '单选、';
          }

          if (item[defaultField.isInterval]) {
            str = str + '区间';
          } else {
            str = str + '普通';
          }
          str = str + ')';
          item.type_name = str;
        } else if (item[defaultField.type] === 2) {
          item.type_name = '日期';
        } else if (item[defaultField.type] === 3) {
          item.type_name = '文本';
        } else if (item[defaultField.type] === 4) {
          item.type_name = '数字';
        }
      }

      return item;
    });

    let defaultFieldColumns: any = [
      {
        title: '类型',
        valueType: 'txt',
        dataIndex: 'type_name',
      },
    ];

    // 录入验证表单处理 赋默认值
    let rFormColumns: any = [];
    if (requiredFormColumns) {
      rFormColumns = requiredFormColumns;
    } else {
      rFormColumns = [
        {
          title: '录入验证',
          valueType: 'radio',
          dataIndex: 'required',
          fieldProps: {
            options: [
              {
                value: 1,
                label: '必填',
              },
              {
                value: 2,
                label: '选填',
              },
              {
                value: 3,
                label: '必填不可改',
                disabled: true,
              },
            ],
          },
          formItemProps: {
            rules: [{ required: true }],
          },
        },
      ];
    }

    // 录入验证表单 合并到表格row显示上
    defaultFieldColumns = [...defaultFieldColumns, ...rFormColumns];

    // 处理自定义表格row显示配置
    if (fieldColumns) {
      defaultFieldColumns = [...defaultFieldColumns, ...fieldColumns];
    }

    setNewField(defaultField);
    setNewFieldValuesField(defaultFieldValuesField);
    setNewDataSource(source);
    setNewFieldColumns(defaultFieldColumns);
    setNewRequiredFormColumns(rFormColumns);
  }, [field, dataSource, requiredFormColumns, fieldColumns, fieldValuesField]);

  const onSubmitRequest = async (type: submitRequestType, ...params: any) => {
    try {
      await submitRequest(type, params[0], rowRecord, tagRecord);
      if (type === 'valueCreate') {
        message.success('新建选项成功');
      } else if (type === 'valueDelete') {
        message.success('删除成功');
      } else if (type === 'valueEdit') {
        message.success('编辑选项成功');
      } else if (type === 'create') {
        message.success('新建字段成功');
      } else if (type === 'edit') {
        message.success('编辑字段成功');
      } else if (type === 'delete') {
        message.success('删除字段成功');
      } else if (type === 'required') {
        message.success('编辑录入验证成功');
      }
      // if (type === 'valueCreate' || type === 'valueDelete' || type === 'valueEdit') {
      //   tableRef.current?.reload();
      // }
      if (tagDeleteVisible) {
        setTagDeleteVisible(false);
      }

      // if (tagFormVisible) {
      //   setTagFormVisible(false);
      // }
      return true;
    } catch (error: any) {
      throw new Error(error);
    }
  };

  const onAuth = (type: submitRequestType, ...params: any) => {
    let record = null;
    let tag = null;
    if (type === 'edit' || type === 'required' || type === 'delete') {
      record = params[1];
    } else if (type === 'valueCreate' || type === 'valueEdit' || type === 'valueDelete') {
      record = params[0];
      tag = params[1];
    }
    if (!tag) {
      tag = tagRecord;
    }
    return auth(type, record, rowRecord, tag);
  };

  const onValuesChange = (value: any, formColumns: any, setFormColumns: any, onFormRef: any) => {
    if (value.hasOwnProperty('type')) {
      if (value.type === 1) {
        const hasMultiple = formColumns.find((i: any) => i.dataIndex === 'isMultiple');
        if (hasMultiple) {
          hasMultiple.hideInForm = false;
        }
        const hasInterval = formColumns.find((i: any) => i.dataIndex === 'isInterval');
        if (hasInterval) {
          hasInterval.hideInForm = false;
        }
        // const hasUnit = formColumns.find((i: any) => i.dataIndex === 'intervalUnit');
        // if (hasUnit) {
        //   hasUnit.hideInForm = false;
        // }
      } else {
        const hasMultiple = formColumns.find((i: any) => i.dataIndex === 'isMultiple');
        if (hasMultiple) {
          hasMultiple.hideInForm = true;
        }
        const hasInterval = formColumns.find((i: any) => i.dataIndex === 'isInterval');
        if (hasInterval) {
          hasInterval.hideInForm = true;
        }
        // const hasUnit = formColumns.find((i: any) => i.dataIndex === 'intervalUnit');
        // if (hasUnit) {
        //   hasUnit.hideInForm = true;
        // }
      }
      onFormRef.current?.setFieldsValue({
        isMultiple: false,
        isInterval: false,
        intervalUnit: null,
      });
      setFormColumns(formColumns);
    }

    if (value.hasOwnProperty('isInterval')) {
      const hasUnit = formColumns.find((i: any) => i.dataIndex === 'intervalUnit');
      if (hasUnit) {
        if (value.isInterval) {
          hasUnit.hideInForm = false;
        } else {
          hasUnit.hideInForm = true;
        }
        setFormColumns(formColumns);
      }
    }
  };

  const onColumnBefor = (type: submitRequestType, columns: any, initialValues: any) => {
    let newColumns = columns;
    if (type === 'edit') {
      //只可修改 字段名 和区间单位
      const hasType = newColumns.find((i: any) => i.dataIndex === 'type');
      if (hasType) {
        hasType.fieldProps.disabled = true;
      }

      if (newField.type && initialValues[newField.type]) {
        if (initialValues[newField.type] === 1) {
          const hasMultiple = newColumns.find((i: any) => i.dataIndex === 'isMultiple');
          if (hasMultiple) {
            hasMultiple.hideInForm = false;
            hasMultiple.fieldProps.disabled = true;
          }
          delete hasMultiple.initialValue;

          const hasInterval = newColumns.find((i: any) => i.dataIndex === 'isInterval');
          if (hasInterval) {
            hasInterval.hideInForm = false;
            hasInterval.fieldProps.disabled = true;
          }
          delete hasInterval.initialValue;

          if (newField.isInterval && initialValues[newField.isInterval]) {
            const hasUnit = newColumns.find((i: any) => i.dataIndex === 'intervalUnit');
            if (hasUnit) {
              hasUnit.hideInForm = false;
            }
          }
        }
      }
    }
    if (typeof columnBefor === 'function') {
      newColumns = columnBefor(type, newColumns, initialValues);
    }
    return newColumns;
  };

  const tagClick = (type: submitRequestType, record: any, itemTag: any) => {
    setClickType(type);

    if (type === 'valueEdit' && !onAuth('valueEdit', record, itemTag)) {
      return;
    }
    if (newField.isInterval) {
      itemTag[newField.isInterval] = record[newField.isInterval];
    }
    if (itemTag.isAbove) {
      itemTag.isAbove = [true];
    } else {
      delete itemTag.isAbove;
    }

    if (itemTag.isUnder) {
      itemTag.isUnder = [true];
    } else {
      delete itemTag.isUnder;
    }

    //弹窗打开前调用回调 可自行处理逻辑
    let tagColumns: any = TagFormColumnsFn(newField, newFieldValuesField, record, type, itemTag);
    if (typeof tagFormColumnsBefor === 'function') {
      tagColumns = tagFormColumnsBefor(type, tagColumns, record, itemTag);
    }
    setTagFormColumns(tagColumns);
    setRowRecord(record);
    setTagRecord(itemTag);
    if (type === 'valueCreate' || type === 'valueEdit') {
      setTagFormVisible(true);
    } else if (type === 'valueDelete') {
      setTagDeleteVisible(true);
    }
  };

  const onTagValuesChange = (value: any) => {
    if (value.hasOwnProperty(newFieldValuesField.isUnder)) {
      const columns = deepCopy(tagFormColumns);
      let has: any = null;
      columns.forEach((item: any) => {
        if (item.columns) {
          const isItem = item.columns.find(
            (i: any) => i.dataIndex === newFieldValuesField.minValue,
          );
          if (isItem) {
            has = isItem;
          }
        }
      });
      if (has) {
        if (newFieldValuesField.isUnder && value[newFieldValuesField.isUnder].length === 0) {
          has.fieldProps.disabled = false;
          has.formItemProps.rules = [{ required: true }];
        } else {
          formRef.current?.setFieldsValue({
            [`${newFieldValuesField.minValue}`]: null,
          });
          has.fieldProps.disabled = true;
          has.formItemProps.rules = [];
        }
        setTagFormColumns(columns);
      }
    }
    if (value.hasOwnProperty(newFieldValuesField.isAbove)) {
      const columns = deepCopy(tagFormColumns);
      let has: any = null;
      columns.forEach((item: any) => {
        if (item.columns) {
          const isItem = item.columns.find(
            (i: any) => i.dataIndex === newFieldValuesField.maxValue,
          );
          if (isItem) {
            has = isItem;
          }
        }
      });
      if (has) {
        if (newFieldValuesField.isAbove && value[newFieldValuesField.isAbove].length === 0) {
          has.fieldProps.disabled = false;
          has.formItemProps.rules = [{ required: true }];
        } else {
          formRef.current?.setFieldsValue({
            [`${newFieldValuesField.maxValue}`]: null,
          });
          has.fieldProps.disabled = true;
          has.formItemProps.rules = [];
        }
        setTagFormColumns(columns);
      }
    }
  };

  const setTitle = (text: any, type: submitRequestType) => {
    if (titleBefore) {
      return titleBefore(text, type, rowRecord, tagRecord);
    }
    if (
      (type === 'edit' || type === 'required' || type === 'delete' || type === 'valueCreate') &&
      rowRecord &&
      newField.name
    ) {
      return `${text}(${rowRecord[newField.name]})`;
    } else if (
      (type === 'valueEdit' || type === 'valueDelete') &&
      tagRecord &&
      newField.name &&
      newFieldValuesField.name
    ) {
      return `${text}(${rowRecord[newField.name]}-${tagRecord[newFieldValuesField.name]})`;
    }
    return text;
  };

  //处理一些默认传进来的配置属性
  const config: any = {
    search,
    options: false,
    operationBtnShowMax,
    dataSource: newDataSource,
  };
  if (tabelColumns) {
    config.columns = tabelColumns;
  }
  if (pagination === false) {
    config.pagination = false;
  }
  if (loading) {
    config.loading = loading;
  }

  if (!newField) {
    return null;
  }

  return (
    <>
      <TableCustom
        className="pro-configure-list-custom"
        {...config}
        split={true}
        tableType="ProList"
        rowKey="id"
        headerTitleConfig={{
          create: {
            type: 'dashed',
            style: {},
            text: '添加一行数据',
            block: true,
            auth: onAuth.bind(null, 'create'),
            modalConfig: {
              modalType: 'Form',
              columns: defaultFormColumns,
              config: {
                title: setTitle('新建字段', 'create'),
                columnBefor: onColumnBefor.bind(null, 'create'),
                onValuesChange,
                submitRequest: onSubmitRequest.bind(null, 'create'),
                submitOnDone: () => {},
              },
            },
          },
        }}
        operationConfig={{
          required: {
            text: '录入验证',
            type: 'link',
            auth: onAuth.bind(null, 'required'),
            modalConfig: {
              modalType: 'Form',
              edit: true,
              columns: newRequiredFormColumns,
              config: {
                title: setTitle('编辑录入验证', 'required'),
                columnBefor: onColumnBefor.bind(null, 'required'),
                submitRequest: onSubmitRequest.bind(null, 'required'),
                submitOnDone: () => {},
              },
            },
          },
          edit: {
            type: 'link',
            auth: onAuth.bind(null, 'edit'),
            modalConfig: {
              modalType: 'Form',
              edit: true,
              columns: defaultFormColumns,
              config: {
                title: setTitle('编辑字段', 'edit'),
                columnBefor: onColumnBefor.bind(null, 'edit'),
                onValuesChange,
                submitRequest: onSubmitRequest.bind(null, 'edit'),
                submitOnDone: () => {},
              },
            },
          },
          delete: {
            type: 'link',
            auth: onAuth.bind(null, 'delete'),
            modalConfig: {
              modalType: 'Delete',
              edit: true,
              config: {
                title: setTitle('删除', 'delete'),
                submitRequest: onSubmitRequest.bind(null, 'delete'),
                submitOnDone: () => {},
              },
            },
          },
        }}
        bordered={true}
        metas={{
          content: {
            render: (text: any, record: any) => {
              return (
                <>
                  <Space direction="vertical" size={[8, 16]} key={record.id}>
                    <Space align="center" wrap>
                      <Title level={4} style={{ marginBottom: 0 }}>
                        {newField.name && record[newField.name]}
                      </Title>
                      {newField.levelType && record.level_type_name && (
                        <Text
                          type={
                            record[newField.levelType] == 1
                              ? 'danger'
                              : record[newField.levelType] == 2
                              ? 'warning'
                              : 'secondary'
                          }
                        >
                          ({record.level_type_name})
                        </Text>
                      )}
                      {newField.tooltip && record[newField.tooltip] && (
                        <Tooltip title={record[newField.tooltip]}>
                          <QuestionCircleOutlined
                            style={{ color: '#999' }}
                            onPointerEnterCapture={undefined}
                            onPointerLeaveCapture={undefined}
                          />
                        </Tooltip>
                      )}
                    </Space>
                    <Space align="center" wrap>
                      <ProDescriptions
                        dataSource={record}
                        column={6}
                        size="small"
                        columns={newFieldColumns}
                      />
                    </Space>
                    {newField.type && record[newField.type] === 1 && (
                      <Space align="center" wrap>
                        {newField.fieldValues &&
                          record[newField.fieldValues]?.map((itemTag: any) => {
                            const isDisabled =
                              newFieldValuesField.isDisabled &&
                              itemTag[newFieldValuesField.isDisabled];

                            return (
                              <Tag
                                color={isDisabled ? '#cccccc' : '#1677ff'}
                                onClick={() => tagClick('valueEdit', record, itemTag)}
                                key={`${record.id}-${
                                  newFieldValuesField.id && itemTag[newFieldValuesField.id]
                                }`}
                              >
                                <Space>
                                  {newFieldValuesField.name && itemTag[newFieldValuesField.name]}
                                  {/* {newField.intervalUnit && record[newField.intervalUnit]} */}
                                  {onAuth('valueDelete', record, itemTag) && (
                                    <CloseOutlined
                                      onClick={(e: any) => {
                                        tagClick('valueDelete', record, itemTag);
                                        e.stopPropagation();
                                      }}
                                      onPointerEnterCapture={undefined}
                                      onPointerLeaveCapture={undefined}
                                    />
                                  )}
                                </Space>
                              </Tag>
                            );
                          })}
                        {onAuth('valueCreate', record, {}) && (
                          <Tag
                            key="valueCreate"
                            onClick={() => tagClick('valueCreate', record, {})}
                          >
                            <PlusOutlined
                              onPointerEnterCapture={undefined}
                              onPointerLeaveCapture={undefined}
                            />
                          </Tag>
                        )}
                      </Space>
                    )}
                  </Space>
                </>
              );
            },
          },
        }}
      />
      {tagFormVisible && (
        <FormCustom
          key="ProConfigureListCustom-FormCustom"
          formRef={formRef}
          onFinish={clickType && onSubmitRequest.bind(null, clickType)}
          title={
            clickType === 'valueCreate'
              ? setTitle('新建选项', 'valueCreate')
              : setTitle('编辑选项', 'valueEdit')
          }
          layoutType="ModalForm"
          visible={true}
          initialValues={tagRecord}
          onValuesChange={onTagValuesChange}
          onVisibleChange={(value: any) => {
            if (!value) {
              setTagFormVisible(value);
            }
          }}
          columns={tagFormColumns}
        />
      )}
      <DeleteModalCustom
        title={setTitle('删除', 'valueDelete')}
        visible={tagDeleteVisible}
        onOk={clickType && onSubmitRequest.bind(null, clickType)}
        onCancel={() => setTagDeleteVisible(false)}
      />
    </>
  );
}

export default ProConfigureListCustom;
