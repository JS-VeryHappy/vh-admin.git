import React, { useState, useEffect, useRef, useImperativeHandle, useCallback } from 'react';
import type { EditableFormInstance, ProColumns } from '@ant-design/pro-table';
import type { CustomType } from '../f-form-custom/types';
import './index.less';
import TableCustom from '../t-table-custom';
import { advancedFieldsFinish, advancedFieldsInitial } from '../f-form-custom';
import { Popconfirm } from 'antd';
import { debounce } from '@vh-admin/pro-utils';

export declare type EditableTableCustomType = {
  /**
   * 唯一id取值
   * @default 'id'
   */
  rowKey?: string;
  /**
   * 表格配置项
   */
  columns?: ProColumns<any, any>[] | any;
  /**
   * 表格最大高度
   */
  maxHeight?: number;

  /**
   * 表格数据改变回调函数
   */
  onChange?: any;

  /**
   * 添加一行按钮显示位置 如果不配置则不显示添加按钮
   */
  position?: string | 'top' | 'bottom' | undefined;

  /**
   * 新增一行按钮文案
   */
  creatorButtonText?: string;

  /**
   * 自定义编辑的操作
   */
  actionRender?: any;

  /**
   * 操作编辑表格的ref
   */
  editableFormRef?: any;

  /**
   * editable 编辑行配置
   */
  editable?: any;
  /**
   * 默认值
   * 格式为[{id:xxx,...}] 对象中id为必须的
   */
  value?: any;

  /**
   * 返回可以操作的Ref
   */
  editableRef: React.MutableRefObject<{
    editableFormRef: any;
  }>;
  /**
   * 是否受控, 如果受控每次编辑都会触发 onChange，并且会修改 dataSource
   * @default false
   */
  controlled?: boolean;
  /**
   * 是否开启默认编辑模式
   * @default true
   */
  autoEdit?: boolean;
  /**
   *  配置行操作按钮
   * @default []
   */
  option: ('edit' | 'delete')[];
  /**
   *  默认编辑的row
   * @default []
   */
  defaultEditablekeys: any[];
} & CustomType;

const setRecordColumns = (record: any, columns: any) => {
  if (columns instanceof Array && columns && columns.length > 0) {
    columns.forEach((column: any) => {
      if (column.dataIndex) {
        record[column.dataIndex] = null;
        if (column.initialValue) {
          record[column.dataIndex] = column.initialValue;
        }
      }
      if (columns instanceof Array && column.columns && columns.length > 0) {
        setRecordColumns(record, column.columns);
      }
    });
  }
};

const EditableTableCustom = (props: EditableTableCustomType) => {
  const {
    rowKey = 'id',
    value,
    onChange,
    style,
    className,
    columns,
    maxHeight,
    position,
    creatorButtonText = '添加',
    actionRender,
    editable,
    editableFormRef,
    form,
    controlled = true,
    autoEdit = true,
    option = [],
    defaultEditablekeys = [],
    ...rest
  } = props;

  const [values, setValues] = useState<any>([]);
  const valuesRef = useRef<any>([]);
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>(defaultEditablekeys);
  const editableRef = useRef<EditableFormInstance<any>>();
  const formRef = useRef<any>();

  const setValuesFn = (newValues: any) => {
    if (newValues && newValues.length > 0) {
      setValues(newValues);
      valuesRef.current = newValues;
      if (autoEdit) {
        setEditableRowKeys(newValues.map((i: any) => i[rowKey]));
      }
    } else {
      setValues([]);
      valuesRef.current = [];
      if (autoEdit) {
        setEditableRowKeys([]);
      }
    }
  };

  useEffect(() => {
    if (value) {
      const newValues: any = [];
      if (value) {
        value.forEach((item: any) => {
          const formInitialValues = { ...item };
          advancedFieldsInitial(columns, formInitialValues);
          newValues.push(formInitialValues);
        });
      }
      setValuesFn(newValues);
    } else {
      setValuesFn([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  const handerChange = (newValue: any) => {
    if (typeof onChange === 'function') {
      const newValues: any = [];
      if (newValue) {
        newValue.forEach((item: any) => {
          const nvalue: any = { ...item };
          const oldValue: any = { ...item };
          advancedFieldsFinish(columns, nvalue, oldValue);
          newValues.push(nvalue);
        });
      }
      onChange(newValues);
    }
  };

  const debounceHanderChange = debounce(handerChange, 200);

  const onCancel = (key: any) => {
    const keys = [...editableKeys];
    const index = keys.findIndex((i: any) => i === key);
    if (index !== -1) {
      keys.splice(index, 1);
    }
    setEditableRowKeys(keys);
  };

  const onSave = async (key: any, data: any, row: any) => {
    const newValues = [...values];
    const index = newValues.findIndex((i: any) => i[rowKey] === key);
    if (index !== -1) {
      const nvalue: any = { ...data };
      const oldValue: any = { ...data };
      advancedFieldsFinish(columns, nvalue, oldValue);
      newValues.splice(index, 1, nvalue);
      onChange(newValues);
      if (!autoEdit) {
        onCancel(key);
      }
    }
  };

  const onEdit = (key: any) => {
    const keys: any = [...editableKeys];
    keys.push(key);
    setEditableRowKeys(keys);
  };

  const onDelete = (key: any) => {
    const newValues: any = [...valuesRef.current];
    const index = newValues.findIndex((i: any) => i[rowKey] === key);

    if (index !== -1) {
      newValues.splice(index, 1);
      onChange(newValues);
    }
  };

  useEffect(() => {
    if (
      form &&
      form.formRef &&
      form.formRef.current &&
      form.formRef.current.customValidate &&
      editableRef &&
      formRef
    ) {
      form.formRef.current.customValidate.push(formRef);
      // form.customValidate = editableRef;
    }

    if (form && form.customValidate && editableRef && formRef) {
      form.customValidate.push(formRef);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form, formRef.current, editableRef.current]);

  useImperativeHandle(
    editableFormRef,
    () => {
      return {
        ...editableRef.current,
        ...formRef.current,
        onDelete,
        onEdit,
        onSave,
        onCancel,
        getValues: () => {
          return valuesRef.current;
        },
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [editableRef.current, formRef.current],
  );

  const optionNode = useCallback(
    (text: any, record: any) => {
      const node: any = [];
      if (option.includes('edit')) {
        node.push(
          <a
            key="editable"
            onClick={() => {
              onEdit(record[rowKey]);
            }}
          >
            编辑
          </a>,
        );
      }
      if (option.includes('delete')) {
        node.push(
          <Popconfirm
            title="删除此行?"
            onConfirm={() => {
              onDelete(record[rowKey]);
            }}
            okText="确定"
            cancelText="取消"
          >
            <a key="delete" style={{ color: '#ff4d4f' }}>
              删除
            </a>
          </Popconfirm>,
        );
      }

      return (
        <div className="option" key="option">
          {node.map((n: any) => {
            return n;
          })}
        </div>
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [option],
  );

  const newColumns: any = [...columns];
  const hasOption = newColumns.find((i: any) => i.valueType === 'option');
  if (hasOption && !hasOption.render) {
    hasOption.render = optionNode;
  }

  return (
    <>
      <div
        className={`editable-protable-custom ${className ? className : ''}`}
        style={{ ...style, overflowX: 'auto', height: `${maxHeight ? maxHeight + 'px' : ''}` }}
      >
        <TableCustom
          tableType="EditableProTable"
          search={false}
          options={false}
          editableFormRef={editableRef}
          columns={newColumns}
          rowKey={rowKey}
          value={[...values]}
          onChange={debounceHanderChange}
          controlled={controlled}
          recordCreatorProps={
            position
              ? {
                  creatorButtonText: creatorButtonText ? creatorButtonText : '',
                  newRecordType: 'dataSource',
                  record: (index: any) => {
                    const obj = {
                      [rowKey]: parseInt(index + (Math.random() * 1000000).toFixed(0)),
                    };
                    setRecordColumns(obj, columns);
                    return obj;
                  },
                }
              : false
          }
          editable={{
            formProps: {
              formRef: formRef,
            },
            type: 'multiple',
            editableKeys,
            actionRender: actionRender,
            onSave,
            onCancel,
            ...editable,
          }}
          pagination={false}
          {...rest}
        />
      </div>
    </>
  );
};
export default EditableTableCustom;
