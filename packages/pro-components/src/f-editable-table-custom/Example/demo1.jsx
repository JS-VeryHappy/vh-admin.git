import React, { useState } from 'react';
import { EditableTableCustom } from '@vh-admin/pro-components';

const EditableTableCustomDemo = () => {
  const [values, setValues] = useState([
    { id: 1123, title: '活动名称1', state: 1, decs: '这个活动真好玩' },
    { id: 1253, title: '活动名称2', state: 1, decs: '这个活动真好玩' },
    { id: 1243, title: '活动名称3', state: 2, decs: '这个活动真好玩' },
    { id: 1263, title: '活动名称4', state: 1, decs: '这个活动真好玩' },
  ]);

  const columns = [
    {
      dataIndex: 'index',
      editable: false,
      valueType: 'indexBorder',
      width: 48,
    },
    {
      title: '标题',
      dataIndex: 'title',
      formItemProps: {
        rules: [
          {
            required: true,
            whitespace: true,
            messdecs: '此项是必填项',
          },
        ],
      },
    },
    {
      title: '状态',
      dataIndex: 'state',
      valueType: 'select',
      fieldProps: {
        options: [
          {
            label: '是',
            value: 1,
          },
          {
            label: '否',
            value: 2,
          },
        ],
      },
    },
    {
      title: '描述',
      dataIndex: 'decs',
    },
    {
      title: '操作',
      valueType: 'option',
      width: 250,
    },
  ];
  const actionRender = (row, config, defaultDoms) => {
    return [defaultDoms.delete];
  };

  const onChange = (e) => {
    setValues(e);
  };

  return (
    <EditableTableCustom
      creatorButtonText="添加数据"
      position="bottom"
      columns={columns}
      value={values}
      onChange={onChange}
      actionRender={actionRender}
    />
  );
};

export default EditableTableCustomDemo;
