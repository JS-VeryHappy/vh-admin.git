//@ts-ignore
import { FormCustom } from '@vh-admin/pro-components';
//@ts-ignore
import type { FormCustomColumnsType } from '@vh-admin/pro-components/f-form-custom/types';
import { message } from 'antd';

const columns: FormCustomColumnsType<any>[] = [
  {
    title: '输入',
    dataIndex: 'input',
  },
  {
    title: '状态',
    dataIndex: '["status","status1"]',
    valueType: 'SelectInputCustom',
    initialValue: [1, 2],
    fieldProps: {
      options: [
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: 'type',
    dataIndex: 'type',
    valueType: 'SelectMultipleCustom',
    apiFormat: {
      type: 'split',
      split: ',',
    },
    fieldProps: {
      fieldProps: {
        options: [
          {
            label: '启用',
            value: 1,
          },
          {
            label: '禁用',
            value: 2,
          },
          {
            label: '等待',
            value: 3,
          },
        ],
      },
    },
  },
  {
    title: '规则参数',
    dataIndex: 'EditableTableCustom',
    valueType: 'EditableTableCustom',
    width: '100%',
    hideInTable: true,
    apiFormat: {
      type: 'json',
    },
    fieldProps: {
      columns: [
        {
          title: '阶段',
          dataIndex: 'a',
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
            // shouldUpdate: (prevValues: any, curValues: any) => {
            //   return false;
            //   console.log('===================');
            //   console.log(prevValues, curValues);
            //   console.log('===================');
            // },
          },
        },
        {
          title: '业务22',
          dataIndex: 'aaa',
          valueType: 'InputNumberSelectCustom',
          width: 200,
          fieldProps: {
            options: [
              { value: 1, label: '%单套' },
              { value: 3, label: '元整体' },
            ],
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '阶段比例%',
          dataIndex: 'b',
          valueType: 'digit',
          fieldProps: {
            min: 0,
            max: 100,
            addonAfter: '%',
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '导出表标题',
          dataIndex: 'd',
          editable: false,
        },
        {
          title: '操作',
          valueType: 'option',
        },
      ],
      maxHeight: 200,
      // controlled: false,
      defaultEditablekeys: [1],
      actionRender: (row: any, config: any, defaultDoms: any) => {
        return [defaultDoms.save, defaultDoms.cancel];
      },
      position: 'bottom',
      // autoEdit: false,
      option: ['edit', 'delete'],
    },
    initialValue: [{ id: 1, a: undefined, b: 2 }],
  },
];

function Demo3() {
  const initialValues: any = {
    status: 3,
    status1: 'ssssssss',
    EditableTableCustom: JSON.stringify([
      { id: 1, a: '阶段1', b: 2 },
      // { id: 2, a: '阶段2', b: 2 },
      // { id: 3, a: '阶段1', b: 2 },
      // { id: 4, a: '阶段2', b: 2 },
      // { id: 5, a: '阶段1', b: 2 },
      // { id: 6, a: '阶段2', b: 2 },
      // { id: 7, a: '阶段1', b: 2 },
      // { id: 8, a: '阶段2', b: 2 },
      // { id: 9, a: '阶段1', b: 2 },
      // { id: 10, a: '阶段2', b: 2 },
      // { id: 11, a: '阶段1', b: 2 },
      // { id: 12, a: '阶段2', b: 2 },
      // { id: 13, a: '阶段1', b: 2 },
      // { id: 14, a: '阶段2', b: 2 },
      // { id: 15, a: '阶段1', b: 2 },
      // { id: 16, a: '阶段2', b: 2 },
      // { id: 17, a: '阶段1', b: 2 },
      // { id: 18, a: '阶段2', b: 2 },
      // { id: 19, a: '阶段1', b: 2 },
      // { id: 20, a: '阶段2', b: 2 },
      // { id: 21, a: '阶段1', b: 2 },
      // { id: 22, a: '阶段2', b: 2 },
      // { id: 23, a: '阶段1', b: 2 },
      // { id: 24, a: '阶段2', b: 2 },
      // { id: 25, a: '阶段1', b: 2 },
      // { id: 26, a: '阶段2', b: 2 },
      // { id: 27, a: '阶段1', b: 2 },
      // { id: 28, a: '阶段2', b: 2 },
      // { id: 29, a: '阶段1', b: 2 },
      // { id: 30, a: '阶段2', b: 2 },
      // { id: 31, a: '阶段1', b: 2 },
      // { id: 32, a: '阶段2', b: 2 },
      // { id: 33, a: '阶段1', b: 2 },
      // { id: 34, a: '阶段2', b: 2 },
      // { id: 35, a: '阶段1', b: 2 },
      // { id: 36, a: '阶段2', b: 2 },
      // { id: 37, a: '阶段1', b: 2 },
      // { id: 38, a: '阶段2', b: 2 },
    ]),
    type: '1,2',
  };
  const onFinish = async (values: any) => {
    try {
      console.log('====================================');
      console.log(values);
      console.log('====================================');
      message.success('成功');
      return true;
    } catch (error) {
      console.log('====================================');
      console.log(error);
      console.log('====================================');
      return false;
    }
  };

  return (
    <>
      <FormCustom
        key="formDemo"
        // shouldUpdate={false}
        initialValues={initialValues}
        columns={columns}
        onFinish={onFinish}
      />
    </>
  );
}

export default Demo3;
