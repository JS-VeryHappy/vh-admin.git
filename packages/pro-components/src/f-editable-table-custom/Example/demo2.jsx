import React, { useState, useRef } from 'react';
import { EditableTableCustom } from '@vh-admin/pro-components';

const EditableTableCustomDemo1 = () => {
  const valueEnum = {
    all: { text: '全部', status: 'Default' },
    open: {
      text: '未解决',
      status: 'Error',
    },
    closed: {
      text: '已解决',
      status: 'Success',
      disabled: true,
    },
    processing: {
      text: '解决中',
      status: 'Processing',
    },
  };

  const [values, setValues] = useState([]);
  const refTable = useRef();

  const columns = [
    {
      key: 'title',
      title: '标题',
      dataIndex: 'title',
      initialValue: '必填',
      formItemProps: {
        rules: [
          {
            required: true,
            message: '此项为必填项',
          },
        ],
      },
      width: 'm',
    },
    {
      title: '状态',
      dataIndex: 'state',
      valueType: 'select',
      valueEnum,
      width: 'm',
      tooltip: '当title为disabled时状态无法选择',
      fieldProps: (form, { rowKey }) => {
        const has = values.find((i) => i.id == rowKey);

        if (has && has.title === 'disabled') {
          return {
            disabled: true,
            placeholder: 'disabled',
          };
        } else {
          return {
            placeholder: 'normal',
          };
        }
      },
    },
    {
      key: 'labels',
      title: '标签',
      dataIndex: 'labels',
      width: 'm',
      tooltip: '当title为必填时此项将为必填',
      formItemProps(form, config) {
        const has = values.find((i) => i.id == config.rowKey);
        if (has && has.title === '必填') {
          return {
            rules: [
              {
                required: true,
              },
            ],
          };
        } else {
          return {};
        }
      },
    },
    {
      key: 'money',
      dataIndex: 'money',
      title: '优惠金额',
      width: 'm',
      valueType: 'select',
      dependencies: ['labels', 'title'],
      params: () => {
        return { iddddd: 1 };
      },
      request: async (params) => {
        console.log('====================================');
        console.log(params);
        console.log('====================================');
        //无效中
        return [
          {
            value: 1,
            label: '1',
          },
        ];
      },
    },
    {
      title: '创建时间',
      key: 'showTime',
      dataIndex: 'createName',
      valueType: 'date',
    },
    {
      title: '操作',
      valueType: 'option',
      width: 250,
    },
  ];
  const actionRender = (row, config, defaultDoms) => {
    return [defaultDoms.delete];
  };

  const onChange = (e) => {
    console.log('====================================');
    console.log(refTable);
    console.log('====================================');
    setValues(e);
  };

  return (
    <EditableTableCustom
      editableFormRef={refTable}
      creatorButtonText="添加数据"
      position="bottom"
      columns={columns}
      value={values}
      onChange={onChange}
      actionRender={actionRender}
    />
  );
};

export default EditableTableCustomDemo1;
