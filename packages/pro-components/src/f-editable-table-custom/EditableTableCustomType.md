### EditableTableCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| rowKey | 唯一id取值 | `string` | `id` | - |
| columns | 表格配置项 | `ProColumns<any,any>[]\|any` | - | - |
| maxHeight | 表格最大高度 | `number` | - | - |
| onChange | 表格数据改变回调函数 | `any` | - | - |
| position | 添加一行按钮显示位置，如果不配置则不显示添加按钮 | `string\|'top'\|'bottom'\|undefined` | - | - |
| creatorButtonText | 新增一行按钮文案 | `string` | - | - |
| actionRender | 自定义编辑的操作 | `any` | - | - |
| editableFormRef | 操作编辑表格的ref | `any` | - | - |
| editable | editable 编辑行配置 | `any` | - | - |
| value | `格式为[{id:xxx,...}] 对象中id为必须的` | `any[]` | [] | - |
| editableRef | 操作表格的`React.MutableRefObject<{editableFormRef: any;}>` | `any` | -| - |
| controlled | 是否受控, 如果受控每次编辑都会触发 onChange，并且会修改 dataSource | `boolean` | false | - |
| autoEdit | 是否开启默认编辑模式 | `boolean` | true | - |
| option |  配置行操作按钮 | `('edit' | 'delete')[]` | [] | - |
| defaultEditablekeys | 默认编辑的row | `any[]` | [] | - |
| `...` | 其它的参数[editable-table](https://procomponents.ant.design/components/editable-table) | `any` | - | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
