---
title: 编辑表格
toc: content
order: 2
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. EditableTableCustom 可增加修改删除

<code src="./Example/demo1.jsx" 
      title="可增加修改删除"
      description="可增加修改删除">可增加修改删除</code>


> 2. EditableTableCustom 联动

<code src="./Example/demo2.jsx" 
      title="联动"
      description="联动">联动</code>

> 3. EditableTableCustom 默认不开启编辑

<code src="./Example/demo3.tsx" 
      title="和表单一起使用"
      description="默认不开启编辑，开启按钮">默认不开启编辑</code>


## API

<embed src="./EditableTableCustomType.md"></embed>


