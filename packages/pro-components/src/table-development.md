---
title: 内置功能开发说明
order: 1
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表格组件
  path: /table-custom
  order: 2
---

## 自定义开发表格内置组件说明

- 一.自定义内置功能开发必须遵循规则
  - **1.其本身有独立使用说明文档和运行 Demo**
- 二.开发中可以使用以下组件、可以单独使用、也可以混合使用：
  - **[ProComponents](https://procomponents.ant.design/components/form)**
  - **[Antd](https://ant.design/components/overview-cn/)**
  - **[html 语义化标签]**
  - **[内部业务组件]**
- 三.自定义弹窗类型必须完成 3 个属性对应实现方法
  - **1.使用 props 属性 modelchildName 设置组件的 key 和 id**
  ```js
  return <FormCustom id={modelchildName} key={modelchildName} {...newConfig} />;
  ```
  - **2.但是隐藏后必须调用 closeModal() 关闭弹窗**
  ```js
       {
          afterClose: () => {
          if (closeModal) {
             closeModal();
          }
          },
       }
  ```
  - **3.尽量都使用 antd 弹窗组件作为外部组件、因为他挂载 body 下，在关闭回调中有特殊关闭，可查看 closeModal 方法**
  - **4.在 modalTypeRenderConfig.tsx 导出内置组件，并且 types.d.ts 定义 modalType 内置功能类型，类型必须和导出函数命名相同**
    ```js
       // modalTypeRenderConfig.tsx
       import Form from './FormModal';
       const components = {
          Form,
          ...
       };
       // types.d.ts
       export declare type modalType = 'Form';
    ```
  - 5.组件命名规则**Modal 结尾**
    - 例如：内置表单弹窗 FormModal
- 四.自定义函数方法

  - **1.在 modalTypeRenderConfig.tsx 直接定义函数方法**
  - ````js
          const Download = () => {
           console.log('====================================');
           console.log(1);
           console.log('====================================');
          };
          const components = {
              Download,
              ...
           };
        ```

    ````

## 命令调用modalTypeRenderConfig
````js
 1.表格中调用：

 
 {
      title: '小程序列表',
      dataIndex: 'isDisplay',
      valueType: 'text',
      businessStyle: {
        type: 'within',
        btnConfig: {
          text: (value: any) => {
            return value == true ? '显示' : '隐藏';
          },
          disabled: !checkPermissionAuth({ accessId: 110101 }),
          onClick: (record: any, btnConfig: any) => {
            ModalTypeRenderConfig({
              children: {
                modalType: 'Confirm',
                props: {
                  btnConfig: {
                    ...btnConfig,
                    modalConfig: {
                      edit: true,
                      modalType: 'Confirm',
                      config: {
                        title: '',
                        desc: '提示：隐藏后当前项目在小程序项目列表中不可见，是否继续隐藏？',
                        submitRequest: apiProjectDisplay,
                        submitOnDone: ({ status }: any) => {
                          if (status == 'success') {
                            actionRef.current.reload();
                          }
                        },
                      },
                    },
                  },
                  type: 'cell',
                  clickConfig: {
                    irecord: record,
                  },
                },
              },
              btnConfig: btnConfig,
            });
          },
        },
      },
      search: false,
      hideInForm: true,
      width: 100,
    },
 
 2.直接调用：
  ModalTypeRenderConfig({
    children: {
      modalType: 'Confirm',
      props: {
        btnConfig: {
          key:'cell-isDisplay', // 唯一
          modalConfig: {//内置的配置
            edit: true,
            config: {////内置的配置文件
              title: '',
              desc: '提示：隐藏后当前项目在小程序项目列表中不可见，是否继续隐藏？',
              submitRequest: apiProjectDisplay,
              submitOnDone: ({ status }: any) => {
                if (status == 'success') {
                  actionRef.current.reload();
                }
              },
            },
          },
        },
        clickConfig: {
          irecord: record,
        },
      },
    },
    btnConfig: {
      key:'cell-isDisplay' // 唯一 和上面必须一样
    },
  });
````

## API 


<embed src="./t-table-custom/Example/TableCustomTypes/ModalRenderPropsType.md"></embed>

<embed src="./t-table-custom/Example/TableCustomTypes/ModalType.md"></embed>
