import ModalCustom from '../m-modal-custom';
import './index.less';
import type { ModalProps } from 'antd';
import React from 'react';

export declare type DeleteCustomType = {
  /**
   * 显示标题
   * @description
   * @default 你确定要删除？
   */
  title?: string;
  /**
   * 显示描述
   * @description
   * @default 删除数据后将无法恢复
   */
  desc?: string | React.ReactNode;
} & ModalProps;

/**
 * 业务确认删除弹窗
 * @param Props
 * @returns
 */
function DeleteModalCustom(Props: DeleteCustomType) {
  const { title = '删除', desc = '删除后数据将无法恢复', onOk, onCancel, ...rest } = Props;

  const handleOk = async (e: any) => {
    if (onOk && typeof onOk === 'function') {
      await onOk(e);
    }
  };

  const handleCancel = (e: any) => {
    if (onCancel && typeof onCancel === 'function') {
      onCancel(e);
    }
  };

  return (
    <>
      <ModalCustom
        {...rest}
        title={title}
        destroyOnClose={true}
        maskClosable={false}
        wrapClassName="delete-modal-custom"
        okType="danger"
        onOk={handleOk}
        onCancel={handleCancel}
      >
        {desc}
      </ModalCustom>
    </>
  );
}

export default DeleteModalCustom;
