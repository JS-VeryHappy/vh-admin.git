### DeleteCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| title | 显示标题<br>@description<br> | `string` | 你确定要删除？ | - |
| desc | 显示描述<br>@description<br> | `string\|React.ReactNode` | 删除数据后将无法恢复 | - |
| `...ModalProps` | antd [ModalProps](https://ant.design/components/modal-cn) | `ModalProps` | - | - |
