---
title: 弹窗删除
toc: content
order: 5
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 弹窗组件
  path: /modal-custom
  order: 3
---

## 基础组件具体参数说明参考：[基础弹窗](/pro-components/m-modal-custom)

```jsx
import React, { useState } from 'react';
import { DeleteModalCustom } from '@vh-admin/pro-components';
import { Button } from 'antd';
import { waitTime } from '@vh-admin/pro-utils';

function Demo1() {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = async () => {
    await waitTime(2000);
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button danger onClick={showModal}>
        删除弹窗
      </Button>
      <DeleteModalCustom visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} />
    </>
  );
}
export default Demo1;
```

## API

<embed src="./DeleteCustomType.md"></embed>

