import { DatePicker, Select, Space } from 'antd';
import dayjs from 'dayjs';
import quarterOfYear from 'dayjs/plugin/quarterOfYear';
import { useEffect, useState } from 'react';
import type { CustomType } from '../f-form-custom/types';
// import './index.less';
dayjs.extend(quarterOfYear);

const { RangePicker } = DatePicker;

export declare type SelectRangePickerCustomType = {
  /**
   * 区间格式为： [ 'month' | 'date' | 'year' | 'quarter', ['2022-11-17', '2022-11-17']]
   *  @default ['month',[]]
   */
  value?: ['month' | 'date' | 'year' | 'quarter', any[]];
  /**
   * 同时禁用日期选择框和周期选择框
   * @default false
   */
  disabled?: boolean;
  /**
   *  禁用日期选择框
   * @default false
   */
  dateDisabled?: boolean;
  /**
   * 禁用周期选择框
   * @default false
   */
  selectdDisabled?: boolean;
  /**
   * 周期选择框选项配置
   * @default [ { value: 'date', label: '单次不重复' },  { value: 'month', label: '月度重复' }, { value: 'quarter', label: '季度重复' }, { value: 'year', label: '年度重复' }, ]
   */
  selectOptions?: any[];
} & CustomType;

/**
 * 验证合法性
 */
export const SelectRangePickerCustomValidator = async (rule: any, value: any) => {
  if (value && !value[0]) {
    throw new Error(`${rule.message || '请选择考核周期'}`);
  } else if (value && (!value[1][0] || !value[1][1])) {
    throw new Error(`${rule.message || '请选择生效时段'}`);
  }
};

const options: any = [
  { value: 'date', label: '单次不重复' },
  { value: 'month', label: '月度重复' },
  { value: 'quarter', label: '季度重复' },
  { value: 'year', label: '年度重复' },
];

const getAssessCycleValue = (value: any) => {
  if (value == 1) {
    return 'date';
  } else if (value == 2) {
    return 'month';
  } else if (value == 3) {
    return 'quarter';
  } else if (value == 4) {
    return 'year';
  } else {
    return '';
  }
};

/**
 * 处理返回回填数据
 */
export const periodSelectorInitialValuesBefor = (newParams: any) => {
  newParams.periodSelector = [
    getAssessCycleValue(newParams.assessCycle),
    [newParams.startDate, newParams.endDate],
  ];
  return newParams;
};

const SelectRangePickerCustom = (props: SelectRangePickerCustomType) => {
  const {
    id,
    style,
    value = ['month', []],
    onChange,
    className,
    fieldProps,
    dateDisabled = false,
    disabled,
    selectdDisabled = false,
    selectOptions = options,
  } = props;
  const [dateType, setDateType] = useState<'month' | 'date' | 'year' | 'quarter'>();
  const [datePickerValue, setDatePickerValue] = useState<any>([]);

  const changeFormat: any = (dayjsDate: any, index: any) => {
    let newDateString: string = '';
    if (dateType === 'month') {
      newDateString = dayjsDate.format('YYYY-MM-DD');
    } else if (dateType === 'year') {
      if (index == 0) {
        newDateString = dayjsDate.format('YYYY-01-01');
      } else {
        newDateString = dayjsDate.format('YYYY-12-31');
      }
    } else if (dateType === 'quarter') {
      newDateString = dayjsDate.format('YYYY-MM-DD');
    } else {
      newDateString = dayjsDate.format('YYYY-MM-DD');
    }
    return newDateString;
  };
  const onDatePickerChange = (date: any) => {
    console.log('date: ', date);
    if (!date) {
      setDatePickerValue([]);
      return;
    }
    let newDatePickerValue: any = [...date];
    if (dateType == 'quarter') {
      newDatePickerValue = [
        dayjs(date[0].startOf('quarter').format('YYYY-MM-DD'), 'YYYY-MM-DD'),
        dayjs(date[1].endOf('quarter').format('YYYY-MM-DD'), 'YYYY-MM-DD'),
      ];
    } else if (dateType == 'month') {
      newDatePickerValue = [
        dayjs(date[0].startOf('month').format('YYYY-MM-DD'), 'YYYY-MM-DD'),
        dayjs(date[1].endOf('month').format('YYYY-MM-DD'), 'YYYY-MM-DD'),
      ];
    }
    setDatePickerValue(newDatePickerValue);

    let dateString: any;
    // eslint-disable-next-line prefer-const
    dateString = newDatePickerValue.map((d: any, i: any) => {
      return changeFormat(d, i);
    });
    if (onChange && typeof onChange === 'function') {
      onChange([dateType, dateString]);
    }
  };

  const handleChange = (valueu: any) => {
    setDateType(valueu);
    setDatePickerValue([]);
    if (onChange && typeof onChange === 'function') {
      onChange([valueu, []]);
    }
  };

  useEffect(() => {
    /**
     * 如果父级传有默认值则赋值默认值 或者默认值变换
     */
    if (value && value[1] && value[1].length) {
      setDatePickerValue(
        value[1].map((d: any) => {
          return dayjs(d, 'YYYY-MM-DD');
        }),
      );
    }
    if (value && value[1] && !value[1].length) {
      setDatePickerValue(null);
    }
    if (value && value[0]) {
      setDateType(value[0]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);
  return (
    <>
      <div className={`f-select-range-picker-custom ${className}`} id={id} style={style}>
        <Space.Compact block>
          <Select
            className="period-selector-item"
            value={dateType}
            disabled={disabled ? disabled : selectdDisabled}
            {...fieldProps}
            options={selectOptions}
            onChange={handleChange}
          />
          <RangePicker
            className="period-selector-item"
            picker={dateType}
            disabled={disabled ? disabled : dateDisabled}
            value={datePickerValue}
            {...fieldProps}
            onChange={onDatePickerChange}
          />
        </Space.Compact>
      </div>
    </>
  );
};

export default SelectRangePickerCustom;
