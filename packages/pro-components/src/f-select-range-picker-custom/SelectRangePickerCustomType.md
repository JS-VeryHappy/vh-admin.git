### SelectRangePickerCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| value | 默认值<br>自定义必须要实现的 | `[string\|number,any]` | - | - |
| value | 区间格式为：<br>['month'\|'date'\|'year'\|'quarter',['2022-11-17','2022-11-17']] | `['month'\|'date'\|'year'\|'quarter',any[]]` |  | - |
| disabled | 同时禁用日期选择框和周期选择框<br> | `boolean` | false | - |
| dateDisabled | 禁用日期选择框<br> | `boolean` | false | - |
| selectdDisabled | 禁用周期选择框<br> | `boolean` | false | - |
| selectOptions | 周期选择框选项配置<br> | `any[]` |  | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
