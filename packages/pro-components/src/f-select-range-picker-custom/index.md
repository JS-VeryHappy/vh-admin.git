---
title: SelectRangePicker
toc: content
order: 10
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. SelectRangePicker

```jsx
/**
 * title: SelectRangePicker
 */
import React from 'react';
import { SelectRangePickerCustom } from '@vh-admin/pro-components';

function SelectRangePickerCustomDemo() {
  const onChange = (data)=>{
    console.log('data: ', data);
  }

  return <SelectRangePickerCustom onChange = {onChange} value = {['month', []]} />;
}

export default SelectRangePickerCustomDemo;
```

## API

<embed src="./SelectRangePickerCustomType.md"></embed>


