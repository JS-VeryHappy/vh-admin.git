import type { ActionType, ProColumns, ProTableProps } from '@ant-design/pro-table';
import type {
  ValueType,
  FormCustomProps,
  OptionType,
  FormCustomColumnsTypeCustom,
} from '../f-form-custom/types';
// import type { ProListProps } from '@ant-design/pro-list';
import type { DeleteCustomType } from '../m-delete-modal-custom';
import type { PreviewHtmlModalCustomType } from '../m-preview-html-modal-custom';
import type { ImportModalCustomType } from '../m-import-modal-custom';
import type { ConfirmModalCustomType } from '../m-confirm-modal-custom';
import type { FormInstance } from 'antd';
import type { EditableProTableProps } from '@ant-design/pro-table/es/components/EditableTable';
import type { WithinTableModalType } from '../t-within-table-modal';
import type { SortOrder } from 'antd/es/table/interface';

/**
 * 表格单元格特殊样式配置
 */
export declare type BusinessStyleType =
  | {
      /**
       * 类型 tag 表格单元格显示形式为tag
       * 类型 ...
       */
      type: 'tag' | 'colorText';
      /**
       * 自定义使用判断的的字段
       * 如果不配置则使用column.dataIndex
       */
      field?: any;
      /**
       * 样式参数配置信息
       */
      options?: OptionType[];
    }
  | {
      /**
       * 类型 tag 表格单元格显示形式为tag
       * 类型 ...
       */
      type: 'urllink';
      /**
       * 点击回调配置时不跳转路由
       * 类型为string默认附带id参数
       */
      handelClick: (record: any) => void;
      /**
       * 自定义样式style
       */
      style?: any;
    }
  | {
      /**
       * 类型 previewImage 表格单元格显示形式为 点击查看图片组
       * 类型 ...
       */
      type: 'previewImage';
      /**
       * 列表显示的文案
       * @default '查看'
       */
      text?: string;
      /**
       * 是否有权限
       *  @default 显示 返回false置灰
       */
      auth?: (record: any) => boolean;
      /**
       * 自定义样式style
       */
      style?: any;
    }
  | {
      /**
       * 类型 download 表格单元格显示形式为 点击下载，跳转a标签和自定义下载
       * 类型 ...
       */
      type: 'download';
      /**
       * 列表显示的文案
       * @default '查看'
       */
      text?: string;
      /**
       * 自定义样式style
       */
      style?: any;
      /**
       * 配置使用自定义请求。不配置则使用a标签
       */
      request?:
        | any
        | ((data: any) => Promise<any>)
        | ((
            params: any & {
              pageSize?: number;
              current?: number;
              keyword?: string;
            },
            sort: Record<string, SortOrder>,
            filter: Record<string, (string | number)[] | null>,
          ) => Promise<any>);
      /**
       * 参数回调，返回给接口的请求额外参数
       * @param record
       * @returns
       */
      paramsBefor?: (record: any) => Record<string, any>;
      /**
       * 是否有权限
       *  @default 显示 返回false置灰
       */
      auth?: (record: any) => boolean;
      /**
       * 配置文件名，不配置则自动后去
       */
      fileName?: any;
    }
  | {
      /**
       * 类型 within 使用表格的内置功能
       * 类型 ...
       */
      type: 'within';
      /**
       * 按钮配置
       */
      btnConfig?: BtnConfigType;
      /**
       * 父级使用的类型
       * @default 'button'
       */
      parentType?: 'button' | 'div';
    }
  | {
      type: string;
      [key: string]: any;
    };

declare type BaseProColumns<T> = ProColumns<T, ValueType> & {
  request?: any;
  columns?: CustomProColumnsTypes<T>[] | ((record: any) => CustomProColumnsTypes<T>[]);
  /**
   * dependency 下监听的字段值
   */
  name?: string[];
};

declare type CustomProColumnsTypes<T> =
  | BaseProColumns<T>
  | (FormCustomColumnsTypeCustom & BaseProColumns<T>);

export declare type ProColumnsTypes<T> = CustomProColumnsTypes<T> & {
  // 网络发起请求快捷配置 优先级低于 request参数
  // requestConfig?: requestConfigType;
  children?: CustomProColumnsTypes<T>[];
  /**
   *
   * 如果需要再column单独一个配置中获取数据行的值，可以使用下方方式
   * 1.((record: any) => CustomProColumnsTypes<T>[]) 需要再 valueType: 'dependency', 下生效
   *
   * 2.{
        dataIndex: 'objData',
        title: '',
        width: '668px',
        renderFormItem: (_: any, initialValues: any) => {
          // objData 字段是虚拟字段 在initialValuesBefor赋值时可以这样来赋值给initialValues,这样当前的dataIndex就可以获取全部的行数据
          // initialValuesBefor: (data: any) => {return { ...data, objData: data };},
          return (
            <div>
              <Row>
                <Col span={24}>票号：{initialValues.value?.invoiceNo}</Col>
              </Row>
            </div>
          );
        },
      },
   *
   */
  columns?: CustomProColumnsTypes<T>[] | ((record: any) => CustomProColumnsTypes<T>[]);
  /**
   * 配置用于为表单分组
   */
  formGroup?: number;
  /**
   * 配置用于为表单显示顺序
   */
  formOrder?: number;
  /**
   * 只生效表格的宽度
   */
  tableWidth?: number;
  /**
   * 只生效表单的宽度
   */
  formWidth?: number;
  /**
   * 业务样式类型
   * type = 1 显示tag标签显示
   */
  businessStyle?: BusinessStyleType;

  /**
   *与实体映射的key
   */
  dataIndex?: any;
};

export declare type SubmitOnDoneType = {
  /**
   * 请求完成的回调状态
   */
  status?: 'success' | 'error';
  /**
   * 请求的请求数据
   */
  params?: Record<any, any>;
  /**
   * 请求结果
   */
  result?: Record<any, any>;
  /**
   * 操作表格的tableRef
   */
  tableRef?: TableRefType | any;
  /**
   * 当前表单的配置
   */
  columns?: any;
  /**
   * 编辑时的行数据
   */
  irecord?: Record<string, any>;
};

// 内置功能的类型
export declare type ModalType =
  | 'Form'
  | 'Delete'
  | 'Download'
  | 'PreviewHtml'
  | 'Import'
  | 'Confirm'
  | 'OnClick'
  | 'Table'
  | 'CustomModal';
// 内置组件配置项
export declare type ModalPropsConfigType = {
  /**
   * 弹窗名称设置
   * @default 弹窗表单
   */
  title?:
    | string
    | ((record: any) => React.ReactNode | ((props: any, type: any, dom: any) => React.ReactNode));
  /**
   * 提交请求的前数据的钩子
   * submitValue 提交数据
   * columns 当前表单配置
   * @default
   */
  submitValuesBefor?: (
    submitValue: any,
    columns: any,
    tableRef: TableRefType,
    tableFormRef: React.MutableRefObject<FormInstance>,
    irecord: Record<string, any>,
  ) => any;
  /**
   * 提交请求的Request
   * submitValue 提交数据
   * columns 当前表单配置
   * @default
   */
  submitRequest?:
    | any
    | ((
        submitValue: any,
        columns: any,
        tableRef: TableRefType,
        tableFormRef: React.MutableRefObject<FormInstance>,
        irecord: Record<string, any>,
      ) => any);
  /**
   * 请求完成成功回调
   * @default
   */
  submitOnDone?: (params: SubmitOnDoneType) => void;
  /**
   * 发起网络请求的参数,与 request 配合使用
   * @default
   */
  params?: Record<any, any>;
  /**
   * 发起网络请求的参数,返回值会覆盖给 initialValues
   * @default
   */
  request?: (params: any, initialValues: any) => Promise<any> | ((data: any) => Promise<any>);
  /**
   * 操作表单的Ref
   */
  formCustomRef?: any;
  /**
   * 数据初始化复制之前的钩子
   * @default
   */
  initialValuesBefor?: (data: any) => any;

  /**
   * 渲染表单配置文件之前
   * @default
   */
  columnBefor?: (columns: any, initialValues: any) => any;
  /**
   * 表单值改变的回调
   * @default
   */
  onValuesChange?: (
    /**
     * 变化的值
     */
    value: any,
    /**
     * 当前表单配置
     */
    formColumns: any,
    /**
     * 设置当前表配置
     */
    setFormColumns: any,
    /**
     * 表单Ref
     */
    formRef: any,
    /**
     * 表单原始值，不会因为回调columnBefor 改变变化
     */
    oldFormColumns: any,
  ) => any;
  /**
   * 弹窗类型时：弹窗的宽度
   * @default single
   */
  width?: 'single' | 'double' | number | any;
  /**
   * 是否只读
   */
  readonly?: boolean;
  /**
   * 弹窗类型时：自定义页脚
   */
  submitter?: any;

  /**
   * 表格上方显示内容
   */
  tableHeaderRender?: (initialValues: any) => React.ReactNode;
  /**
   * 表格下方显示内容
   */
  tableBottomRender?: (initialValues: any) => React.ReactNode;
  /**
   * 透传给使用的自定义组件
   * 具体看内置的什么组件类型去查看对应的内置说明
   * 会合并 config 传过去
   */
  customProps?: any;

  [key: string]: any;
};
// 内置功能配置
export declare type ModalPropsType =
  | {
      /**
       * key
       * @default 自动生成的可忽略
       */
      key?: string;
      /**
       * 触发的业务位置
       * @default 自动生成的可忽略
       */
      type?: ClickType;
      /**
       * 表单自定义columns配置参数、如果不配置默认取表格columns配置
       * @default 弹窗表单
       */
      columns?: CustomProColumnsTypes<any>[];
      /**
       * 是否按照tabel传入格式化columns
       */
      formatColumns?: boolean;
      /**
       * 是否为编辑模式
       * @default false
       */
      edit?: boolean;
      /**
       *  关联弹窗类型
       * 查看 ModalType 属性说明
       */
      modalType?: ModalType;
      /**
       * 内置组件的一些功能性配置
       * 查看 ModalPropsConfigType
       * @default false
       */
      config?: ModalPropsConfigType;
    }
  | {
      /**
       * 类型Form时
       */
      modalType?: 'Form';
      /**
       * 内置组件的一些功能性配置
       */
      config?: FormCustomProps;
    }
  | {
      /**
       * 类型为Delete时
       */
      modalType?: 'Delete';
      /**
       * 内置组件的一些功能性配置
       */
      config?: DeleteCustomType;
    }
  | {
      /**
       * 类型为Download时
       */
      modalType?: 'Download';
      /**
       * 内置组件的一些功能性配置
       */
      config?: {
        /**
         * 文件名称
         */
        fileName?: string;
        /**
         * 下载时携带搜索参数
         * @default false
         */
        searchParams?: boolean;
      };
    }
  | {
      /**
       * 类型为PreviewHtml时
       */
      modalType?: 'PreviewHtml';
      /**
       * 内置组件的一些功能性配置
       */
      config?: PreviewHtmlModalCustomType;
    }
  | {
      /**
       * 类型为Import时
       */
      modalType?: 'Import';
      /**
       * 内置组件的一些功能性配置
       */
      config?: ImportModalCustomType & {
        /**
         * 是否展示错误信息
         * @default true
         */
        showError?: boolean;
      };
    }
  | {
      /**
       * 类型为Confirm时
       */
      modalType?: 'Confirm';
      /**
       * 内置组件的一些功能性配置
       */
      config?: ConfirmModalCustomType;
    }
  | {
      /**
       * 类型为Table时
       */
      modalType?: 'Table';
      /**
       * 内置组件的一些功能性配置
       */
      config?: WithinTableModalType;
    }
  | {
      /**
       * 类型为CustomModal时
       */
      modalType?: 'CustomModal';
      /**
       * 内置组件的一些功能性配置
       */
      config?: ModalPropsType;
      /**
       *
       * 定义的内容
       *
       */
      render?: (initialValues: any) => React.ReactNode;
    };

export type BtnConfigTooltipType = {
  /**
   * 提示文字
   */
  text: string;
  /**
   * 传给tooltip的Props
   */
  tooltipProps?: any;
  /**
   * 传给ticon的Props
   */
  iconProps?: any;
};

export type BtnConfigType = {
  /**
   * 唯一值
   * @default 自动生成的可忽略
   */
  key?: string;
  /**
   * className
   * @default 自动生成的可忽略
   */
  className?: string;
  /**
   * 按钮名称
   * @default 按钮
   */
  text?: string | ((dom: React.ReactNode, record: any) => string | React.ReactNode);
  /**
   *  icon
   * @default
   */
  icon?: React.ReactNode | any;
  /**
   *  按钮类型
   * @default primary
   */
  type?: 'default' | 'primary' | 'link' | 'text' | 'ghost' | 'dashed';
  /**
   *  按钮类型
   * @default primary
   */
  size?: 'default' | 'large' | 'small';
  /**
   *  按钮样式
   */
  style?: Record<string, any>;
  /**
   *  按钮属性危险
   * @default false
   */
  danger?: boolean;
  /**
   *  是否禁用
   * 可配置为 boolean 和 函数
   * headerTitleConfig (btnConfig, selectKeys, tableRef, tableFormRef) => boolean
   * selectionConfig  (selectedRowKeys,onCleanSelected,btnConfig,selectKeys,tableRef,tableFormRef) => boolean
   * operationConfig (irecord, btnConfig, selectKeys, tableRef, tableFormRef) => boolean
   * @default false
   */
  disabled?: ((...params: any[]) => boolean) | boolean;
  /**
   *  按钮权限 控制是否显示 return false 隐藏 true显示
   *  按钮如果是row记录会返回 记录值等参数
   * headerTitleConfig (btnConfig, selectKeys, tableRef, tableFormRef) => boolean
   * selectionConfig (btnConfig, selectedRowKeys, onCleanSelected, selectKeys, tableRef, tableFormRef) => boolean
   * operationConfig (btnConfig, irecord, selectKeys, tableRef, tableFormRef) => boolean
   * @default true
   */
  auth?: (() => boolean) | any;
  /**
   *  按钮点击回调
   * 回调中会返回 按钮参数，行数据等
   */
  onClick?: ((config: any) => any) | any;
  /**
   *  内置功能的配置
   *  查看 ModalPropsType
   */
  modalConfig?: ModalPropsType;
  /**
   * 按钮旁提示帮助信息
   */
  tooltip?: BtnConfigTooltipType;
};

// 点击位置的类型
export type ClickType = 'selection' | 'header' | 'operation';
// 按钮参数
export type BtnConfigTypes = Record<any, BtnConfigType | ((config: any) => void)>;

export declare type BaseTableCustomType = {
  /**
   * header设置快捷按钮
   */
  headerTitleConfig?: BtnConfigTypes;
  /**
   * 全选快捷配置
   */
  selectionConfig?: BtnConfigTypes;
  /**
   *菜单快捷配置
   */
  operationConfig?: BtnConfigTypes;
  /**
   * 行菜单操作显示最大数据 超过就...
   */
  operationBtnShowMax?: number;
  /**
   * 表格自动请求
   */
  request?: any | ((data: any) => Promise<any>);
  /**
   * 请求前的参数钩子可以处理请求数据
   */
  requestBefor?: (params: any) => any;
  /**
   * 表格字段声明
   */
  columns?: ProColumnsTypes<any> | any;
  /**
   * 表格类型为ProList 持有
   */
  metas?: any;
  /**
   * 表格类型为ProList 持有
   */
  showActions?: any;
  /**
   * 表格类型为ProList 持有
   */
  itemLayout?: any;
  /**
   * 表格类型为ProList 持有
   */
  grid?: any;
  /**
   * 汇总栏类型 local：本地 remote：远程
   */
  summaryType?: 'local' | 'remote';
  /**
   * 汇总栏第一栏显示文字
   * @default 合计
   */
  summaryText?: string;
  /**
   * 可以自定义渲染汇总栏
   */
  summaryRender?: (dom: any, totalRow: any) => React.ReactNode;
  /**
   * 汇总栏类型 远程获取 接口返回的字段名
   * @default summary
   */
  summaryRemoteField?: string;
  /**
   * 汇总栏类型 本地需要统计的字段名
   * @default []
   */
  summarylocalFields?: string[];

  /**
   * 表格为EditableProTable时
   */
  value?: any;

  [key: string]: any;
};

export declare type BaseProTablePropsType<T> = ProTableProps<T, any, ValueType> & {
  request?: any;
};

export declare type BaseEditableProTablePropsType<T> = EditableProTableProps<T, any, ValueType> & {
  request?: any;
};

// 表格参数
export declare type TableCustomTypes<T> =
  | ({
      /**
       * 表格类型 同 ProTable 和 ProList 一直切换更简单
       @default ProTable
      */
      tableType?: 'ProTable' | 'ProList';
    } & BaseProTablePropsType<T> &
      BaseTableCustomType)
  | ({
      /**
     * 表格类型 同 ProTable 和 ProList 一直切换更简单
     @default ProTable
    */
      tableType?: 'EditableProTable';
    } & BaseEditableProTablePropsType<T> &
      BaseTableCustomType);

// 表格的ref
export declare type TableRefType = ActionType;

export declare type ModalRenderChildrenPropsClickConfigType = {
  /**
   * 赋值的数据
   */
  irecord: any;
  /**
   * 其它
   */
  [key: string]: any;
};

export declare type ModalRenderChildrenPropsType = {
  /**
   * 内置功能的类型
   */
  modalType?: ModalType;
  /**
   * 传递给对于内置props
   */
  props?: {
    /**
     * btnConfig配置
     */
    btnConfig?: BtnConfigType;
    /**
     * 类型
     */
    type?: any;
    /**
     * 点击配置
     */
    clickConfig?: ModalRenderChildrenPropsClickConfigType;
    [key: string]: any;
  };
  /**
   * 其它
   */
  [key: string]: any;
};

// 动态插入渲染弹窗的参数
export declare type ModalRenderPropsType = {
  /**
   * 保留字段后面用
   */
  render?: any;
  /**
   * 弹窗方法本身分配的组件唯一值
   * @default 自动生成的可忽略
   */
  modelName?: string;
  /**
   * 弹窗方法分配 给内容组件唯一值
   * @default 自动生成的可忽略
   */
  modelchildName?: string;
  /**
   * 关闭整个弹窗函数 调用可关闭弹窗
   * @default 自动生成的可忽略
   */
  closeModal?: any;
  /**
   * 表格点击业务位置  例如 header
   * @default 自动生成的可忽略
   */
  type?: ClickType;
  /**
   * 渲染的子集
   * @default 自动生成的可忽略
   */
  children: ModalRenderChildrenPropsType;
  /**
   * 按钮配置
   * 可查看表格 BtnConfigType
   */
  btnConfig?: BtnConfigType | any;
  /**
   * 按钮点击回调配置和数据
   * 具体为 ClickType点击位置不同数据不同
   */
  clickConfig?: any;
  /**
   * 表格整个配置信息
   * 可查看表格 TableCustomTypes
   */
  tableProps?: TableCustomTypes<any> | any;
  /**
   * 操作表格tableRef
   */
  tableRef?: TableRefType | any;
  /**
   * 操作表格ormRef
   */
  tableFormRef?: React.MutableRefObject<FormInstance>;
};
