import ReactDOM from 'react-dom/client'; // 修改后的引入路径
import type { ModalRenderPropsType } from './types';
import React from 'react';
// 点击弹窗
import Form from '../t-within-form-modal';
import Delete from '../t-within-delete-modal';
import Confirm from '../t-within-confirm-modal';
import Import from '../t-within-import-modal';
import Table from '../t-within-table-modal';
import CustomModal from '../t-within-custom-modal';
import PreviewHtml from '../t-within-preview-html-modal';
import { ConfigProvider } from 'antd';

// 直接运行
import Download from '../t-within-download';
import OnClick from '../t-within-on-click';
import { ThemeConfig } from 'antd';

const components = {
  Form,
  Delete,
  PreviewHtml,
  Import,
  Confirm,
  OnClick,
  Download,
  CustomModal,
  Table,
};

export const getModalDom = (key: string) => {
  return document.getElementById(key);
};

export const closeModal = (key: string) => {
  const node = getModalDom(key);
  if (node) {
    document.body.removeChild(node);
  }
};

/**
 * 动态插入页面
 */
const Modal = (props: ModalRenderPropsType) => {
  const { children, btnConfig } = props;

  // @ts-ignore
  if (!components[children.modalType]) {
    console.log(`Table内置组件:${children.modalType}无法识别`);
    return;
  }

  // 判断是否是弹窗组件
  if (children.modalType === 'Download' || children.modalType === 'OnClick') {
    // 如果不是 就直接执行函数
    // @ts-ignore
    components[children.modalType](children.props);
    return;
  }

  const modelName = `${btnConfig.key}-modal`;
  const modelchildName = `${btnConfig.key}-modal-child`;

  // 如果弹窗dom存在 则不弹窗
  if (getModalDom(modelName)) {
    return;
  }
  // 向组件内添加props属性
  // @ts-ignore
  const newChildren = React.createElement(components[children.modalType], {
    closeModal: () => {
      // const node = document.getElementById(modelchildName);
      // if (node && node.parentNode) {
      //   document.body.removeChild(node.parentNode);
      // }

      const node: any = document.getElementById(modelName);
      if (node.nextElementSibling) {
        const has = node.nextElementSibling.getElementsByClassName(modelchildName);
        if (has.length > 0) {
          has[0].parentNode.remove();
        }
      }

      closeModal(modelName);
    },
    modelName,
    modelchildName,
    ...children.props,
  });

  const rootDom = document.body;
  const node = document.createElement('div');
  node.style.display = 'unset';
  node.id = modelName;
  // @ts-ignore
  rootDom.appendChild(node);

  let theme: ThemeConfig = {};
  if (window?.vhAdmin?.theme) {
    // @ts-ignore
    theme = window.vhAdmin?.theme;
  }

  ReactDOM.createRoot(node).render(<ConfigProvider theme={theme}>{newChildren}</ConfigProvider>);
};

export default Modal;
