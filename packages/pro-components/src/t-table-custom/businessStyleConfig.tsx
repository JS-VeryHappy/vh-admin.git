import type { BusinessStyleType, ProColumnsTypes } from './types';
import { Tag, Image, Button, Tooltip, Space } from 'antd';
import React, { useState } from 'react';
import { downloadFile } from '@vh-admin/pro-utils';
import modalTypeRenderConfig from './modalTypeRenderConfig';
import { InfoCircleOutlined } from '@ant-design/icons';

/**
 * 单元格已tag方式显示
 */
export const tagView = (props: ProColumnsTypes<any>) => {
  const { businessStyle = {}, dataIndex } = props;
  //@ts-ignore
  const { options, field }: BusinessStyleType = businessStyle;
  let defaultOptions = [
    {
      label: 'green',
      value: 0,
    },
    {
      label: 'blue',
      value: 1,
    },
    {
      label: 'magenta',
      value: 2,
    },
    {
      label: 'red',
      value: 3,
    },
    {
      label: 'cyan',
      value: 4,
    },
  ];
  let defaultField = dataIndex;
  if (options) {
    defaultOptions = options;
  }
  if (field) {
    defaultField = field;
  }
  return (dom: any, record: any) => {
    const domOptions: any = [...defaultOptions];
    const domField: any = defaultField;
    const data: any = domOptions.find((i: any) => i.value == record[domField]);

    return <Tag color={data.label}>{dom}</Tag>;
  };
};

/**
 * 单元格已tag方式显示
 */
export const colorTextView = (props: ProColumnsTypes<any>) => {
  const { businessStyle = {}, dataIndex } = props;
  //@ts-ignore
  const { options, field }: BusinessStyleType = businessStyle;
  let defaultOptions = [
    {
      label: 'pink',
      value: 0,
    },
    {
      label: 'red',
      value: 1,
    },
    {
      label: '#1677ff',
      value: 2,
    },
    {
      label: 'orange',
      value: 3,
    },
    {
      label: 'green',
      value: 4,
    },
    {
      label: 'blue',
      value: 5,
    },
    {
      label: 'blpurpleue',
      value: 6,
    },
    {
      label: 'geekblue',
      value: 7,
    },
    {
      label: 'magenta',
      value: 8,
    },
    {
      label: 'volcano',
      value: 9,
    },
  ];
  let defaultField = dataIndex;
  if (options) {
    defaultOptions = options;
  }
  if (field) {
    defaultField = field;
  }
  return (dom: any, record: any) => {
    const domOptions: any = [...defaultOptions];
    const domField: any = defaultField;
    const data: any = domOptions.find((i: any) => i.value == record[domField]);

    return <span style={{ color: data.label }}>{dom}</span>;
  };
};

/**
 * 单元格已urlLink方式显示
 */
export const urlLink = (props: ProColumnsTypes<any>) => {
  //@ts-ignore
  const { businessStyle: { style, handelClick } = {} } = props;

  return (dom: any, record: any) => {
    const onClick = () => {
      if (typeof handelClick === 'function') {
        handelClick(record);
      }
    };
    return (
      <span
        onClick={onClick}
        style={{ color: '#1677ff', cursor: 'pointer', textDecoration: 'underline', ...style }}
      >
        {dom}
      </span>
    );
  };
};

const PreviewImageCustom = (props: any) => {
  const { images, text, style } = props;
  const [visible, setVisible] = useState<boolean>(false);
  const onClick = () => {
    setVisible(true);
  };
  return (
    <>
      <span onClick={onClick} style={{ color: '#1677ff', cursor: 'pointer', ...style }}>
        {text}
      </span>
      <div style={{ display: 'none' }}>
        <Image.PreviewGroup preview={{ visible, onVisibleChange: (vis) => setVisible(vis) }}>
          {images.map((url: any) => {
            return <Image key={url} src={url} />;
          })}
        </Image.PreviewGroup>
      </div>
    </>
  );
};
/**
 * 预览图片
 */
export const previewImage = (props: ProColumnsTypes<any>) => {
  //@ts-ignore
  const { businessStyle: { auth, text = '查看', style } = {}, dataIndex } = props;

  return (dom: any, record: any) => {
    const value: any = record[dataIndex];
    const multiple: any = value instanceof Array ? true : false;
    const images = multiple ? value : [value];
    let isAuth = true;

    if (typeof auth === 'function') {
      isAuth = auth(record);
    }

    return (
      <>
        {(multiple && value.length > 0) || (!multiple && value && isAuth) ? (
          <>
            <PreviewImageCustom images={images} text={text} style={style} />
          </>
        ) : (
          <span style={{ color: '#aaa' }}>{text}</span>
        )}
      </>
    );
  };
};

/**
 * 下载文件
 */
export const download = (props: ProColumnsTypes<any>) => {
  const {
    //@ts-ignore
    businessStyle: { request, paramsBefor, auth, text = '下载', style, fileName = '' } = {},
    dataIndex,
  } = props;

  return (dom: any, record: any) => {
    const value: any = record[dataIndex];
    let isAuth = true;
    let params = record;

    if (typeof paramsBefor === 'function') {
      params = paramsBefor(params);
    }

    if (typeof auth === 'function') {
      isAuth = auth(params);
    }

    const downloadFn = () => {
      if (!request) {
        downloadFile('a', {
          url: value,
          params,
        });
      } else {
        request(params)
          .them((res: any) => {
            downloadFile('', { data: res.data, fileName, headers: res.headers });
          })
          .catch(() => {});
      }
    };

    return (
      <>
        {value && isAuth ? (
          <>
            <span onClick={downloadFn} style={{ color: '#1677ff', cursor: 'pointer', ...style }}>
              {text}
            </span>
          </>
        ) : (
          <span style={{ color: '#aaa' }}>{text}</span>
        )}
      </>
    );
  };
};

/**
 * 单元格使用表格内置功能
 */
const withinFn = (props: ProColumnsTypes<any>) => {
  const {
    //@ts-ignore
    businessStyle: { btnConfig, parentType = 'button' } = {},
    dataIndex,
  } = props;
  const type = 'cell';
  btnConfig.key = `cell-${dataIndex}`;
  btnConfig.className = `cell-${dataIndex}`;
  if (!btnConfig.text) {
    btnConfig.text = '查看';
  }
  if (!btnConfig.type) {
    btnConfig.type = 'link';
  }
  const { modalConfig, text, icon, onClick, auth, disabled, tooltip, ...config } = btnConfig;
  // 处理自定义弹窗类型
  const { modalType, render } = modalConfig || {};

  return (dom: any, record: any) => {
    // const value: any = record[dataIndex];
    const nodes: any = [];

    if (!auth || (typeof auth === 'function' && auth(btnConfig, record))) {
      let newDisable: any;
      if (typeof disabled === 'function') {
        newDisable = disabled(record, btnConfig);
      } else if (typeof disabled === 'boolean' && disabled) {
        newDisable = disabled;
      }

      const clickConfig = {
        btnConfig,
        itext: text,
        irecord: record,
      };

      const onClickFn = () => {
        if (newDisable) {
          return;
        }
        if (typeof onClick === 'function') {
          onClick(record, btnConfig);
        } else {
          if (modalType) {
            modalTypeRenderConfig({
              children: {
                modalType,
                props: {
                  render,
                  btnConfig,
                  tableProps: props,
                  type,
                  clickConfig,
                  tableRef: undefined,
                  tableFormRef: undefined,
                },
              },
              btnConfig,
              clickConfig,
            });
          }
        }
      };

      if (parentType === 'div') {
        nodes.push(
          <div {...config} disabled={newDisable} onClick={onClickFn} key={`${btnConfig.key}`}>
            {icon}
            {typeof text === 'function' ? text(dom, record) : text}
          </div>,
        );
      } else {
        nodes.push(
          <Button {...config} disabled={newDisable} onClick={onClickFn} key={`${btnConfig.key}`}>
            {icon}
            {typeof text === 'function' ? text(dom, record) : text}
          </Button>,
        );
      }

      if (tooltip) {
        nodes.push(
          <Tooltip
            style={{}}
            key={`${btnConfig.key}-Tooltip`}
            placement="top"
            title={tooltip.text}
            {...tooltip.tooltipProps}
          >
            <InfoCircleOutlined
              style={{ color: '#faad14', marginLeft: -12 }}
              {...tooltip.iconProps}
            />
          </Tooltip>,
        );
      }

      return (
        <>
          <Space>{nodes}</Space>
        </>
      );
    } else {
      return <>-</>;
    }
  };
};
/**
 * 标签方法对应表
 */
let tableBusinessStyleArr: any = {
  tag: tagView,
  colorText: colorTextView,
  urllink: urlLink,
  previewImage: previewImage,
  download: download,
  within: withinFn,
};

// @ts-ignore
if (window?.vhAdmin?.tableBusinessStyleArr) {
  // @ts-ignore
  tableBusinessStyleArr = { ...tableBusinessStyleArr, ...window.vhAdmin?.tableBusinessStyleArr };
}

const getBusinessStyle = (props: ProColumnsTypes<any>) => {
  const type = props.businessStyle?.type;
  if (type) {
    return tableBusinessStyleArr[type](props);
  }
};

export default getBusinessStyle;
