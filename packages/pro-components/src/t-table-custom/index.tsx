import ProTable, { TableDropdown, EditableProTable } from '@ant-design/pro-table';
import ProList from '@ant-design/pro-list';
import type { TableCustomTypes, ProColumnsTypes, TableRefType } from './types';
import * as components from '../f-form-custom/components';
import React, { useState, useEffect, useRef, useImperativeHandle } from 'react';
import { Button, Space, Table, Tooltip } from 'antd';
import type { FormInstance } from 'antd';
import './index.less';
import modalTypeRenderConfig from './modalTypeRenderConfig';
import {
  headerTitleConfigArr,
  tableAlertOptionRenderConfigArr,
  operationConfigRenderConfigArr,
} from './btnConfig';
import getBusinessStyle from './businessStyleConfig';
import { InfoCircleOutlined } from '@ant-design/icons';
import { isMobile, deepGet } from '@vh-admin/pro-utils';
import type { PaginationProps } from 'antd';
import type { RowSelectMethod } from 'antd/es/table/interface';

/**
 * 获取第一个表格的可视化高度
 * @param {*} costomHeight 额外的高度(表格底部的内容高度 Number类型,默认为74)
 * @param {*} id 当前页面中有多个table时需要制定table的id
 */
function getTableScroll(Props: any) {
  let extraHeight = Props.costomHeight;
  const id = Props.costomId || null;

  if (typeof extraHeight === 'undefined') {
    // @ts-ignore
    if (window?.vhAdmin?.tableExtraHeight) {
      // @ts-ignore
      extraHeight = window.vhAdmin?.tableExtraHeight;
    } else {
      //  默认底部分页24+32 44额外高度
      extraHeight = 56 + 44;
    }
  }

  // 如果没有分页
  if (Props.pagination === false) {
    extraHeight = extraHeight - 56;
  }
  // 如果开启汇总栏
  if (Props.summaryType || Props.summary) {
    extraHeight = extraHeight + 39;
  }

  let tHeader = null;
  if (id) {
    tHeader = document.getElementById(id)
      ? // @ts-ignore
        document.getElementById(id).getElementsByClassName('ant-table-thead')[0]
      : null;
  } else {
    tHeader = document.getElementsByClassName('ant-table-thead')[0];
  }
  // 表格内容距离顶部的距离
  let number = 39; //算上自己高度
  if (tHeader) {
    number = tHeader.getBoundingClientRect().top + tHeader.getBoundingClientRect().height;
  }

  // 窗体高度-表格内容顶部的高度-表格内容底部的高度
  // let height = document.body.clientHeight - tHeaderBottom - extraHeight
  return (document.body.offsetHeight - number - extraHeight) * 1;
  const height = `calc(100vh - ${number + extraHeight}px)`;
  return height;
}

const requestParamsTrim = (params: any) => {
  Object.keys(params).forEach((key: string) => {
    if (typeof params[key] === 'string') {
      params[key] = params[key].trim();
    }
  });
  return params;
};

function TableCustom<T>(Props: TableCustomTypes<T>) {
  let operationMax = 2;
  // @ts-ignore
  if (window?.vhAdmin?.tableOperationMax) {
    // @ts-ignore
    operationMax = window.vhAdmin?.tableOperationMax;
  }

  const {
    className,
    tableType = 'ProTable',
    columns,
    request,
    requestBefor,
    search,
    headerTitle,
    headerTitleConfig,
    rowSelection,
    pagination,
    tableAlertRender,
    selectionConfig,
    tableAlertOptionRender,
    operationConfig,
    scroll,
    actionRef,
    formRef,
    operationBtnShowMax = operationMax,
    onDataSourceChange,
    summary,
    summaryType,
    summaryText = '合计',
    summaryRender,
    summaryRemoteField = 'summary',
    summarylocalFields = [],
    ...rest
  } = Props;

  const [scrollY, setScrollY] = useState<string | number>(() => {
    return Props.virtual ? 300 : '';
  });
  const tableRef = useRef<TableRefType>();
  const tableFormRef = useRef<FormInstance<any>>();
  const summaryRef = useRef<any>();
  const selectKeys = useRef<any>([]);

  useEffect(() => {
    setTimeout(() => {
      setScrollY(getTableScroll(Props));
    }, 300);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useImperativeHandle(
    formRef,
    () => {
      return tableFormRef.current;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [tableFormRef.current],
  );

  useImperativeHandle(
    actionRef,
    () => {
      return tableRef.current;
    },
    [tableRef],
  );

  //@ts-ignore
  // tableRef.current.formRef = tableFormRef;
  /**
   * 如果配置使用了智能弹窗模式 寻找对应弹窗dom结构
   * @param kitem
   * @param btnConfig
   * @returns
   */
  const setModalType = (kitem: string, btnConfig: any, type: string) => {
    // 处理自定义弹窗类型
    const { modalConfig, ...modelRest } = btnConfig;

    const { modalType, render } = modalConfig || {};

    // 如果设置了弹窗类型
    if (modalType) {
      // 重置按钮点击事件、如果是点击事件中间件处理不同点击分发
      modelRest.onClick = (clickConfig: any) => {
        return modalTypeRenderConfig({
          children: {
            modalType,
            props: {
              render,
              btnConfig,
              tableProps: Props,
              type,
              clickConfig,
              tableRef,
              tableFormRef,
            },
          },
          btnConfig,
          clickConfig,
        });
      };
    }
    return modelRest;
  };
  /**
   *
   * @param kitem // 按钮key
   * @param configArr // 默认配置
   * @param config  // 传入配置
   * @param type // 按钮业务类型 header:表头 select:多选  operation:row菜单 cell:单元格
   * @returns
   */
  const setBtnConfig = (kitem: string, configArr: any, config: any, type: string = 'header') => {
    // 按钮取默认值
    const defaultBtnConfig = configArr.default;
    let btnConfig: any = {};
    // 如果自定义有按钮配置 合并
    if (configArr[kitem]) {
      btnConfig = { ...defaultBtnConfig, ...configArr[kitem] };
    } else {
      btnConfig = { ...defaultBtnConfig };
    }
    const text = btnConfig.text;
    // key名称 必须是唯一
    btnConfig.key = `${type}-${kitem}`;
    // 按钮类型
    btnConfig.className = `${type}-item ${type}-item-${kitem}`;

    // 如果传入的是方法
    if (typeof config[kitem] === 'function') {
      btnConfig.onClick = config[kitem];
    } else if (typeof config[kitem] === 'object') {
      if (config[kitem].modalConfig?.config?.customProps) {
        const customProps = config[kitem].modalConfig.config.customProps;
        delete config[kitem].modalConfig.config.customProps;
        config[kitem].modalConfig.config = { ...config[kitem].modalConfig.config, ...customProps };
      }
      btnConfig = {
        ...btnConfig,
        ...config[kitem],
      };
    }

    if (!btnConfig.text) {
      btnConfig.text = text;
    }
    if (!btnConfig.type) {
      btnConfig.type = 'link';
    }

    return setModalType(kitem, btnConfig, type);
  };
  /**
   * 处理传递过来的配置
   */
  const setCustomColumns = (oldColumns: any) => {
    const customColumns: ProColumnsTypes<any>[] = [];
    let searchCustom: boolean | any = false;

    if (oldColumns) {
      oldColumns.forEach((item: any) => {
        if (item.valueType === 'option') {
          return;
        }
        if (typeof item.search === 'undefined' || item.search === true) {
          searchCustom = {
            filterType: 'query',
            defaultCollapsed: true,
            span: 6,
          };
          if (isMobile()) {
            searchCustom.span = 24;
          }
        }
        // 业务样式快捷使用
        if (item.businessStyle && !item.render) {
          item.render = getBusinessStyle(item);
        }
        // 如果是自定义组件
        if (
          item.valueType &&
          item.valueType.indexOf('Custom') !== -1 &&
          (!item.renderFormItem || !item.render)
        ) {
          // @ts-ignore
          if (components[item.valueType]) {
            // @ts-ignore
            const CustomComponent: any = components[item.valueType];

            if (!item.render) {
              item.render = (text: any, record: any) => {
                let newProps: any = {
                  readonly: true,
                  value: record[item.dataIndex] || undefined,
                  customMode: 'tableRead',
                };

                if (tableType === 'EditableProTable' && item.fieldProps) {
                  newProps = {
                    ...newProps,
                    ...item.fieldProps,
                  };
                }
                // @ts-ignore
                return <CustomComponent {...newProps} />;
              };
            }
            if (!item.renderFormItem) {
              // eslint-disable-next-line no-param-reassign
              item.renderFormItem = (i: any, { type, isEditable }: any) => {
                if (type === 'form' && !isEditable) {
                  return null;
                }

                let newProps: any = {
                  customMode: 'table',
                };

                if (tableType === 'EditableProTable' && item.fieldProps) {
                  newProps = {
                    ...newProps,
                    ...item.fieldProps,
                  };
                }

                return <CustomComponent {...newProps} />;
              };
            }
          } else {
            console.log(`自定义组件:${item.valueType}无法识别`);
          }
        }

        if (isMobile()) {
          item.width = 120;
          item.ellipsis = true;
        }

        // 如果配置只生效表格的宽度 赋值
        if (item.tableWidth) {
          item.width = item.tableWidth;
        }

        // 如果是select 默认开始搜索
        if (
          item.valueType === 'select' &&
          item.fieldProps &&
          item.fieldProps.showSearch == undefined
        ) {
          item.fieldProps.showSearch = true;
        }

        //处理dependency联动
        if (item.valueType === 'dependency' && item.columns && typeof item.columns === 'function') {
          const fn = item.columns;
          item.columns = (data: any) => {
            const dependencyData = setCustomColumns(fn(data));
            return dependencyData.customColumns;
          };
        }

        //如果表格含有children子集，分组表头则需要处理
        if (item.children && Array.isArray(item.children) && item.children.length > 0) {
          const childrenData = setCustomColumns(item.children);
          item.children = childrenData.customColumns;
          if (!searchCustom && childrenData.searchCustom) {
            searchCustom = childrenData.searchCustom;
          }
        }

        customColumns.push(item);
      });
    }

    return { customColumns, searchCustom };
  };

  const { customColumns, searchCustom } = setCustomColumns(columns);

  let operationConfigRenderFun;
  // 处理快捷配置row操作菜单
  if (operationConfig) {
    const operationKeys = Object.keys(operationConfig);

    operationConfigRenderFun = (itext: any, irecord: any, _: any, iaction: any) => {
      const nodes: any = [];
      const dropdownBtns: any = [];
      operationKeys.forEach((kitem, kindex) => {
        const btnConfig = setBtnConfig(
          kitem,
          operationConfigRenderConfigArr,
          operationConfig,
          'operation',
        );

        // 拆分参数
        const { key, text, icon, onClick, auth, disabled, tooltip, ...config } = btnConfig;

        if (
          !auth ||
          (typeof auth === 'function' &&
            auth(btnConfig, irecord, selectKeys, tableRef, tableFormRef))
        ) {
          let newDisable;
          if (typeof disabled === 'function') {
            newDisable = disabled(irecord, btnConfig, selectKeys, tableRef, tableFormRef);
          } else if (typeof disabled === 'boolean' && disabled) {
            newDisable = disabled;
          }
          if (kindex + 1 > operationBtnShowMax) {
            dropdownBtns.push({
              key,
              name: text,
              disabled: newDisable,
              ...config,
              onClick: onClick.bind(null, {
                btnConfig,
                itext,
                irecord,
                _,
                iaction,
              }),
            });
            return;
          }
          nodes.push(
            <Button
              {...config}
              disabled={newDisable}
              onClick={onClick.bind(null, {
                btnConfig,
                itext,
                irecord,
                _,
                iaction,
              })}
              key={key}
            >
              {icon}
              {text}
            </Button>,
          );
          if (tooltip) {
            nodes.push(
              <Tooltip
                key={`${config.key}-Tooltip`}
                placement="top"
                title={tooltip.text}
                {...tooltip.tooltipProps}
              >
                <InfoCircleOutlined
                  style={{
                    zIndex: 9,
                    position: 'relative',
                    color: '#faad14',
                    cursor: 'pointer',
                    marginLeft: -6,
                  }}
                  {...tooltip.iconProps}
                />
              </Tooltip>,
            );
          }
        }
      });

      if (dropdownBtns && dropdownBtns.length > 0) {
        nodes.push(
          <TableDropdown key={`${irecord.id}operation-actionGroup`} menus={dropdownBtns} />,
        );
      }
      return nodes;
    };
    const optionMenu = columns?.find((i: any) => i.valueType === 'option');
    let newOptionMenu: any = {};
    if (optionMenu) {
      newOptionMenu = {
        ...optionMenu,
        render: operationConfigRenderFun,
      };
    } else {
      newOptionMenu = {
        title: '操作',
        key: 'option',
        valueType: 'option',
        render: operationConfigRenderFun,
      };
    }
    if (isMobile()) {
      if (!newOptionMenu.width) {
        newOptionMenu.width = 120;
      }
      if (newOptionMenu.fixed) {
        newOptionMenu.fixed = undefined;
      }
    }
    customColumns.push(newOptionMenu);
  }

  let customHeaderTitle = headerTitle;
  // 处理自定义 headerTitle 快捷设置
  if (!headerTitle && headerTitleConfig) {
    const keys = Object.keys(headerTitleConfig);
    const nodes: any = [];
    keys.forEach((kitem: any) => {
      const btnConfig = setBtnConfig(kitem, headerTitleConfigArr, headerTitleConfig, 'header');
      // 拆分参数
      const { text, icon, onClick, auth, disabled, tooltip, ...config } = btnConfig;
      if (
        !auth ||
        (typeof auth === 'function' && auth(btnConfig, selectKeys, tableRef, tableFormRef))
      ) {
        let newDisable;
        if (typeof disabled === 'function') {
          newDisable = disabled(btnConfig, selectKeys, tableRef, tableFormRef);
        } else if (typeof disabled === 'boolean' && disabled) {
          newDisable = disabled;
        }
        let newIcon = icon;
        if (icon) {
          newIcon = React.createElement(icon, { key: `icon-${kitem}` });
        }

        nodes.push(
          <Button
            {...config}
            key={config.key}
            onClick={onClick.bind(null, { btnConfig })}
            disabled={newDisable}
          >
            {newIcon}
            {text}
          </Button>,
        );
        if (tooltip) {
          nodes.push(
            <Tooltip
              key={`${config.key}-Tooltip`}
              placement="top"
              title={tooltip.text}
              {...tooltip.tooltipProps}
            >
              <InfoCircleOutlined
                style={{
                  zIndex: 9,
                  position: 'relative',
                  color: '#faad14',
                  cursor: 'pointer',
                  marginLeft: -6,
                }}
                {...tooltip.iconProps}
              />
            </Tooltip>,
          );
        }
      }
    });

    customHeaderTitle = (
      <>
        <Space>{nodes}</Space>
      </>
    );
  }

  let customRowSelection: any = rowSelection ? { ...rowSelection } : undefined;
  let customTableAlertRender = tableAlertRender;
  let customTableAlertOptionRender = tableAlertOptionRender;

  const onSelectChange = (
    selectedRowKeys: any[],
    selectedRows: any[],
    info: {
      type: RowSelectMethod;
    },
  ) => {
    if (selectionConfig || rowSelection) {
      if (rowSelection && rowSelection.onChange) {
        rowSelection.onChange(selectedRowKeys, selectedRows, info);
      }
    }
    // if (
    //   (selectKeys.current.length === 0 && selectedRowKeys.length !== 0) ||
    //   (selectKeys.current.length > 0 && selectedRowKeys.length === 0)
    // ) {
    //   setTimeout(() => {
    //     setScrollY(getTableScroll(Props));
    //   }, 0);
    // }

    selectKeys.current = selectedRowKeys;
  };

  if (selectionConfig && !rowSelection) {
    customRowSelection = {
      // 注释该行则默认不显示下拉选项
      // selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
      fixed: true,
      columnWidth: 48,
    };
  }

  if (rowSelection) {
    if (rowSelection.fixed === undefined) {
      customRowSelection.fixed = true;
    }
    if (rowSelection.columnWidth === undefined) {
      customRowSelection.columnWidth = 48;
    }
  }

  if (selectionConfig || rowSelection) {
    customRowSelection.onChange = onSelectChange;
  }

  // 处理自定义多选快捷设置
  if (!tableAlertRender && selectionConfig) {
    if (!tableAlertRender) {
      customTableAlertRender = ({ selectedRowKeys, onCleanSelected }: any) => (
        <Space size={24}>
          <span>
            已选 {selectedRowKeys.length} 项
            <a style={{ marginLeft: 8 }} onClick={onCleanSelected}>
              取消选择
            </a>
          </span>
        </Space>
      );
    }
    if (!tableAlertOptionRender) {
      const keys = Object.keys(selectionConfig);

      customTableAlertOptionRender = ({ selectedRowKeys, onCleanSelected }: any) => {
        const nodes: any = [];
        keys.forEach((kitem) => {
          const btnConfig = setBtnConfig(
            kitem,
            tableAlertOptionRenderConfigArr,
            selectionConfig,
            'selection',
          );

          // 拆分参数
          const { text, onClick, auth, disabled, tooltip, ...config } = btnConfig;

          if (
            !auth ||
            (typeof auth === 'function' &&
              auth(btnConfig, selectedRowKeys, onCleanSelected, selectKeys, tableRef, tableFormRef))
          ) {
            let newDisable;
            if (typeof disabled === 'function') {
              newDisable = disabled(
                selectedRowKeys,
                onCleanSelected,
                btnConfig,
                selectKeys,
                tableRef,
                tableFormRef,
              );
            } else if (typeof disabled === 'boolean' && disabled) {
              newDisable = disabled;
            }

            nodes.push(
              <Button
                {...config}
                key={config.key}
                disabled={newDisable}
                onClick={onClick.bind(null, {
                  btnConfig,
                  irecord: {
                    id: selectedRowKeys,
                  },
                  selectedRowKeys,
                  onCleanSelected,
                })}
              >
                {text}
              </Button>,
            );
            if (tooltip) {
              nodes.push(
                <Tooltip
                  key={`${config.key}-Tooltip`}
                  placement="top"
                  title={tooltip.text}
                  {...tooltip.tooltipProps}
                >
                  <InfoCircleOutlined
                    style={{
                      zIndex: 9,
                      position: 'relative',
                      color: '#faad14',
                      cursor: 'pointer',
                      marginLeft: -14,
                    }}
                    {...tooltip.iconProps}
                  />
                </Tooltip>,
              );
            }
          }
        });

        return <Space size={0}>{nodes}</Space>;
      };
    }
  }

  let TableComponent: any = ProTable;
  if (tableType === 'ProList') {
    // 如果是使用 ProList数据模式
    TableComponent = ProList;
    // 如果有设置operation按钮 并且没有设置actions
    if (operationConfig && operationConfigRenderFun && rest.metas && !rest.metas.actions) {
      rest.metas.actions = {
        render: operationConfigRenderFun,
      };
    }
  } else if (tableType === 'EditableProTable') {
    // 如果是使用 ProList数据模式
    TableComponent = EditableProTable;
    // 因为上面是过滤了的 这里如果找到就添加进入
    const optionMenu = columns?.find((i: any) => i.valueType === 'option');
    if (optionMenu) {
      customColumns.push(optionMenu);
    }
  }

  const onDataSourceChangeFn = (dataSource: any) => {
    if (onDataSourceChange && typeof onDataSourceChange === 'function') {
      onDataSourceChange(dataSource);
    }
    //如果开启保留则不清空
    if (customRowSelection && customRowSelection.preserveSelectedRowKeys) {
      return;
    }
    //统一清空多选状态，在数据发生变化的时候
    if (selectionConfig && tableRef && tableRef?.current?.clearSelected) {
      tableRef.current.clearSelected();
    }
  };

  let summaryFn = undefined;
  let summaryIndex: number = 0;
  const totalRow: any = {};

  /**
   * 递归循环找出对应的数据
   * @param summaryColumns
   * @param totalRow
   * @param pageData
   */
  const getSummaryColumns = (summaryColumns: any, pageData: any) => {
    summaryColumns
      .filter((i: any) => !i.hideInTable || i.hideInTable === false)
      .forEach((i: any) => {
        if (i.children && i.children.length > 0) {
          getSummaryColumns(i.children, pageData);
          return;
        }
        if (summaryIndex !== 0) {
          totalRow[i.dataIndex] = '-';
          if (summaryType === 'local') {
            if (summarylocalFields.includes(i.dataIndex)) {
              let number: any = 0;
              pageData.forEach((data: any) => {
                if (data[i.dataIndex]) {
                  number += data[i.dataIndex] * 1;
                }
              });
              if (number.toString().indexOf('.') != -1) {
                number = number.toFixed(2);
              }
              totalRow[i.dataIndex] = number * 1;
            }
          } else {
            if (summaryRef.current) {
              const remoteField = summaryRef.current[summaryRemoteField];
              if (remoteField && remoteField[i.dataIndex] !== undefined) {
                totalRow[i.dataIndex] = remoteField[i.dataIndex];
              }
            }
          }
        }
        summaryIndex = summaryIndex + 1;
      });
  };

  if (summary) {
    summaryFn = summary;
  } else if (summaryType) {
    summaryFn = (pageData: any) => {
      getSummaryColumns(customColumns, pageData);
      let key: number = 0;
      if (selectionConfig || rowSelection) {
        key = 1;
      }
      let summaryNode: any = (
        <Table.Summary fixed>
          <Table.Summary.Row>
            {(selectionConfig || rowSelection) && (
              <Table.Summary.Cell key={0} index={0}>
                -
              </Table.Summary.Cell>
            )}
            <Table.Summary.Cell key={key} index={key}>
              {summaryText}
            </Table.Summary.Cell>
            {Object.keys(totalRow).map((i: any, index: any) => {
              return (
                <Table.Summary.Cell key={index + key + 1} index={index + key + 1}>
                  {totalRow[i]}
                </Table.Summary.Cell>
              );
            })}
          </Table.Summary.Row>
        </Table.Summary>
      );

      if (typeof summaryRender === 'function') {
        summaryNode = summaryRender(summaryNode, totalRow);
      }

      return summaryNode;
    };
  }

  let tableSearchDefaultCollapsed: boolean = true;
  // @ts-ignore
  if (window?.vhAdmin?.tableSearchDefaultCollapsed !== undefined) {
    // @ts-ignore
    tableSearchDefaultCollapsed = window.vhAdmin?.tableSearchDefaultCollapsed;
  }

  let customPagination: PaginationProps | false | undefined = {
    showSizeChanger: true,
  };

  if (pagination) {
    customPagination = {
      ...customPagination,
      ...pagination,
    };
  } else if (pagination === false) {
    customPagination = false;
  }

  return (
    <>
      <TableComponent<T>
        className={`table-custom ${className ? className : ''}`}
        rowKey="id"
        actionRef={tableRef}
        formRef={tableFormRef}
        scroll={
          isMobile()
            ? {}
            : {
                y: scrollY,
                ...scroll,
              }
        }
        onDataSourceChange={onDataSourceChangeFn}
        columns={customColumns}
        size="small"
        revalidateOnFocus={false}
        request={async (
          // 第一个参数 params 查询表单和 params 参数的结合
          // 第一个参数中一定会有 pageSize 和  current ，这两个参数是 antd 的规范
          requestParams: any,
          sort: any,
          filter: any,
        ) => {
          // 有个版本 sort由 {} 变为 undefined 解决下
          let newSort: any = {};
          if (sort) {
            newSort = sort;
          }
          const tableData = {
            data: [],
            // success 请返回 true，
            // 不然 table 会停止解析数据，即使有数据
            success: false,
            // 不传会使用 data 的长度，如果是分页一定要传
            total: 0,
          };
          let responseTableConfig: any = {
            data: 'data', // 表格数据列表字段名称
            total: 'total', // 表格数据列表总数字段名称
          };
          // @ts-ignore
          if (window?.vhAdmin?.responseTableConfig) {
            // @ts-ignore
            responseTableConfig = window.vhAdmin?.responseTableConfig;
          }

          try {
            // 这里需要返回一个 Promise,在返回之前你可以进行数据转化
            // 如果需要转化参数可以在这里进行修改
            let customParams = {
              ...requestParams,
              page: requestParams.current,
              pageSize: requestParams.pageSize,
              sort: newSort,
              filter,
            };

            // @ts-ignore
            if (window?.vhAdmin?.tableRequestParamsHook) {
              // @ts-ignore
              customParams = window.vhAdmin?.tableRequestParamsHook(requestParams, newSort, filter);
            }

            // @ts-ignore
            if (window?.vhAdmin?.requestParamsTrim !== false) {
              customParams = requestParamsTrim(customParams);
            }

            if (typeof requestBefor === 'function') {
              customParams = requestBefor(customParams);
            }

            const res: any = await request(customParams);

            tableData.data = deepGet(res.data, responseTableConfig.data);
            tableData.total = deepGet(res.data, responseTableConfig.total);
            tableData.success = true;

            if (summaryType && summaryType === 'remote') {
              summaryRef.current = res.data;
            }
          } catch (error) {}

          return tableData;
        }}
        summary={summaryFn}
        search={
          search === false
            ? false
            : { ...searchCustom, ...search, defaultCollapsed: tableSearchDefaultCollapsed }
        }
        options={{ fullScreen: false, reload: true, setting: true, density: false, search: false }}
        headerTitle={customHeaderTitle}
        rowSelection={customRowSelection}
        tableAlertRender={customTableAlertRender}
        tableAlertOptionRender={customTableAlertOptionRender}
        pagination={customPagination}
        {...rest}
      />
    </>
  );
}

export default TableCustom;
