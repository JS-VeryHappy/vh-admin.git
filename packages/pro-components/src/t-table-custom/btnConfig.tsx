import type { BtnConfigTypes } from './types';
import { PlusOutlined, ImportOutlined, ExportOutlined, DownloadOutlined } from '@ant-design/icons';
import { deepMerge } from '@vh-admin/pro-utils';

/**
 * 配置顶部header快捷菜单按钮
 */
let nheaderTitleConfigArr: BtnConfigTypes = {
  create: {
    text: '新增',
    icon: PlusOutlined,
    type: 'primary',
  },
  import: {
    text: '导入',
    icon: ImportOutlined,
    type: 'primary',
    style: {
      background: '#faad14',
      borderColor: '#faad14',
    },
  },
  export: {
    text: '导出',
    icon: ExportOutlined,
    type: 'primary',
    style: {
      background: '#269884',
      borderColor: '#269884',
    },
  },
  download: {
    text: '下载',
    icon: DownloadOutlined,
    type: 'primary',
    style: {
      background: '#2EB9C3',
      borderColor: '#2EB9C3',
    },
  },
  default: {
    text: '按钮', // 按钮显示名称
    type: 'primary', // 按钮类型
    auth: () => true, // 显示权限
  },
};

/**
 * 配置顶部select选中快捷菜单配置
 */
let ntableAlertOptionRenderConfigArr: BtnConfigTypes = {
  delete: {
    text: '批量删除',
    type: 'link',
    danger: true,
  },
  export: {
    text: '导出',
    type: 'link',
  },
  default: {
    text: '按钮', // 按钮显示名称
    type: 'link',
    auth: () => true, // 显示权限
  },
};

/**
 * 配置row记录快捷菜单配置
 */
let noperationConfigRenderConfigArr: BtnConfigTypes = {
  edit: {
    text: '编辑',
    type: 'link',
  },
  delete: {
    text: '删除',
    type: 'link',
    danger: true,
  },
  download: {
    text: '下载',
    type: 'link',
  },
  default: {
    text: '按钮', // 按钮显示名称
    type: 'link',
    auth: () => true, // 显示权限
  },
};

//@ts-ignore
if (window?.vhAdmin?.btnConfig) {
  //@ts-ignore
  const btnConfig = window.vhAdmin.btnConfig;
  if (btnConfig.headerTitleConfigArr) {
    nheaderTitleConfigArr = deepMerge(nheaderTitleConfigArr, btnConfig.headerTitleConfigArr);
  }
  if (btnConfig.tableAlertOptionRenderConfigArr) {
    ntableAlertOptionRenderConfigArr = deepMerge(
      ntableAlertOptionRenderConfigArr,
      btnConfig.tableAlertOptionRenderConfigArr,
    );
  }
  if (btnConfig.operationConfigRenderConfigArr) {
    noperationConfigRenderConfigArr = deepMerge(
      noperationConfigRenderConfigArr,
      btnConfig.operationConfigRenderConfigArr,
    );
  }
}

export const headerTitleConfigArr = nheaderTitleConfigArr;
export const tableAlertOptionRenderConfigArr = ntableAlertOptionRenderConfigArr;
export const operationConfigRenderConfigArr = noperationConfigRenderConfigArr;
