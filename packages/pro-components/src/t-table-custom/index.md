---
title: 使用样例和API说明
order: 0
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表格组件
  path: /table-custom
  order: 2
---

## TableCustom API 和 更多使用样例

- [ProComponents-Table](https://procomponents.ant.design/components/table)
- [Antd-Table](https://ant.design/components/table-cn/)

## 普通使用

<code src="./Example/demo1.jsx" 
      title="普通使用" 
      description="正常表格使用、表单部分和FormCustom的使用基一样、可以使用自定义组件">普通使用</code>

## 配置快捷菜单

<code src="./Example/demo2.jsx" 
      title="配置快捷菜单" 
      description="表单可以使用按钮菜单的快捷配置、结合外部弹窗组件使用。
      可以配置按钮disabled和auth显示权限、tooltip提示。
      开启弹窗表单只读模式
      ">配置快捷菜单</code>

## 使用内置功能

<code src="./Example/demo3.jsx" 
      title="使用内置功能" 
      description="使用表格内置功能业务组件、表格使用表格配置一个配置就搞定、开发起飞">使用内置功能</code>

## 复杂表头

<code src="./Example/demo4.jsx" 
      title="复杂表头" 
      description="使用表格内置功能业务组件、复杂的表格表单的配置">复杂表头</code>

## ProList 使用 [ProList](https://procomponents.ant.design/components/list)

<code src="./Example/demo5.jsx" 
      title="ProList使用" 
      description="使用ProList表格和内置功能业务组件组合说那个">ProList 使用</code>

## 单元格快捷样式的使用

<code src="./Example/demo6.jsx" 
      title="表格单元格样式" 
      description="可以配置一些统一的单元格样式">表格单元格样式</code>

## 汇总栏本地统计和远程统计

<code src="./Example/demo10.jsx" 
      title="表格汇总栏" 
      description="可以配置表格显示汇总栏，分为本地和远程">表格汇总栏</code>

## 开启虚拟滚动

<code src="./Example/demo11.jsx" 
      title="开启虚拟滚动" 
      description="开启虚拟">开启虚拟滚动</code>


## API

<embed src="./Example/TableCustomTypes/TableCustomTypes.md"></embed>

<embed src="./Example/TableCustomTypes/TableCustomType.md"></embed>

<embed src="./Example/TableCustomTypes/BtnConfigType.md"></embed>

<embed src="./Example/TableCustomTypes/BtnConfigTooltipType.md"></embed>

<embed src="./Example/TableCustomTypes/ModalRenderPropsType.md"></embed>

<embed src="./Example/TableCustomTypes/ModalType.md"></embed>

<embed src="./Example/TableCustomTypes/ModalPropsType.md"></embed>

<embed src="./Example/TableCustomTypes/ModalPropsConfigType.md"></embed>

<embed src="./Example/TableCustomTypes/SubmitOnDoneType.md"></embed>

<embed src="./../f-form-custom/Example/FromCustomTypes/OptionType.md"></embed>

<embed src="./Example/TableCustomTypes/BusinessStyleType.md"></embed>

