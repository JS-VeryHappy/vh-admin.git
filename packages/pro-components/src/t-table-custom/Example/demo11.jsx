import { TableCustom } from '@vh-admin/pro-components';
import { getProTable } from '../../example';
import React from 'react';
import { Table } from 'antd';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    search: false,
    valueType: 'indexBorder',
    hideInForm: true,
    width: 200,
  },
  {
    title: '金额',
    search: false,
    width: 200,
    dataIndex: 'user_id',
  },
  {
    title: '姓名',
    search: false,
    width: 200,
    dataIndex: 'title',
  },
  {
    title: '类型',
    search: false,
    width: 200,
    dataIndex: 'type',
  },
  {
    title: '金额1',
    search: false,
    width: 200,
    dataIndex: 'user_id1',
  },
  {
    title: '姓名1',
    search: false,
    width: 200,
    dataIndex: 'title1',
  },
  {
    title: '类型1',
    search: false,
    width: 200,
    dataIndex: 'type1',
  },
  {
    title: '金额2',
    search: false,
    width: 200,
    dataIndex: 'user_id2',
  },
  {
    title: '姓名2',
    search: false,
    width: 200,
    dataIndex: 'title2',
  },
  {
    title: '类型2',
    search: false,
    width: 200,
    dataIndex: 'type2',
  },
];

function Demo11() {
  return (
    <>
      <TableCustom
        virtual
        search={false}
        request={getProTable}
        columns={columns}
        selectionConfig={{
          exportBatch: {
            text: '导出表',
            modalConfig: {
              modalType: 'Download',
              config: {
                title: '导出批量调整底表',
                searchParams: true,
                submitValuesBefor: () => {},
                submitRequest: () => {},
              },
            },
          },
        }}
        pagination={{
          pageSize: 10,
        }}
        rowSelection={{
          onChange: (a, b, c) => {
            console.log('===================');
            console.log(a, b, c);
            console.log('===================');
          },
        }}
        // scroll={{
        //   y: 300,
        // }}
      />
    </>
  );
}

export default Demo11;
