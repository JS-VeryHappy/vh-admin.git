###  ModalPropsConfigType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| title | 弹窗名称设置<br> | `string`\|<br/>`((record:any)=>React.ReactNode`\|<br/>`((props:any,type:any,dom:any)=>React.ReactNode))` | 弹窗表单 | - |
| submitValuesBefor | 提交请求的前数据的钩子<br>submitValue提交数据<br>columns当前表单配置<br> | (`submitValue:any`,<br>`columns:any`,<br>`tableRef:TableRefType`,<br>`tableFormRef:React.MutableRefObject<FormInstance>`,<br>`irecord:Record<string,any>`)=>any |  | - |
| submitRequest | 提交请求的Request<br>submitValue提交数据<br>columns当前表单配置<br> | any\|<br>((`submitValue:any`,<br>`columns:any`,<br>`tableRef:TableRefType`,<br>`tableFormRef:React.MutableRefObject<FormInstance>`,<br>`irecord:Record<string,any>`)=>any) |  | - |
| submitOnDone | 请求完成成功回调<br> | `(params:SubmitOnDoneType)=>void` |  | - |
| params | 发起网络请求的参数,与request配合使用<br> | `Record<any,any>` |  | - |
| request | 发起网络请求的参数,返回值会覆盖给initialValues<br> | `(params:any)=>Promise<any>` |  | - |      
| formCustomRef | 操作表单的Ref | `any` | - | - |
| initialValuesBefor | 数据初始化复制之前的钩子<br> | `(data:any)=>any` |  | - |
| columnBefor | 渲染表单配置文件之前<br> | `(columns:any,initialValues:any)=>any` |  | - |
| onValuesChange | 表单值改变的回调<br> | (`/***变化的值*/value:any`,<br>`/***当前表单配置*/formColumns:any`,<br>`/***设置当前表配置*/setFormColumns:any`,<br>`/***表单Ref*/formRef:any`,<br>`/***表单原始值，不会因为回调columnBefor改变变化*/oldFormColumns:any`)=>any |  | - |
| width | 弹窗类型时：弹窗的宽度<br> | `'single'\|'double'\|number\|any` | single | - |
| readonly | 是否只读 | `boolean` | - | - |
| submitter | 弹窗类型时：自定义页脚 | `any` | - | - |
| tableProps | 表格内置弹窗表格TableCustom的props | `TableCustomTypes<any>` | - | - |
| tableHeaderRender | 表格上方显示内容 | `(initialValues:any)=>React.ReactNode` | - | - |
| tableBottomRender | 表格下方显示内容 | `(initialValues:any)=>React.ReactNode` | - | - |
| customProps | 透传给使用的自定义组件<br>具体看内置的什么组件类型去查看对应的内置说明<br>会合并config传过去 | `any` | - | - |
| ... | 其他参数 都会透传给调用组件.<br>比例： `<PreviewHtmlModal {...rest} /> `| `any` | - | - |
