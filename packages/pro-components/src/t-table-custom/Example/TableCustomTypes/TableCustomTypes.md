###  TableCustomTypes

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| request | 表格自动请求 | `any` | - | - |  
| tableType | 表格类型同ProTable和ProList一直切换更简单 | `'ProTable'\|'ProList'\|'EditableProTable'` | ProTable | - |
| headerTitleConfig | header设置快捷按钮 | `BtnConfigTypes` | - | - |
| selectionConfig | 全选快捷配置 | `BtnConfigTypes` | - | - |
| summaryType | 汇总栏类型local：本地remote：远程 | `'local'\|'remote'` | - | - |
| summaryText | 汇总栏第一栏显示文字<br> | `string` | 合计 | - |
| summaryRender | 可以自定义渲染汇总栏 | `(dom:any,totalRow:any)=>React.ReactNode` | - | - |
| summaryRemoteField | 汇总栏类型远程获取接口返回的字段名<br> | `string` | summary | - |
| summarylocalFields | 汇总栏类型本地需要统计的字段名<br> | `string[]` | [] | - |
