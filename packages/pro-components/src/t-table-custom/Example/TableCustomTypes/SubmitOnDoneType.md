###  SubmitOnDoneType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| status | 请求完成的回调状态 | `'success'\|'error'` | - | - |
| params | 请求的请求数据 | `Record<any,any>` | - | - |
| result | 请求结果 | `Record<any,any>` | - | - |
| tableRef | 操作表格的tableRef | `TableRefType\|any` | - | - |
| columns | 当前表单的配置 | `any` | - | - |
| irecord | 编辑时的行数据 | `Record<string,any>` | - | - |
