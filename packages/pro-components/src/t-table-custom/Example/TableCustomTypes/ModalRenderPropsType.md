###  ModalRenderPropsType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| render | 保留字段后面用 | `any` | - | - |
| modelName | 弹窗方法本身分配的组件唯一值<br> | `string` | 自动生成的可忽略 | - |
| modelchildName | 弹窗方法分配给内容组件唯一值<br> | `string` | 自动生成的可忽略 | - |
| closeModal | 关闭整个弹窗函数调用可关闭弹窗<br> | `any` | 自动生成的可忽略 | - |
| type | 表格点击业务位置例如header<br> | `ClickType` | 自动生成的可忽略 | - |
| children | 渲染的子集<br> | `ModalRenderChildrenPropsType` | 自动生成的可忽略 | - |
| btnConfig | 按钮配置<br>可查看表格BtnConfigType | `BtnConfigType\|any` | - | - |
| clickConfig | 按钮点击回调配置和数据<br>具体为ClickType点击位置不同数据不同 | `any` | - | - |
| tableProps | 表格整个配置信息<br>可查看表格TableCustomTypes | `TableCustomTypes<any>\|any` | - | - |      
| tableRef | 操作表格tableRef | `TableRefType\|any` | - | - |
| tableFormRef | 操作表格ormRef | `React.MutableRefObject<FormInstance>` | - | - |


###  ModalRenderChildrenPropsType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 内置功能的类型 | `any` | - | - |
| props | 传递给对于内置功能props | `any` | 自动生成的可忽略 | - |
| ... | 其它 | `any` | 自动生成的可忽略 | - |
