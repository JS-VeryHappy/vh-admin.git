###  BusinessStyleTypeApi

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| ... | 根据内容 | `` | - | - |

###  BusinessStyleTypeApi - tag

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| type(必须) | 类型tag表格单元格显示形式为tag<br>类型... | `'tag'\|'colorText'` | - | - |
| field | 自定义使用判断的的字段<br>如果不配置则使用column.dataIndex | `any` | - | - |
| options | 样式参数配置信息 | `OptionType[]` | - | - |

###  BusinessStyleTypeApi - colorText

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| type(必须) | 类型tag表格单元格显示形式为tag<br>类型... | `'tag'\|'colorText'` | - | - |
| field | 自定义使用判断的的字段<br>如果不配置则使用column.dataIndex | `any` | - | - |
| options | 样式参数配置信息 | `OptionType[]` | - | - |

###  BusinessStyleTypeApi - urllink

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| type(必须) | 类型tag表格单元格显示形式为tag<br>类型... | `'urllink'` | - | - |
| handelClick(必须) | 点击回调配置时不跳转路由<br>类型为string默认附带id参数 | `(record:any)=>void` | - | - |
| style | 自定义样式style | `any` | - | - |

###  BusinessStyleTypeApi - previewImage

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| type(必须) | 类型previewImage表格单元格显示形式为点击查看图片组<br>类型... | `'previewImage'` | - | - |   
| text | 列表显示的文案<br> | `string` | '查看' | - |
| auth | 是否有权限<br> | `(record:any)=>boolean` | 显示返回false置灰 | - |
| style | 自定义样式style | `any` | - | - |

###  BusinessStyleTypeApi - download

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| type(必须) | 类型download表格单元格显示形式为点击下载，跳转a标签和自定义下载<br>类型... | `'download'` | - | - |
| text | 列表显示的文案<br> | `string` | '查看' | - |
| style | 自定义样式style | `any` | - | - |
| request | 配置使用自定义请求。不配置则使用a标签 | `any` | - | - |
| paramsBefor | 参数回调，返回给接口的请求额外参数<br>@paramrecord<br>@returns | `(record:any)=>Record<string,any>` | - | - |
| auth | 是否有权限<br> | `(record:any)=>boolean` | 显示返回false置灰 | - |
| fileName | 配置文件名，不配置则自动后去 | `any` | - | - |

###  BusinessStyleTypeApi - within

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| type(必须) | 类型within使用表格的内置功能<br>类型... | `'within'` | - | - |
| btnConfig | 按钮配置 | `BtnConfigType` | - | - |
| parentType | 单元格类型 | `'button' \| 'div'`  | button | - |
