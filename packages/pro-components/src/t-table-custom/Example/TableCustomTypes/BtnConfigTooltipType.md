###  BtnConfigTooltipType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| text(必须) | 提示文字 | `string` | - | - |
| tooltipProps | 传给tooltip的Props | `any` | - | - |
| iconProps | 传给ticon的Props | `any` | - | - |
