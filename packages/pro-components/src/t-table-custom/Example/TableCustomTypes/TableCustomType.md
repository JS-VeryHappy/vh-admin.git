###  TableCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| headerTitleConfig | header设置快捷按钮查看BtnConfigType<br> | `'Record<string,BtnConfigType>'` | - | - |  
| selectionConfig | 全选快捷配置查看BtnConfigType | `'Record<string,BtnConfigType>'` | - | - |
| operationConfig | 菜单快捷配置查看BtnConfigType | `'Record<string,BtnConfigType>'` | - | - |
| '...' | 其他参数查看TableCustomAPI和更多使用样例 | `any` | - | - |
