###  ModalPropsType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| key | key<br> | `string` | 自动生成的可忽略 | - |
| type | 触发的业务位置<br> | `ClickType` | 自动生成的可忽略 | - |
| columns | 表单自定义columns配置参数、如果不配置默认取表格columns配置<br> | `any` | 弹窗表单 | - |
| formatColumns | 是否按照tabel传入格式化columns | `boolean` | - | - |
| edit | 是否为编辑模式<br> | `boolean` | false | - |
| modalType | 关联弹窗类型<br>查看ModalType属性说明 | `ModalType` | - | - |
| config | 内置组件的一些功能性配置<br>查看ModalPropsConfigType<br> | `ModalPropsConfigType` | false | - |


###  ModalPropsType - Form

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型Form时 | `'Form'` | - | - |
| config | 内置组件的一些功能性配置 | `FormCustomProps` | - | - |

###  ModalPropsType - Delete

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型Delete时 | `'Delete'` | - | - |
| config | 内置组件的一些功能性配置 | `DeleteCustomType` | - | - |

###  ModalPropsType - Download

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型Download时 | `'Download'` | - | - |
| `config.fileName` | 文件名称 | `string` | - | - |
| `config.searchParams` | 下载时携带搜索参数 | `boolean` | false | - |

###  ModalPropsType - PreviewHtml

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型PreviewHtml时 | `'PreviewHtml'` | - | - |
| config | 内置组件的一些功能性配置 | `PreviewHtmlModalCustomType` | - | - |

###  ModalPropsType - Import

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型Import时 | `'Import'` | - | - |
| config | 内置组件的一些功能性配置 | `ImportModalCustomType` \| <br>{<br>/*** 是否展示错误信息 *<br> @default true */<br>showError?: boolean;<br>} | - | - |

###  ModalPropsType - Confirm

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型Confirm时 | `'Confirm'` | - | - |
| config | 内置组件的一些功能性配置 | `ConfirmModalCustomType` | - | - |

###  ModalPropsType - Table

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型Table时 | `'Table'` | - | - |
| config | 内置组件的一些功能性配置 | `ModalPropsConfigType` | - | - |
| showFooter | 是否显示底部 | `boolean` | false | - |
| field | 取值的字段 | `string` | 'id' | - |

###  ModalPropsType - CustomModal

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| modalType | 类型Table时 | `'CustomModal'` | - | - |
| config | 内置组件的一些功能性配置 | `ModalPropsType` | - | - |
| render | 自定义内容 | `(initialValues)=>React.Node` | - | - |
