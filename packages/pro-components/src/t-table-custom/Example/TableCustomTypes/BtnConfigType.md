###  BtnConfigType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| key | 唯一值<br> | `string` | 自动生成的可忽略 | - |
| className | className<br> | `string` | 自动生成的可忽略 | - |
| text | 按钮名称<br> | `string` | 按钮 | - |
| icon | icon<br> | `React.ReactNode\|any` |  | - |
| type | 按钮类型<br> | `'default'\|'primary'\|'link'\|'text'\|'ghost'\|'dashed'` | primary | - |
| size | 按钮类型<br> | `'default'\|'large'\|'small'` | primary | - |
| style | 按钮样式 | `Record<string,any>` | - | - |
| danger | 按钮属性危险<br> | `boolean` | false | - |
| disabled | 是否禁用<br>可配置为boolean和函数<br> | `((...params:any[])=>boolean)\|boolean` | false | - |  
| auth | 按钮权限控制是否显示returnfalse隐藏true显示<br>按钮如果是row记录会返回记录值等参数<br>(btnConfig)=>boolean<br>(btnConfig,selectedRowKeys,onCleanSelected)=>boolean<br>(btnConfig,irecord)=>boolean<br> | `(()=>boolean)\|any` | true | - |
| onClick | 按钮点击回调<br>回调中会返回按钮参数，行数据等 | `((config:any)=>any)\|any` | - | - |
| modalConfig | 内置功能的配置<br>查看ModalPropsType | `ModalPropsType` | - | - |
| tooltip | 按钮旁提示帮助信息 | `BtnConfigTooltipType` | - | - |
