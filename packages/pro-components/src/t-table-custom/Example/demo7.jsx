import { TableCustom } from '@vh-admin/pro-components';
import { getProTable } from '../../example';
import React from 'react';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    search: false,
    valueType: 'indexBorder',
    hideInForm: true,
    width: 120,
  },
  {
    title: '状态',
    search: false,
    width: 120,
    dataIndex: 'status',
    valueType: 'select',
    formGroup: 2,
    formOrder: 0,
    businessStyle: {
      type: 'tag',
    },
    fieldProps: {
      options: [
        {
          label: '全部',
          value: null,
        },
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: '类型',
    search: false,
    width: 120,
    dataIndex: 'type',
    valueType: 'select',
    formGroup: 2,
    formOrder: 0,
    businessStyle: {
      type: 'colorText',
    },
    fieldProps: {
      options: [
        {
          label: '你好',
          value: 1,
        },
        {
          label: '啊啊',
          value: 2,
        },
        {
          label: '什么',
          value: 3,
        },
      ],
    },
  },
];

function Demo7() {
  return (
    <>
      <TableCustom
        search={false}
        request={getProTable}
        columns={columns}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          y: 300,
        }}
      />
    </>
  );
}

export default Demo7;
