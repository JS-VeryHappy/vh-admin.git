import { TableCustom } from '@vh-admin/pro-components';
import { getProTable } from '../../example';
import React from 'react';
import {
  headerTitleConfigArr,
  tableAlertOptionRenderConfigArr,
  operationConfigRenderConfigArr,
} from '../btnConfig';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    search: false,
    valueType: 'indexBorder',
    hideInForm: true,
    width: 120,
  },
];

delete headerTitleConfigArr.default;
delete tableAlertOptionRenderConfigArr.default;
delete operationConfigRenderConfigArr.default;

for (const key of Object.keys(headerTitleConfigArr)) {
  //@ts-ignore
  headerTitleConfigArr[key].onClick = () => {};
}
for (const key of Object.keys(tableAlertOptionRenderConfigArr)) {
  //@ts-ignore
  tableAlertOptionRenderConfigArr[key].onClick = () => {};
}
for (const key of Object.keys(operationConfigRenderConfigArr)) {
  //@ts-ignore
  operationConfigRenderConfigArr[key].onClick = () => {};
}

function Demo8() {
  return (
    <>
      <TableCustom
        search={false}
        request={getProTable}
        columns={columns}
        headerTitleConfig={headerTitleConfigArr}
        selectionConfig={tableAlertOptionRenderConfigArr}
        operationConfig={operationConfigRenderConfigArr}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          y: 300,
        }}
      />
    </>
  );
}

export default Demo8;
