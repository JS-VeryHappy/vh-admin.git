import { TableCustom } from '@vh-admin/pro-components';
import { getProTable } from '../../example';
import React from 'react';
import { Table } from 'antd';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    search: false,
    valueType: 'indexBorder',
    hideInForm: true,
    width: 200,
    fixed: true,
  },
  {
    title: '金额',
    search: false,
    width: 200,
    dataIndex: 'user_id',
  },
  {
    title: '姓名',
    search: false,
    width: 200,
    dataIndex: 'title',
  },
  {
    title: '类型',
    search: false,
    width: 200,
    dataIndex: 'type',
  },
  {
    title: '金额1',
    search: false,
    width: 200,
    dataIndex: 'user_id1',
  },
  {
    title: '姓名1',
    search: false,
    width: 200,
    dataIndex: 'title1',
  },
  {
    title: '类型1',
    search: false,
    width: 200,
    dataIndex: 'type1',
  },
  {
    title: '金额2',
    search: false,
    width: 200,
    dataIndex: 'user_id2',
  },
  {
    title: '姓名2',
    search: false,
    width: 200,
    dataIndex: 'title2',
  },
  {
    title: '类型2',
    search: false,
    width: 200,
    dataIndex: 'type2',
  },
];

function Demo10() {
  return (
    <>
      <TableCustom
        headerTitle="本地统计当前页"
        search={false}
        request={getProTable}
        columns={columns}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          y: 300,
        }}
        summaryType="local"
        summarylocalFields={['user_id', 'type']}
      />
      <TableCustom
        headerTitle="远程统计|固定栏"
        search={false}
        request={getProTable}
        columns={columns}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          y: 300,
        }}
        summaryType="remote"
        summaryRemoteField="summary"
        selectionConfig={{
          delete: ({ btnConfig, selectedRowKeys, onCleanSelected }) => {
            console.log(btnConfig, selectedRowKeys, onCleanSelected);
            message.success('delete');
          },
        }}
      />
    </>
  );
}

export default Demo10;
