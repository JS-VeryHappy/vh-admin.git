import { TableCustom } from '@vh-admin/pro-components';
import { getProTable, proTableDetails } from '../../example';
import React from 'react';

const formColumns = [
  {
    title: '标题',
    dataIndex: 'title',
    ellipsis: true,
    tip: '标题过长会自动收缩',
    fieldProps: {
      placeholder: '请输入账号',
      maxLength: 20,
    },
    formItemProps: {
      rules: [{ required: true, message: '请填写标题' }],
    },
  },
  {
    title: '描述',
    dataIndex: 'description',
    valueType: 'InputTooltipCustom',
    fieldProps: {
      tooltipTitle: '使用自定义表单组件',
      fieldProps: {
        style: {
          width: '100%',
        },
      },
    },
  },
];

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    search: false,
    valueType: 'indexBorder',
    hideInForm: true,
    width: 120,
  },

  {
    title: '小区',
    search: false,
    width: 120,
    dataIndex: 'type',
    businessStyle: {
      type: 'urllink',
      handelClick: (record) => {
        console.log('record: ', record);
      },
    },
  },
  {
    title: '图片',
    search: false,
    width: 120,
    dataIndex: 'avatar',
    businessStyle: {
      type: 'previewImage',
    },
  },
  {
    title: '文件',
    search: false,
    width: 120,
    dataIndex: 'file',
    businessStyle: {
      type: 'download',
    },
  },
  {
    title: '内置功能',
    search: false,
    width: 120,
    dataIndex: 'file',
    businessStyle: {
      type: 'within',
      btnConfig: {
        text: '查看',
        tooltip: {
          text: '可以使用表格内置的功能',
        },
        modalConfig: {
          edit: true, // 是否是编辑模式 如果是会给当前弹窗赋值默认值：值由config.request远程拉取或者row读取
          modalType: 'Form',
          columns: formColumns,
          config: {
            title: '查看表单',
            // 配置编辑时 远程请求数据动态赋值默认值、如果不配置则自动会从row中取数据
            request: proTableDetails,
            // 远程请求的参数
            params: {
              aa: 11,
            },
            // 赋值默认值前 数据的猴子
            initialValuesBefor: (data) => {
              console.log('===================');
              console.log(data);
              console.log('===================');
              return { ...data, aa: 111 };
            },
            // 不配置提交接口 触发onClick自行处理
            submitRequest: proTableDetails,
          },
        },
      },
    },
  },
];

function Demo9() {
  return (
    <>
      <TableCustom
        search={false}
        request={getProTable}
        columns={columns}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          y: 300,
        }}
      />
    </>
  );
}

export default Demo9;
