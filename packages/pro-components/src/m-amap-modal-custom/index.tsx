import ModalCustom from '../m-modal-custom';
import './index.less';
import type { AmapPropsType } from '../c-amap-custom';
import AmapCustom from '../c-amap-custom';
import React from 'react';
import type { ModalProps } from 'antd';

declare type AmapModalCustomType = {
  /**
   * 地图显示的Props 查看地图api
   * @description
   */
  amapProps?: AmapPropsType;
} & ModalProps;

/**
 * 业务确认删除弹窗
 * @param Props
 * @returns
 */
function AmapModalCustom(Props: AmapModalCustomType) {
  const { amapProps, onOk, onCancel, ...rest } = Props;

  const handleOk = async (e: any) => {
    if (onOk && typeof onOk === 'function') {
      await onOk(e);
    }
  };

  const handleCancel = (e: any) => {
    if (onCancel && typeof onCancel === 'function') {
      onCancel(e);
    }
  };

  return (
    <>
      <ModalCustom
        {...rest}
        title=""
        destroyOnClose={true}
        maskClosable={false}
        wrapClassName="amap-modal-custom"
        okType="danger"
        onOk={handleOk}
        onCancel={handleCancel}
        width="800px"
      >
        <AmapCustom {...amapProps} />
      </ModalCustom>
    </>
  );
}

export default AmapModalCustom;
