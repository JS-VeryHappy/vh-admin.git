---
title: 地图组件
toc: content
order: 5
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 通用
  path: /Exception
  order: 4
---

# 说明

> 1. [react-amap](https://elemefe.github.io/react-amap/articles/start)

```jsx
/**
 * title: 基础使用显示地图 显示定位
 */
import React from 'react';
import { AmapCustom } from '@vh-admin/pro-components';

function AmapCustomDemo1() {
  const markers = [
    {
      latitude: 30.665368,
      longitude: 104.060585,
    },
  ];
  return (
    <>
      <AmapCustom markers={markers} />
    </>
  );
}
export default AmapCustomDemo1;
```

```jsx
/**
 * title: 地图开启坐标拾取功能
 */
import React from 'react';
import { AmapCustom } from '@vh-admin/pro-components';

function AmapCustomDemo() {
  const value = {
    latitude: 30.665368,
    longitude: 104.060585,
  };
  const onChange = (cvalue) => {
    console.log(cvalue);
  };

  return (
    <>
      <AmapCustom selectMarker={true} value={value} onChange={onChange} />
    </>
  );
}
export default AmapCustomDemo;
```

## API

<embed src="./AmapPropsType.md"></embed>

