### AmapPropsType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| zoom | 地图显示级别<br>默认14 | `number` | - | - |
| width | 宽度<br> | `string` | '100%' | - |
| height | 高度<br> | `string` | '400px%' | - |
| center | 地图的中心点<br>例如：longitude,latitude | `[]` | - | - |
| tools | 需要开启的事件值高德工具<br>'marker'、'polyline'、'polygon'、'circle'、'rectangle'、'rule'、'close'.... | `string[]` | - | - |
| selectMarker | 是否开启选择点 | `boolean` | - | - |
| value | 开启selectMarker默认值 | `AmapItudeType` | - | - |
| onChange | 开启selectMarker切换回调 | `(value:AmapItudeType)=>void` | - | - |
| markers | 显示的定位点<br> | `AmapItudeType[]` | - | - |

### AmapItudeType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| longitude(必须) | 经度 | `string` | - | - |
| latitude(必须) | 纬度 | `string` | - | - |
