//@ts-ignore
import { CTypeitCustom } from '@vh-admin/pro-components';
import { waitTime } from '@vh-admin/pro-utils';
import type { FC } from 'react';
import { useCallback, useEffect, useRef } from 'react';
import type { Container } from 'react-dom';
import { render } from 'react-dom';
const Index: FC = () => {
  const domRef = useRef<Container | any>(<></>);

  const TypertDom = useCallback((val: string): any => {
    return (
      <CTypeitCustom
        getBeforeInit={(instance: any) => {
          instance.type(val);
          return instance;
        }}
      />
    );
  }, []);

  useEffect(() => {
    (async () => {
      await waitTime(3000);
      if (domRef.current) {
        render(TypertDom('这是一段异步调用文案。'), domRef.current);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div ref={domRef}>加载中...</div>
    </>
  );
};

export default Index;
