//@ts-ignore
import { CTypeitCustom } from '@vh-admin/pro-components';
import type { FC } from 'react';
import { waitTime } from '@vh-admin/pro-utils';

const Index: FC = () => {
  const promiseSum = () => {
    return new Promise(async (resolve) => {
      await waitTime(3000);
      resolve('');
    });
  };

  return (
    <>
      <CTypeitCustom
        style={{ fontFamily: 'cursive' }}
        options={{
          cursor: false,
          loop: true,
        }}
        getBeforeInit={(instance: any) => {
          instance
            .type('正在变身中...')
            .exec(async () => await promiseSum())
            .delete(null, { instant: true })
            .type('<strong style="color: #f95d5d;font-size:80px">只因你太美</strong>', {
              instant: true,
            })
            .pause(3000);
          return instance;
        }}
      />
    </>
  );
};

export default Index;
