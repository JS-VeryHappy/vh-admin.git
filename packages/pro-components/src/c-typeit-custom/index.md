---
title: 打字机
toc: content
order: 8
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 通用
  path: /Exception
  order: 4
---


### 基础用法

<code src="./Example/base.tsx"> 基础用法</code>

### 动态修改

<code src="./Example/base1.tsx">动态修改</code>

### 异步插入

<code src="./Example/base2.tsx">异步插入</code>

### 异步等待

<code src="./Example/base3.tsx">异步等待</code>



## API

<embed src="./CTypeitCustomType.md"></embed>

