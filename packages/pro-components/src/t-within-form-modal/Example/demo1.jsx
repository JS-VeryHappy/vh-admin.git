import { TableCustom } from '@vh-admin/pro-components';
import { getProTable, proTableAddRow, proTableDetails } from '../../example';
import { message } from 'antd';
import { requestDebounce } from '@vh-admin/pro-utils';
import React from 'react';
import { useRef } from 'react';

const debounceProTableAddRow = requestDebounce(proTableAddRow, 500);

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    valueType: 'indexBorder',
    hideInForm: true,
    width: 48,
  },
  {
    title: '标题',
    dataIndex: 'title',
    formGroup: 1,
    formOrder: 1,
  },
  {
    title: '描述',
    dataIndex: 'description',
    copyable: true,
    formGroup: 1,
    formOrder: 1,
  },
  {
    title: '状态',
    dataIndex: 'status',
    valueType: 'select',
    formGroup: 2,
    initialValue: 1,
    fieldProps: {
      options: [
        {
          label: '全部',
          value: null,
        },
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: '时间',
    dataIndex: 'datetime',
    valueType: 'dateTime',
    formGroup: 2,
    sorter: (a, b) => a.datetime - b.datetime,
  },
  {
    title: '状态',
    dataIndex: '["field1","field2"]',
    valueType: 'SelectInputCustom',
    initialValue: [1, 2],
    fieldProps: {
      options: [
        {
          label: '全部',
          value: null,
        },
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: '规则参数',
    dataIndex: 'EditableTableCustom',
    valueType: 'EditableTableCustom',
    width: '100%',
    hideInTable: true,
    fieldProps: {
      columns: [
        {
          title: '阶段',
          dataIndex: 'a',
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '业务11',
          dataIndex: 'aa',
          valueType: 'SelectMultipleCustom',
          editable: false,
          fieldProps: {
            options: [
              { value: 1, label: '%单套' },
              { value: 3, label: '元整体' },
            ],
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '业务22',
          dataIndex: '["field1","field2"]',
          valueType: 'InputNumberSelectCustom',
          width: 200,
          fieldProps: {
            options: [
              { value: 1, label: '%单套' },
              { value: 3, label: '元整体' },
            ],
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '阶段比例%',
          dataIndex: 'b',
          valueType: 'digit',
          fieldProps: {
            min: 0,
            max: 100,
            addonAfter: '%',
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '导出表标题',
          dataIndex: 'd',
          editable: false,
        },
      ],
      maxHeight: 300,
    },
    initialValue: [
      { id: 1, a: undefined, b: 2, aa: [1] },
      { id: 2, a: 1, b: 2, aa: [2] },
    ],
  },
];

const formColumns = [
  {
    title: '分组',
    valueType: 'group',
    columns: [
      {
        title: '标题',
        width: 'md',
        dataIndex: 'groupTitle',
        formItemProps: {
          rules: [
            {
              required: true,
              message: '此项为必填项',
            },
          ],
        },
      },
    ],
  },
  {
    title: '列表',
    valueType: 'formList',
    dataIndex: 'list',
    initialValue: [{ state: 'all', title: '标题' }],
    columns: [
      {
        valueType: 'group',
        columns: [
          {
            title: '状态',
            dataIndex: 'state',
            valueType: 'select',
            width: 'xs',
            initialValue: 2,
            fieldProps: {
              options: [
                {
                  label: '全部',
                  value: null,
                },
                {
                  label: '未解决',
                  value: 1,
                },
                {
                  label: '已解决',
                  value: 2,
                },
                {
                  label: '解决中',
                  value: 3,
                },
              ],
            },
          },
          {
            title: '标题',
            dataIndex: 'title',
            formItemProps: {
              rules: [
                {
                  required: true,
                  message: '此项为必填项',
                },
              ],
            },
            width: 'm',
          },
        ],
      },
    ],
  },
  {
    title: 'FormSet',
    valueType: 'formSet',
    dataIndex: 'formSet',
    columns: [
      {
        title: '状态',
        dataIndex: 'groupState',
        valueType: 'select',
        width: 'xs',
        initialValue: 2,
        fieldProps: {
          options: [
            {
              label: '全部',
              value: null,
            },
            {
              label: '未解决',
              value: 1,
            },
            {
              label: '已解决',
              value: 2,
            },
            {
              label: '解决中',
              value: 3,
            },
          ],
        },
      },
      {
        title: '标题',
        dataIndex: 'groupTitle',
        tip: '标题过长会自动收缩',
        formItemProps: {
          rules: [
            {
              required: true,
              message: '此项为必填项',
            },
          ],
        },
        width: 'm',
      },
    ],
  },
  {
    title: '创建时间',
    dataIndex: 'created_at',
    valueType: 'dateRange',
    transform: (value) => {
      return {
        startTime: value && value[0] ? value[0] : undefined,
        endTime: value && value[1] ? value[1] : undefined,
      };
    },
  },
];
function Demo1() {
  const formRef = useRef();

  return (
    <>
      <TableCustom
        request={getProTable}
        columns={columns}
        search={false}
        pagination={{
          pageSize: 5,
        }}
        headerTitleConfig={{
          create: {
            modalConfig: {
              modalType: 'Form',
              config: {
                width: 'double',
                title: '新增表单',
                formRef: formRef,
                // 初始化数据。数据钩子
                initialValuesBefor: (data) => {
                  return { ...data, title: 111 };
                },
                // 提交数据前。数据钩子
                submitValuesBefor: (data) => {
                  console.log('===================');
                  console.log(formRef);
                  console.log('===================');
                  return { ...data, name: '小周周' };
                },
                // 提交数据的接口配置
                submitRequest: proTableAddRow,
                // // 完成时回调
                // submitOnDone: ({ status }: SubmitOnDoneType) => {
                //   if (status === 'success') {
                //     message.success('新增成功');
                //   } else {
                //     message.success('失败啦');
                //   }
                // },
              },
            },
          },
        }}
        operationConfig={{
          edit: {
            // 不配置 submitRequest 提交数据的接口 会调用onClick自己处理 如果submitRequest会内置功能自动完成请求业务逻辑
            onClick: async (values, tableRef) => {
              const res = await debounceProTableAddRow(values);
              // 刷新表格
              if (tableRef && tableRef.current && tableRef.current.reload) {
                tableRef.current.reload();
              }
              console.log('====================================');
              console.log(res);
              console.log('====================================');
              message.success('返回false不会关闭弹窗');
              return false;
            },
            modalConfig: {
              edit: true, // 是否是编辑模式 如果是会给当前弹窗赋值默认值：值由config.request远程拉取或者row读取
              modalType: 'Form',
              columns: formColumns,
              formatColumns: true,
              config: {
                title: '编辑表单',
                // 配置编辑时 远程请求数据动态赋值默认值、如果不配置则自动会从row中取数据
                request: proTableDetails,
                // 远程请求的参数
                params: {
                  aa: 11,
                },
                // 赋值默认值前 数据的猴子
                initialValuesBefor: (data) => {
                  return { ...data, aa: 111 };
                },
                // 不配置提交接口 触发onClick自行处理
                // submitRequest: debounceProTableAddRow,
              },
            },
          },
          edit1: {
            text: '编辑1',
            modalConfig: {
              edit: true, // 是否是编辑模式 如果是会给当前弹窗赋值默认值：值由config.request远程拉取或者row读取
              modalType: 'Form',
              config: {
                width: 'double',
                title: '编辑表单1',
                // 赋值默认值前 数据的猴子
                initialValuesBefor: (data) => {
                  return {
                    ...data,
                    aa: 111,
                    field1: 2,
                    field2: 'asdasd',
                    EditableTableCustom: [
                      { id: 1, a: undefined, b: 2, field1: 3, field2: 'asd', aa: [1] },
                      { id: 2, a: 1, b: 2, field1: 1, field2: 2, aa: [3] },
                    ],
                  };
                },
                // 提交数据前。数据钩子
                submitValuesBefor: (data) => {
                  return { ...data, name: '小周周' };
                },
                // 不配置提交接口 触发onClick自行处理
                submitRequest: (submitValue) => {
                  console.log('===================');
                  console.log(submitValue);
                  console.log('===================');
                },
                // 完成时回调 不配置会自动根据title提示
                // submitOnDone: ({ status }: SubmitOnDoneType) => {
                //   if (status === 'success') {
                //     message.success('新增成功');
                //   } else {
                //     message.success('失败啦');
                //   }
                // },
              },
            },
          },
          details: {
            text: '详情',
            modalConfig: {
              edit: true,
              modalType: 'Form',
              config: {
                // 动态的弹窗标题
                title: (value) => {
                  return `${value.title}详情`;
                },
                readonly: true,
                submitter: false,
              },
            },
          },
        }}
      />
    </>
  );
}

export default Demo1;
