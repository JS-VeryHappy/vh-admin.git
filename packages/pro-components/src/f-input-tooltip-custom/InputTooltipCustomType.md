### InputTooltipCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| onClick | 按钮点击后触发事件<br>@paramvalue | `(value:any)=>any\|undefined` | - | - |
| tooltipText | tooltip显示文字<br> | `string\|undefined` | NeedHelp? | - |
| tooltipTitle | tooltip提示文字<br> | `string\|undefined` | 我是提示 | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
