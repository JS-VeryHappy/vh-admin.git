---
title: InputTooltip
toc: content
order: 11
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. InputTooltip

```jsx
import React from 'react';
import { InputTooltipCustom } from '@vh-admin/pro-components';

function InputTooltipCustomDemo() {
  const fieldProps = {
    style: {
      width: '200px',
    },
  };
  return <InputTooltipCustom tooltipText="提示" tooltipTitle="提示文字" fieldProps={fieldProps} />;
}
export default InputTooltipCustomDemo;
```

## API

<embed src="./InputTooltipCustomType.md"></embed>


