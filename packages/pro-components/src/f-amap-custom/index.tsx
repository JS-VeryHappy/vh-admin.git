import AmapCustom from '../c-amap-custom';
import type { CustomType } from '../f-form-custom/types';
import type { AmapPropsType } from '../c-amap-custom';
import React from 'react';

export declare type AmapFormCustomType = {
  fieldProps?:
    | {
        amapProps?: AmapPropsType;
      }
    | any;
} & CustomType;

function AmapFormCustom(Props: AmapFormCustomType) {
  const { style, className, customMode, readonly, value, onChange, fieldProps = {} } = Props;
  const { amapProps = {} }: any = fieldProps;

  return (
    <div className={`${className} ${customMode}`} style={style}>
      {readonly ? (
        <AmapCustom value={value} />
      ) : (
        <AmapCustom selectMarker={true} value={value} onChange={onChange} {...amapProps} />
      )}
    </div>
  );
}

export default AmapFormCustom;
