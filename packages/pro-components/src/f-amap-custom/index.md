---
title: 地图
toc: content
order: 8
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. 地图组件[AmapModalCustom](/pro-components/c-amap-custom)

```jsx
/**
 * title: 表单地图子组件
 */
import React from 'react';
import { Button } from 'antd';
import { AmapFormCustom } from '@vh-admin/pro-components';

import { message } from 'antd';

function AmapFormCustomDemo() {
  const fieldProps = {
    onChange: (value) => {
      message.success(JSON.stringify(value));
    },
  };
  return (
    <>
      <AmapFormCustom {...fieldProps} />
    </>
  );
}
export default AmapFormCustomDemo;
```

## API

<embed src="./AmapFormCustomType.md"></embed>


