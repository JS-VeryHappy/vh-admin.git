import type { ModalProps } from 'antd';
import ModalCustom from '../m-modal-custom';
import { Space, Image, Input } from 'antd';
import { useState, useEffect } from 'react';
import { uuid } from '@vh-admin/pro-utils';
import React from 'react';

const codeToken = uuid();

declare type CaptchaModalCustomType = {
  /** 点击确定回调 */
  onOk?: (data: any, e: React.MouseEvent<HTMLElement>) => void;
  /**
   * 请求验证码的方法类型
   * @default post
   */
  method?: string;
  /**
   * 请求验证码的方法
   */
  captcha: any;
} & ModalProps;

function CaptchaModalCustom(Props: CaptchaModalCustomType) {
  const { onOk, onCancel, captcha, method = 'post', visible, ...rest } = Props;
  const [url, setUrl] = useState<any>(null);
  const [input, setInput] = useState<any>(null);

  const handleOk = async (e: any) => {
    if (onOk && typeof onOk === 'function') {
      try {
        const res = await onOk({ code_token: codeToken, code: input }, e);
        // setInput(null);
        return res;
      } catch (error: any) {
        throw new Error(error);
      }
    }
  };

  const handleCancel = (e: any) => {
    if (onCancel && typeof onCancel === 'function') {
      setInput(null);
      onCancel(e);
    }
  };

  const loadUrl = () => {
    if (typeof captcha === 'function') {
      if (method === 'post') {
        captcha({ code_token: codeToken }).then((res: any) => {
          setUrl(res.data.src);
        });
      } else {
        setUrl(captcha(codeToken));
      }
    }
  };

  const onInput = (e: any) => {
    setInput(e.target.value);
  };

  useEffect(() => {
    if (visible) {
      loadUrl();
    }
  }, [visible]);

  if (!url) {
    return null;
  }

  return (
    <>
      <ModalCustom
        {...rest}
        title="图形验证码"
        destroyOnClose={true}
        maskClosable={false}
        onOk={handleOk}
        onCancel={handleCancel}
        visible={visible}
      >
        <div style={{ width: '100%', textAlign: 'center' }}>
          <Space direction="horizontal" align="center">
            <Image style={{ cursor: 'pointer' }} src={url} preview={false} onClick={loadUrl} />
            <Input
              maxLength={5}
              style={{ width: 100, height: 40 }}
              onChange={onInput}
              value={input}
              onPressEnter={handleOk}
            />
          </Space>
        </div>
      </ModalCustom>
    </>
  );
}

export default CaptchaModalCustom;
