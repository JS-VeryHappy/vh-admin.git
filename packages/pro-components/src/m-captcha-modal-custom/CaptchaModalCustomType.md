### CaptchaModalCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| onOk |点击确定回调 | `(data:any,e:React.MouseEvent<HTMLElement>)=>void` | - | - |
| method | 请求验证码的方法类型<br> | `string` | post | - |
| captcha(必须) | 请求验证码的方法 | `any` | - | - |
| `...ModalProps` | antd [ModalProps](https://ant.design/components/modal-cn) | `ModalProps` | - | - |
