---
title: 弹窗图形验证码
toc: content
order: 3
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 弹窗组件
  path: /modal-custom
  order: 3
---

## 基础组件具体参数说明参考：[基础弹窗](/pro-components/m-modal-custom)

```jsx
import React, { useState } from 'react';
import { CaptchaModalCustom } from '@vh-admin/pro-components';
import { Button } from 'antd';
import { waitTime } from '@vh-admin/pro-utils';

function CaptchaModalCustomDemo() {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = async (data) => {
    try {
      console.log(data);
      await waitTime(2000);
      setIsModalVisible(false);
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const captcha = async () => {
    return { data: { src: 'xxxxx' } };
  };
  return (
    <>
      <Button onClick={showModal}>验证码</Button>
      <CaptchaModalCustom
        visible={isModalVisible}
        captcha={captcha}
        onOk={handleOk}
        onCancel={handleCancel}
      />
    </>
  );
}
export default CaptchaModalCustomDemo;
```

## API

<embed src="./CaptchaModalCustomType.md"></embed>

