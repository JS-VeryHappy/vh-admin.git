---
title: 内置-直接点击
order: 3
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表格组件
  path: /table-custom
  order: 2
---

- 一.配合快捷菜单按钮使用内置功能
  - 1.  方便快捷的配置出增、改业务功能逻辑，值需要简单的配置就可以完成、一般不能单独使用，必须配合表格快捷菜单。
- 二. 按钮类型:
  - **_modalType="OnClick"_**
- 三.submitRequest 如果是 string 则创建一个 a 标签 ，否则直接请求二进制数据，前端做下载

## 完整使用例子

<code src="./Example/demo1.jsx" 
      title="完整使用例子"
      description="下载" >完整使用例子</code>

## API 

<embed src="../t-table-custom/Example/TableCustomTypes/ModalPropsType.md"></embed>

<embed src="../t-table-custom/Example/TableCustomTypes/ModalPropsConfigType.md"></embed>

<embed src="../t-table-custom/Example/TableCustomTypes/SubmitOnDoneType.md"></embed>
