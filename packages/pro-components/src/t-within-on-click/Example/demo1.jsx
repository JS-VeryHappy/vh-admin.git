import React from 'react';
import { TableCustom } from '@vh-admin/pro-components';
import { getProTable, proTableDownload } from '../../example';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    valueType: 'indexBorder',
    hideInForm: true,
    width: 48,
  },
  {
    title: '标题',
    dataIndex: 'title',
  },
  {
    title: '描述',
    dataIndex: 'description',
    copyable: true,
  },
  {
    title: '状态',
    dataIndex: 'status',
    valueType: 'select',
    fieldProps: {
      options: [
        {
          label: '全部',
          value: null,
        },
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: '时间',
    dataIndex: 'datetime',
    valueType: 'dateTime',
    sorter: (a, b) => a.datetime - b.datetime,
  },
];

function Demo1() {
  return (
    <>
      <TableCustom
        request={getProTable}
        columns={columns}
        search={false}
        pagination={{
          pageSize: 5,
        }}
        operationConfig={{
          download: {
            modalConfig: {
              modalType: 'OnClick',
              config: {
                initialValuesBefor: (data) => {
                  return { ...data, title: 111 };
                },
                submitValuesBefor: (data) => {
                  return { id: 'db.sql', ...data };
                },
                submitRequest: proTableDownload, // 配置函数调用函数执行返回二进制 自动下载
              },
              edit: true,
            },
          },
        }}
      />
    </>
  );
}

export default Demo1;
