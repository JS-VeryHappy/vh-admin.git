### BraftEditorCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| request | 自定义请求地址api方法默认使用公共用上传方法uploadFile | `any` | - | - |
| format | 限制上传文件的后缀名<br> | `[]` | [] | - |
| size | 限制上传图片的大小单位kb<br> | `number` | 2000 | - |
| braftEditorProps | 透传给BraftEditor参插件的属性 | `any` | - | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
