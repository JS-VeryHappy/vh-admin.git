---
title: 富文本编辑器
toc: content
order: 7
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. 插架[BraftEditor](https://braft.margox.cn/demos/antd-form)

```jsx
/**
 * title: 富文本
 */
import React from 'react';
import { Button } from 'antd';
import { BraftEditorCustom } from '@vh-admin/pro-components';

const fieldProps = {
  format: ['jpg', 'jpeg', 'png'],
  imgCrop: true,
  imgCropFieldProps: {
    aspect: 2,
  },
  listType: 'picture-card',
  maxCount: 1,
  size: 2000,
};
export default () => <BraftEditorCustom {...fieldProps} />;
```

## API

<embed src="./BraftEditorCustomType.md"></embed>


