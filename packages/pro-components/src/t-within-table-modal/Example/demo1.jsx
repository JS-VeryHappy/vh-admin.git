import { TableCustom } from '@vh-admin/pro-components';
import { getProTable, proTableAddRow } from '../../example';
import { Table } from 'antd';
import { requestDebounce } from '@vh-admin/pro-utils';
import React from 'react';

const debounceProTableAddRow = requestDebounce(proTableAddRow, 500);

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    valueType: 'indexBorder',
    hideInForm: true,
    width: 48,
  },
  {
    title: '标题',
    dataIndex: 'title',
    formGroup: 1,
    formOrder: 1,
  },
  {
    title: '描述',
    dataIndex: 'description',
    copyable: true,
    formGroup: 1,
    formOrder: 1,
  },
  {
    title: '状态',
    dataIndex: 'status',
    valueType: 'select',
    formGroup: 2,
    fieldProps: {
      options: [
        {
          label: '全部',
          value: null,
        },
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: '时间',
    dataIndex: 'datetime',
    valueType: 'dateTime',
    formGroup: 2,
    sorter: (a, b) => a.datetime - b.datetime,
  },
];

function Demo1() {
  return (
    <>
      <TableCustom
        request={getProTable}
        columns={columns}
        search={false}
        pagination={{
          pageSize: 5,
        }}
        operationConfig={{
          eidt: {
            text: '编辑',
            modalConfig: {
              edit: true,
              modalType: 'Table',
              config: {
                title: '列表明细',
                tableHeaderRender: (initialValues) => {
                  console.log('===================');
                  console.log(initialValues);
                  console.log('===================');
                  return <>tableHeaderRendertableHeaderRendertableHeaderRender</>;
                },
                tableProps: {
                  request: getProTable,
                  columns: columns,
                  //处理请求参数
                  requestBefor: (params) => {
                    console.log(params);
                  },
                  search: false,
                  pagination: {
                    pageSize: 5,
                  },
                  operationConfig: {
                    edit1: {
                      text: '编辑1',
                      modalConfig: {
                        edit: true, // 是否是编辑模式 如果是会给当前弹窗赋值默认值：值由config.request远程拉取或者row读取
                        modalType: 'Form',
                        config: {
                          title: '编辑表单1',
                          // 赋值默认值前 数据的猴子
                          initialValuesBefor: (data) => {
                            return { ...data, aa: 111 };
                          },
                          // 提交数据前。数据钩子
                          submitValuesBefor: (data) => {
                            return { ...data, name: '小周周' };
                          },
                          // 不配置提交接口 触发onClick自行处理
                          submitRequest: proTableAddRow,
                          // 完成时回调 不配置会自动根据title提示
                          // submitOnDone: ({ status }: SubmitOnDoneType) => {
                          //   if (status === 'success') {
                          //     message.success('新增成功');
                          //   } else {
                          //     message.success('失败啦');
                          //   }
                          // },
                        },
                      },
                    },
                  },
                },
                tableBottomRender: (initialValues) => {
                  console.log('===================');
                  console.log(initialValues);
                  console.log('===================');
                  return <>tableBottomRendertableBottomRender</>;
                },
              },
            },
          },
          form: {
            text: '选择',
            modalConfig: {
              edit: true,
              modalType: 'Table',
              config: {
                title: '列表选择',
                showFooter: true,
                field: 'age1',
                submitRequest: async (keys) => {
                  console.log('===================');
                  console.log(keys);
                  console.log('===================');
                },
                tableProps: {
                  request: getProTable,
                  columns: columns,
                  //处理请求参数
                  requestBefor: (params) => {
                    console.log(params);
                  },
                  pagination: {
                    pageSize: 5,
                  },
                  rowSelection: {
                    // 注释该行则默认不显示下拉选项
                    selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
                    preserveSelectedRowKeys: true,
                    // type: 'radio',
                  },
                },
              },
            },
          },
        }}
        headerTitleConfig={{
          create: {
            modalConfig: {
              modalType: 'Table',
              config: {
                title: '详情明细',
                tableProps: {
                  request: getProTable,
                  columns: columns,
                  search: false,
                  pagination: {
                    pageSize: 5,
                  },
                },
              },
            },
          },
        }}
      />
    </>
  );
}

export default Demo1;
