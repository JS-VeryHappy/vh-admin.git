import { useState, useEffect, useRef } from 'react';
import type { ModalRenderPropsType, TableCustomTypes, TableRefType } from '../t-table-custom/types';
import React from 'react';
import TableCustom from '../t-table-custom/index';
import ModalCustom from '../m-modal-custom';
import { message } from 'antd';

export declare type WithinTableModalType = ModalRenderPropsType & {
  /**
   * 是否显示底部
   * @default false
   */
  showFooter?: boolean;
  /**
   * 取值字段
   */
  field?: string;
  /**
   * 表格内置弹窗 表格TableCustom的props
   */
  tableProps: TableCustomTypes<any>;
};
/**
 * 内置功能表单
 */
function Table(props: ModalRenderPropsType) {
  // 存放表单的ref
  const { modelchildName, closeModal, btnConfig, clickConfig, tableFormRef } = props;
  // 内部显示状态
  const [visible, setVisible] = useState<boolean>(true);
  // 解构按钮配置的弹窗配置
  const { config, edit = false } = btnConfig.modalConfig || {};
  // 存放一份进入就保存的表单配置 不会因为回调columnBefor 改变变化
  // 存放进入就计算好的表单参数配置
  const [newConfig, setNewConfig] = useState<any>(null);
  // 表单默认赋值
  const [formInitialValues, setFormInitialValues] = useState<any>(null);
  // 表单配置参数
  const {
    initialValuesBefor,
    tableProps,
    tableHeaderRender,
    tableBottomRender,
    submitRequest,
    submitValuesBefor,
    submitOnDone,
    field = 'id',
    params,
    showFooter = false,
    ...configRest
  } = config;
  const tableRef = useRef<TableRefType>();
  const selectedRowKeysRef = useRef<any>([]);
  const [rowSelectionConfig, setRowSelectionConfig] = useState<any>(undefined);

  useEffect(() => {
    if (visible) {
      let initialValues: any = {};
      // 如果显示 并且 开启编辑模式
      if (edit) {
        initialValues = { ...clickConfig.irecord };

        // 如果动态标题
        if (typeof configRest.title === 'function') {
          configRest.title = configRest.title(initialValues);
        }
        if (typeof configRest.title === 'function') {
          configRest.title = configRest.title(initialValues);
        }
      }

      // 如果配置了展示请初始化数据的钩子
      if (initialValuesBefor) {
        initialValues = initialValuesBefor(initialValues);
      }
      setFormInitialValues(initialValues);

      const defaultConfig = {
        layoutType: 'ModalForm',
        title: '弹窗表单',
        // footer: null,
        onOk: async () => {
          // 遍历处理默认数据
          let submitValue: any = { [field]: selectedRowKeysRef.current };
          if (edit) {
            // 如果是编辑默认带上id
            submitValue = { ...clickConfig.irecord, ...submitValue };
          }
          // 数据提交前的钩子函数
          if (submitValuesBefor) {
            submitValue = submitValuesBefor(
              submitValue,
              null,
              tableRef,
              tableFormRef,
              clickConfig.irecord || {},
            );
          }
          // 如果配置了自动请求
          if (submitRequest) {
            try {
              const result = await submitRequest(
                submitValue,
                null,
                tableRef,
                tableFormRef,
                clickConfig.irecord || {},
              );

              // 如果设置请求回调
              if (submitOnDone) {
                submitOnDone({
                  status: 'success',
                  result,
                  params: submitValue,
                  tableRef,
                });
              } else {
                message.success(config.title + '成功');
              }
              setVisible(false);

              if (tableRef && tableRef.current && tableRef.current.reload) {
                tableRef.current.reload();
              }
              // //如果有开启select多选 清空
              // if (tableProps.selectionConfig && tableRef.current.clearSelected) {
              //   tableRef.current.clearSelected();
              // }
            } catch (error) {
              if (submitOnDone) {
                submitOnDone({
                  status: 'error',
                  result: error,
                  params: submitValue,
                  tableRef,
                });
              }
            }
          }

          try {
            await btnConfig.onClick(submitValue, tableRef);
            setVisible(false);
          } catch (error) {}
        },
        onCancel: () => {
          setVisible(false);
        },
        afterClose: () => {
          if (closeModal) {
            closeModal();
          }
        },
        width: 'double',
      };

      setNewConfig({ ...defaultConfig, ...configRest });

      // 选择
      let rowSelection = undefined;
      if (tableProps.rowSelection) {
        rowSelection = tableProps.rowSelection;
        rowSelection.onChange = (rowKeys: any) => {
          selectedRowKeysRef.current = rowKeys;
        };
        if (edit) {
          if (initialValues && initialValues[field]) {
            let rowKeys: any = [];

            if (initialValues[field] instanceof Array) {
              rowKeys = initialValues[field];
            } else {
              rowKeys = rowKeys.push(initialValues[field]);
            }
            rowSelection.defaultSelectedRowKeys = rowKeys;
            selectedRowKeysRef.current = rowKeys;
          }
        }
        setRowSelectionConfig(rowSelection);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!newConfig) {
    return null;
  }
  if (!showFooter) {
    newConfig.footer = null;
  }

  return (
    <ModalCustom key={modelchildName} className={modelchildName} visible={visible} {...newConfig}>
      {tableHeaderRender && tableHeaderRender(formInitialValues)}
      <TableCustom<any>
        actionRef={tableRef}
        params={
          tableProps.params ? { ...formInitialValues, ...tableProps.params } : formInitialValues
        }
        options={{
          fullScreen: false,
          reload: false,
          setting: false,
          density: false,
          search: false,
        }}
        rowSelection={rowSelectionConfig}
        {...tableProps}
      />
      {tableBottomRender && tableBottomRender(formInitialValues)}
    </ModalCustom>
  );
}

export default Table;
