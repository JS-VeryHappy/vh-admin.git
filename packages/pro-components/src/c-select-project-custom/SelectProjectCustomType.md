### SelectProjectCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| treeData(必须) | 选择的三级树型数据 | `[]` | - | - |
| value | 选中值 | `string\|number` | - | - |
| onChange | 切换触发回调 | `(value:any)=>void` | - | - |
| field | 配置递归数据的取值字段 | `fieldType` | - | - |
| filter | 是否开启筛选<br> | `boolean` | true | - |
| filterColumns | 筛选的表单配置<br>不设置则使用默认的一套<br>tree只能识别三级情况并且表单的dataindex必须有select1，select2 | `[]` | - | - |
| cacheLastRecord | 是否缓存上传选择记录<br> | `boolean` | false | - |

### FieldType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| label(必须) | 显示取值字段名<br> | `string\|number` | label | - |
| value(必须) | 值取值字段名<br> | `string\|number` | value | - |
| children(必须) | 子集取值字段名<br> | `string` | children | - |
| parentId(必须) | 父级id取值字段名<br> | `string` | parentId | - |
