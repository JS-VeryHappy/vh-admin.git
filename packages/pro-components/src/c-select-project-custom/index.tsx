import React, { useState, useRef } from 'react';
import { Typography, Button, Space } from 'antd';
import ModalCustom from '../m-modal-custom';
import FormCustom from '../f-form-custom';
import { loopTreeDataAllNode, recursionTreeData, recursionTreeTop } from '@vh-admin/pro-utils';
import { useDeepCompareEffect } from 'ahooks';
import { Tag } from 'antd';
import { Empty } from 'antd';
import ProCard from '@ant-design/pro-card';
import './index.less';
import { useMount } from 'ahooks';

const { Title } = Typography;

export declare type FieldType = {
  /**
   * 显示取值字段名
   * @default label
   */
  label: string | number;
  /**
   * 值取值字段名
   * @default value
   */
  value: string | number;
  /**
   * 子集取值字段名
   * @default children
   */
  children: string;
  /**
   * 父级id取值字段名
   * @default parentId
   */
  parentId: string;
};

export declare type SelectProjectCustomType = {
  /**
   * 选择的三级树型数据
   */
  treeData: [];
  /**
   * 选中值
   */
  value?: string | number;
  /**
   * 切换触发回调
   */
  onChange?: (value: any) => void;
  /**
   * 配置递归数据的取值字段
   */
  field?: FieldType;
  /**
   * 是否开启筛选
   * @default true
   */
  filter?: boolean;
  /**
   * 筛选的表单配置
   * 不设置则使用默认的一套
   * tree只能识别三级情况 并且表单的 dataindex 必须有 select1，select2
   */
  filterColumns?: [];
  /**
   * 是否缓存上传选择记录
   * @default false
   */
  cacheLastRecord?: boolean;
};

function SelectProjectCustom(Props: SelectProjectCustomType) {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const formRef = useRef<any>();
  const [treeLabels, setTreeLabels] = useState<[]>([]);
  const [select, setSelect] = useState<any>(null);
  const [formValues, setFormValues] = useState<any>({});

  const {
    treeData,
    value,
    onChange,
    field = { label: 'label', value: 'value', children: 'children', parentId: 'parentId' },
    filter = true,
    filterColumns,
    cacheLastRecord = false,
  } = Props;

  useMount(() => {
    if (cacheLastRecord) {
      setTimeout(() => {
        const v = localStorage.getItem('SelectProjectCustom');
        setSelect(v);
        if (typeof onChange === 'function') {
          onChange(v);
        }
      }, 100);
    }
  });

  useDeepCompareEffect(() => {
    if (value) {
      setSelect(value);
    }
  }, [value]);

  const showModal = () => {
    setIsModalVisible(true);

    if (select) {
      const has = recursionTreeTop(treeData, select, field);
      let select1: any = null;
      if (has) {
        select1 = has[field.value];
        if (select1) {
          setTreeLabels(has[field.children]);

          setFormValues({
            select1,
          });

          setTimeout(() => {
            formRef.current?.setFieldsValue({
              select1,
            });
          }, 100);
        }
      }
    }
  };

  const handleCancel = () => {
    setTimeout(() => {
      setIsModalVisible(false);
    }, 100);
    setFormValues({});
    formRef.current?.resetFields();
  };

  const onValuesChange = (values: any) => {
    if (values.hasOwnProperty('select1')) {
      formRef.current?.setFieldsValue({
        select2: null,
      });
    }
  };

  const onFinish = async (cvalue: any) => {
    const tree = recursionTreeData(treeData, cvalue.select1, field);
    setFormValues(cvalue);
    if (tree[field.children] && tree[field.children].length > 0) {
      let newTree = tree[field.children];
      if (cvalue.select2) {
        newTree = newTree.filter((item: any) => {
          return item[field.value] == cvalue.select2;
        });
      }
      setTreeLabels(newTree);
    } else {
      setTreeLabels([]);
    }
    return true;
  };

  const onCheckCardChange = (item: any) => {
    // const tree = recursionTreeData(treeData, cvalue, field);
    if (select !== item[field.value]) {
      setSelect(item[field.value]);
      if (cacheLastRecord) {
        localStorage.setItem('SelectProjectCustom', item[field.value]);
      }
      if (typeof onChange === 'function') {
        onChange(item[field.value]);
      }
    }

    handleCancel();
  };

  let selectName = '';
  if (select) {
    const has = recursionTreeData(treeData, select, field);
    if (has && has[field.label]) {
      const hasParent = recursionTreeData(treeData, has[field.parentId], field);
      selectName = has[field.label];
      if (hasParent && hasParent[field.label]) {
        selectName = `${hasParent[field.label]}-${selectName}`;
      }
    }
  }
  if (!selectName) {
    selectName = '未选择项目';
  }
  const labelsNode = (tree: any) => {
    const labels = loopTreeDataAllNode(tree[field.children], 3, {
      value: 'type',
      children: field.children,
    });
    return (
      <>
        {labels
          .filter(
            (item: any) =>
              (formValues.kw && item[field.label].includes(formValues.kw)) || !formValues.kw,
          )
          .map((item: any) => {
            return (
              <Tag
                key={item[field.value]}
                color={select && select == item[field.value] ? '#1677ff' : 'blue'}
                onClick={onCheckCardChange.bind(null, item)}
              >
                {item[field.label]}
              </Tag>
            );
          })}
      </>
    );
  };

  const newTreeLabels = treeLabels.filter(
    (item: any) => item[field.children] && item[field.children].length > 0,
  );

  const type1Data = loopTreeDataAllNode(treeData, 1, {
    value: 'type',
    children: field.children,
  });

  return (
    <>
      <div className="select-project-custom">
        <Space align="baseline" className="content-baseline">
          <Title level={5} style={{ color: '#1890ff' }}>
            {selectName}
          </Title>
          <Button type="primary" onClick={showModal}>
            选择项目
          </Button>
        </Space>
        {isModalVisible && (
          <ModalCustom
            className="select-project-modal-custom"
            title="选择项目"
            visible={isModalVisible}
            width="double"
            onCancel={handleCancel}
            footer={null}
          >
            {filter && (
              <FormCustom
                layout="vertical"
                layoutType="QueryFilter"
                formRef={formRef}
                span={6}
                onValuesChange={onValuesChange}
                onFinish={onFinish}
                columns={
                  filterColumns
                    ? filterColumns
                    : [
                        {
                          title: '公司',
                          dataIndex: 'select1',
                          valueType: 'select',
                          fieldProps: {
                            options: type1Data.map((item: any) => {
                              return {
                                value: item[field.value],
                                label: item[field.label],
                              };
                            }),
                          },
                          formItemProps: {
                            rules: [{ required: true }],
                          },
                        },
                        {
                          title: '部门',
                          dataIndex: 'select2',
                          valueType: 'select',
                          fieldProps: (form: any) => {
                            let select1 = form.getFieldValue('select1');
                            // 如果默认进入选中 异步表单无法获取值
                            if (!select1 && formValues.select1) {
                              select1 = formValues.select1;
                            }

                            if (!select1) {
                              return {
                                options: [],
                              };
                            }
                            const type1Node = recursionTreeData(treeData, select1, field);

                            if (!type1Node[field.children]) {
                              return {
                                options: [],
                              };
                            }

                            const type2Data = loopTreeDataAllNode(type1Node[field.children], 2, {
                              value: 'type',
                              children: field.children,
                            });

                            return {
                              showSearch: true,
                              options: type2Data.map((item: any) => {
                                return {
                                  value: item[field.value],
                                  label: item[field.label],
                                };
                              }),
                            };
                          },
                        },
                        {
                          title: '项目名称',
                          dataIndex: 'kw',
                          valueType: 'text',
                        },
                      ]
                }
                submitter={{
                  // 配置按钮的属性
                  resetButtonProps: {
                    style: {
                      // 隐藏重置按钮
                      display: 'none',
                    },
                  },
                }}
              />
            )}
            {newTreeLabels.length > 0 ? (
              newTreeLabels.map((item: any) => {
                return (
                  <ProCard
                    className="select-project-modal-custom-pro-card"
                    key={item[field.value]}
                    title={item[field.label]}
                    headerBordered
                    collapsible
                  >
                    <Space size={[8, 16]} wrap style={{ marginTop: 12 }}>
                      {labelsNode(item)}
                    </Space>
                  </ProCard>
                );
              })
            ) : (
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            )}
          </ModalCustom>
        )}
      </div>
    </>
  );
}

export default SelectProjectCustom;
