---
title: 选择项目
toc: content
order: 7
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 通用
  path: /Exception
  order: 4
---

# 说明

```jsx
/**
 * title: 选择项目组件使用
 */
import React from 'react';
import { SelectProjectCustom } from '@vh-admin/pro-components';

function SelectProjectCustomDemo1() {
  const treeData = [
    {
      id: '1x18527',
      name: '成都代理',
      type: 1,
      parentId: '-1',
      childOrganizations: [
        {
          id: '1x18547',
          name: '总经理办公室',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18548',
          name: '大客户服务部',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18549',
          name: '策略服务中心',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '2680885',
              name: '策略服务中心',
              type: 2,
              parentId: '1x18549',
            },
            {
              id: '2680886',
              name: '项目一组',
              type: 2,
              parentId: '1x18549',
            },
          ],
        },
        {
          id: '1x18550',
          name: '运营部',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18551',
          name: '策管部',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18552',
          name: '销售管理部',
          type: 2,
          parentId: '1x18527',
        },

        {
          id: '1x18556',
          name: '微代中心',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '1x18639',
              name: '万科怡心湖岸',
              type: 3,
              parentId: '1x18556',
            },
            {
              id: '1x18641',
              name: '万科翡翠和悦',
              type: 3,
              parentId: '1x18556',
            },
          ],
        },
        {
          id: '1x18557',
          name: '新业务部',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '1x63755',
              name: '万科库存车位销售',
              type: 3,
              parentId: '1x18557',
            },
          ],
        },
        {
          id: '1x18558',
          name: '运营一部',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '2722735',
              name: '中铁芙蓉1891',
              type: 2,
              parentId: '1x18558',
            },
            {
              id: '1x18642',
              name: '花样年香门第',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18643',
              name: '三里花城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18644',
              name: '七一国际广场',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18645',
              name: '花样年江山',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18646',
              name: '滨江樾城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18647',
              name: '花样年家天下',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x63750',
              name: '花样年106亩',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x63751',
              name: '朗诗未来著',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x63758',
              name: '首创禧瑞光华',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x86156',
              name: '五矿西棠',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x86157',
              name: '乡林涵碧天下',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1925565',
              name: '环球融创未来城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2157110',
              name: '中旅千川阅',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2325920',
              name: '环球融创未来城长岛',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2364026',
              name: '富豪三里花城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2449009',
              name: '荣盛时代天府',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2484304',
              name: '北京城建国誉府',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2644285',
              name: '港中旅龙潭寺',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2701x26',
              name: '天府麓城',
              type: 3,
              parentId: '1x18558',
            },
          ],
        },
      ],
    },
    {
      id: '9999999',
      name: '支持团队',
      type: 1,
      parentId: '-1',
    },
  ];
  return (
    <>
      <SelectProjectCustom
        treeData={treeData}
        field={{ label: 'name', value: 'id', children: 'childOrganizations', parentId: 'parentId' }}
        cacheLastRecord={true}
      />
    </>
  );
}
export default SelectProjectCustomDemo1;
```

```jsx
/**
 * title: 默认值和切换监听
 */
import React, { useState } from 'react';
import { SelectProjectCustom } from '@vh-admin/pro-components';

function SelectProjectCustomDemo2() {
  const [value, setValue] = useState('1x18639');

  const onChange = (value) => {
    setValue(value);
  };
  const treeData = [
    {
      id: '1x18527',
      name: '成都代理',
      type: 1,
      parentId: '-1',
      childOrganizations: [
        {
          id: '1x18547',
          name: '总经理办公室',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18548',
          name: '大客户服务部',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18549',
          name: '策略服务中心',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '2680885',
              name: '策略服务中心',
              type: 2,
              parentId: '1x18549',
            },
            {
              id: '2680886',
              name: '项目一组',
              type: 2,
              parentId: '1x18549',
            },
          ],
        },
        {
          id: '1x18550',
          name: '运营部',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18551',
          name: '策管部',
          type: 2,
          parentId: '1x18527',
        },
        {
          id: '1x18552',
          name: '销售管理部',
          type: 2,
          parentId: '1x18527',
        },

        {
          id: '1x18556',
          name: '微代中心',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '1x18639',
              name: '万科怡心湖岸',
              type: 3,
              parentId: '1x18556',
            },
            {
              id: '1x18641',
              name: '万科翡翠和悦',
              type: 3,
              parentId: '1x18556',
            },
          ],
        },
        {
          id: '1x18557',
          name: '新业务部',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '1x63755',
              name: '万科库存车位销售',
              type: 3,
              parentId: '1x18557',
            },
          ],
        },
        {
          id: '1x18558',
          name: '运营一部',
          type: 2,
          parentId: '1x18527',
          childOrganizations: [
            {
              id: '2722735',
              name: '中铁芙蓉1891',
              type: 2,
              parentId: '1x18558',
            },
            {
              id: '1x18642',
              name: '花样年香门第',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18643',
              name: '三里花城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18644',
              name: '七一国际广场',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18645',
              name: '花样年江山',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18646',
              name: '滨江樾城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x18647',
              name: '花样年家天下',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x63750',
              name: '花样年106亩',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x63751',
              name: '朗诗未来著',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x63758',
              name: '首创禧瑞光华',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x86156',
              name: '五矿西棠',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1x86157',
              name: '乡林涵碧天下',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '1925565',
              name: '环球融创未来城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2157110',
              name: '中旅千川阅',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2325920',
              name: '环球融创未来城长岛',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2364026',
              name: '富豪三里花城',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2449009',
              name: '荣盛时代天府',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2484304',
              name: '北京城建国誉府',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2644285',
              name: '港中旅龙潭寺',
              type: 3,
              parentId: '1x18558',
            },
            {
              id: '2701x26',
              name: '天府麓城',
              type: 3,
              parentId: '1x18558',
            },
          ],
        },
      ],
    },
    {
      id: '9999999',
      name: '支持团队',
      type: 1,
      parentId: '-1',
    },
  ];
  return (
    <>
      <SelectProjectCustom
        treeData={treeData}
        value={value}
        onChange={onChange}
        field={{ label: 'name', value: 'id', children: 'childOrganizations', parentId: 'parentId' }}
      />
    </>
  );
}
export default SelectProjectCustomDemo2;
```

## API

<embed src="./SelectProjectCustomType.md"></embed>

