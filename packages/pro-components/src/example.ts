import type { RequestData } from '@ant-design/pro-table';

export const getProTableUserList = () => {
  return {
    data: [
      {
        id: 1,
        name: 'Shirley Young',
      },
      {
        id: 2,
        name: 'Ruth Harris',
      },
      {
        id: 3,
        name: 'Daniel Johnson',
      },
      {
        id: 4,
        name: 'Melissa Thomas',
      },
      {
        id: 5,
        name: 'Daniel Jackson',
      },
      {
        id: 6,
        name: 'William Martinez',
      },
      {
        id: 7,
        name: 'Helen Thomas',
      },
      {
        id: 8,
        name: 'Jessica Hall',
      },
      {
        id: 9,
        name: 'Sarah Anderson',
      },
      {
        id: 10,
        name: 'Robert Lopez',
      },
      {
        id: 11,
        name: 'Paul Garcia',
      },
      {
        id: 12,
        name: 'Steven Gonzalez',
      },
      {
        id: 13,
        name: 'Richard White',
      },
      {
        id: 14,
        name: 'Jessica Anderson',
      },
      {
        id: 15,
        name: 'Robert Martinez',
      },
      {
        id: 16,
        name: 'Cynthia Taylor',
      },
      {
        id: 17,
        name: 'George Clark',
      },
      {
        id: 18,
        name: 'Laura Lewis',
      },
      {
        id: 19,
        name: 'Maria Lopez',
      },
      {
        id: 20,
        name: 'Anna Lee',
      },
    ],
    code: 0,
    msg: '成功',
  };
};

export const proTableAddRow = () => {
  return {
    data: {},
    code: 0,
    msg: '成功',
  };
};
export const getProTable = async () => {
  const data: RequestData<any> = {
    total: 100,
    data: [
      {
        id: 81,
        avatar: 'http://dummyimage.com/100x100/8679f2',
        file: 'http://dummyimage.com/100x100/8679f2',
        title: '起适常管',
        datetime: '1982-01-29 06:34:39',
        description: '起适常管',
        user_id: 17,
        status: 3,
        type: 2,
        age: 2,
        age1: [81, 85],
      },
      {
        id: 82,
        avatar: 'http://dummyimage.com/100x100/8ff279',
        file: 'http://dummyimage.com/100x100/8679f2',
        title: '你转候多把风',
        datetime: '2001-04-26 16:56:19',
        description: '你转候多把风',
        user_id: 20,
        status: 2,
        type: 3,
        age: 2,
        age1: [],
      },
      {
        id: 83,
        avatar: 'http://dummyimage.com/100x100/f279b2',
        file: 'http://dummyimage.com/100x100/8679f2',
        title: '如集天厂',
        datetime: '1971-01-26 14:55:56',
        description: '如集天厂',
        user_id: 18,
        status: 2,
        type: 3,
        age: 2,
        age1: [],
      },
      {
        id: 84,
        avatar: 'http://dummyimage.com/100x100/79d6f2',
        file: 'http://dummyimage.com/100x100/8679f2',
        title: '处持意信非',
        datetime: '1977-08-24 22:37:32',
        description: '处持意信非',
        user_id: 7,
        status: 3,
        type: 3,
        age: 2,
        age1: [],
      },
      {
        id: 85,
        avatar: 'http://dummyimage.com/100x100/f2ea79',
        file: 'http://dummyimage.com/100x100/8679f2',
        title: '命果型后近面',
        datetime: '2007-03-02 23:32:12',
        description: '命果型后近面',
        user_id: 7,
        status: 2,
        type: 1,
        age: 2,
        age1: [],
      },
      {
        id: 86,
        avatar: 'http://dummyimage.com/100x100/c779f2',
        title: '红说型线教',
        datetime: '2021-02-03 22:15:48',
        description: '红说型线教',
        user_id: 3,
        status: 2,
        type: 2,
        age: 2,
        age1: [],
      },
      {
        id: 87,
        avatar: 'http://dummyimage.com/100x100/79f2a4',
        title: '满较观干',
        datetime: '2001-06-08 09:32:28',
        description: '满较观干',
        user_id: 13,
        status: 1,
        type: 1,
        age: 2,
        age1: [],
      },
      {
        id: 88,
        avatar: 'http://dummyimage.com/100x100/f28079',
        title: '受千统传',
        datetime: '1983-01-22 15:30:30',
        description: '受千统传',
        user_id: 15,
        status: 1,
        type: 2,
        age: 2,
        age1: [],
      },
      {
        id: 89,
        avatar: 'http://dummyimage.com/100x100/7994f2',
        title: '员队图',
        datetime: '1987-02-03 07:04:11',
        description: '员队图',
        user_id: 13,
        status: 3,
        type: 2,
        age: 2,
        age1: [],
      },
      {
        id: 90,
        avatar: 'http://dummyimage.com/100x100/b8f279',
        title: '行铁形',
        datetime: '2018-01-29 13:11:07',
        description: '行铁形',
        user_id: 14,
        status: 1,
        type: 1,
        age: 2,
        age1: [],
      },
      {
        id: 91,
        avatar: 'http://dummyimage.com/100x100/f279db',
        title: '料打厂值都',
        datetime: '1986-05-28 10:19:02',
        description: '料打厂值都',
        user_id: 1,
        status: 2,
        age: 2,
        type: 2,
        age1: [],
      },
      {
        id: 92,
        avatar: 'http://dummyimage.com/100x100/79f2e5',
        title: '易取报',
        datetime: '1971-05-18 09:52:57',
        description: '易取报',
        user_id: 18,
        status: 1,
        type: 1,
        age: 2,
      },
      {
        id: 93,
        avatar: 'http://dummyimage.com/100x100/f2c279',
        title: '元想确位用',
        datetime: '1980-10-24 10:09:40',
        description: '元想确位用',
        user_id: 18,
        status: 2,
        type: 1,
        age: 2,
      },
      {
        id: 94,
        avatar: 'http://dummyimage.com/100x100/9f79f2',
        title: '被织那全空单',
        datetime: '2012-10-26 10:18:13',
        description: '被织那全空单',
        user_id: 12,
        status: 2,
        type: 3,
        age: 2,
      },
      {
        id: 95,
        avatar: 'http://dummyimage.com/100x100/79f27b',
        title: '织将收元层些',
        datetime: '2012-12-05 04:51:58',
        description: '织将收元层些',
        user_id: 5,
        status: 2,
        type: 3,
        age: 2,
      },
      {
        id: 96,
        avatar: 'http://dummyimage.com/100x100/f27999',
        title: '至写更',
        datetime: '1976-07-26 15:29:08',
        description: '至写更',
        user_id: 4,
        status: 1,
        type: 2,
        age: 2,
      },
      {
        id: 97,
        avatar: 'http://dummyimage.com/100x100/79bdf2',
        title: '龙名红制准',
        datetime: '1997-01-04 12:15:11',
        description: '龙名红制准',
        user_id: 3,
        status: 1,
        type: 2,
        age: 2,
      },
      {
        id: 98,
        avatar: 'http://dummyimage.com/100x100/e0f279',
        title: '以革法众领许世',
        datetime: '1997-02-17 22:33:05',
        description: '以革法众领许世',
        user_id: 19,
        status: 1,
        type: 2,
        age: 2,
      },
      {
        id: 99,
        avatar: 'http://dummyimage.com/100x100/e079f2',
        title: '流清我运起市还',
        datetime: '1998-01-21 12:52:22',
        description: '流清我运起市还',
        user_id: 6,
        status: 3,
        type: 3,
        age: 2,
      },
      {
        id: 100,
        avatar: 'http://dummyimage.com/100x100/79f2bd',
        title: '自没根打花',
        datetime: '1993-09-29 02:15:39',
        description: '自没根打花',
        user_id: 7,
        status: 2,
        type: 2,
        age: 2,
      },
    ],
    summary: { user_id: 200, age: 40 },
    success: true,
  };
  return {
    data: data,
    code: 0,
    msg: '成功',
  };
};

export const proTableDetails = () => {
  return {
    data: {
      id: 1,
      avatar: 'http://dummyimage.com/100x100/f29979',
      title: '保利质术式',
      datetime: '1986-02-18 14:54:26',
      description: '保利质术式',
      'user_id|+1': 7,
      status: 3,
      type: 2,
    },
    code: 0,
    msg: '成功',
  };
};

export const proTableDelete = () => {
  return {
    data: {},
    code: 0,
    msg: '成功',
  };
};

export const proTableDownload = () => {
  return {
    data: {},
    code: 0,
    msg: '成功',
  };
};

export const getDemoNoticeMessage = () => {
  return {
    data: {
      total: 100,
      data: [
        {
          id: 1,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 2,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 3,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 4,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 5,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 6,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 7,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 8,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 9,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
        {
          id: 10,
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: '已通过第三轮面试',
          description: '你推荐的 曲妮妮 已通过第三轮面试',
          date: '3 年前',
          is_urgent: 2,
          is_urgent_name: '重要',
          read: false,
          content:
            '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
        },
      ],
    },
    code: 0,
    msg: '成功',
  };
};

export const getDemoNoticeMessageRed = async () => {
  return {
    data: {
      id: 1,
      read: false,
      avatar: 'https://gw.alipayobjects.com/zos/rmsportal/fcHMVNCjPOsbUGdEduuv.jpeg',
      title: '曲丽丽 评论了你',
      date: '3 年前',
      description: '描述信息描述信息描述信息',
      is_urgent_name: '重要',
      content:
        '<p>大神大神大d大神大神大d大神大神大d大神大神大d大神大神大d</p><p></p><p></p><div class="media-wrap image-wrap"><img src="http://www.adminapi.com/uploads/api/files/2021-10-14/ab1f561f86378b43e264e1860f3afc44/WechatIMG20988.jpeg"/></div><p>adasdasdasdasdasd</p><p>大神大神大是啊实打实的</p><ol><li>阿萨德啊实打实大声道</li><li>阿萨德1啊实打实的</li><li>11212sssssasdasdasda</li></ol><p></p>',
    },
    code: 0,
    msg: '成功',
  };
};

export const captcha = () => {
  return '';
};
