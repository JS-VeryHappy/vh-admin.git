---
title: 组件使用必读
order: 1
toc: content
nav:
  order: 1
  title: 组件
  path: /components
group:
  title: 组件使用必读
---

## 开发组件说明

- 一.开发必须遵循规则
  - **1.其本身有独立使用说明文档和运行 Demo**
  - **2.本身可以独立运行不受业务组件的使用环境所影响**
  - **3.本身尽可能是业务最小化模块**
- 二.组件开发中可以使用以下组件、可以单独使用、也可以混合使用：
  - **[ProComponents](https://procomponents.ant.design/components/form)**
  - **[Antd](https://ant.design/components/overview-cn/)**
  - **[html 语义化标签]**
- 三.组件统一后缀以:**Custom**结尾
  - 命名规则头部组件的性质：p = page 页面、c = common 常用、f = form 表单、t = table 表单、m = Modal 弹窗 ...
  - 命名规则中部组件的业务说明：403、 loading、...
  - 命名规则尾部固定：custom
  - 例如：表单组件 c-loading-custom
- 四.组件导出在**src/index.ts**中，导出名称为组件名称修改为大驼峰
  - 例如：表单组件 CLoadingCustom
- 四.修改组件时需要考虑组件被使用的业务情况
