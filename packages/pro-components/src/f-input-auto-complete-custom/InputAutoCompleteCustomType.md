### InputAutoCompleteCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| request | 从远程请求网络数据，一般用于选择类组件 | `(params:any,props:any)=>Promise<OptionType[]>` | - | - |
| params | 额外传递给request的参数，组件不做处理,但是变化会引起request重新请求数据 | `Record<string,any>` | - | - |
| options | select选择数据<br>[OptionType](/pro-components/f-input-auto-complete-custom#optiontype) | `OptionType[]` | [] | - |
| style | 样式<br> | `any` | [] | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
