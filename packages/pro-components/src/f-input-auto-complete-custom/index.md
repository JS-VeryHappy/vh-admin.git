---
title: Input自动完成
toc: content
order: 9
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. Input 自动完成

```jsx
/**
 * title: Input自动完成
 */
import React from 'react';
import { Button } from 'antd';
import { InputAutoCompleteCustom } from '@vh-admin/pro-components';

function InputAutoCompleteCustomDemo() {
  const options = [
    {
      label: '1',
      value: 1,
    },
    {
      label: '2',
      value: 2,
    },
  ];
  const fieldProps = {
    style: {
      width: '200px',
    },
  };
  return (
    <>
      <InputAutoCompleteCustom options={options} fieldProps={fieldProps} />
    </>
  );
}
export default InputAutoCompleteCustomDemo;
```


## API

<embed src="./InputAutoCompleteCustomType.md"></embed>



<embed src="../f-form-custom/Example/FromCustomTypes/OptionType.md"></embed>
