---
title: 数值时间计器
toc: content
order: 10
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 通用
  path: /Exception
  order: 4
---


### 基础用法

<code src='./Example/demo1.tsx'></code>

### 时间类型

<code src="./Example/demo2.tsx"> </code>

### 改变样式

<code src="./Example/demo3.tsx"></code>

### 动态数字

<code src="./Example/demo4.tsx"></code>

### 金额分隔符

<code src="./Example/demo5.tsx"></code>

### 改变大小

<code src="./Example/demo6.tsx"></code>

## API

<embed src="./CNumberRollCustomType.md"></embed>




