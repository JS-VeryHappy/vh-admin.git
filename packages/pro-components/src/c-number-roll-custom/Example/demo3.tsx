//@ts-ignore
import { CNumberRollCustom } from '@vh-admin/pro-components';
import dayjs from 'dayjs';
import type { FC } from 'react';
import { useEffect, useState } from 'react';
import './index.less';

const center = {
  display: 'flex',
  justifyContent: 'center',
};

const Demo: FC = () => {
  const [value, setValue] = useState<string>(dayjs(new Date()).format('HH:mm:ss'));
  useEffect(() => {
    const timer = setInterval(() => {
      setValue(dayjs(new Date()).format('HH:mm:ss'));
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <>
      <CNumberRollCustom style={center} className="numberStyle" value={value} type="date" />
    </>
  );
};

export default Demo;
