//@ts-ignore
import { CNumberRollCustom } from '@vh-admin/pro-components';
import dayjs from 'dayjs';
import type { FC } from 'react';
import { useEffect, useState } from 'react';

const center = {
  display: 'flex',
  justifyContent: 'center',
};
const Demo: FC = () => {
  const [value, setValue] = useState<string>(dayjs(new Date()).format('HH:mm:ss'));
  const [value2, setValue2] = useState<string>(dayjs(new Date()).format('yyyy-MM-DD HH:mm'));

  useEffect(() => {
    const timer = setInterval(() => {
      setValue(dayjs(new Date()).format('HH:mm:ss'));
      setValue2(dayjs(new Date()).format('yyyy-MM-DD HH:mm'));
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <>
      <CNumberRollCustom
        style={center}
        value={value}
        type="date"
        // onFinish={(value) => {
        //   console.log('动画结束', value);
        // }}
      />
      <br />
      <CNumberRollCustom style={center} value={value2} type="date" />
    </>
  );
};

export default Demo;
