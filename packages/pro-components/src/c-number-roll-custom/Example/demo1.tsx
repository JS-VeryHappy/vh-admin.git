//@ts-ignore
import { CNumberRollCustom } from '@vh-admin/pro-components';
import type { FC } from 'react';
import { useEffect, useState } from 'react';

const center = {
  display: 'flex',
  justifyContent: 'center',
};
const Demo: FC = () => {
  const [value, setValue] = useState(10 as number);

  const load = () => {
    setInterval(() => {
      const random = value * Math.floor(Math.random() * 1000);
      setValue(random);
    }, 3000);
  };
  useEffect(() => {
    load();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <CNumberRollCustom style={center} value={value} />
    </>
  );
};

export default Demo;
