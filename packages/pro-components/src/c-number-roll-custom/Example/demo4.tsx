import { Button, Input, Space } from 'antd';
//@ts-ignore
import { CNumberRollCustom } from '@vh-admin/pro-components';
import type { FC } from 'react';
import { useState } from 'react';

const center = {
  display: 'flex',
  justifyContent: 'center',
};
const Demo: FC = () => {
  const [count, setCount] = useState<number>(999.99);
  const [value, setValue] = useState<number>(999.99);
  return (
    <>
      <CNumberRollCustom style={center} value={value} />
      <br />
      <Space.Compact style={center}>
        <Input
          style={{ width: 200 }}
          value={count}
          placeholder="请输入数字"
          onChange={(e: any) => setCount(e.target.value)}
        />
        <Button type="primary" onClick={() => setValue(Number(count))}>
          设置
        </Button>
      </Space.Compact>
    </>
  );
};

export default Demo;
