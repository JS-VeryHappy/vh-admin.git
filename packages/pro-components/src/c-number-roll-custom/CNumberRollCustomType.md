### CNumberRollCustomType


| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| value | 默认值 | `number\|string` | - | - |
| minLength | 最小位数（个位数起） | `number` | - | - |
| symbol | 分割符号（禁用"."）1,000,000,000 | `string` | - | - |
| speed | 动画速度ms | `number` | - | - |
| dot | 保留几位小数 | `number` | - | - |
| type | 类型 | `'number'\|'date'` | - | - |
| scale | 缩放大小 | `number` | - | - |
| style | 样式 | `React.CSSProperties` | - | - |
| className | 类名 | `string` | - | - |
| onFinish | 动画结束的回调 | `(value:number\|string)=>void` | - | - |
