### SelectInputCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| options | select选择数据<br>[OptionType](/pro-components/f-input-auto-complete-custom#optiontype) | `OptionType[]\|undefined` | [] | - |
| `inputProps` | antd input的inputProps<br>[input](https://ant.design/components/input-cn) | `inputProps` | - | - |
| `selectProps` | antd select的inputProps<br>[select](https://ant.design/components/select-cn) | `selectProps` | - | - |
| inputNumber | input 是否使用InputNumber | `boolean` |false | - |
| clearInput | 切换时是否清空input的值 | `boolean` |true | - |
| hideInput | 如果选择的值存在数组中，则隐藏Input | `any[]` |[] | - |
| disabledInput | 如果选择的值存在数组中，则禁用Input | `any[]` |[] | - |
| `...CustomType` | 参考<a href="/pro-components/form-development#customtype" target="_blank">CustomType</a> | `any` | - | - |
