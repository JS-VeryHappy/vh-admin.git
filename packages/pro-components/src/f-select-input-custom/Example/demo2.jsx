import React from 'react';
import { SelectInputCustom } from '@vh-admin/pro-components';
import { useState } from 'react';

function SelectInputCustomDemo() {
  const [value, setValue] = useState([4, 111]);

  const options = [
    {
      label: 'a',
      value: 1,
    },
    {
      label: 'b',
      value: 2,
    },
    {
      label: 'c',
      value: 3,
    },
    {
      label: 'd',
      value: 4,
    },
  ];

  const inputProps = {
    style: {
      width: '50%',
    },
  };

  const onChange = (newValue) => {
    setValue(newValue);
    console.log('===================');
    console.log(newValue);
    console.log('===================');
  };

  return (
    <SelectInputCustom
      hideInput={[1]}
      disabledInput={[3]}
      inputNumber={true}
      options={options}
      inputProps={inputProps}
      value={value}
      onChange={onChange}
    />
  );
}

export default SelectInputCustomDemo;
