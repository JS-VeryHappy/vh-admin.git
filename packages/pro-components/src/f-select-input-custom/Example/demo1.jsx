import React from 'react';
import { SelectInputCustom } from '@vh-admin/pro-components';
import { useState } from 'react';

function SelectInputCustomDemo() {
  const [value, setValue] = useState([2, 222]);

  const options = [
    {
      label: '1',
      value: 1,
    },
    {
      label: '2',
      value: 2,
    },
  ];

  const inputProps = {
    style: {
      width: '50%',
    },
  };

  const onChange = (newValue) => {
    setValue(newValue);
    console.log('===================');
    console.log(newValue);
    console.log('===================');
  };

  return (
    <SelectInputCustom
      options={options}
      inputProps={inputProps}
      value={value}
      onChange={onChange}
    />
  );
}

export default SelectInputCustomDemo;
