import React, { useState, useEffect } from 'react';
import { Input, InputNumber, Select, Space } from 'antd';
import type { CustomType, OptionType } from '../f-form-custom/types';

export declare type SelectInputCustomType = {
  /**
   * select选择数据
   * @default []
   */
  options?: OptionType[] | undefined;

  /**
   * antd input的inputProps
   */
  inputProps?: any;
  /**
   * antd select的inputProps
   */
  selectProps?: any;

  /**
   * input 是否使用InputNumber
   * @default false
   */
  inputNumber?: boolean;
  /**
   * 是否在切换时清空Input的值
   *  @default true
   */
  clearInput?: boolean;
  /**
   * 如果选择的值存在数组中，则隐藏Input
   *  @default []
   */
  hideInput?: any[];
  /**
   * 如果选择的值存在数组中，则禁用Input
   *  @default []
   */
  disabledInput?: any[];
} & CustomType;

function SelectInputCustom(Props: SelectInputCustomType) {
  const [inputValue, setInputValue] = useState<any>(null);
  const [selectValue, setSelectValue] = useState<any>(null);

  const {
    id,
    style,
    className,
    readonly,
    onChange,
    value,
    options,
    inputProps,
    selectProps,
    inputNumber = false,
    clearInput = true,
    hideInput = [],
    disabledInput = [],
  } = Props;

  useEffect(() => {
    /**
     * 如果父级传有默认值则赋值默认值 或者默认值变换
     */
    if (value && value[1] !== undefined) {
      setInputValue(value[1]);
    } else {
      setInputValue(undefined);
    }
    if (value && value[0] !== undefined) {
      setSelectValue(value[0]);
    } else {
      setSelectValue(undefined);
    }
  }, [value]);

  /**
   * input切换值变换。如果父级传入监听方法调用
   * @param e
   */
  const onInputChange = (e: any) => {
    let nvalue: any;
    if (inputNumber) {
      nvalue = e;
    } else {
      nvalue = e.target.value;
    }

    setInputValue(nvalue);
    if (onChange && typeof onChange === 'function') {
      onChange([selectValue, nvalue]);
    }
  };
  /**
   * input切换值变换。如果父级传入监听方法调用
   * @param paramValue
   */
  const onSelectChange = (paramValue: any) => {
    setSelectValue(paramValue);
    let ninputValue: any = inputValue;
    if (clearInput || hideInput.includes(selectValue)) {
      ninputValue = undefined;
      setInputValue(ninputValue);
    }
    if (onChange && typeof onChange === 'function') {
      onChange([paramValue, ninputValue]);
    }
  };

  let InputNode: any = Input;
  if (inputNumber) {
    InputNode = InputNumber;
  }

  return (
    <div
      id={id}
      className={`f-select-input-custom ${className ? className : ''}`}
      style={{ ...style }}
    >
      {readonly ? (
        `${value && value[0] ? options?.find((i: any) => i.value === value[0])?.label : ''}-${
          value && value[1] ? value[1] : ''
        }`
      ) : (
        <Space.Compact block>
          <Select
            style={{ width: '50%' }}
            placeholder="请选择"
            onChange={onSelectChange}
            // @ts-ignore
            options={options}
            value={selectValue}
            {...selectProps}
          />

          {!hideInput.includes(selectValue) && (
            <InputNode
              style={{ width: '50%' }}
              disabled={disabledInput.includes(selectValue)}
              placeholder="请输入"
              {...inputProps}
              value={inputValue}
              onChange={onInputChange}
            />
          )}
        </Space.Compact>
      )}
    </div>
  );
}

export default SelectInputCustom;
