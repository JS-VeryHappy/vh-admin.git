import React from 'react';
import { FRadioInputCustom } from '@vh-admin/pro-components';
import { useState } from 'react';

function FRadioInputCustomDemo3() {
  const [value, setValue] = useState([2, 222]);

  const options = [
    {
      label: 'a',
      value: 1,
    },
    {
      label: 'b',
      value: 2,
    },
    {
      label: 'c',
      value: 3,
    },
    {
      label: 'd',
      value: 4,
    },
  ];

  const inputProps = {
    style: {
      width: '50%',
    },
  };

  const onChange = (newValue) => {
    setValue(newValue);
    console.log('===================');
    console.log(newValue);
    console.log('===================');
  };

  return (
    <FRadioInputCustom
      hideInput={[1]}
      disabledInput={[3]}
      options={options}
      value={value}
      onChange={onChange}
      inputNumber={true}
      inputProps={inputProps}
    />
  );
}

export default FRadioInputCustomDemo3;
