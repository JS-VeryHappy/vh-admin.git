import React from 'react';
import { FRadioInputCustom } from '@vh-admin/pro-components';
import { useState } from 'react';

function FRadioInputCustomDemo2() {
  const [value, setValue] = useState([2, 222]);

  const options = [
    {
      label: '代理费同比打折',
      value: 1,
    },
    {
      label: '差异打折',
      value: 2,
    },
  ];

  const inputProps = {
    style: {
      width: '50%',
    },
  };

  const onChange = (newValue) => {
    setValue(newValue);
    console.log('===================');
    console.log(newValue);
    console.log('===================');
  };

  return (
    <FRadioInputCustom
      options={options}
      value={value}
      onChange={onChange}
      inputNumber={true}
      inputProps={inputProps}
    />
  );
}

export default FRadioInputCustomDemo2;
