import React from 'react';
import { FRadioInputCustom } from '@vh-admin/pro-components';
import { useState } from 'react';

function FRadioInputCustomDemo() {
  const [value, setValue] = useState([1, undefined]);

  const options = [
    {
      label: '代理费同比打折',
      value: 1,
    },
    {
      label: '差异打折',
      value: 2,
    },
  ];

  const inputProps = {
    style: {
      width: '50%',
    },
  };

  const onChange = (newValue) => {
    setValue(newValue);
    console.log('===================');
    console.log(newValue);
    console.log('===================');
  };

  return (
    <FRadioInputCustom
      options={options}
      inputProps={inputProps}
      value={value}
      onChange={onChange}
    />
  );
}

export default FRadioInputCustomDemo;
