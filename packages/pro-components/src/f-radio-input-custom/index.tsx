import React, { useState, useEffect } from 'react';
import { Input, Space, Radio, InputNumber } from 'antd';
import type { CustomType, OptionType } from '../f-form-custom/types';

export declare type RadioInputCustomType = {
  /**
   * radio选择数据
   * @default []
   */
  options?: OptionType[] | undefined;
  /**
   * antd input的inputProps
   */
  inputProps?: any;
  /**
   * antd radio的inputProps
   */
  radioProps?: any;
  /**
   * input 是否使用InputNumber
   * @default false
   */
  inputNumber?: boolean;
  /**
   * 是否在切换时清空Input的值
   *  @default true
   */
  clearInput?: boolean;
  /**
   * 如果选择的值存在数组中，则隐藏Input
   *  @default []
   */
  hideInput?: any[];
  /**
   * 如果选择的值存在数组中，则禁用Input
   *  @default []
   */
  disabledInput?: any[];
} & CustomType;

function RadioInputCustom(Props: RadioInputCustomType) {
  const [inputValue, setInputValue] = useState<any>(null);
  const [radioValue, setRadioValue] = useState<any>(null);

  const {
    id,
    style,
    className,
    readonly,
    onChange,
    value,
    options,
    inputProps,
    radioProps,
    inputNumber = false,
    clearInput = true,
    hideInput = [],
    disabledInput = [],
  } = Props;

  useEffect(() => {
    /**
     * 如果父级传有默认值则赋值默认值 或者默认值变换
     */
    if (value && value[1] !== undefined) {
      setInputValue(value[1]);
    } else {
      setInputValue(undefined);
    }
    if (value && value[0] !== undefined) {
      setRadioValue(value[0]);
    } else {
      setRadioValue(undefined);
    }
  }, [value]);

  /**
   * input切换值变换。如果父级传入监听方法调用
   * @param e
   */
  const onInputChange = (e: any) => {
    let nvalue: any;
    if (inputNumber) {
      nvalue = e;
    } else {
      nvalue = e.target.value;
    }
    setInputValue(nvalue);
    if (onChange && typeof onChange === 'function') {
      onChange([radioValue, nvalue]);
    }
  };
  /**
   * input切换值变换。如果父级传入监听方法调用
   * @param paramValue
   */
  const onRadioChange = (e: any) => {
    const nvalue: any = e.target.value;
    setRadioValue(nvalue);

    let ninputValue: any = inputValue;
    if (clearInput || hideInput.includes(radioValue)) {
      ninputValue = undefined;
      setInputValue(ninputValue);
    }

    if (onChange && typeof onChange === 'function') {
      onChange([nvalue, ninputValue]);
    }
  };

  let InputNode: any = Input;
  if (inputNumber) {
    InputNode = InputNumber;
  }

  return (
    <div
      className={`f-radio-input-custom ${className ? className : ''}`}
      id={id}
      style={{ ...style }}
    >
      {readonly ? (
        `${value && value[0] ? options?.find((i: any) => i.value === value[0])?.label : ''}-${
          value && value[1] ? value[1] : ''
        }`
      ) : (
        <Space align="center">
          <Radio.Group
            style={{ width: '100%' }}
            onChange={onRadioChange}
            options={options}
            value={radioValue}
            {...radioProps}
          />
          {!hideInput.includes(radioValue) && (
            <InputNode
              style={{ width: '50%' }}
              disabled={disabledInput.includes(radioValue)}
              placeholder="请输入"
              value={inputValue}
              onChange={onInputChange}
              {...inputProps}
            />
          )}
        </Space>
      )}
    </div>
  );
}

export default RadioInputCustom;
