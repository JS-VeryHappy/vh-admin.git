---
title: RadioInput
toc: content
order: 10
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. SelectInput


<code src="./Example/demo1.jsx" 
      title="使用例子"
      description="使用例子">使用例子</code>

<code src="./Example/demo2.jsx" 
      title="使用例子inputNumber"
      description="使用例子inputNumber">使用例子inputNumber</code>

<code src="./Example/demo3.jsx" 
      title="隐藏、禁用"
      description="隐藏、禁用">隐藏、禁用</code>
## API

<embed src="./RadioInputCustomType.md"></embed>


