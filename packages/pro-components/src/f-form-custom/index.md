---
title: 使用样例和API说明
order: 1
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

## 内嵌使用

<code src="./Example/demo2.jsx" 
      description="自定义按钮样式、结合自定义验证库自动验证" >登陆例子</code>

## 表单验证

<code src="./Example/demo3.jsx" description="防抖函数防止重复提交+本地赋值默认值"> 表单验证</code>

## 弹窗使用

<code src="./Example/demo1.jsx" title="弹窗使用" description="点击按钮弹窗+远程请求赋值默认值+远程请求 select options"> 弹窗使用</code>

## 使用自定义表单组件

<code src="./Example/demo4.jsx" title="使用自定义表单组件" description="表单使用自定义基础业务组件"> 使用自定义表单组件</code>

## 表单嵌套表单使用

<code src="./Example/demo5.jsx" title="使用自定义表单组件" description="内嵌模式，只生成表单项，不生成 Form 可以混合使用"> 使用自定义表单组件</code>

## 表单只读模式

<code src="./Example/demo6.jsx" 
      title="表单只读模式" 
      description="不显示按钮">表单只读模式</code>

## 分步骤表单

<code src="./Example/demo7.jsx" title="分步骤表单" description="分步骤表单">分步骤表单</code>

## 表单方法调用和配合联动，动态隐藏显示表单

<code src="./Example/demo8.jsx" 
      title="表单方法调用" 
      description="表单方法调用、实现联动、自定义获取数据提交、动态隐藏显示表单">表单方法调用</code>

## 赋值表单默认值和提交表单时，匹配对应字段写法 `dataIndex:'["field1","field2"]'`
`注意：在dependency联动情况下，赋值初始值的阶段是无效的，无法正常获取columns的值，在提交的阶段是有效的`
<code src="./Example/demo9.jsx" 
      title="高级字段匹配应用、多级、联动" 
      description="可以直接配置，数组的字段值直接对于返回字段的字段">高级字段匹配应用</code>

## 字段多种格式快捷配置，影响默认赋值和提交表单数据两个行为，提交使用json或','分割上传
`注意：在dependency联动情况下，赋值初始值的阶段是无效的，无法正常获取columns的值，在提交的阶段是有效的`
<code src="./Example/demo10.tsx" 
      title="字段多种格式快捷配置" 
      description="配置赋值和提交数据需要给后端 json或字段返回','分割等">字段多种格式快捷配置</code>


## FormCustom API 和 更多使用样例

- [ProComponents-Form](https://procomponents.ant.design/components/form)
- [Antd-Form](https://ant.design/components/form-cn/)

## API

<embed src="./Example/FromCustomTypes/OptionType.md"></embed>

<embed src="./Example/FromCustomTypes/FormCustomProps.md"></embed>

<embed src="./Example/FromCustomTypes/FormCustomColumnsType.md"></embed>

<embed src="./Example/FromCustomTypes/FormCustomColumnsTypeCustom.md"></embed>

<embed src="./Example/FromCustomTypes/ApiFormat.md"></embed>

<embed src="./Example/FromCustomTypes/FormCustomColumnsFieldPropsType.md"></embed>
