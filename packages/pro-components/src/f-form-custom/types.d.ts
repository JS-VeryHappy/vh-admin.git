import type { ProFormColumnsType } from '@ant-design/pro-form';
import type { FormFieldType, FormSchema } from '@ant-design/pro-form/es/components/SchemaForm';
import type { SelectMultipleCustomType } from '../f-select-multiple-custom';
import type { AmapFormCustomType } from '../f-amap-custom';
import type { BraftEditorCustomType } from '../f-braft-editor-custom';
import type { ProFormCaptchaCustomType } from '../f-captcha-custom';
import type { CheckCardCustomType } from '../f-check-card-custom';
import type { CityCascadeCustomType } from '../f-city-cascade-custom';
import type { EditableTableCustomType } from '../f-editable-table-custom';
import type { InputAutoCompleteCustomType } from '../f-input-auto-complete-custom';
import type { InputNumberSelectCustomType } from '../f-input-number-select-custom';
import type { InputTooltipCustomType } from '../f-input-tooltip-custom';
import type { SelectRangePickerCustomType } from '../f-select-range-picker-custom';
import type { SelectInputCustomType } from '../f-select-input-custom';
import type { SelectDatePickerCustomType } from '../f-select-date-picker-custom';
import type { FSelectTableCustomType } from '../f-select-table-custom';
import type { TreeSelectCustomType } from '../f-tree-select-custom';
import type { UploadCustomType } from '../f-upload-custom';
import type { RadioInputCustomType } from '../f-radio-input-custom';
import type { CascaderCustomType } from '../f-cascader-custom';
import type { Rule } from 'rc-field-form/lib/interface';
import type { BusinessStyleType } from '../t-table-custom/types';

/**
 * formColumns列类型
 */
export declare type ValueType =
  | 'text'
  | 'InputTooltipCustom'
  | 'SelectInputCustom'
  | 'InputAutoCompleteCustom'
  | 'ProFormCaptchaCustom'
  | 'TreeSelectCustom'
  | 'UploadCustom'
  | 'BraftEditorCustom'
  | 'AmapFormCustom'
  | 'CityCascadeCustom'
  | 'CascaderCustom'
  | 'SelectMultipleCustom'
  | 'EditableTableCustom'
  | 'CheckCardCustom'
  | 'SelectDatePickerCustom'
  | 'InputNumberSelectCustom'
  | 'SelectRangePickerCustom'
  | 'RadioInputCustom'
  | 'FSelectTableCustom'
  | 'FRadioInputCustom'
  | FormFieldType;

// 字典数据类型
export interface OptionType {
  /**
   * 数据显示值
   */
  label: string;
  /**
   * 数据返回值
   */
  value: any;
}

export declare type FormCustomColumnsTypeBusinessStyleCustom = {
  /**
   * 处理api赋值和提交的字段格式快捷配置
   */
  apiFormat?: ApiFormat;
  /**
   * 业务样式类型
   * type = 1 显示tag标签显示
   */
  businessStyle?: BusinessStyleType;
  formItemProps?: {
    rules?: rulesTypes[];
    labelWrap?: boolean;
  };
};

// formColumns参数类型
export declare type FormCustomColumnsType<T = any> =
  | ProFormColumnsType<T, ValueType>
  | FormCustomColumnsTypeCustom;

export declare type FormCustomColumnsFieldPropsType =
  | ({
      valueType: 'SelectMultipleCustom';
      fieldProps: SelectMultipleCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'AmapFormCustom';
      fieldProps: AmapFormCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'BraftEditorCustom';
      fieldProps: BraftEditorCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'ProFormCaptchaCustom';
      fieldProps: ProFormCaptchaCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'CascaderCustom';
      fieldProps: CascaderCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'CheckCardCustom';
      fieldProps: CheckCardCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'CityCascadeCustom';
      fieldProps: CityCascadeCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'EditableTableCustom';
      fieldProps: EditableTableCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'InputAutoCompleteCustom';
      fieldProps: InputAutoCompleteCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'InputNumberSelectCustom';
      fieldProps: InputNumberSelectCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'InputTooltipCustom';
      fieldProps: InputTooltipCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'RadioInputCustom';
      fieldProps: RadioInputCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'SelectDatePickerCustom';
      fieldProps: SelectDatePickerCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'SelectInputCustom';
      fieldProps: SelectInputCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'SelectRangePickerCustom';
      fieldProps: SelectRangePickerCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'FSelectTableCustom';
      fieldProps: FSelectTableCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'TreeSelectCustom';
      fieldProps: TreeSelectCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'UploadCustom';
      fieldProps: UploadCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom)
  | ({
      valueType: 'FRadioInputCustom';
      fieldProps: RadioInputCustomType;
    } & FormCustomColumnsTypeBusinessStyleCustom);

export declare type ApiFormat = {
  /**
   * 处理的格式 json，或者split分割
   */
  type: 'json' | 'split';
  /**
   * 如果是分割 分割符号
   */
  split?: string;
  /**
   * 如果是分割 分割符号 分割后是字符串还是number
   * @default number
   */
  splitType?: 'number' | 'string';
  /**
   * 需要处理的对应字段 如果不填写默认是配置中的dataIndex
   */
  dataIndex?: string | string[];
};

declare type rulesTypes = Rule & {
  /**
   * 联合自定义验证库导出的方法 声明方法名称 可以直接加入验证库
   */
  validatorExtend?: string;
};

export declare type FormCustomColumnsTypeCustom =
  | FormCustomColumnsFieldPropsType
  | FormCustomColumnsTypeBusinessStyleCustom;

// form参数类型
export declare type FormCustomProps =
  | (FormSchema<any, ValueType> & {
      /**
       * 是否只读模式
       * @default false
       */
      readonly?: boolean;
      /**
       * 弹窗类型时：弹窗的宽度
       * @default single
       */
      width?: 'single' | 'double' | number | any;
      /**
       * modalProps
       */
      modalProps?: any;
    })
  | {
      /**
       * layoutType
       */
      layoutType?: any;
      /**
       * 字段
       */
      columns: FormCustomColumnsType<any>[];
      /**
       * 其它
       */
      [key: string]: any;
    };

/**
 * 自定义子组件会传递过来的值
 */
export declare type CustomType = {
  /**
   * 是否只读模式
   * 自定义必须要实现的
   * @default false
   */
  readonly?: boolean;
  /**
   * 表单渲染的位置
   * tableRead 表格只读
   * table  表格表单
   * form 表单
   * formRead 表单只读
   * @default form
   */
  customMode?: 'tableRead' | 'table' | 'form' | 'formRead';
  /**
   * 父级的类穿过来的class
   * 实现到最外层div上 和 其他样式统一
   */
  className?: string;
  /**
   * 父级的类穿过来的style
   * 实现到最外层div上 和 其他样式统一
   */
  style?: any;
  /**
   * 父级传递过来的placeholder显示值
   */
  placeholder?: string;
  /**
   * 默认值
   * 自定义必须要实现的
   */
  value?: any;
  /**
   * 切换触发方法
   * 自定义必须要实现的
   */
  onChange?: (value: any) => void | undefined;
  /**
   * 传递给antd组件属性或者内容使用的组件 参考ant组件属性
   */
  fieldProps?: any;

  /**
   * 表单一些操作
   */
  form?: any;

  /**
   * 组件的唯一id，可以用来提交验证失败定位使用
   */
  id?: any;
};
