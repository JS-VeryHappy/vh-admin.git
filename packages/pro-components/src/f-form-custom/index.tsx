import { BetaSchemaForm } from '@ant-design/pro-form';
import type { ProFormInstance } from '@ant-design/pro-form';
import type { FormCustomColumnsType, ValueType, FormCustomProps } from './types';
import * as components from './components';
import * as validatorExtend from '@vh-admin/pro-validator';
import React, { useEffect, useImperativeHandle, useRef } from 'react';
import { deepGet } from '@vh-admin/pro-utils';

/**
 * 递归遍历表格配置文件
 * @param column
 * @param props
 * @returns
 */
const recursionChildren = (column: any, props: any) => {
  //处理dependency联动的支持
  if (column.valueType === 'dependency' && column.columns && typeof column.columns === 'function') {
    const fn = column.columns;
    column.columns = (data: any) => {
      const newDependencyData: any = [];
      const dependencyData = fn(data);
      if (!dependencyData) {
        return [];
      }
      dependencyData.forEach((item: any) => {
        const dependency: any = { ...item };
        recursionChildren(dependency, props);
        newDependencyData.push(dependency);
      });
      return newDependencyData;
    };
    return;
  }

  if (!column.columns) {
    const { readonly, layoutType, grid } = props;

    // 如果大小没有设置默认m:328
    if (!column.width && !grid) {
      column.width = 'm';

      if (layoutType === 'ModalForm') {
        if (!column.formItemProps) {
          column.formItemProps = {
            style: {},
          };
        }
        if (!column.formItemProps.style) {
          column.formItemProps.style = {};
        }

        // @ts-ignore
        if (!column.formItemProps.style.width) {
          column.formItemProps.style.width = '328px';
        }
      }
    } else if (grid && !column.colProps) {
      column.colProps = {
        span: 12,
      };
    }

    // 如果是只读模式
    if (readonly) {
      column.readonly = true;
    }

    if (!column.valueType) {
      column.valueType = 'text';
    }
    // 如果是自定义组件
    if (column.valueType.indexOf('Custom') !== -1) {
      // @ts-ignore
      if (components[column.valueType]) {
        // @ts-ignore
        const CustomComponent: React.ReactNode = components[column.valueType];
        // custom[column.valueType] = {
        //   render: (text: any, props: any) => {
        //     // @ts-ignore
        //     return <CustomComponent readonly={true} {...props} config={column} />;
        //   },
        //   renderFormItem: (text: any, props: any) => (
        //     // @ts-ignore
        //     <CustomComponent {...props} config={column} />
        //   ),
        // };
        column.render = (_dom: any, entity: any) => {
          return (
            // @ts-ignore
            <CustomComponent {...entity} customMode="formRead" readonly={true} config={column} />
          );
        };
        column.renderFormItem = (schema: any, config: any, form: any) => {
          // @ts-ignore
          return <CustomComponent customMode="form" form={form} />;
        };
      } else {
        // @ts-ignore
        console.log(`自定义组件:${column.valueType}无法识别`);
      }
    }

    // 如果包含验证 并且有自定义验证使用 自动加入
    if (
      column.formItemProps &&
      column.formItemProps.rules &&
      column.formItemProps.rules.length > 0
    ) {
      column.formItemProps.rules.forEach((rule: any) => {
        if (
          [
            'select',
            'treeSelect',
            'date',
            'dateTime',
            'dateWeek',
            'dateMonth',
            'dateQuarter',
            'dateYear',
            'dateRange',
            'dateTimeRange',
            'time',
            'timeRange',
            'checkbox',
            'rate',
            'radio',
            'radioButton',
            'cascader',
            'color',
            'switch',
            'fromNow',
            'UploadCustom',
            'CheckCardCustom',
          ].includes(column.valueType) &&
          rule.message == undefined
        ) {
          rule.message = '请选择' + column.title;
        }
        if (rule.validatorExtend) {
          for (const key in validatorExtend) {
            if (rule.validatorExtend == key) {
              // @ts-ignore
              rule.validator = validatorExtend[key];
              delete rule.validatorExtend;
            }
          }
        }
      });
    }

    if (
      column.valueType === 'select' &&
      column.fieldProps &&
      column.fieldProps.showSearch == undefined
    ) {
      column.fieldProps.showSearch = true;
    }

    return;
  }
  column.columns.forEach((i: any) => {
    recursionChildren(i, props);
  });
};

/**
 * 递归遍历表格配置文件 得到没有返回值的字段 默认为null
 * @param columns
 * @param item
 * @returns
 */
const onFinishChildren = (columns: any, value: any) => {
  // 处理 dependency 的动态数据
  if (columns && typeof columns === 'function') {
    onFinishChildren(columns(value), value);
    return;
  }

  if (!columns || columns.length === 0) {
    return;
  }
  columns.forEach((item: any) => {
    if (item.columns && item.columns.length > 0) {
      onFinishChildren(item.columns, value);
    } else {
      if (!value.hasOwnProperty(item.dataIndex)) {
        value[item.dataIndex] = null;
      }
    }
  });
};

export const apiFormatFn = (item: any, formInitialValues: any, type: any) => {
  if (!item.apiFormat) {
    return;
  }
  let dataIndexArr: any[] = [item.dataIndex];
  if (item.apiFormat.dataIndex) {
    dataIndexArr = item.apiFormat.dataIndex;
  }
  let split = ',';
  let splitType = 'number';
  if (item.apiFormat.split) {
    split = item.apiFormat.split;
  }
  if (item.apiFormat.splitType) {
    splitType = item.apiFormat.splitType;
  }
  try {
    dataIndexArr.forEach((i: any) => {
      if (formInitialValues[i]) {
        if (item.apiFormat.type === 'json') {
          if (type === 'initial') {
            if (typeof formInitialValues[i] === 'string') {
              formInitialValues[i] = JSON.parse(formInitialValues[i]);
            }
          } else {
            if (typeof formInitialValues[i] === 'object') {
              formInitialValues[i] = JSON.stringify(formInitialValues[i]);
            }
          }
        } else if (item.apiFormat.type === 'split') {
          if (type === 'initial') {
            let arr: any[] = formInitialValues[i].split(split);
            if (splitType === 'number') {
              arr = arr.map((n: any) => {
                return n * 1;
              });
            }
            formInitialValues[i] = arr;
          } else {
            formInitialValues[i] = formInitialValues[i].join(split);
          }
        }
      }
    });
  } catch (error) {
    console.error(error);
  }
};

/**
 * 处理高级字段使用
 * @param columns
 * @param formInitialValues
 */
export const advancedFieldsInitial = (columns: any, formInitialValues: any) => {
  columns.forEach((item: any) => {
    if (typeof item.columns === 'function') {
      advancedFieldsInitial(item.columns(formInitialValues), formInitialValues);
    } else if (item.columns) {
      advancedFieldsInitial(item.columns, formInitialValues);
    } else {
      if (item.dataIndex && item.dataIndex.match(/\[.*?\]/g)) {
        formInitialValues[item.dataIndex] = undefined;
        try {
          let values: any = [];
          const dataIndexArr = JSON.parse(item.dataIndex);
          let isNullNumber: any = 0;
          dataIndexArr.forEach((i: any) => {
            if (formInitialValues[i] === undefined) {
              isNullNumber += 1;
            }
            values.push(formInitialValues[i]);
          });

          // 如果没有值则默认为null
          if (isNullNumber === dataIndexArr.length) {
            values = undefined;
          }

          formInitialValues[item.dataIndex] = values;
        } catch (error) {}
      }
      apiFormatFn(item, formInitialValues, 'initial');
    }
  });
};

/**
 * 处理高级字段提交
 * @param columns
 * @param formInitialValues
 */
export const advancedFieldsFinish = (columns: any, newValue: any, oldValue: any) => {
  columns.forEach((item: any) => {
    if (typeof item.columns === 'function') {
      advancedFieldsFinish(item.columns(oldValue), newValue, oldValue);
    } else if (item.columns) {
      advancedFieldsFinish(item.columns, newValue, oldValue);
    } else {
      if (item.dataIndex && item.dataIndex.match(/\[.*?\]/g)) {
        const newData = newValue[item.dataIndex];
        delete newValue[item.dataIndex];
        try {
          const dataIndexArr = JSON.parse(item.dataIndex);
          dataIndexArr.forEach((i: any, index: any) => {
            let nv: any = undefined;
            if (newData && newData.hasOwnProperty(index)) {
              nv = newData[index];
            }
            newValue[i] = nv;
            oldValue[i] = nv;
          });
        } catch (error) {
          console.error(error);
        }
      }
      apiFormatFn(item, newValue, 'finish');
    }
  });
};

function FormCustom(Props: FormCustomProps) {
  const {
    formRef,
    className,
    columns,
    onFinish,
    readonly,
    initialValues,
    request,
    layoutType = 'Form',
  } = Props;
  const ref = useRef<
    ProFormInstance<any> & {
      // 其他自定义的很多需要提交验证的都注入到这里面 提交的时候会遍历验证
      customValidate?: any;
    }
  >();

  useImperativeHandle(
    formRef,
    () => {
      if (ref.current && !ref.current.customValidate) {
        ref.current.customValidate = [];
      }
      return ref.current;
    },
    [ref],
  );

  useEffect(() => {
    if (ref.current && !ref.current.customValidate) {
      ref.current.customValidate = [];
    }
  }, [ref]);

  // useEffect(() => {
  //   if (request) {
  //     request =
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  const newColumns: any = [];
  // 重新整理字段
  if (layoutType !== 'StepsForm') {
    columns.forEach((item: any) => {
      const column: FormCustomColumnsType = { ...item };
      recursionChildren(column, Props);
      newColumns.push(column);
    });
  } else {
    columns.forEach((itemColumn: any) => {
      const newChildren: any = [];
      itemColumn.forEach((item: any) => {
        const column: FormCustomColumnsType = { ...item };
        recursionChildren(column, Props);
        newChildren.push(column);
      });
      newColumns.push(newChildren);
    });
  }

  const formInitialValues: any = { ...initialValues };
  // 处理高级赋值字段
  advancedFieldsInitial(newColumns, formInitialValues);

  const customProps: FormCustomProps = { ...Props };

  // 如果是ModalForm 弹窗表单 设置默认值
  if (layoutType === 'ModalForm') {
    if (!customProps.width) {
      customProps.width = 'single';
    }
    if (customProps.width == 'single') {
      customProps.width = '378px';
    } else if (customProps.width == 'double') {
      customProps.width = '738px';
    }
    if (!customProps.layout) {
      customProps.layout = 'vertical';
    }

    if (!customProps.modalProps) {
      customProps.modalProps = {};
    }
    // if (customProps.modalProps.maskClosable !== true) {
    //   // 默认点击遮罩不关闭
    //   customProps.modalProps.maskClosable = false
    // }
    // 弹窗自定义class
    if (Props.id) {
      customProps.modalProps.wrapClassName = Props.id;
    }

    if (readonly) {
      delete customProps.readonly;
    }
  }

  const formOnFinish = async (value: any) => {
    // 赋值没有返回的默认值为null
    onFinishChildren(newColumns, value);

    try {
      if (typeof onFinish === 'function') {
        if (ref.current) {
          let customValidate: any = [];
          if (ref.current.customValidate) {
            customValidate = ref.current?.customValidate;
            //@ts-ignore
          } else if (ref.current.formRef?.current.customValidate) {
            //@ts-ignore
            customValidate = ref.current.formRef?.current.customValidate;
          }

          for (let i = 0; i < customValidate.length; i++) {
            if (
              customValidate[i].current &&
              customValidate[i].current.validateFieldsReturnFormatValue
            ) {
              const cref: any = customValidate[i].current;
              // console.log(cref);
              await cref.validateFieldsReturnFormatValue();
            }
          }
        }
        const newValue: any = { ...value };
        const oldValue: any = { ...value };
        advancedFieldsFinish(newColumns, newValue, oldValue);

        return await onFinish(newValue);
      }
    } catch (error) {
      console.error(error);
      return false;
    }
  };

  if (customProps.request) {
    customProps.request = async (p: any) => {
      try {
        const data: any = await request(p);

        let iv: any = {};
        let responseConfig: any = {
          data: 'data', // 存放数据字段
          success: 0, // 判断成功值
          code: 'code', // 错误码字段
          message: 'msg', // 返回信息字段
        };
        // @ts-ignore
        if (window?.vhAdmin?.responseConfig) {
          // @ts-ignore
          responseConfig = window.vhAdmin?.responseConfig;
        }

        if (
          deepGet(data, responseConfig.code) !== undefined &&
          deepGet(data, responseConfig.message) &&
          deepGet(data, responseConfig.data)
        ) {
          iv = deepGet(data, responseConfig.data);
        } else {
          iv = data;
        }

        const ivValues: any = { ...iv };
        // 处理高级赋值字段
        advancedFieldsInitial(newColumns, ivValues);

        return ivValues;
      } catch (error) {
        console.error(error);
      }
    };
  }

  return (
    <>
      <BetaSchemaForm<any, ValueType>
        scrollToFirstError
        omitNil={false}
        {...customProps}
        formRef={ref}
        onFinish={formOnFinish}
        columns={newColumns}
        layoutType={layoutType}
        className={`form-custom ${className ? className : ''}`}
        initialValues={formInitialValues}
      />
    </>
  );
}

export default FormCustom;
