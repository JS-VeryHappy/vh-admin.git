import InputTooltipCustom from '../f-input-tooltip-custom';
import SelectInputCustom from '../f-select-input-custom';
import InputAutoCompleteCustom from '../f-input-auto-complete-custom';
import ProFormCaptchaCustom from '../f-captcha-custom';
import TreeSelectCustom from '../f-tree-select-custom';
import UploadCustom from '../f-upload-custom';
import BraftEditorCustom from '../f-braft-editor-custom';
import AmapFormCustom from '../f-amap-custom';
import CityCascadeCustom from '../f-city-cascade-custom';
import CascaderCustom from '../f-cascader-custom';
import SelectMultipleCustom from '../f-select-multiple-custom';
import EditableTableCustom from '../f-editable-table-custom';
import CheckCardCustom from '../f-check-card-custom';
import SelectDatePickerCustom from '../f-select-date-picker-custom';
import FSelectTableCustom from '../f-select-table-custom';
import FRadioInputCustom from '../f-radio-input-custom';
import InputNumberSelectCustom from '../f-input-number-select-custom';
import SelectRangePickerCustom from '../f-select-range-picker-custom';

// 增加一个自定义需要添加一个 并且 types.d.ts 添加允许类型
export {
  InputTooltipCustom,
  SelectInputCustom,
  InputAutoCompleteCustom,
  ProFormCaptchaCustom,
  TreeSelectCustom,
  UploadCustom,
  BraftEditorCustom,
  AmapFormCustom,
  CityCascadeCustom,
  CascaderCustom,
  SelectMultipleCustom,
  EditableTableCustom,
  CheckCardCustom,
  SelectDatePickerCustom,
  FSelectTableCustom,
  FRadioInputCustom,
  InputNumberSelectCustom,
  SelectRangePickerCustom,
};
