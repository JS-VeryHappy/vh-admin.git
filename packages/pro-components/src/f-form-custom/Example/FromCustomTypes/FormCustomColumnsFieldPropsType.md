### FormCustomColumnsFieldPropsType

```js
FormCustomColumnsFieldPropsType =
  | {
      valueType: 'SelectMultipleCustom';
      fieldProps: SelectMultipleCustomType;
    }
  | {
      valueType: 'AmapFormCustom';
      fieldProps: AmapFormCustomType;
    }
  ....
```
