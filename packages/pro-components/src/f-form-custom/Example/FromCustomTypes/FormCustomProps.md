### FormCustomProps

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| `FormSchema<any, ValueType>` | '' | [FormSchema](https://procomponents.ant.design/components/form/)  | - |  - |
| readonly | 是否只读模式<br> | `boolean` | false | - |
| width | 弹窗类型时：弹窗的宽度<br> | `'single'\|'double'\|number\|any` | single | - |
| modalProps | modalProps | `any` | - | - |

### FormCustomProps | (...以下)  

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| layoutType | layoutType | `any` | - | - |
| columns(必须) | 字段 | `FormCustomColumnsType<any>[]` | - | - |
| ... | 其它 | `any` | - | - |
