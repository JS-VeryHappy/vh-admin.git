### FormCustomColumnsType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| `ProFormColumnsType<T, ValueType>` | 额外的属性 | [ProFormColumnsType](https://procomponents.ant.design/components/form/)  | - |  - |
| `FormCustomColumnsTypeCustom` | 额外的属性 | `[ProFormColumnsType](https://procomponents.ant.design/components/schema-form#schema-%E5%AE%9A%E4%B9%89)` | - | - |
