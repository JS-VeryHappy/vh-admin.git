### ApiFormat

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| type(必须) | 处理的格式json，或者split分割 | `'json'\|'split'` | - | - |
| split | 如果是分割分割符号 | `string` | - | - |
| splitType | 如果是分割分割符号分割后是字符串还是number<br> | `'number'\|'string'` | number | - |     
| dataIndex | 需要处理的对应字段如果不填写默认是配置中的dataIndex | `string\|string[]` | - | - | 
