### FormCustomColumnsTypeCustom

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| `formItemProps.rules.validatorExtend` | 联合自定义验证库导出的方法 声明方法名称 可以直接加入验证库 | [pro-validators](/pro-validators)  | - |  - |
| `FormCustomColumnsFieldPropsType` | 子组件ts | `FormCustomColumnsFieldPropsType` | - |
| apiFormat | 配置格式 | `ApiFormat` | - | - |
