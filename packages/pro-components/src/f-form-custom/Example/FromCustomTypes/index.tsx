/* eslint-disable @typescript-eslint/no-unused-vars */
import type {
  OptionType as OptionTypeApi,
  FormCustomColumnsType as FormCustomColumnsTypeApi,
  CustomType as CustomTypeApi,
} from '../../types';

export function OptionType(props: OptionTypeApi) {}

export function FormCustomColumnsType(props: FormCustomColumnsTypeApi) {}

export function CustomType(props: CustomTypeApi) {}
