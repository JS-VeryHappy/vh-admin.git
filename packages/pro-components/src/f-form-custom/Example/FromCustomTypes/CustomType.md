### CustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| readonly | 是否只读模式<br>自定义必须要实现的<br> | `boolean` | false | - |
| customMode | 表单渲染的位置<br>tableRead表格只读<br>table表格表单<br>form表单<br>formRead表单只读<br> | `'tableRead'\|'table'\|'form'\|'formRead'` | form | - |
| className | 父级的类穿过来的class<br>实现到最外层div上和其他样式统一 | `string` | - | - |
| style | 父级的类穿过来的style<br>实现到最外层div上和其他样式统一 | `any` | - | - |
| placeholder | 父级传递过来的placeholder显示值 | `string` | - | - |
| value | 默认值<br>自定义必须要实现的 | `any` | - | - |
| onChange | 切换触发方法<br>自定义必须要实现的 | `(value` | - | - |
| fieldProps | 传递给antd组件属性或者内容使用的组件参考ant组件属性 | `any` | - | - |
| form | 表单一些操作 | `any` | - | - |
