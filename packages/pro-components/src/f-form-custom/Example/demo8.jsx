import { FormCustom } from '@vh-admin/pro-components';
import { useState, useRef } from 'react';
import { message, Button } from 'antd';
import React from 'react';

const valueEnum = {
  all: { text: '全部', status: 'Default' },
  open: {
    text: '未解决',
    status: 'Error',
  },
  closed: {
    text: '已解决',
    status: 'Success',
    disabled: true,
  },
  processing: {
    text: '解决中',
    status: 'Processing',
  },
};

const columnsFn = (hideInForm = true) => {
  return [
    {
      title: '标题',
      dataIndex: 'title',
      valueType: 'InputTooltipCustom',
      fieldProps: {
        tooltipTitle: '使用自定义表单组件',
        fieldProps: {
          style: {
            width: '50%',
          },
        },
      },
      formItemProps: {
        rules: [
          {
            required: true,
            message: '此项为必填项',
          },
        ],
      },
      width: 'm',
    },
    {
      title: '状态',
      dataIndex: 'state',
      valueType: 'select',
      valueEnum,
      width: 'm',
    },
    {
      title: '状态1',
      dataIndex: 'state1',
      valueType: 'select',
      hideInForm: hideInForm,
      valueEnum,
      width: 'm',
    },
    {
      title: '标题22',
      dataIndex: 'title22',
      valueType: 'text',
      //此方式方便动态修改 属性值，和动态修改表单值
      fieldProps: (form) => {
        const title = form.getFieldValue('title');
        if (title) {
          setTimeout(() => {
            form.setFieldValue('title22', 'ok');
          }, 0);

          return {
            disabled: true,
          };
        }

        return {
          disabled: false,
        };
      },
      formItemProps: (form) => {
        console.log(form);
        return {
          rules: [
            {
              required: true,
              message: '此项为必填项',
            },
          ],
        };
      },
      width: 'm',
    },
    {
      //此方式方便动态增加和减少表单的字段
      valueType: 'dependency',
      name: ['state'],
      columns: ({ state }) => {
        if (state === 'open') {
          return [
            {
              title: '状态2',
              dataIndex: 'state2',
              valueType: 'select',
              hideInForm: hideInForm,
              valueEnum,
              width: 'm',
            },
          ];
        }
        return [];
      },
    },
  ];
};

function Demo8() {
  const formRef = useRef();
  const [columns, setColumns] = useState(() => columnsFn());

  const onFinish = async (values) => {
    try {
      console.log('====================================');
      console.log(values);
      console.log('====================================');
      message.success('成功');
      return true;
    } catch (error) {
      console.log('====================================');
      console.log(error, setColumns);
      console.log('====================================');
      return false;
    }
  };
  const onValuesChange = async (values) => {
    console.log('====================================');
    console.log(formRef.current);
    console.log('====================================');
    if (values.state) {
      formRef?.current?.setFieldsValue({
        title: values.state,
      });
      if (values.state === 'open') {
        setColumns(columnsFn(false));
      } else {
        setColumns(columnsFn());
      }
    }
  };

  const getFormatValues = () => {
    console.log('格式化后的所有数据：', formRef.current?.getFieldsFormatValue?.());
  };

  const validateAndGetFormatValue = () => {
    formRef.current?.validateFieldsReturnFormatValue?.().then((values) => {
      console.log('校验表单并返回格式化后的所有数据：', values);
    });
  };

  return (
    <>
      <FormCustom
        formRef={formRef}
        key="formDemo"
        columns={columns}
        onFinish={onFinish}
        onValuesChange={onValuesChange}
        submitter={{
          render: (props, doms) => {
            return [
              ...doms,
              <Button.Group key="refs" style={{ display: 'block' }}>
                <Button htmlType="button" onClick={getFormatValues} key="format">
                  获取格式化后的所有数据
                </Button>
                <Button htmlType="button" onClick={validateAndGetFormatValue} key="format">
                  校验表单并返回格式化后的所有数据
                </Button>
              </Button.Group>,
            ];
          },
        }}
      />
    </>
  );
}

export default Demo8;
