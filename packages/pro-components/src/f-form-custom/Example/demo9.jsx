import { FormCustom } from '@vh-admin/pro-components';
import { useState, useRef } from 'react';
import { message, Button } from 'antd';
import React from 'react';
import { isAmount, isInt } from '@vh-admin/pro-validator';

const validator = async (rule, value) => {
  if (!value) {
    return true;
  }
  if (!value || value[0] === undefined || value[0] === null || !value[1]) {
    throw new Error('请输入');
  }
  const type = value[1];
  const v = value[0];

  if (type === 1) {
    try {
      await isAmount({ ...rule, min: 0, max: 100, number: 2 }, v);
    } catch (error) {
      throw new Error(`只能输入2个小数点，0% ≤ X ≤ 100%`);
    }
  } else {
    const mes = `只能输入0和正整数，最大值999999999`;

    try {
      if (0 > v) {
        throw new Error(mes);
      }
      if (999999999 < v) {
        throw new Error(mes);
      }
      await isInt(rule, v);
    } catch (error) {
      throw new Error(mes);
    }
  }
};

const columns = [
  {
    title: '标题',
    dataIndex: 'title',
  },
  {
    title: '描述',
    dataIndex: 'description',
  },
  {
    title: '状态',
    dataIndex: '["status","status1"]',
    valueType: 'SelectInputCustom',
    initialValue: [1, 2],
    fieldProps: {
      options: [
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: '生效时段',
    dataIndex: '["startDate","endDate"]',
    valueType: 'dateRange',
    fieldProps: {
      rangePicker: true,
    },
    // formItemProps: {
    //   rules: [{ required: true }],
    // },
  },
  {
    title: '分组',
    valueType: 'group',
    columns: [
      {
        title: '规则编号',
        dataIndex: 'id',
        valueType: 'text',
      },
      {
        title: '类型',
        dataIndex: '["type","type1"]',
        valueType: 'SelectInputCustom',
        fieldProps: {
          options: [
            {
              label: '类型1',
              value: 1,
            },
            {
              label: '类型2',
              value: 2,
            },
            {
              label: '类型3',
              value: 3,
            },
          ],
        },
      },
    ],
  },
  {
    //此方式方便动态增加和减少表单的字段
    valueType: 'dependency',
    name: ['["status","status1"]'],
    columns: (data) => {
      if (data['["status","status1"]'][0] === 1) {
        return [
          {
            title: '状态2',
            dataIndex: 'state1112',
          },
          {
            title: '分组',
            dataIndex: '["group","group1"]',
            valueType: 'SelectInputCustom',
            fieldProps: {
              options: [
                {
                  label: '分组1',
                  value: 1,
                },
                {
                  label: '分组2',
                  value: 2,
                },
                {
                  label: '分组3',
                  value: 3,
                },
              ],
            },
          },
        ];
      }
      return [];
    },
  },
  {
    title: '规则参数',
    dataIndex: 'EditableTableCustom',
    valueType: 'EditableTableCustom',
    width: '100%',
    hideInTable: true,
    fieldProps: {
      columns: [
        {
          title: '阶段',
          dataIndex: 'a',
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },

        {
          title: '业务22',
          dataIndex: 'aaa',
          valueType: 'InputNumberSelectCustom',
          width: 200,
          fieldProps: {
            options: [
              { value: 1, label: '%单套' },
              { value: 3, label: '元整体' },
            ],
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
              {
                validator,
              },
            ],
          },
        },
        {
          title: '阶段比例%',
          dataIndex: 'b',
          valueType: 'digit',
          fieldProps: {
            min: 0,
            max: 100,
            addonAfter: '%',
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '导出表标题',
          dataIndex: 'd',
          editable: false,
        },
      ],
      maxHeight: 200,
    },
    initialValue: [{ id: 1, a: undefined, b: 2 }],
  },
];

function Demo9() {
  const initialValues = {
    title: 'title',
    description: 'description',
    status: 3,
    status1: 'ssssssss',
    EditableTableCustom: [{ id: 1, a: '阶段1', b: 2 }],
    type: 1,
    type1: 'type1',
    group: 1,
    group1: 'group1',
  };
  const onFinish = async (values) => {
    try {
      console.log('====================================');
      console.log(values);
      console.log('====================================');
      message.success('成功');
      return true;
    } catch (error) {
      console.log('====================================');
      console.log(error, setColumns);
      console.log('====================================');
      return false;
    }
  };

  return (
    <>
      <FormCustom
        grid={true}
        key="formDemo"
        initialValues={initialValues}
        columns={columns}
        onFinish={onFinish}
      />
    </>
  );
}

export default Demo9;
