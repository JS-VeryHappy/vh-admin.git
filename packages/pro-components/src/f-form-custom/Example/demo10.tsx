//@ts-ignore
import { FormCustom } from '@vh-admin/pro-components';
import { message } from 'antd';
//@ts-ignore
import type { FormCustomColumnsType } from '@vh-admin/pro-components/f-form-custom/types';

const columns: FormCustomColumnsType<any>[] = [
  {
    title: '状态',
    dataIndex: '["status","status1"]',
    valueType: 'SelectInputCustom',
    initialValue: [1, 2],
    fieldProps: {
      options: [
        {
          label: '启用',
          value: 1,
        },
        {
          label: '禁用',
          value: 2,
        },
        {
          label: '等待',
          value: 3,
        },
      ],
    },
  },
  {
    title: 'type',
    dataIndex: 'type',
    valueType: 'SelectMultipleCustom',
    apiFormat: {
      type: 'split',
      split: ',',
    },
    fieldProps: {
      fieldProps: {
        options: [
          {
            label: '启用',
            value: 1,
          },
          {
            label: '禁用',
            value: 2,
          },
          {
            label: '等待',
            value: 3,
          },
        ],
      },
    },
  },
  {
    title: '规则参数',
    dataIndex: 'EditableTableCustom',
    valueType: 'EditableTableCustom',
    width: '100%',
    hideInTable: true,
    apiFormat: {
      type: 'json',
    },
    fieldProps: {
      columns: [
        {
          title: '阶段',
          dataIndex: 'a',
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },

        {
          title: '业务22',
          dataIndex: '["aaa","aaa1"]',
          valueType: 'InputNumberSelectCustom',
          width: 200,
          fieldProps: {
            options: [
              { value: 1, label: '%单套' },
              { value: 3, label: '元整体' },
            ],
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '阶段比例%',
          dataIndex: 'b',
          valueType: 'digit',
          fieldProps: {
            min: 0,
            max: 100,
            addonAfter: '%',
          },
          formItemProps: {
            rules: [
              {
                required: true,
              },
            ],
          },
        },
        {
          title: '导出表标题',
          dataIndex: 'd',
          editable: false,
        },
        {
          title: '操作',
          valueType: 'option',
        },
      ],
      maxHeight: 150,
      controlled: false,
      defaultEditablekeys: [1],
      actionRender: (row: any, config: any, defaultDoms: any) => {
        return [defaultDoms.save, defaultDoms.cancel];
      },
      position: 'bottom',
      autoEdit: false,
      option: ['edit', 'delete'],
    },
    initialValue: [{ id: 1, a: undefined, b: 2 }],
  },
];

function Demo10() {
  const initialValues: any = {
    status: 3,
    status1: 0,
    EditableTableCustom: JSON.stringify([
      { id: 1, a: '阶段1', b: 2, aaa: 0, aaa1: 3 },
      { id: 2, a: '阶段2', b: 2 },
    ]),
    type: '1,2',
  };
  const onFinish = async (values: any) => {
    try {
      console.log('====================================');
      console.log(values);
      console.log('====================================');
      message.success('成功');
      return true;
    } catch (error) {
      console.log('====================================');
      console.log(error);
      console.log('====================================');
      return false;
    }
  };

  return (
    <>
      <FormCustom
        key="formDemo"
        initialValues={initialValues}
        request={async () => {
          return {
            status: 3,
            status1: 0,
            EditableTableCustom: JSON.stringify([
              { id: 1, a: '阶段1', b: 2, aaa: 0, aaa1: 3 },
              { id: 2, a: '阶段2', b: 2 },
            ]),
            type: '1,2',
          };
        }}
        params={{ aa: 11 }}
        columns={columns}
        onFinish={onFinish}
      />
    </>
  );
}

export default Demo10;
