### ConfirmModalCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| title | 显示标题<br>@description<br> | `string` | 确认信息 | - |
| desc | 显示描述<br>@description<br> | `string\|React.ReactNode` | 确定要此操作？ | - |
| `...ModalProps` | antd [ModalProps](https://ant.design/components/modal-cn) | `ModalProps` | - | - |
