import { Result, Button } from 'antd';
import React from 'react';

const P403Custom = () => {
  return (
    <Result
      status="404"
      title="404"
      subTitle="此页面未找到"
      extra={
        <Button type="primary">
          <a href="/" style={{ color: '#fff' }}>
            去首页
          </a>
        </Button>
      }
    />
  );
};
export default P403Custom;
