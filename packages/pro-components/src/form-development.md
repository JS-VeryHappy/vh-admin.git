---
title: 组件开发说明
order: 1
toc: content
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

## 自定义开发表格单组件说明

- 一.自定义表单开发必须遵循规则
  - **1.其本身有独立使用说明文档和运行 Demo**
  - **2.本身可以独立运行不受业务组件的使用环境所影响**
  - **3.本身尽可能是业务最小化模块**
- 二.自定义组件开发中可以使用以下组件、可以单独使用、也可以混合使用：
  - **[ProComponents-Form](https://procomponents.ant.design/components/form)**
  - **[Antd-Form](https://ant.design/components/form-cn/)**
  - **[html 语义化标签]**
- 三.自定义组件必须完成 4 个属性对应实现方法、**value、onChange、readonly、fieldProps**

  - **1.value 接受默认值**
  - **2.onChange 值发生变化需要触发该方法**
  - **3.readonly 只读模式下应该有 固定 ui 呈现**
  - **4.fieldProps 传递给下级组件的属性**
  - **5.className 是由 ProForm 组件统一传递过来的字段宽度和字段样式、为了样式统一建议使用**
  - **6.style 是由 ProForm 组件统一传递过来的字段宽度和字段样式、为了样式统一建议使用**
  - **7.id 是由 ProForm 组件统一传递过来的唯一值、提交验证错误时可以定位错误位置**


  ```js
  const { style, className, onChange, value, options, fieldProps, readonly } = props;

  useEffect(() => {
    /**
     * 如果父级传有默认值则赋值默认值
     */
    setInputValue(value);
  }, []);

  /**
   * input切换值变换。如果父级传入监听方法调用
   * @param value
   */
  const onInputChange = (avalue: any) => {
    setInputValue(avalue);
    if (onChange && typeof onChange === 'function') {
      onChange(avalue);
    } else {
      message.info(`Input切换值${avalue}`);
    }
  };

  return (
    <>
      <div className={className} style={style}>
        {readonly ? (
          value
        ) : (
          <AutoComplete
            value={inputValue}
            placeholder="请输入"
            {...fieldProps}
            options={nowOptions}
            // onSelect={onSelect}
            // onSearch={onSearch}
            onChange={onInputChange}
          />
        )}
      </div>
    </>
  );
  ```

  - **7.在 components.tsx 中导出组件**

  ```js
  import InputAutoCompleteCustom from './InputAutoCompleteCustom';
  export { InputAutoCompleteCustom };
  ```

  - **8.在 types.d.ts 定义 ValueType 类型**

  ```js
       export declare type ValueType =
    | 'text'
    | 'InputTooltipCustom'
    | 'SelectInputCustom'
    | 'InputAutoCompleteCustom'
    | 'ProFormCaptchaCustom'
    | FormFieldType;
  ```
  - **9.在 types.d.ts 定义 FormCustomColumnsFieldPropsType 类型**

  ```js
       export declare type FormCustomColumnsFieldPropsType = {
          valueType: 'SelectMultipleCustom';
          fieldProps: SelectMultipleCustomType;
        }
  ```

- 四.组件统一后缀以:**Custom**结尾
  - 例如：表单子组件:自动完成 InputAutoCompleteCustom

---

## 子组件会接收公用 Props

<embed src="./f-form-custom/Example/FromCustomTypes/CustomType.md"></embed>
