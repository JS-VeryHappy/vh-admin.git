---
title: 文件预览器
toc: content
order: 9
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 通用
  path: /Exception
  order: 4
---


文件预览器，目前已支持 `image` `audio` `video` `pdf` 等文件类型预览，其他文件类型会提供点击下载提示。


 ### 图片格式 

<code src='./Example/demo1.tsx'></code>

### 其他文件格式

<code src='./Example/demo2.tsx'></code> 


## API

<embed src="./CFilePreviewCustomType.md"></embed>




