import { ProFormCaptcha } from '@ant-design/pro-form';
import type { CustomType } from '../f-form-custom/types';
import React from 'react';

export declare type ProFormCaptchaCustomType = {
  /**
   * 点击获取验证码的事件，如果配置了 phoneName 会自动注入
   */
  onGetCaptcha: any;
} & CustomType;

function ProFormCaptchaCustom(Props: ProFormCaptchaCustomType) {
  const { style, className, customMode, readonly, fieldProps, ...rest } = Props;

  return (
    <div className={className} style={style}>
      <ProFormCaptcha {...rest} fieldProps={fieldProps} />
    </div>
  );
}

export default ProFormCaptchaCustom;
