### ProFormCaptchaCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| onGetCaptcha(必须) | 点击获取验证码的事件，如果配置了phoneName会自动注入 | `any` | - | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
