---
title: Select多选
toc: content
order: 13
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1.Select 多选,带全部选择和清空全部

```jsx
/**
 * title: Select多选
 */
import React from 'react';
import { Button } from 'antd';
import { useState } from 'react';
import { SelectMultipleCustom } from '@vh-admin/pro-components';

function SelectMultipleCustomDemo() {
  const [selectValue, setSelectValue] = useState([]);

  const onChange = (values) => {
    setSelectValue(values);
  };

  const options = [
    {
      label: '1啊',
      value: 1,
    },
    {
      label: '2的',
      value: 2,
    },
    {
      label: '3发',
      value: 3,
      disabled: true,
    },
  ];
  const fieldProps = {
    style: {
      width: '200px',
    },
  };
  return (
    <>
      <SelectMultipleCustom
        options={options}
        value={selectValue}
        onChange={onChange}
        fieldProps={fieldProps}
      />
    </>
  );
}
export default SelectMultipleCustomDemo;
```

```jsx
/**
 * title: Select多选 远程加载数据
 */
import React from 'react';
import { Button } from 'antd';
import { useState } from 'react';
import { SelectMultipleCustom } from '@vh-admin/pro-components';

function SelectMultipleCustomDemo1() {
  const [selectValue, setSelectValue] = useState([]);

  const onChange = (values) => {
    setSelectValue(values);
  };

  const options = [
    {
      label: '远程1',
      value: 1,
    },
    {
      label: '远程2',
      value: 2,
    },
    {
      label: '远程3',
      value: 3,
      disabled: true,
    },
  ];
  const fieldProps = {
    style: {
      width: '200px',
    },
  };
  return (
    <>
      <SelectMultipleCustom
        request={async ()=>{
          return options
        }}
        params={{a:1}}
        value={selectValue}
        onChange={onChange}
        fieldProps={fieldProps}
      />
    </>
  );
}
export default SelectMultipleCustomDemo1;
```

## API

<embed src="./SelectMultipleCustomType.md"></embed>


