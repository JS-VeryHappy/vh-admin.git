### SelectMultipleCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| options | select选择数据<br> | `OptionType[]\|undefined` | [] | - |
| maxCount | 最多显示多少个tag，响应式模式会对性能产生损耗<br> | `number\|'responsive'` | 2 | - |  
| maxTagCount | 最大选择数据，默认不限制 | `number` | - | - |        
| disabled | 是否禁用<br> | `boolean` | false | - |
| request | 从网络请求枚举数据 | `(params: any, Props: any) => Promise<any>;` | - | - |
| params | 发起网络请求的参数,与 request 配合使用 | `Record<any, any>` | - | - |
| allText | 全选文案 | `string` | `选择全部` | - |
| clearText | 清除文案 | `string` | `清空选择` | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
