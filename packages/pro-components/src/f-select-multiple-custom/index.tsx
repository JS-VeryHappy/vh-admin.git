import React, { useState, useEffect } from 'react';
import { Select, Divider, Button } from 'antd';
import type { CustomType, OptionType } from '../f-form-custom/types';
import { useDeepCompareEffect } from 'ahooks';

export declare type SelectMultipleCustomType = {
  /**
   * select选择数据
   * @default []
   */
  options?: OptionType[] | undefined;
  /**
   * 最大选择数据，默认不限制
   * @default
   */
  maxCount?: number;
  /**
   * 最多显示多少个 tag，响应式模式会对性能产生损耗
   * @default 1
   */
  maxTagCount?: number | 'responsive';
  /**
   * 是否禁用
   * @default false
   */
  disabled?: boolean;
  /**
   * 从网络请求枚举数据
   */
  request?: (params: any, Props: any) => Promise<any>;
  /**
   * 发起网络请求的参数,与 request 配合使用
   */
  params?: Record<any, any>;
  /**
   * 全选文案
   * @default 选择全部
   */
  allText?: string;
  /**
   * 清除文案
   * @default 清空选择
   */
  clearText?: string;
} & CustomType;

function SelectMultipleCustom(Props: SelectMultipleCustomType) {
  const [selectValue, setSelectValue] = useState<any>([]);
  const [currentOptions, setCurrentOptions] = useState<OptionType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const {
    id,
    style,
    className,
    readonly,
    onChange,
    value,
    options,
    maxTagCount = 1,
    maxCount,
    fieldProps,
    disabled = false,
    request,
    params,
    allText = '选择全部',
    clearText = '清空选择',
  } = Props;

  useDeepCompareEffect(() => {
    if (request) {
      const getOptions = async () => {
        setLoading(true);
        const data = await request(params, Props);
        setCurrentOptions(data);
        setLoading(false);
      };
      getOptions();
    } else {
      if (options) {
        setCurrentOptions(options);
      }
    }
  }, [params, options]);

  useEffect(() => {
    if (value) {
      setSelectValue(value);
    }
  }, [value]);

  const selectAll = () => {
    if (currentOptions) {
      const all: any[] = currentOptions
        .filter((i: any) => i.disabled !== true)
        .map((i: any) => i.value);

      let result: any[] = [...all];

      if (maxCount && all.length > maxCount) {
        result = all.slice(0, maxCount);
      }
      if (typeof onChange === 'function') {
        onChange(result);
      }
    }
  };

  const selectClear = () => {
    if (typeof onChange === 'function') {
      onChange([]);
    }
  };

  const selectOnChange = (values: any) => {
    if (typeof onChange === 'function') {
      onChange(values);
    }
  };

  return (
    <div
      className={`f-select-multiple-custom ${className ? className : ''}`}
      id={id}
      style={{ ...style }}
    >
      {readonly ? (
        `${value && value[0] ? currentOptions?.find((i: any) => i.value === value[0])?.label : ''}`
      ) : (
        <Select
          loading={loading}
          mode="multiple"
          placeholder="请选择"
          allowClear
          value={selectValue}
          maxTagCount={maxTagCount}
          maxCount={maxCount}
          options={currentOptions}
          optionFilterProp="label"
          onChange={selectOnChange}
          disabled={disabled}
          dropdownRender={(menu) => (
            <>
              {menu}
              <Divider style={{ margin: '8px 0' }} />
              <div
                style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}
              >
                <Button type="link" onClick={selectAll}>
                  {allText}
                </Button>
                <Divider type="vertical" />
                <Button type="link" onClick={selectClear}>
                  {clearText}
                </Button>
              </div>
            </>
          )}
          {...fieldProps}
        />
      )}
    </div>
  );
}

export default SelectMultipleCustom;
