import { Result, Button } from 'antd';
import React from 'react';

const P500Custom = () => {
  return (
    <Result
      status="500"
      title="500"
      subTitle="服务器发生了错误"
      extra={
        <Button type="primary">
          <a href="/" style={{ color: '#fff' }}>
            去首页
          </a>
        </Button>
      }
    />
  );
};
export default P500Custom;
