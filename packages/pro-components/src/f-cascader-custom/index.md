---
title: Cascader级联选择
toc: content
order: 5
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. Cascader 级联选择

```jsx
import React from 'react';
import { CascaderCustom } from '@vh-admin/pro-components';
import { MailTwoTone } from '@ant-design/icons';
import { message } from 'antd';

function CascaderCustomDemo() {
  const onChange = (value) => {
    console.log(value);
  };
  const options = [
    {
      label: 'Node1',
      value: '0-0',
      children: [
        {
          label: 'Child Node1',
          value: '0-0-1',
        },
        {
          label: 'Child Node2',
          value: '0-0-2',
        },
      ],
    },
    {
      label: 'Node2',
      value: '0-1',
    },
  ];
  const fieldProps = {
    onChange,
    options,
  };
  return <CascaderCustom {...fieldProps} />;
}
export default CascaderCustomDemo;
```

## API

<embed src="./CascaderCustomType.md"></embed>


