import { Cascader } from 'antd';
import type { CustomType } from '../f-form-custom/types';
import React from 'react';

export declare type CascaderCustomType = CustomType;

function CascaderCustom(Props: CascaderCustomType) {
  const { style, className, customMode, readonly, fieldProps, value, onChange, ...rest } = Props;
  return (
    <div className={`${className} ${customMode}`} style={style}>
      {readonly ? value : <Cascader value={value} onChange={onChange} {...rest} />}
    </div>
  );
}

export default CascaderCustom;
