### CascaderCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
