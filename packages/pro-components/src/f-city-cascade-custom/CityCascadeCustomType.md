### CityCascadeCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| select | 需要选择模块默认4级选择province省city城市area区域plate板块['province','city','area','plate']<br> | `any` | 全部 | - |
| citys(必须) | 如果是表单弹窗中使用models状态管理是不行的<br>全局可以获取城市配置 | `any` | - | - |
| disabled | 禁用 | `boolean` | - | - |
| getPublicAreaPlate | 得到区域和板块的数据 | `any` | - | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
