---
title: 地区级联选择
toc: content
order: 3
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

```jsx
/**
 * title: 地区级联选择
 */
import React from 'react';
import { Button } from 'antd';
import { CityCascadeCustom } from '@vh-admin/pro-components';

function CityCascadeCustomDemo() {
  const citys = [
    {
      city: '成都',
      id: 860028,
      province: '四川',
    },
    {
      city: '绵阳',
      id: 860029,
      province: '四川',
    },
  ];

  return (
    <>
      <CityCascadeCustom citys={citys} select={['province', 'city']} />
    </>
  );
}
export default CityCascadeCustomDemo;
```

## API

<embed src="./CityCascadeCustomType.md"></embed>


