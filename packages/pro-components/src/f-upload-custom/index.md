---
title: 上传文件
toc: content
order: 15
nav:
  order: 2
  title: 组件
  path: /components
group:
  title: 表单组件
  path: /form-custom
  order: 1
---

# 说明

> 1. 剪辑插架[antd-img-crop](https://github.com/nanxiaobei/antd-img-crop/blob/main/README.zh-CN.md)
> 1. 上传插架[upload](https://ant.design/components/upload-cn/)

```jsx
/**
 * title: 上传头像加剪辑
 */
import React from 'react';
import { Button } from 'antd';
import { UploadCustom } from '@vh-admin/pro-components';

function UploadCustomDemo() {
  const fieldProps = {
    format: ['jpg', 'jpeg', 'png'],
    imgCrop: true,
    imgCropFieldProps: {
      aspect: 2,
    },
    listType: 'picture-card',
    maxCount: 1,
    size: 2000,
  };
  return <UploadCustom {...fieldProps} />;
}
export default UploadCustomDemo;
```

```jsx
/**
 * title: 上传文件 返回二进制数据
 */
import React from 'react';
import { Button } from 'antd';
import UploadCustom from './index.tsx';
import { waitTime } from '@vh-admin/pro-utils';

function UploadCustomDemo1() {
  const fieldProps = {
    maxCount: 3,
    blob: true,
    request: async () => {
      await waitTime(2000);
      return {
        data: {
          src: 'test.png',
        },
      };
    },
  };
  return <UploadCustom {...fieldProps} />;
}
export default UploadCustomDemo1;
```

```jsx
/**
 * title: 上传图片
 */
import React from 'react';
import { Button } from 'antd';
import UploadCustom from './index.tsx';

function UploadCustomDemo2() {
  const fieldProps = {
    maxCount: 3,
    format: ['jpg', 'jpeg', 'png'],
    listType: 'picture',
  };
  return <UploadCustom {...fieldProps} />;
}
export default UploadCustomDemo2;
```

## API

<embed src="./UploadCustomType.md"></embed>

