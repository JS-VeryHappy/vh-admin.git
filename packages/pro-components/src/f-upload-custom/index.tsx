import React, { useState, useEffect, useImperativeHandle, useRef } from 'react';
import { Upload, Button, message } from 'antd';
import { UploadOutlined, PlusOutlined } from '@ant-design/icons';
import ImgCrop from 'antd-img-crop';
import type { CustomType } from '../f-form-custom/types';
import './index.less';

export declare type UploadCustomType = {
  /**
   * 最大选择数量
   */
  maxCount?: number;
  /**
   * 请求地址api方法
   */
  request?: any;

  /**
   * 限制上传文件的后缀名
   */
  format?: [];

  /**
   * 限制上传图片的大小 单位kb
   */
  size?: number;
  /**
   * 按钮显示的文字
   * @default 上传文件
   */
  buttonName?: string;
  /**
   * 是否禁用
   * @default false
   */
  disabled?: boolean;

  /**
   * 上传列表的内建样式，支持三种基本样式 text, picture 和 picture-card
   * @default text
   */
  listType?: string;

  /**
   * 是否开启图片裁剪
   * @default false
   */
  imgCrop?: boolean;

  /**
   * 图片裁剪FieldProps
   * @default false
   */
  imgCropFieldProps?: any;

  /**
   * 上传按钮的Props 和antd一样
   */
  buttonProps?: any;

  /**
   * 文件夹前缀
   * @default common
   */
  path?: string;
  /**
   * 是否返回blob数据
   * @default false
   */
  blob?: boolean;
  /**
   * 上传字段的 字段名称
   * @default file
   */
  name?: string;
  /**
   * 是否开启decodeURIComponent转义地址
   * @default true
   */
  openDecodeURIComponent: boolean;
} & CustomType;

function UploadCustom(Props: UploadCustomType) {
  const {
    id,
    style,
    disabled = false,
    buttonName,
    maxCount = 1,
    request,
    format,
    size,
    className,
    readonly,
    onChange,
    value,
    fieldProps,
    listType = 'text',
    imgCrop = false,
    imgCropFieldProps,
    buttonProps,
    path = 'common',
    blob = false,
    name = 'file',
    openDecodeURIComponent = true,
  } = Props;

  const [fileList, setFileList] = useState<any>([]);
  const uploadRef = useRef<any>();

  useEffect(() => {
    if (value) {
      let values = [];
      if (typeof value === 'string') {
        values.push(value);
      } else if (value instanceof Array) {
        values = [...value];
      } else {
        values.push(value);
      }

      const list: any = [];
      values.forEach((item: any) => {
        if (typeof item === 'string') {
          const arr = item.split('/');
          let urlName: string = arr[arr.length - 1];
          if (openDecodeURIComponent) {
            urlName = decodeURIComponent(urlName);
          }
          list.push({
            status: 'done',
            name: urlName,
            url: item,
          });
        } else if (typeof value === 'object') {
          list.push({
            status: 'done',
            name: item.name,
            file: item,
          });
        }
      });
      setFileList(list);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useImperativeHandle(
    uploadRef,
    () => {
      return {
        getFileListData: () => {
          return fileList;
        },
        setFileListData: (data: any) => {
          return setFileList(data);
        },
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    },
    [fileList],
  );

  const change = (newList: any) => {
    if (typeof onChange === 'function') {
      if (maxCount === 1) {
        onChange(newList[0]);
      } else {
        onChange(newList);
      }
    }
  };

  const onUplaodChange = (info: any) => {
    if (info.file.status !== 'uploading') {
      const list: any = [];
      const afileList: any = [];

      info.fileList.forEach((item: any) => {
        if (item.response) {
          if (blob) {
            list.push(item.response);
            afileList.push({
              status: 'done',
              name: item.response.name,
              file: item.response,
            });
          } else {
            const url = item.response.data.src;
            let fileName: any = item.response.data.name;
            const arr = url.split('/');
            if (!fileName) {
              fileName = arr[arr.length - 1];
            }
            list.push(url);
            afileList.push({
              status: 'done',
              name: fileName,
              url: url,
            });
          }
        } else {
          if (item.status === 'error') {
            afileList.push({ ...item });
            return;
          }
          list.push(item.url);
          afileList.push({ ...item });
        }
      });
      change(list);

      setFileList([...afileList]);
    } else {
      setFileList([...info.fileList]);
    }
  };

  const beforeUpload = (file: any) => {
    if (format && format.length > 0) {
      let formatArr: any = [...format];
      formatArr = formatArr.map((i: string) => i.toLowerCase());
      const nameArr = file.name.split('.');
      const newName = nameArr[nameArr.length - 1].toLowerCase();
      if (!formatArr.includes(newName)) {
        message.error(`需要上传文件格式：${formatArr.join('、')} `);
        return Upload.LIST_IGNORE;
      }
    }
    if (size && size > 0) {
      if (file.size > size * 1000) {
        const newSize = size * 1000;
        let str = `上传文件大小限制为：${newSize}b`;
        if (newSize > 1000 && newSize < 1000000) {
          str = `上传文件大小限制为：${(newSize / 1000).toFixed(0)}kb`;
        } else if (newSize > 1000000) {
          str = `上传文件大小限制为：${(newSize / 1000 / 1000).toFixed(0)}m`;
        }
        message.error(str);
        return Upload.LIST_IGNORE;
      }
    }
    return true;
  };

  const customRequest = async (file: any) => {
    if (blob) {
      setTimeout(() => {
        file.onSuccess(file.file);
      }, 0);
      return;
    }
    const formData = new FormData();
    formData.append(name, file.file);
    formData.append('path', path);
    if (file.data) {
      Object.keys(file.data).forEach((key) => {
        formData.append(key, file.data[key]);
      });
    }
    try {
      let res: any = {};
      if (request) {
        res = await request(formData, uploadRef);
      } else {
        // @ts-ignore
        if (window?.vhAdmin?.uploadFile) {
          // @ts-ignore
          res = await window.vhAdmin?.uploadFile(formData, uploadRef);
        }
      }

      file.onSuccess(res);
    } catch (e) {
      console.error(e);

      file.onError(e);
    }
  };

  const uploadDom = (
    <Upload
      name="file"
      maxCount={maxCount}
      customRequest={customRequest}
      beforeUpload={beforeUpload}
      fileList={fileList}
      onChange={onUplaodChange}
      disabled={disabled || readonly}
      listType={listType}
      {...fieldProps}
    >
      {fileList.length < maxCount &&
        !readonly &&
        (listType === 'picture-card' ? (
          <>
            <PlusOutlined onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined} />
            {buttonName || '单击上传'}
          </>
        ) : (
          <Button
            disabled={disabled}
            icon={
              <UploadOutlined onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined} />
            }
            {...buttonProps}
          >
            {buttonName || '单击上传'}
          </Button>
        ))}
    </Upload>
  );
  return (
    <div className={`${className ? className : ''} upload-custom`} id={id} style={style}>
      {imgCrop ? (
        <ImgCrop rotate {...imgCropFieldProps}>
          {uploadDom}
        </ImgCrop>
      ) : (
        uploadDom
      )}
    </div>
  );
}

export default UploadCustom;
