### UploadCustomType

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| maxCount | 最大选择数量 | `number` | - | - |
| request | 请求地址api方法 | `any` | - | - |
| format | 限制上传文件的后缀名 | `[]` | - | - |
| size | 限制上传图片的大小单位kb | `number` | - | - |
| buttonName | 按钮显示的文字<br> | `string` | 上传文件 | - |
| disabled | 是否禁用<br> | `boolean` | false | - |
| listType | 上传列表的内建样式，支持三种基本样式text,picture和picture-card<br> | `string` | text | - |     
| imgCrop | 是否开启图片裁剪<br> | `boolean` | false | - |
| imgCropFieldProps | 图片裁剪FieldProps<br> | `any` | false | - |
| buttonProps | 上传按钮的Props和antd一样 | `any` | - | - |
| path | 文件夹前缀<br> | `string` | common | - |
| blob | 是否返回blob数据<br> | `boolean` | false | - |
| name | 上传字段的字段名称<br> | `string` | file | - |
| openDecodeURIComponent(必须) | 是否开启decodeURIComponent转义地址<br> | `boolean` | true | - |
| `...CustomType` | 参考[CustomType](/pro-components/form-development#customtype) | `any` | - | - |
