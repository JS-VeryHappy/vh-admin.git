---
title: 公用验证
order: 3
toc: content
nav:
  order: 4
  title: 验证
  path: /validator
group:
  title: 公用验证
  path: /index
  order: 3
---

## 说明

> 1. 1.各种验证表单的和验证

## 邮箱验证

```js

  // 调用
  import { isEmail } from '@vh-admin/pro-validator';
  //普通使用
  (async () => {
    try {
      await isEmail(
        {
          message: '邮箱啊',
        },
        'www.baidu,com',
      );
    } catch (error) {
      console.log(error.message);
    }
  })();

  //表单使用
  {
     ...
      formItemProps: {
        rules: [
          {
            validator: isEmail,
            message: '邮箱邮箱',
          },
        ],
      },
  },

```

## 验证必须

```js
import { isRequired } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证不能为空

```js
import { isNoEmpty } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证必须正则

```js
import { matchRegexp } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证 url

```js
import { isUrl } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证数字

```js
import { isNumeric } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证字母

```js
import { isAlpha } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证字母或数值

```js
import { isAlphanumeric } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证整数

```js
import { isInt } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证浮点数

```js
import { isFloat } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证中文

```js
import { isChinese } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证手机号码

```js
import { isTel } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证生身份证

```js
import { isIdCard } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证车牌

```js
import { isCarNo } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```

## 验证金额,只允许2位小数或者rule.number

```js
import { isAmount } from '@vh-admin/pro-validator';
//使用方式同邮箱↑
```
