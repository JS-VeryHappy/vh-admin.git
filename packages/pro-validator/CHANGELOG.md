# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.1.8](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.1.7...@vh-admin/pro-validator@0.1.8) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([32b1222](https://gitee.com/JS-VeryHappy/vh-admin/commit/32b1222f18f796660ca3d8785d198636fb2f9bc5))



### [0.1.7](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.1.6...@vh-admin/pro-validator@0.1.7) (2024-08-29)


###   🐛 Bug Fixes | Bug 修复

* **all:**  使用阿里云仓库 ([4eaad85](https://gitee.com/JS-VeryHappy/vh-admin/commit/4eaad856bac78ee28f348c001a997e30ee3bfd37))
* **all:**  使用阿里云仓库 ([d7a734e](https://gitee.com/JS-VeryHappy/vh-admin/commit/d7a734ea08b9abefc84c652a9091d091d67b5d02))



### [0.1.6](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.1.2...@vh-admin/pro-validator@0.1.6) (2023-10-23)


###   ✨ Features | 新功能

* **优化:** ts ([cda4969](https://gitee.com/JS-VeryHappy/vh-admin/commit/cda496910771f668a8e33ff2a57f3618fcf806b4))



### [0.1.2](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.1.1...@vh-admin/pro-validator@0.1.2) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([e005fa3](https://gitee.com/JS-VeryHappy/vh-admin/commit/e005fa347e2fbd0b0e064b125882c2e7a6355362))



### [0.1.1](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.27...@vh-admin/pro-validator@0.1.1) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([bcbf864](https://gitee.com/JS-VeryHappy/vh-admin/commit/bcbf8644cead892593b1719ede343f2ef520726b))



### [0.0.27](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.25...@vh-admin/pro-validator@0.0.27) (2023-05-25)


###   ✨ Features | 新功能

* **优化:** 编译node需要的扩展 ([3dfb77a](https://gitee.com/JS-VeryHappy/vh-admin/commit/3dfb77af0ee96789a4aac9b4d6e36c5f83e93110))




### [0.0.25](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.24...@vh-admin/pro-validator@0.0.25) (2023-05-11)


###   ✨ Features | 新功能

* **升级:** 优化father4 ([14ed9a6](https://gitee.com/JS-VeryHappy/vh-admin/commit/14ed9a6012042164a7d8255148979079b89c8f77))



### [0.0.24](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.22...@vh-admin/pro-validator@0.0.24) (2023-05-05)


###   ✨ Features | 新功能

* **表单设计器:** 优化 ([09544ba](https://gitee.com/JS-VeryHappy/vh-admin/commit/09544bab47883c5fa75eb2c528c57fd78333b54a))



### [0.0.22](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.21...@vh-admin/pro-validator@0.0.22) (2023-04-27)


###   ✨ Features | 新功能

* **表单:**  编辑表单优化 ([ad23228](https://gitee.com/JS-VeryHappy/vh-admin/commit/ad23228edeb9e2f6a1be167b94a694341db2dccb))




### [0.0.21](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.20...@vh-admin/pro-validator@0.0.21) (2023-04-23)


###   ✨ Features | 新功能

* **表单:**  表单格式配置 ([4540ab1](https://gitee.com/JS-VeryHappy/vh-admin/commit/4540ab1f4debbcd43b6db116df5e5458ac1d276c))




### [0.0.20](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.19...@vh-admin/pro-validator@0.0.20) (2023-04-11)


###   ✨ Features | 新功能

* **表格:** 汇总婆优化 ([f6fd3b7](https://gitee.com/JS-VeryHappy/vh-admin/commit/f6fd3b75c036d6e09ea50ebdb26eb0672c36cc5c))
* **表格:** 汇总婆优化 ([bf47d0a](https://gitee.com/JS-VeryHappy/vh-admin/commit/bf47d0aa2c8c5e8d5ec4ae7f8db6ff005317179c))




### [0.0.19](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.18...@vh-admin/pro-validator@0.0.19) (2022-11-22)

**Note:** Version bump only for package @vh-admin/pro-validator

### [0.0.18](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.17...@vh-admin/pro-validator@0.0.18) (2022-11-16)

### ✨ Features | 新功能

- **表单:** dumi2 升级 ([546b9da](https://gitee.com/JS-VeryHappy/vh-admin/commit/546b9daecdf31aeb4244db821445ae7a02ee57f9))
- **表单:** dumi2 升级 ([8d94a4a](https://gitee.com/JS-VeryHappy/vh-admin/commit/8d94a4ac978d7458e4fc76c5197e309e9a518c9d))

### [0.0.17](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.16...@vh-admin/pro-validator@0.0.17) (2022-06-06)

### ✨ Features | 新功能

- **验证:** 增加金额最大最小值验证 ([b85d120](https://gitee.com/JS-VeryHappy/vh-admin/commit/b85d120a837ce505b05863e43f8432ae7406f6c8))

### [0.0.16](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.15...@vh-admin/pro-validator@0.0.16) (2022-05-25)

### 🐛 Bug Fixes | Bug 修复

- **tree:** 修复无法选中值 ([41441e7](https://gitee.com/JS-VeryHappy/vh-admin/commit/41441e78e55331f684866d402faa07e1dfcf3546))

### [0.0.15](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.14...@vh-admin/pro-validator@0.0.15) (2022-05-25)

**Note:** Version bump only for package @vh-admin/pro-validator

### [0.0.14](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.13...@vh-admin/pro-validator@0.0.14) (2022-05-19)

### ⚡ Performance Improvements | 性能优化

- **升级 react:** 1.8.0 ([a75bd60](https://gitee.com/JS-VeryHappy/vh-admin/commit/a75bd6039afcc06fcd993e6733a60bf2b7aa07ed))

### [0.0.13](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.12...@vh-admin/pro-validator@0.0.13) (2022-04-21)

### 🎫 Chores | 其他更新

- **Merge:** branch 'review' ([a6764e1](https://gitee.com/JS-VeryHappy/vh-admin/commit/a6764e125d5b65c7f9274cf3a5b02cde88d8af64))

## [0.0.12](https://gitee.com/JS-VeryHappy/vh-admin/compare/@vh-admin/pro-validator@0.0.11...@vh-admin/pro-validator@0.0.12) (2022-04-10)

**Note:** Version bump only for package @vh-admin/pro-validator

## [0.0.11](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-validator@0.0.10...@vh-admin/pro-validator@0.0.11) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-validator

## [0.0.10](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-validator@0.0.9...@vh-admin/pro-validator@0.0.10) (2022-03-23)

**Note:** Version bump only for package @vh-admin/pro-validator

## [0.0.9](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-validator@0.0.8...@vh-admin/pro-validator@0.0.9) (2022-03-18)

**Note:** Version bump only for package @vh-admin/pro-validator

## [0.0.8](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-validator@0.0.7...@vh-admin/pro-validator@0.0.8) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-validator

## [0.0.7](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-validator@0.0.5...@vh-admin/pro-validator@0.0.7) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-validator

## [0.0.5](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-validator@0.0.3...@vh-admin/pro-validator@0.0.5) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-validator

## [0.0.3](https://gitee.com/umijs-admin-react/vh-common/compare/@vh-admin/pro-validator@0.0.2...@vh-admin/pro-validator@0.0.3) (2022-03-16)

**Note:** Version bump only for package @vh-admin/pro-validator
