module.exports = {
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json',
    },
  },
  // collectCoverage: true,
  // coverageDirectory: 'reports',
  // collectCoverageFrom: ['packages/**/*', '!**/node_modules/**'],
  // coverageReporters: ['clover', 'html'],
  // coverageThreshold: {
  //   global: {
  //     branches: 80,
  //     functions: 80,
  //     lines: 80,
  //     statements: 80,
  //   },
  // },
  // moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
  // modulePaths: ['<rootDir>'],
};
